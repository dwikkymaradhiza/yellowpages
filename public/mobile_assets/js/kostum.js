$(function(){

    // Initializing the swiper plugin for the slider.
    // Read more here: http://idangero.us/swiper/api/
    
    var mySwiper = new Swiper ('.swiper-container', {
        loop: true,
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });
    
});

//Menu Trigger Halaman Index
$("#trigger").click(function() {
  $("#menuMobile").toggleClass("active");
});
$("#trigger").click(function() {
  $("#trigger").toggleClass("active");
});
// Trigger posisi float menu di Halaman Index //
$("#trigger-index").click(function() {
  $("#menuMobile").toggleClass("active");
});
$("#trigger-index").click(function() {
  $("#trigger-index").toggleClass("active");
});

$("#triggerSlide").click(function() {
  $("#menuMobile").toggleClass("active");
});
$("#triggerSlide").click(function() {
  $("#triggerSlide").toggleClass("active");
});

//fungsi trigger posisi Normal Menu di halaman produk
$("#trigger-produk").click(function() {
  $("#menuMobile").toggleClass("active");
});
$("#trigger-produk").click(function() {
  $("#trigger-produk").toggleClass("active");
});


$("#trigger-normal").click(function() {
  $("#menuMobile").toggleClass("active");
});
$("#trigger-normal").click(function() {
  $("#trigger-normal").toggleClass("active");
});


// Script Typewriter
var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };

function iconSuka() {
    document.getElementById("ic-suka").style.color = "#ed145b";
}

$('#pop-share').popover({
    html: true,
    content: $('#share-konten').html()
});
$(document).ready(function() {
  
$(".carousel").swiperight(function() {
    $(this).carousel('prev');
});
$(".carousel").swipeleft(function() {  
    $(this).carousel('next');
});
    $('.m-link-kolaps').click(function() {
        opened = $('#deskripsi-produk').is(':visible');
        if(!opened) {
            $('.m-ic-kolaps').html('arrow_upward')
        } else {
            $('.m-ic-kolaps').html('arrow_downward')
        }
    })

$('.m-btnKolapse').click(function() {
	if(!$('#buka-tutup').is(':visible')) {
		$(this).find('.m-textCollapse').text('Sembunyikan Menu')
   	} else {
		$(this).find('.m-textCollapse').text('Tampilan Menu Selengkapnya')
   	}
})

    $('.promote').each(function() {
        $(this).attr('href', $(this).attr('data-link'));
    })
});


$('.topup-input').click(function(){
    $(this).select()
})