/* 
 * get Client location
 * 
 */


function showlocation() {
    // One-shot position request.
    if (navigator.geolocation) {
        data = navigator.geolocation.getCurrentPosition(callback);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function callback(position) {
    console.log(position);
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;

    displayLocation(lat, lon);
}

function displayLocation(latitude, longitude) {
    var request = new XMLHttpRequest();
    var method = 'GET';
    var url = '//maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=true&languange=id';
    var async = true;

    request.open(method, url, async);
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var data = JSON.parse(request.responseText);
            var address = data.results[0];

            for (var i in data.results[0]['address_components']) {
                if (data.results[0]['address_components'][i].types[0] === "administrative_area_level_2") {
                    var city = $.trim(data.results[0]['address_components'][i].short_name.replace('Kota ',''));
                    localStorage.setItem('localCity', city);
                }
            }
        }
    };
    request.send();
}
;


$(document).on('ready', function () {
    var hours = 24; // Reset when storage is more than 24hours
    var now = new Date().getTime();
    var setupTime = localStorage.getItem('setupTime');
    if (setupTime == null && typeof localStorage.getItem('setupTime') === 'undefined') {
        localStorage.setItem('setupTime', now);
        showlocation();
    } else {
        if (now - setupTime > hours * 60 * 60 * 1000) {
            localStorage.clear()
            localStorage.setItem('setupTime', now);
            showlocation();
        } else {
            //nothing todo
        }
    }
})