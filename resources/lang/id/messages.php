<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match messages
    | that are given after some activity in website, 
    | such as save data to database.
    |
    */
    
    'errors' => [
        'insert_fail' => 'Gagal menambah data. Silahkan coba lagi.',
        'delete_fail' => 'Gagal menghapus data. Silahkan coba lagi.',
        'update_fail' => 'Gagal mengubah data. Silahkan coba lagi.'
    ],
    
    'success' => [
        'insert_inquiry' => 'Inkuiri anda berhasil dikirim.',
        'insert_sms' => 'SMS anda berhasil dikirim.',
        'delete_inquiry' => 'Berhasil menghapus inkuiri.',
        
        'insert_directory' => 'Berhasil menambahkan bisnis.',
        'delete_directory' => 'Berhasil menghapus bisnis.',
        'delete_directory_picture' => 'Berhasil menghapus gambar bisnis.',
        'update_directory' => 'Berhasil mengubah direktori.',
        
        'insert_product_rating' => 'Terima kasih telah memberikan rating terhadap produk ini.',
        
        'insert_directory_rating' => 'Terima kasih telah memberikan rating terhadap bisnis ini.',
        
        'insert_directory_info' => 'Berhasil menambahkan status.',
        'delete_directory_info' => 'Berhasil menghapus status.',
        'update_directory_info' => 'Berhasil mengubah status.',
        
        'update_product' => 'Berhasil mengubah produk.',
        'insert_product' => 'Berhasil menambahkan produk.',
        'delete_product_picture' => 'Berhasil menghapus gambar produk.',
        'delete_product' => 'Berhasil menghapus produk.',
        'payment' => 'Konfirmasi pembayaran berhasil, admin kami akan segera memproses.',
        'verify_submit' => 'Terima kasih, tim kami akan segera memproses verifikasi bisnis Anda.'
    ]
];

