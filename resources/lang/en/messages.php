<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match messages
    | that are given after some activity in website, 
    | such as save data to database.
    |
    */
    
    'errors' => [
        'insert_fail' => 'Failed to save data. Please try again.',
        'delete_fail' => 'Failed to remove data. Please try again.',
        'update_fail' => 'Failed to update data. Please try again.'
    ],
    
    'success' => [
        'insert_inquiry' => 'Your inquiry data has been sent.',
        'insert_sms' => 'Your SMS has been sent.',
        'delete_inquiry' => 'Success to delete inquiry.',
        
        'insert_directory' => 'Success to add directory.',
        'delete_directory' => 'Success to delete directory.',
        'delete_directory_picture' => 'Success to delete directory picture.',
        'update_directory' => 'Success to update directory.',
        
        'insert_product_rating' => 'Thank you for rating the product.',
        
        'insert_directory_rating' => 'Thank you for rating the bussiness.',
        
        'insert_directory_info' => 'Success to add status.',
        'delete_directory_info' => 'Success to delete status.',
        'update_directory_info' => 'Success to update status.',
        
        'update_product' => 'Success to update product.',
        'insert_product' => 'Success to add product.',
        'delete_product_picture' => 'Success to delete product picture.',
        'delete_product' => 'Success to delete product.',
        'payment' => 'Success to do payment, wait Confirmation from admin'
    ]
];

