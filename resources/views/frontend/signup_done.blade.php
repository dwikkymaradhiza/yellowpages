@extends('frontend.layouts.homepage')

@section('header')
{!! Html::style('/css/main.min.css') !!}
@include('frontend.layouts.main_header')
<style>
body{background:#FFF;}
</style>
@stop

@section('content')
<div class="sukses">
    <h2>TERIMA KASIH!</h2>
    <p class="center">Terima kasih, sudah menyelesaikan proses pendaftaran.<br />Silahkan kembali ke <a href="{{route('index')}}">beranda</a> atau kunjungi halaman <a href="{{ route('directory', session('wizard_dir_slug')) }}">bisnis Anda</a></p>
</div>
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        
    });
</script>
@endsection