@extends('frontend.layouts.homepage')

@section('seo-meta')
    @include('seo.directory')
@stop

@section('header')
@include('frontend.layouts.main_header')
@stop

@section('content')
<div class="modal fade popoup-kueri" role="dialog" tabindex="-1" id="kirim-kueri">
    <div class="modal-dialog" role="document">
        <form id="inquiryForm" method="POST" action="{{ route('inquiry') }}">
            {{ csrf_field() }}
            <input type="hidden" name="for" id="for" />
            <input type="hidden" name="directory_id" value="{{ $directory->id }}" />
            <div class="modal-content">
                <div class="modal-body">
                    <h3 class="headerModal">Kirim Inquiry</h3>
                    @if($directory->user_id)
                    <div class="textArea-pesan">
                        <p class="labelTextArea">Pesan </p>
                        <textarea required id="i-pesan" name="message" class="input-lg textArea">{{ old('message') }}</textarea>
                    </div>
                    <div class="nama-nope">
                        <div class="textArea-nama">
                            <p class="labelTextArea">Nama </p>
                            <input required id="i-nama" name="name" type="text" class="inputNama" value="{{ (Auth::check()) ? Auth::user()->first_name . " " . Auth::user()->last_name : old('name') }}">
                        </div>
                        <div class="textArea-nope">
                            <p class="labelTextArea">No. Hp</p>
                            <input required id="i-nohp" name="phone" type="text" class="inputNope" value="{{ (Auth::check()) ? Auth::user()->handphone : old('phone') }}">
                        </div>
                    </div>
                    <div class="nama-email">
                        <div class="textArea-email">
                            <p class="labelTextArea">Email </p>
                            <input required id="i-email" name="email" type="text" class="inputEmail" value="{{ (Auth::check()) ? Auth::user()->email : old('email') }}">
                        </div>
                    </div>
                    <div>
                        <div class="Captcha-code" id="RecaptchaField1"></div>
                    </div>
                    @else
                        <div class="textArea-pesan">
                            <p class="m-label-input">
                                <strong>{{$directory->name}}</strong> belum mengklaim listing ini. Silahkan menghubungi via telfon dibawah.<br />
                                Anda pemilik Bisnis ini? <a href="{{route('claim', $directory->slug)}}">Klaim sekarang!</a>
                            </p>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    @if($directory->user_id)
                    <input class="btn btn-primary kirim-inquiri" id="btnModal" type="submit" value="Kirim Inquiry">
                    @else
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <a class="btn btn-default kirim-sms" role="button" href="tel:{{$directory->phone_1}}" data-toggle="modal">
                                <i class="fa fa-phone" style="font-size: 15px; font-weight: bold;"></i>&nbsp;&nbsp;Telepon Bisnis
                            </a>
                        </div>
                        <div class="col-md-6 text-center">
                            <a class="btn btn-default kirim-sms" role="button" href="{{ route('claim', $directory->slug) }}" data-toggle="modal">
                                <i class="mdi mdi-briefcase-check claim" aria-hidden="true"></i>Klaim Bisnis
                            </a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>
<div class="divBodiKonten">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="row rowKontenBisnis">
        <div class="col-lg-8 col-md-8 col-sm-8 kontenProduk">
            <div class="JudulDLL">
                <div class="bagianJudul">
                    <div class="judulDetailProduk">
                        <h2 class="judulProduk-detail">{{ $directory->name }}@if($directory->is_verified)<i class="mdi mdi-check-circle verified" aria-hidden="true"></i>@endif</h2></div>
                    <div class="divShare">
                        <a target="_blank" href='{{ Share::load(Request::url(), $directory->name)->facebook() }}'><i class="fa fa-facebook-square" style="font-size: 25px; color: #2f2f2f;"></i></a> &nbsp;
                        <a target="_blank" href='{{ Share::load(Request::url(), $directory->name)->twitter() }}'><i class="fa fa-twitter-square" style="font-size: 25px; color: #2f2f2f;"></i></a> &nbsp;
                        <a target="_blank" href='{{ Share::load(Request::url(), $directory->name)->linkedin() }}'><i class="fa fa-linkedin-square" style="font-size: 25px; color: #2f2f2f;margin-right:7px;"></i></a>
                    </div>
                </div>
                <div class="divBreadcum">
                    <ol class="breadcrumb breadcrumProduk">
                        <li><a href="{{ route('index') }}"><span class="textBreadcum">Beranda</span></a></li>
			             @if (isset($directory->categories[0]))
                        <li><a href="{{ route('directory-category', $directory->categories[0]->slug) }}"><span class="textBreadcum">{{ $directory->categories[0]->name_lid or "No Category" }} </span></a></li>
            			@else
            			<li><a href="#"><span class="textBreadcum">No Category </span></a></li>
            			@endif
                    </ol>
                </div>
                <div class="gambarDanInfo">
                    <div class="col-lg-5 col-md-5 col-sm-5 kolomFotoProduk">
                        @php 
                            if($directory->logo)
                                $directoryPicture = asset('uploads/directories/' . $directory->logo) ;
                            else
                                $directoryPicture = asset('img/img-default.jpg') ;
                        @endphp
                        <div class="fotoProduk" {!! 'style="background-size: cover;background-position: center center ;background-image: url(' . $directoryPicture . ')"' !!}></div>
                        <div class="other-img">
                            @if($directory->logo)
                            <a href="{{ asset('uploads/directories/' . $directory->logo) }}" data-toggle="lightbox" data-gallery="{{$directory->name}}"><img src="{{ asset('uploads/directories/thumb_' . $directory->logo) }}" width="50"></a>
                            @endif
                            
                            @foreach($pictures as $pk => $pv)
                                <a href="{{ asset('uploads/directories/' . $pv->picture_file) }}" data-toggle="lightbox" data-gallery="{{$directory->name}}"><img src="{{ asset('uploads/directories/thumb_' . $pv->picture_file) }}" width="50"></a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 kolom-ratingBisnis">
                        <div class="divRatingIcon ratingInfo">
                            @for($r = 0; $r < 5; $r++)
                                <i class="material-icons ikonBintang" {!! $average_rate > 0 ? "" : "style='color:#000'" !!}>stars</i>
                                @php $average_rate-- @endphp
                            @endfor
                        </div>
                        <div class="productInfo">
                            <p>
                                {{ $directory->address_1 }}<br />
                                {{ $directory->address_2 }}
                            </p>
                            <h4 class="hargaHubungi">{{ count($products) }} Produk Tersedia</h4>
                            <!--p>
                                <b>Jam Operasional</b> {{ $directory->op_day_1 }}: {{ $directory->op_time_1 }}
                            </p-->
                            <div class="kontakTelpon">
                                <p class="kontakPar">
                                    <i class="fa fa-phone-square ikonTelpon" style="font-size: 15px; font-weight: bold;"></i><a href="tel:{{$directory->phone_1}}">{{ $directory->phone_1 }} </a>
                                    {!! (!empty($directory->website)) ? '<i class="fa fa-globe ikonWeb"></i><a href="'. $directory->website .'" target="_blank">Kunjungi Website</a>' : "" !!} 
                                </p>
                            </div>
                        </div>
                        <div class="DivTombol">
                            <a class="btn btn-default kirim-inquiri" role="button" href="#kirim-kueri" data-toggle="modal">
                              <i class="fa fa-envelope-o ikonMail"></i>Kirim Inquiry
                            </a>
                            <a class="btn btn-default kirim-sms" role="button" href="#kirim-kueri" data-toggle="modal">Kirim SMS</a>
                            <!--a class="btn btn-default kirim-inquiri" role="button" href="{{ route('claim', $directory->slug) }}" data-toggle="modal">
                                <i class="mdi mdi-briefcase-check claim" aria-hidden="true"></i>Klaim Bisnis Ini
                            </a-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="DetailProduk-dll">
                <div class="tabDetailProduk">
                    <h3 class="text-uppercase judulTabProduk" onclick="window.location.href='#detailTabBisnis'">Detail Bisnis</h3>
                    <h3 class="text-uppercase judulTabProduk" onclick="window.location.href='#detailTabProduk'" >Produk</h3>
                    <h3 class="text-uppercase judulTabProduk" onclick="window.location.href='#detailTabUpdate'">Update</h3>
                    <h3 class="text-uppercase judulTabProduk" onclick="window.location.href='#detailTabUlasan'">Ulasan </h3></div>
                <div class="detailProduk productDetailPage" id="detailTabBisnis">
                    {!! nl2br($directory->description_lid) !!}
                </div>
            </div>
            <div class="divProdukLainnya" id="detailTabProduk">
                <div id="produk-lainnya" class="produkLainnya">
                    <h3 class="text-uppercase headerProdukLainnya">{{ count($products) }} Produk</h3></div>
                <div class="row photosProduk">
                    @foreach($products as $k => $v)
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 itemProduk">
                        <div class="slideBisnis-inner col-item">
                            <a href="{{ route('product-detail', ['slug' => $v->slug]) }}">
                                <div class="photo">
                                    <img class="img-responsive" src="{!! asset('uploads/products/thumb_' . $v->picture) !!}"
                                         onerror="this.src='{{asset('uploads/products/'.$v->picture) }}'; this.onerror=null;">
                                </div>
                                
                                <div class="info">
                                    <div class="row deskripsiBisn">
                                        <h5 class="namaBisnis-slide">{{ $v->name }}</h5>
                                        <p class="lokasiBisnis-slide">{{ $directory->name }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="divUpdate" id="detailTabUpdate">
                <div class="ulasanTerakhir">
                    <h3 class="text-uppercase headerProdukLainnya">Update Terakhir</h3>
                </div>
                <div>
                    @foreach($info as $ki => $vi)
                        <div class="updateInfoList">{{ $vi->message }} - <span>{{ $vi->info_time }}</span></div>
                    @endforeach
                </div>
            </div>
            <div class="divUlasan" id="detailTabUlasan">
                @php $className = '-lainnya' @endphp
                @php $iteration = 0 @endphp
                <div>
                    <h3 class="text-uppercase headerUlasan">
                        @php
                            $ratingCount = count($ratings);
                        @endphp
                        @if($ratingCount > 0)
                            {{ count($ratings) }} Ulasan
                        @else
                            Tidak ada ulasan
                        @endif
                    </h3>
                </div>
                @foreach($ratings as $k => $v) 
                <div class="kontenKomentar{{ $iteration > 0 ? $className : "" }}">
                    <div>
                        @php $rate = (int) $v->rate @endphp
                        @for($r = 0; $r < 5; $r++)
                            <i class="material-icons ikonBintang" {!! $rate > 0 ? "" : "style='color:#000'" !!}>stars</i>
                            @php $rate-- @endphp
                        @endfor
                    </div>
                    <div class="divPengirim">
                        @php
                            $reviewerName = (!empty($v->name)) ? $v->name : "Anonymous";
                        @endphp
                        
                        @if($v->uf_name || $v->ul_name)
                            @php 
                                $userUri = '';
                                $reviewerName = "<a href='". $userUri ."'>".$v->uf_name . " " . $v->ul_name."</a>";
                            @endphp
                        @endif
                        <p class="namaPengirim">{!! $reviewerName !!}</p>
                        <p class="waktuPost">{{ $v->rating_date }}</p>
                    </div>
                    <p class="isiKomentar">{{ $v->comment }}</p>
                </div>
                @php $iteration++ @endphp
                @endforeach
                
                @if(Auth::check())
                <div class="divBuatReview">
                    <form id="ratingForm" method="POST" action="{{ route('directory-rating-create') }}" >
                        <h3 class="text-uppercase headerUlasan">tulis ulasan</h3>
                         {{ csrf_field() }} 
                       <input type="hidden" name="directory_id" value="{{ $directory->id }}" />
                        @if(Auth::user()->id == $directory->user_id)
                            <p>Pemilik bisnis tidak dapat melakukan review untuk bisnisnya sendiri</p>
                            <p>&nbsp;</p>
                        @else
                        <textarea required name="review" class="input-lg textBuatReview">{{ old('review') }}</textarea>
                        <div class="barisKirim">
                            <input type="hidden" value="5" name="rate" class="rate-star" />
                            <div class="beriBintang">
                                <i data-rate="1" class="rate-select-1 material-icons ikonBintang">stars</i>
                                <i data-rate="2" class="rate-select-2 material-icons ikonBintang">stars</i>
                                <i data-rate="3" class="rate-select-3 material-icons ikonBintang">stars</i>
                                <i data-rate="4" class="rate-select-4 material-icons ikonBintang">stars</i>
                                <i data-rate="5" class="rate-select-5 material-icons ikonBintang">stars</i>
                            </div>
                            <div class="barisBintang-danKirim">
                                <input class="btn btn-default tombolUlasan" type="submit" value="Kirim Ulasan" />
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
                @else
                <div class="divBuatReview">
                    <form id="ratingForm" method="POST" action="{{ route('directory-rating-create') }}" >
                        <h3 class="text-uppercase headerUlasan">tulis ulasan</h3>
                         {{ csrf_field() }} 
                       <input type="hidden" name="directory_id" value="{{ $directory->id }}" />
                       <div class="form-group row">
                           <div class="col-md-6">
                               <label for="firstName">Nama </label>
                               <input required type="text" class="form-control" name="name" value="{{ old('name') }}">
                           </div>
                           <div class="col-md-6 secondColumnFormField">
                              <label for="firstName">Nomor Telepon/Mobile </label>
                              <input required type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                          </div>
                       </div>
                       <div class="form-group row">
                           <div class="col-md-6">
                               <label for="firstName">Email </label>
                               <input required type="email" class="form-control" name="email" value="{{ old('email') }}">
                           </div>
                           <div class="col-md-6 rating secondColumnFormField">
                            <label>Rating </label>
                              <input type="hidden" value="5" name="rate" class="rate-star" />
                              <br />
                              <div class="beriBintang">
                                  <i data-rate="1" class="rate-select-1 material-icons ikonBintang">stars</i>
                                  <i data-rate="2" class="rate-select-2 material-icons ikonBintang">stars</i>
                                  <i data-rate="3" class="rate-select-3 material-icons ikonBintang">stars</i>
                                  <i data-rate="4" class="rate-select-4 material-icons ikonBintang">stars</i>
                                  <i data-rate="5" class="rate-select-5 material-icons ikonBintang">stars</i>
                              </div>
                          </div>
                       </div>
                       <div id="RecaptchaField2"></div>
                        <br />
                        <textarea required name="review" class="input-lg textBuatReview" minlength="10">{{ old('review') }}</textarea>
                        <div class="barisKirim">
                            <div class="barisBintang-danKirim">
                                <input class="btn btn-default tombolUlasan" type="submit" value="Kirim Ulasan" />
                            </div>
                        </div>
                    </form>
                </div>
                @endif
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 sidebarWidget">
            <div class="widgetPeta">
                <iframe allowfullscreen="" frameborder="0" width="100%" height="350" src="//maps.google.com/maps?q={{ $directory->latitude }},{{ $directory->longitude }}&z=15&output=embed" class="petaLokasi"></iframe>
            </div>
            <div class="widgetProdukSerupa">
                <div class="divHeader-proser">
                    <h3 class="text-uppercase headingProdukSerupa">{{ $use_newest_dir ? 'bisnis terbaru' : 'bisnis serupa'}}</h3>
                </div>
                @foreach($similars as $k => $v)
                    <a href="{{ route('directory', ['slug' => $v->slug]) }}">
                        <div class="listProdukSerupa">
                            <p class="namaBisnis-slide">{{ $v->name }}</p>
                            <p class="lokasiBisnis-slide">{{ $v->address_1 .", ". $v->address_name }}</p>
                        </div>
                    </a>
                @endforeach
            </div>
            <div class="widgetArtikelTerkait">
                @if(count($articles)>0)
                <div class="divHeader-ArtikelTerkait">
                    <h3 class="text-uppercase headingArtikelTerkait">artikel terkait</h3>
                </div>
                @endif
                @foreach($articles as $ak => $vk)
                <a href="{{ $vk->full_url }}" target="_blank">
                    <div class="listProdukSerupa">
                        <p class="namaBisnis-slide" style="white-space: normal;">{{ $vk->title }}</p>
                    </div>
                </a>
                @endforeach
            </div>
            <div class="bannerWidget">
                <div class="bannerKode"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-content')
<style>
    .g-recaptcha {
        transform:scale(0.9);
        transform-origin:0 0;
    }
</style>
<script>
    var CaptchaCallback = function(){
        if($('#RecaptchaField1').length) {
            grecaptcha.render('RecaptchaField1', {'sitekey' : "{{ config('recaptcha.public_key') }}" });
        }

        if($('#RecaptchaField2').length) {
            grecaptcha.render('RecaptchaField2', {'sitekey' : "{{ config('recaptcha.public_key') }}" });
        }
    };
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit{{ (isset($lang) ? '&hl='.$lang : '') }}' async defer></script>
<script src="{{ asset('js/messages_id.js') }}"></script>
{!! Html::script('/js/location.js') !!}
<script>
    $(document).ready(function() {
        $("#inquiryForm").validate();
        $("#ratingForm").validate();
        
        $("[class*='rate-select-']").on('click', function() {
            var rate = $(this).data('rate');
            $('.rate-star').val(rate);
            
            $("[class*='rate-select-']").addClass("black");
            for(let r = 1; r <= rate; r++) {
                $(".rate-select-" + r).removeClass('black');
            }
        });

        $('.DivTombol .kirim-sms').click(function() {
            $('.headerModal, .feat-name').html('Kirim SMS');
            $('#btnModal').val('Kirim SMS');
            $('#for').val('sms');
        })

        $('.DivTombol .kirim-inquiri').click(function() {
            $('.headerModal, .feat-name').html('Kirim Inquiri');
            $('#btnModal').val('Kirim Inquiri');
            $('#for').val('inq');
        })
    });
</script>
@endsection
