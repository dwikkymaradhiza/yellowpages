@extends('frontend.layouts.homepage')

@section('header')
{!! Html::style('/css/main.min.css') !!}
@include('frontend.layouts.main_header')
<style>
body{background:#FFF;}
</style>
@stop

@section('content')
<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="div1"></div>
        <div class="div2"></div>
        <div class="div3"></div>
        <div class="stepwizard-step">
            <button class="btn btn-primary btn-circle" type="button">1</button>
        </div>
        <div class="stepwizard-step">
            <button class="btn btn-primary btn-circle" type="button" disabled="disabled">2</button>
        </div>
        <div class="stepwizard-step">
            <button class="btn btn-primary btn-circle" type="button" disabled="disabled">3</button>
        </div>
        <div class="stepwizard-step">
            <button class="btn btn-primary btn-circle" type="button" disabled="disabled">4</button>
        </div>
    </div>
</div>
<!-- content -->
<div class="daftar-data">
    @unless(Auth::check())
    <p align="center">Sudah punya akun Yellow Pages? <a href="{{ route('signin') }}">Masuk disini</a></p>
    @endunless
    <div class="container">
        <div class="row  pad-top">
            <div class="col-md-6 col-md-offset-3" >
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="main-form" role="form" method="POST" action="{{ route('signup_profile_post') }}">
                            {!! csrf_field() !!}
                            <div class="form-group has-feedback">
                                <input type="email" class="form-control" placeholder="Alamat Email" value="{{ $user_info->email or '' }}" @if(Auth::check() && $user_info->email) readonly="readonly" @endif name="email" name="email"/>
                                <span class="fa fa-envelope-o fa-2x form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Nama Depan" value="{{ $user_info->first_name or '' }}" name="first_name"/>
                                <span class="fa fa-user-o fa-2x form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Nama Belakang" name="last_name" value="{{ $user_info->last_name or '' }}"/>
                                <span class="fa fa-user-o fa-2x form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" class="form-control" placeholder="Password" name="password" id="password" />
                                <span class="fa fa-lock fa-2x form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" class="form-control" placeholder="Password" id="password2" name="confirm_password" />
                                <span class="fa fa-lock fa-2x form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="number" class="form-control" placeholder="Nomor Handphone" name="handphone" value="{{ $user_info->handphone or '' }}"/>
                                <span class="fa fa-phone fa-2x form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <select class="form-control auto-province" name="province_id">
                                    <option value="">Provinsi</option>
                                    @foreach($provinces as $province)
                                    <option value="{{$province['id']}}">{{$province['name']}}</option>
                                    @endforeach
                                </select>
                                <span class="fa fa-map-marker fa-2x form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <select class="form-control auto-city" name="city_id">
                                    <option value="">Kota</option>
                                </select>
                                <span class="fa fa-map-marker fa-2x form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <textarea class="form-control" placeholder="Alamat" name="address"></textarea>
                                <span class="fa fa-map-marker fa-2x form-control-feedback"></span>
                            </div>
                        </form>
                        <h6 align="center" >
                        Dengan mendaftar saya menyatakan menyetujui, <a href="{{ route('static-page', 'syarat-ketentuan') }}" target="_blank">Ketentuan Layanan</a><!--a href="#">Kebijakan Non-diskrimnasi</a>, <a href="">Ketentuan Layanan Pembayaran</a--> dan <a href="{{ route('static-page', 'kebijakan-privasi') }}" target="_blank">Kebijakan Privasi</a>
                        </h6>
                    </div>
                    <p class="text-center">
                        <button type="submit" class="btn btn-yellow btn-simpan" form="main-form">DAFTAR</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-content')
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="/js/messages_id.js"></script>
{!! Html::script('/js/location.js') !!}
<script>
$(".auto-province, .auto-city").select2();
$('.auto-province').on('change', function(e) {
    $.ajax({
            url: '/ajax/city/' + $('.auto-province').val() + '/option',
            dataType: 'json',
            success: function(d) {
                $('.auto-city').select2('destroy').html(d.data).select2()
            }
        })
})

$("#main-form").validate({
    rules: {
        email: {
            required: true,
            email: true,
            remote: {
                url: '/ajax/check_email',
                type: 'post',
                data: {
                    _token: window.Laravel.csrfToken,
                }
            }
        },
        password: {
            required: true,
            minlength: 5
        },
        confirm_password: {
            required: true,
            minlength: 5,
            equalTo: "#password"
        },
        first_name: "required",
        last_name: "required",
        handphone: {
            required: true,
            minlength: 5,
            number: true
        },
        province_id: "required",
        city_id: "required",
        address: {
            required: true,
            minlength: 20
        }
    },
    messages: {
        email: {
            remote: "Email sudah pernah terdaftar"
        }
    }
});
</script>
@endsection
