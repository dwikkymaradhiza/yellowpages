@extends('frontend.layouts.homepage')

@section('header')
{!! Html::style('/css/main.min.css') !!}
@include('frontend.layouts.main_header')
<style>
body{background:#FFF;}
</style>
@stop

@section('content')
<!-- content -->
<div class="daftar-data">
    <h2>KEMBANGKAN BISNIS ANDA BERSAMA KAMI</h2>
    <p align="center">Sudah punya akun Yellow Pages? <a href="{{ route('signin') }}">Masuk disini</a></p>
    <div class="container">
        <div class="row  pad-top">
            <div class="col-md-6 col-md-offset-3" >
                <div class="panel panel-default">
                    <button class="btn btn-fb btn-lg btn-daftar" onclick="window.location.href='{{ route('social_redirect', ['social' => 'facebook']) }}'">
                        <span class="pull-left"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></span>Lanjutkan dengan Facebook
                    </button>
                    <button class="btn btn-google btn-lg btn-daftar" onclick="window.location.href='{{ route('social_redirect', ['social' => 'google']) }}'">
                        <span class="pull-left"><i class="fa fa-google" aria-hidden="true"></i></span>Lanjutkan dengan Google
                    </button>
                    <div class="strike">
                        <span>Atau</span>
                    </div>
                    <form role="form" action="{{ route('signup_email') }}" method="POST" id="main-form">
                        {!! csrf_field() !!}
                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" placeholder="Alamat Email" name="email" required>
                            <span class="fa fa-envelope-o fa-2x form-control-feedback"></span>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </form>
                    <button class="btn btn-yellow btn-lg btn-daftar" type="submit" form="main-form">
                        <span class="pull-left"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i></span> Daftar dengan Email
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="/js/messages_id.js"></script>
<script>
    $(document).ready(function() {
        $("#main-form").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: '/ajax/check_email',
                        type: 'post',
                        data: {
                            _token: window.Laravel.csrfToken,
                        }
                    }
                }
            },
            messages: {
                email: {
                    remote: "Email sudah pernah terdaftar"
                }
            }
        });
    });
</script>
@endsection
