@extends('frontend.layouts.homepage')

@section('seo-meta')
    @include('seo.homepage')
@stop

@section('header')
@include('frontend.layouts.homepage_header')
@stop

@section('content')
<div>
    <h2 class="text-uppercase text-center bisnisPilihan">bisnis pilihan</h2>
    <div class="container koroselProduk">
        <div class="row KoroselProduk-row">
            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row koroselBisnis">
                            @php
                            $i = 0;
                            @endphp

                            @foreach($featured_directory_array as $featured_directory_Detail)
                            @php
                            $i = $i+1;

                            if($i === 5){
                            @endphp

                        </div>
                    </div>
                    <div class="item">
                        <div class="row koroselBisnis">
                            @php
                            }
                            @endphp

                            <div class="col-sm-3 col-xs-6 produkShow-item">
                                <div class="col-item">
                                    <a class="promote promote-directory" data-slug="{{ $featured_directory_Detail->slug }}" data-click="{{route('directory-click',$featured_directory_Detail->slug)}}">
                                        <div class="photo">
                                            @if($featured_directory_Detail->logo)
                                            <img src="{{URL::asset('uploads/directories/').'/'.$featured_directory_Detail->logo}}" class="img-responsive" alt="Directory" />
                                            @else
                                            <img src="{{URL::asset('img/img-default.jpg')}}" class="img-responsive" alt="Directory" />
                                            @endif
                                        </div>
                                        <div class="info">
                                            <div class="row deskripsiBisn">
                                                <h5 class="namaBisnis-slide">{{$featured_directory_Detail->name}}</h5>
                                                <p class="lokasiBisnis-slide">{{$featured_directory_Detail->address_name}}</p>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>                                
                            @endforeach
                        </div>
                    </div>                    
                </div>
                <div class="controls">
                    <a class="left fa fa-chevron-left btn btn-success" href="#carousel-example"
                       data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-success" href="#carousel-example"
                       data-slide="next"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="photo-gallery">
    <div class="container galeriProduk">
        <div class="intro">
            <h2 class="text-uppercase text-center bisnisPilihan">produk pilihan</h2></div>
        <div class="row photos">
            @php
            $count = 0;
            @endphp

            @foreach($featured_product_array as $product_detail)

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                <div class="slideBisnis-inner col-item">
                    <a class="promote promote-product" data-slug="{{ $product_detail->slug }}" data-click="{{route('product-detail-click',$product_detail->slug)}}">
                        <div class="photo">
                            @if($product_detail->picture)
                            <img class="img-responsive" src="{{URL::asset('uploads/products/').'/'.$product_detail->picture}}">
                            @else
                            <img class="img-responsive" src="{{URL::asset('img/img-default.jpg')}}">
                            @endif
                        </div>
                        <div class="info">
                            <div class="row deskripsiBisn">
                                <h5 class="namaBisnis-slide">{{$product_detail->name}}</h5>
                                <p class="lokasiBisnis-slide">{{$product_detail->directory_name}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            @php
            $count = $count + 1;
            @endphp    

            @if($count%4 === 0)
        </div>
        <div class="row photos">
            @endif


            @endforeach

        </div>
    </div>
</div>

<div class="seksi-kategoriBisnis">
    <div class="container div-kategoriBisnis">
        <div class="introKategori">
            <h2 class="text-uppercase judulKategoriBisnis">kategori bisnis</h2></div>
        <div class="row rowKategoriBisnis">
            
            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','hotel-motel')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">
                            <img src="{{URL::asset('uploads/images/ic-hotel.png')}}" class="gbr-ikon">
                        </div>
                        <h2 class="nama-ikonBisnis">Hotel </h2>
                    </div>
                </a>    
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','industry')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">
                            <img src="{{URL::asset('uploads/images/ic-industri.png')}}" class="gbr-ikon">
                        </div>
                        <h2 class="nama-ikonBisnis">Industri </h2>
                    </div>
                </a>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','property-building-materials')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">
                            <img src="{{URL::asset('uploads/images/ic-properti.png')}}" class="gbr-ikon">
                        </div>
                        <h2 class="nama-ikonBisnis">Properti </h2>
                    </div>
                </a>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','information-technology')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">
                            <img src="{{URL::asset('uploads/images/ic-komputer.png')}}" class="gbr-ikon">                        
                        </div>
                        <h2 class="nama-ikonBisnis">Komputer </h2>
                    </div>
                </a>    
            </div>
            
            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','electric-electronic')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">
                           <img src="{{URL::asset('uploads/images/ic-radio.png')}}" class="gbr-ikon">                        
                    </div>
                    <h2 class="nama-ikonBisnis">Elektronik </h2>
                    </div>
                </a>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','finance')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">                        
                            <img src="{{URL::asset('uploads/images/ic-duit.png')}}" class="gbr-ikon">                        
                        </div>
                        <h2 class="nama-ikonBisnis">Keuangan </h2>
                    </div>
                </a>    
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','automotive')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">                        
                            <img src="{{URL::asset('uploads/images/ic-otomotif.png')}}" class="gbr-ikon">                        
                        </div>
                        <h2 class="nama-ikonBisnis">Otomotif </h2>
                    </div>
                </a>    
            </div>
            
            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','transportation-communications')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">                        
                            <img src="{{URL::asset('uploads/images/ic-travel.png')}}" class="gbr-ikon">
                        </div>
                        <h2 class="nama-ikonBisnis">Travel </h2>
                    </div>
                </a>
            </div>
            
            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','travel-bureaus')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">
                            <img src="{{URL::asset('uploads/images/ic-toga.png')}}" class="gbr-ikon">
                        </div>
                        <h2 class="nama-ikonBisnis">Edukasi </h2>
                    </div>
                </a>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 kolomKategoriBisnis">
                <a href="{{route('directory-category','restaurants-1')}}">
                    <div class="div-ItemBisnis">
                        <div class="divGambarIcon">
                            <img src="{{URL::asset('uploads/images/ic-resto.png')}}" class="gbr-ikon">
                        </div>
                        <h2 class="nama-ikonBisnis">Restoran </h2>
                    </div>
                </a>    
            </div>

        </div>
    </div>
</div>
<div class="seksi-TigaKolom">
    <div class="container kontainer-Update">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="subFooter-1">
                <h2 class="text-uppercase footer-judul-a">bisnis terbaru</h2></div>
            <div class="ListBisnis">
                @if(count($newest_directory)>0)
                @foreach($newest_directory as $newest_directory_detail)
                <p class="NamaBisnis-A"><a href="{{route('directory',$newest_directory_detail->slug)}}">{{$newest_directory_detail->name}} </a></p>
                @endforeach
                @endif
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="subFooter-1">
                <h2 class="text-uppercase footer-judul-a">produk terbaru</h2></div>
            <div class="listProduk">
                @if(count($newest_product)>0)
                @foreach($newest_product as $newest_product_detail)
                <p class="NamaBisnis-A"><a href="{{route('product-detail',$newest_product_detail->slug)}}">{{$newest_product_detail->name}} </a></p>
                @endforeach                
                @endif
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="subFooter-1">
                <h2 class="text-uppercase footer-judul-a">update terbaru</h2></div>
            <div class="listUpdate">
                @if(count($directory_info)>0)
                @foreach($directory_info as $directory_info_detail)
                <p class="UpdateTerbaru-C"><a href="{{ route('directory', $directory_info_detail->slug) }}">{{$directory_info_detail->message}}</a></p>
                @endforeach                
                @endif
            </div>
        </div>
    </div>
</div>
<div class="koroselArtikel">
    <div class="introNewsUpdate">
        <h2 class="text-uppercase judulKategoriBisnis">Artikel Terbaru</h2></div>
    <div class="container koroselNewArtikel">
        <div class="row barisNewArtikel">
            <div id="korosel-NewArtikel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row">
                            @php
                            $count = 0;
                            @endphp
                            @if(count($affiliate_article)>0)
                            @foreach($affiliate_article as $affiliate_article_detail)
                            @php
                            $count = $count + 1;
                            @endphp

                            @if ($count == 3)
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            @php    
                            $count = 1;
                            @endphp
                            @endif

                            <div class="col-sm-6 NewArtikel-item">
                                <a href='{{$affiliate_article_detail->full_url}}' target='_blank'>
                                    <div class="col-item-NewsUpdate">
                                        <div class="photo fotoNewsUpdate"><img src="{{ asset('uploads/affiliate/'.$affiliate_article_detail->image_url) }}" class='img-responsive'></div>
                                        <div class="info-newUpdate">
                                            <h4 class="judulNewsUpdate">{{$affiliate_article_detail->title}}</h4>
                                            <p class="desk-NewsUpdate" style="margin-top:10px;">{{ strip_tags($affiliate_article_detail->headline) }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>    


                            @endforeach

                            @endif
                        </div>
                    </div>
                </div>
                <div class="controls NewArtikel-arrow">
                    <a href="#korosel-NewArtikel" class="kiri fa fa-chevron-left" data-slide="prev"> </a>
                    <a href="#korosel-NewArtikel" class="kanan fa fa-chevron-right" data-slide="next"> </a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('script-content')
{!! Html::script('/js/location.js') !!}
<script>
    var main_banner = @php echo json_encode($all_banner); @endphp

    var image = JSON.parse(main_banner['image']);

    var main_banner_image_count = image.length;

    var random = Math.floor(Math.random() * main_banner_image_count);
    var random_image = image[random];

    var main_text = JSON.parse(main_banner['main_text']);
    var dynamic_text = JSON.parse(main_banner['dynamic_text']);

    for (var index in main_text) {
        $(".banner-main-text" + index).html(main_text[index]);
    }

    for (var index in main_text) {
        $(".banner-main-text" + index).html(main_text[index]);
    }

    for (var index in dynamic_text) {
        $(".banner-dynamic-text" + index).attr('data-type', JSON.stringify(dynamic_text[index]));
    }

    $(".heroWebsite").css('background-image', 'linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.33)), url("img/' + random_image + '")');

    $('.input-TemukanBisnis a').click(function(e) {
        val = $(this).attr('data-value');
        $('.formInputBisnis').val(val.toUpperCase())
    })

    $('.home-src-form').submit(function(e) {
        e.preventDefault()
        searchAction(e, $(this))
    });

    
</script>

<!-- Piwik -->
<script type="text/javascript">
    var currentURL = function() {
        var title = jQuery(".ui-page-active .ui-title"),
            currentStage = null,
            trackedURL = null;

        if (currentStage != null && typeof(currentStage) != 'undefined') {
            trackedURL = window.location.protocol + "//" + window.location.host + window.location.pathname + "#" + currentStage.toLowerCase().replace(' ', '-') + "?" + window.location.search.substring(1);
        } else {
            trackedURL = window.location.href;
        }

        return trackedURL;
    };

  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  // _paq.push(['trackPageView']);
  // _paq.push(['enableLinkTracking']);
  (function() {
    var u="//192.168.33.10/piwik/";
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);

    $(".promote-directory").click(function(){ 
        _paq.push(['setCustomUrl', currentURL() + 'bisnis/click/' + $(this).data('slug')]);
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);

        window.location = $(this).data('click');
    });

    $(".promote-product").click(function(){ 
        _paq.push(['setCustomUrl', currentURL() + 'produk/click/' + $(this).data('slug')]);
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);

        window.location = $(this).data('click');
    });

    $(window).on('scroll', function() {
        $('.promote-directory').each(function() {
            if($(this).is(':visible')) {
                if(!$(this).hasClass('impression') && typeof($(this).data('slug')) !== 'undefined') {
                    $.ajax({ 
                        url: '{{ route("directory-impression") }}/' + $(this).data('slug'),
                        type: 'POST',
                        data: "_token={{ csrf_token() }}"
                    });

                    _paq.push(['setCustomUrl', currentURL() + 'bisnis/impression/' + $(this).data('slug')]);
                    _paq.push(['trackPageView']);
                    _paq.push(['enableLinkTracking']);

                    $(this).addClass('impression');

                    console.log('impression ' + $(this).data('slug'));
                }
            }
        });
        
        $('.promote-product').each(function() {
            if($(this).is(':visible')) {
                if(!$(this).hasClass('impression')) {
                    $.ajax({ 
                        url: '{{ route("product-detail-impression") }}/' + $(this).data('slug'),
                        type: 'POST',
                        data: "_token={{ csrf_token() }}"
                    });

                    _paq.push(['setCustomUrl', currentURL() + 'produk/impression/' + $(this).data('slug')]);
                    _paq.push(['trackPageView']);
                    _paq.push(['enableLinkTracking']);

                    $(this).addClass('impression');

                    console.log('impression ' + $(this).data('slug'));
                }
            }
        });
    });

  })();
</script>
<!-- End Piwik Code -->

@stop
