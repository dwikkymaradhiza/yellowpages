@extends('frontend.layouts.homepage')

@section('header')
{!! Html::style('/css/main.min.css') !!}
@include('frontend.layouts.main_header')
<style>
body{background:#FFF;}
</style>
@stop

@section('content')
<div class="sukses">
    <h2>PENDAFTARAN BERHASIL!</h2>
    <p align="center">Silahkan cek email anda untuk melakukan proses selanjutnya.<br/>Kembali ke <a href="#">beranda</a></p>
</div>
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        
    });
</script>
@endsection