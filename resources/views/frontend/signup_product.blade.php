@extends('frontend.layouts.homepage')

@section('header')
<style>
body{background: #fff;}
</style>
{!! Html::style('/css/main.min.css') !!}
@include('frontend.layouts.main_header')
@stop

@section('content')
<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="div1"></div>
        <div class="div2"></div>
        <div class="div3"></div>
        <div class="stepwizard-step">
            <button class="btn btn-primary btn-circle" type="button" disabled="disabled">1</button>
        </div>
        <div class="stepwizard-step">
            <button class="btn btn-primary btn-circle" type="button" disabled="disabled">2</button>
        </div>
        <div class="stepwizard-step">
            <button class="btn btn-primary btn-circle" type="button">3</button>
        </div>
        <div class="stepwizard-step">
            <button class="btn btn-primary btn-circle" type="button" disabled="disabled">4</button>
        </div>
    </div>
</div>
<!-- content -->
<div class="divBodiKonten">
    <h2>DAFTARKAN PRODUK ANDA</h2>
    <p>Tambahkan informasi produk yang Anda jual di bisnis Anda. Apabila Anda ingin <br/> melanjutkan, cukup klik tombol "Tambah Nanti"</p>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    
    <div class="row rowKontenProduk">
        <form id="main-form" class="form-horizontal" action="{{ route('signup_product_post') }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <fieldset>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Nama Produk</label>
                        <input type="text" class="form-control" name="name" value="{{old('name')}}">
                    </div>
                    <div class="col-md-6">
                        <label for="firstName">Stock </label>
                        <input type="number" class="form-control" name="stock" value="{{old('stock')}}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="firstName">Gambar Utama </label>
                        <input type="file" class="form-control" name="image" />
                        <span>Max size: 2MB</span>
                    </div>
                    <div class="col-md-6">
                        <label>Deskripsi Bisnis</label>
                        <textarea class="form-control" style="height: 280px;" name="description">{{old('description')}}</textarea>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                    </div>
                </div>
                <p class="text-center">
                    <button type="submit" class="btn btn-yellow btn-simpan">SIMPAN</button>
                </p>
                <p class="text-center">
                    <button type="submit" class="btn btn-yellow btn-nanti" onclick="window.location.href='{{ url('/directory/'.session('wizard_dir_slug')) }}'; return false;">TAMBAHKAN NANTI</button>
                </p>
            </fieldset>
            <input type="hidden" name="directory_id" value="{{session('wizard_dir_id')}}" />
        </form>
    </div>
</div>
@endsection

@section('script-content')
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="/js/messages_id.js"></script>
<script>
$("#main-form").validate({
    rules: {
        name: {
          required: true,
          minlength: 10
        },
        description: {
          required: true,
          minlength: 100
        },
        stock: {
            number: true
        }
    },
    messages: {}
});
</script>
@endsection