@extends('frontend.layouts.homepage')

@section('seo-meta')
    <title>{{ $article->title }} - YellowPages.co.id</title>
@stop

@section('header')
{!! Html::style('/css/main.min.css') !!}
@include('frontend.layouts.main_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten bodyStaticWrapper" style="background: transparent;">
<div class="row rowKontenProduk">
        <div class="DetailProduk-dll">
        	<div class="judulDetailProduk">
                    <h2 class="judulProduk-detail">{{ $article->title }}</h2>
            	</div>
            	<div class="divBreadcum">
                	<ol class="breadcrumb breadcrumProduk">
                    <li><a href="{{route('index')}}"><span class="textBreadcum">Beranda </span></a></li>
                    <li><a><span class="textBreadcum">{{ $article->title }}</span></a></li>
                	</ol>
		</div>
            	<div class="detailProduk">
                {!! $article->content !!}
            	</div>
	</div>
</div>
</div>
@endsection

@section('script-content')
{!! Html::script('/js/location.js') !!}
@endsection
