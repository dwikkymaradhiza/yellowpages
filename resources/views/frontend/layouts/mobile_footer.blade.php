<div class="m-divArtikelFooter">
    <div class="m-mobileCollapse">
        <div class="m-btnKolapse" data-toggle="collapse" aria-expanded="false" aria-control="buka-tutup" role="button" href="#buka-tutup">
            <div class="m-menuFlex"><i class="material-icons ikonMenuMobile">menu</i>
                <h5 class="text-uppercase m-textCollapse">Tampilkan Menu Selengkapnya</h5></div>
        </div>
        <div id="buka-tutup" class="collapse">
            <div class="row m-rowFooter">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m-kolomFooter-2">
                    <div class="m-divFooter-A">
                        <div>
                            <h4 class="text-uppercase m-JudulFooter-1">legal </h4></div>
                        <div class="m-divLinkFooter">
                            <a href="{{ route('static-page', ['url' => 'kebijakan-privasi']) }}" class="m-linkFooter">Kebijakan Privasi</a>
                            <a href="{{ route('static-page', ['url' => 'syarat-ketentuan']) }}" class="m-linkFooter">Syarat &amp; Ketentuan</a>
                            <a href="{{ route('static-page', ['url' => 'sanggahan']) }}" class="m-linkFooter">Sanggahan </a></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m-kolomFooter-2">
                    <div class="m-divFooter-A">
                        <div>
                            <h4 class="text-uppercase m-JudulFooter-1">MITRa </h4></div>
                        <div class="m-divLinkFooter">
                            <a href="http://www.telkomsel.com/" target="_blank" class="m-linkFooter">Telkomsel</a>
                            <a href="https://kofera.com/" target="_blank" class="m-linkFooter">Kofera Technology</a>
                            <a href="http://www.blanja.com/" target="_blank" class="m-linkFooter">Blanja.com</a>
                            <a href="https://indihome.co.id/internet-fiber" target="_blank" class="m-linkFooter">Internet Fiber</a>
                            <a href="http://www.qontak.com/" target="_blank" class="m-linkFooter">Qontak.com</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m-kolomFooter-1">
                    <div class="m-divFooter-A">
                        <div>
                            <h4 class="text-uppercase m-JudulFooter-1">situs jaringan</h4></div>
                        <div class="m-divLinkFooter">
                            <a href="http://jakartafamilia.com/" target="_blank" class="m-linkFooter">Jakartafamilia.com </a><a href="http://tabloidsimpanglima.com/" target="_blank" class="m-linkFooter">Tabloidsimpanglima.com </a><a href="http://surabayafamily.com/" target="_blank" class="m-linkFooter">Surabayafamily.com </a><a href="http://wisatamudik.com" target="_blank" class="m-linkFooter">wisatamudik.com</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m-kolomFooter-1">
                    <div class="m-divFooter-A">
                        <div>
                            <h4 class="text-uppercase m-JudulFooter-1">tentang md media</h4></div>
                        <div class="m-divLinkFooter">
                            <a href="{{ route('static-page', ['url' => 'profil-mdmedia']) }}" class="m-linkFooter">Profil MD Media</a><a href="{{ route('static-page', ['url' => 'profil-yellowpages']) }}" class="m-linkFooter">Profil Yellowpages.co.id</a><a href="{{ route('static-page', ['url' => 'faq']) }}" class="m-linkFooter">FAQ </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-barisCopyright-collaps">
            <div class="col-md-4 offset-md-4 m-kolomCopyright">
                <div class="m-copyrightDiv-1">
                    <div class="m-copyrightKolom"><img src="{!! asset('mobile_assets/img/logotelkom.png ') !!}" class="m-logoTelkom"></div>
                    <div class="m-copyrightKolom"><img src="{!! asset('mobile_assets/img/logomdm.png ') !!}" class="m-logoMDM"></div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-copyrightMobile">
                <div class="m-divTExtCopy">
                    <p class="m-text-copyright">Copyright © 1996-2012 MD Media, a member of Telkom Group. All rights, unles otherwise noted, are reserved. All other business-related trademarks and copyrights, including but not limited to business logos and website screenshots,
                        are property of their respective owners. </p>
                </div>
            </div>
        </div>
    </div>
</div>
