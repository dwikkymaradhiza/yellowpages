@section('style-content')
{!! Html::style('/css/main.min.css') !!}
@endsection
<style>
    body {
        background: #FFF;
    }
</style>
<!-- header -->
<div class="navbar navbar-inverse navbar-fixed">
    <div class="container-fluid2">
        <div class="navbar-header">
            <div class="navbar-brand">
                <a href="{{ route('index') }}"><img src="{{ URL::asset('uploads/images/yp-logo-2.png') }}"></a>
            </div>
        </div>
        <div class="navbar-collapse collapse">
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            @if(Auth::user()->avatar != "")
                                <img src="{{ Auth::user()->avatar }}" class="user-image" alt="User Image">
                            @else
                                <img src="https://gravatar.com/avatar/{{ md5(Auth::user()->email) }}" class="user-image" alt="User Image">
                            @endif
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                <a href="{{ route('dashboard-setting-index') }}">Profil</a>
                                <a href="{{ route('signout') }}">Keluar</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid1">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-nav">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse main-nav">
            <ul class="nav navbar-nav">
                <li class="{{ \Request::is('dashboard') ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><i class="fa fa-tachometer" aria-hidden="true"></i> DASHBOARD</a></li>
                <li class="{{ \Request::is('dashboard/directories*') ? 'active' : '' }}"><a href="{{ route('dashboard-directory-index') }}"><span><i class="fa fa-industry" aria-hidden="true"></i></span> BISNIS</a></li>
                <li class="{{ \Request::is('dashboard/products*') ? 'active' : '' }}"><a href="{{ route('dashboard-product-index') }}"><i class="fa fa-shopping-bag" aria-hidden="true"></i> PRODUK</a></li>
                <li class="{{ \Request::is('dashboard/inquiries*') ? 'active' : '' }}"><a href="{{ route('dashboard-inquiry-index') }}"><i class="fa fa-pencil-square" aria-hidden="true"></i> INQUIRY </a></li>
                <li class="{{ \Request::is('dashboard/status*') ? 'active' : '' }}"><a href="{{ route('dashboard-status-index') }}"><i class="fa fa-pencil" aria-hidden="true"></i> STATUS</a></li>
                <li class="{{ \Request::is('dashboard/settings*') ? 'active' : '' }}"><a href="{{ route('dashboard-setting-index') }}"><i class="fa fa-cog" aria-hidden="true"></i> PENGATURAN</a></li>
            </ul>
        </div>
    </div>
</div>
