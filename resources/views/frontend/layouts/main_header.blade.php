<div class="menuNavbar">
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
            <div>
                <a href="{{ route('index') }}">
                    <img src="{!! asset('mobile_assets/img/logo-yp.png') !!}" class="logoWebsiteFloat" href="index.html" />
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <form class="divInput">
                <div class="divHeaderInput">
                    <div class="dropdown dropTemukan"><a class="btn btn-default dropdown-toggle tombolDropdown" data-toggle="dropdown" aria-expanded="false" role="button" href="#"><span class="search-by-wrapper">TEMUKAN</span> <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-left search-menu" role="menu">
                            <li role="presentation"><a href="javascript:return false;" data-value="bisnis">Bisnis</a></li>
                            <li role="presentation"><a href="javascript:return false;" data-value="produk">Produk</a></li>
                        </ul>
                    </div>
                </div>
                <div class="divInput-produk">
                    <input type="hidden" class="src-by" value="bisnis" />
                    <input type="text" placeholder="Misal: hotel" class="textInputServis src-keyword" value="{{$keyword or ''}}" name="keyword">
                </div>
                <div class="divInput-lokasiProduk">
                    <input type="text" placeholder="Lokasi" class="textInputLokasi src-location" name="location" value="{{$address or ''}}">
                </div>
                <div class="divInput-cari">
                    <button class="btn btn-default btn-block tombolTemukan" type="submit"><i class="material-icons">search</i> </button>
                </div>
            </form>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-10 col-xs-10 navbarMenuFloat">
            <div class="navMenu">
                <a class="text-uppercase navbarLink-Float" href="{{ route('static-page', ['url' => 'cara-kerja']) }}">Cara Kerja</a>
                @if (Auth::check())
                <a class="text-uppercase navbarLink-Float" href="{{ route('dashboard') }}">dashboard </a>
                <a class="text-uppercase navbarLink-Float" href="{{ route('signout') }}">keluar </a>
                @else
                <a class="text-uppercase navbarLink-Float" href="{{ route('signin') }}">masuk </a>
                <a class="text-uppercase navbarLink-Float" href="{{ route('signup') }}">daftar </a>
                @endif
            </div>
            <div class="mobileMenuFloat"><a id="trigger-normal" class="tombolMobileFloat"><i class="material-icons ikonMenuMobile-f">menu</i></a></div>
        </div>
    </div>
</div>
