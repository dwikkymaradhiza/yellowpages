<div class="row rowFooter">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-7 kolomFooter-2 footerLeft">
        <div class="divFooter-A">
            <div>
                <h4 class="text-uppercase JudulFooter-1">legal </h4></div>
            <div class="divLinkFooter"><a href="{{ route('static-page', ['url' => 'kebijakan-privasi']) }}" class="linkFooter">Kebijakan Privasi</a><a href="{{ route('static-page', ['url' => 'syarat-ketentuan']) }}" class="linkFooter">Syarat &amp; Ketentuan</a><a href="{{ route('static-page', ['url' => 'sanggahan']) }}" class="linkFooter">Sanggahan </a></div>
        </div>
        <!--div class="divFooter-B">
            <div>
                <h4 class="text-uppercase JudulFooter-1">informasi </h4></div>
            <div class="divLinkFooter"><a href="#" class="linkFooter">Kebijakan Privasi</a><a href="#" class="linkFooter">Syarat &amp; Ketentuan</a><a href="#" class="linkFooter">Sanggahan </a></div>
        </div-->
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 kolomFooter-1 footerRight">
        <div class="divFooter-A">
            <div>
                <h4 class="text-uppercase JudulFooter-1">Mitra </h4></div>
            <div class="divLinkFooter"><a href="http://www.telkomsel.com/" target="_blank" class="linkFooter">Telkomsel</a><a href="https://kofera.com/" target="_blank" class="linkFooter">Kofera Technology</a><a href="http://www.blanja.com/" target="_blank" class="linkFooter">Blanja.com</a><a href="https://indihome.co.id/internet-fiber" target="_blank" class="linkFooter">Internet Fiber</a><a href="http://www.qontak.com" class="linkFooter" target="_blank">Qontak.com</a><!--a href="#" class="linkFooter">Distributor CCTV Samsung</a>
                <a
                    href="#" class="linkFooter">Peluang Bisnis</a-->
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-7 kolomFooter-1 footerLeft">
        <div class="divFooter-A">
            <div>
                <h4 class="text-uppercase JudulFooter-1">situs jaringan</h4></div>
            <div class="divLinkFooter"><a href="http://jakartafamilia.com/" target="_blank" class="linkFooter">Jakartafamilia.com </a><a href="http://tabloidsimpanglima.com/" target="_blank" class="linkFooter">Tabloidsimpanglima.com </a><a href="http://surabayafamily.com/" target="_blank" class="linkFooter">Surabayafamily.com </a><a href="http://wisatamudik.com" target="_blank" class="linkFooter">wisatamudik.com</a></div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 kolomFooter-1 footerRight">
        <div class="divFooter-A">
            <div>
                <h4 class="text-uppercase JudulFooter-1">tentang md media</h4></div>
            <div class="divLinkFooter"><a href="{{ route('static-page', ['url' => 'profil-mdmedia']) }}" class="linkFooter">Profil MD Media</a><a href="{{ route('static-page', ['url' => 'profil-yellowpages']) }}" class="linkFooter">Profil Yellowpages.co.id</a><a href="{{ route('static-page', ['url' => 'faq']) }}" class="linkFooter">FAQ </a></div>
        </div>
    </div>
</div>
<div class="row barisCopyright">
    <div class="col-md-3 col-sm-4 col-xs-12 kolomCopyright">
        <div class="copyrightDiv-1">
            <div class="copyrightKolom"><img src="{{URL::asset('uploads/images/logotelkom.png')}}" class="logoTelkom"></div>
            <div class="copyrightKolom"><img src="{{URL::asset('uploads/images/logomdm.png')}}" class="logoMDM"></div>
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
        <div class="divTExtCopy">
            <p class="text-copyright">Copyright © 1996-<?php echo date("Y"); ?> MD Media, a member of Telkom Group. All rights, unles otherwise noted, are reserved. All other business-related trademarks and copyrights, including but not limited to business logos and website screenshots,
                are property of their respective owners. </p>
        </div>
    </div>
</div>

