<!DOCTYPE html>
<html lang="id-ID">

    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
        <link rel="shortcut icon" type="image/png" href="/favicon.png"/>

        <link rel="canonical" href="{{Request::url()}}" />
        <meta name="robots" content="{{(isset($search_page)) ? 'nofollow, noindex': 'index, follow'}}" />
        @yield('seo-meta', "<title>YellowPages.co.id</title>")

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        {!! Html::style('/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('/bootstrap/css/responsive.css') !!}
        {!! Html::style('/css/styles.min.css') !!}
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        {!! Html::style('/fonts/font-awesome.min.css') !!}
        {!! Html::style('/fonts/material-icons.css') !!}
        <link rel="stylesheet" href="//cdn.materialdesignicons.com/1.8.36/css/materialdesignicons.min.css
">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.css">
        @yield('style-content')
        
        <script>
            window.Laravel = <?php
            echo json_encode([
            'csrfToken' => csrf_token(),
            ]);
            ?>
        </script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', '{{env('GA')}}', 'auto');
            ga('send', 'pageview');

        </script>
    </head>
    <body {!! isset($is_homepage) ? 'class="homepage"' : ''  !!}>
        <div id="menuMobile">
            <div class="mobileMenuFloat-side"><a id="triggerSlide" class="tombolMobileFloat-side"><i class="material-icons ikonMenuMobile-f">menu</i></a></div>
            <div class="navMenu-side">
                <a class="text-uppercase navbarLink-Sidebar" href="{{ route('static-page', ['url' => 'cara-kerja']) }}">Cara Kerja</a>
                @if (Auth::check())
                <a class="text-uppercase navbarLink-Sidebar" href="{{ route('dashboard') }}">dashboard </a>
                <a class="text-uppercase navbarLink-Sidebar" href="{{ route('signout') }}">keluar </a>
                @else
                <a class="text-uppercase navbarLink-Sidebar" href="{{ route('signin') }}">masuk </a>
                <a class="text-uppercase navbarLink-Sidebar" href="{{ route('signup') }}">daftar </a>
                @endif
                
            </div>
            <form class="divInput-Side src-form">
                <div class="divHeaderInput">
                    <div class="dropdown dropTemukan-side"><a class="btn btn-default dropdown-toggle tombolDropdownSide" data-toggle="dropdown" aria-expanded="false" role="button" href="#"><span class="search-by-wrapper">TEMUKAN PRODUK</span><span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right search-menu" role="menu">
                            <li role="presentation"><a href="javascript:void(0)" data-value="bisnis">Bisnis</a></li>
                            <li role="presentation"><a href="javascript:void(0)" data-value="produk">Produk</a></li>
                        </ul>
                    </div>
                </div>
                <div class="divInputSide-produk">
                    <input type="hidden" class="src-by" value="bisnis" />
                    <input type="text" placeholder="Misal: hotel" class="textInputServis-side src-keyword" value="{{$keyword or ''}}">
                </div>
                <div class="divInputSide-lokasiProduk">
                    <input type="text" placeholder="Lokasi" class="textInputLokasi-side src-location" value="{{$address or ''}}">
                </div>
                <div class="divInput-cariSide">
                    <button class="btn btn-default btn-block tombolTemukanSide" type="submit"> <span>TEMUKAN </span></button>
                </div>
            </form>
        </div>

        <div class="menuOverlay">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
                    <div>
                        <a href="{{ route('index') }}">
                            <img src="{!! asset('mobile_assets/img/logo-yp.png') !!}" class="logoWebsiteFloat" />
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <form class="divInput src-form">
                        <div class="divHeaderInput">
                            <div class="dropdown dropTemukan"><a class="btn btn-default dropdown-toggle tombolDropdown" data-toggle="dropdown" aria-expanded="false" role="button" href="#"><span class="search-by-wrapper">TEMUKAN</span> <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left search-menu" role="menu">
                                    <li role="presentation"><a href="javascript:void(0)" data-value="bisnis">Bisnis</a></li>
                                    <li role="presentation"><a href="javascript:void(0)" data-value="produk">Produk</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="divInput-produk">
                            <input type="hidden" class="src-by" value="bisnis" />
                            <input type="text" placeholder="Misal: hotel" class="textInputServis src-keyword" value="{{$keyword or ''}}" name="keyword">
                        </div>
                        <div class="divInput-lokasiProduk">
                            <input type="text" placeholder="Lokasi" class="textInputLokasi src-location" name="location" value="{{$address or ''}}">
                        </div>
                        <div class="divInput-cari">
                            <button class="btn btn-default btn-block tombolTemukan" type="submit"><i class="material-icons">search</i> </button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-10 navbarMenuFloat">
                    <div class="navMenu">
                        <a class="text-uppercase navbarLink-Float" href="{{ route('static-page', ['url' => 'cara-kerja']) }}">Cara Kerja</a>
                        @if (Auth::check())
                        <a class="text-uppercase navbarLink-Float" href="{{ route('dashboard') }}">dashboard </a>
                        <a class="text-uppercase navbarLink-Float" href="{{ route('signout') }}">keluar </a>
                        @else
                        <a class="text-uppercase navbarLink-Float" href="{{ route('signin') }}">masuk </a>
                        <a class="text-uppercase navbarLink-Float" href="{{ route('signup') }}">daftar </a>
                        @endif
                        
                    </div>
                    <div class="mobileMenuFloat"><a id="trigger-produk" class="tombolMobileFloat"><i class="material-icons ikonMenuMobile-f">menu</i></a></div>
                </div>
            </div>
        </div>

        @yield('header')   
        @yield('content')

        <div class="divArtikelFooter-produk">
            <div class="container-fluid footer-introProduk">
                @include('frontend.layouts.seo_text') 
                @include('frontend.layouts.main_footer')            
            </div>
        </div>

        @include('frontend.layouts.mobile_footer')

        {!! Html::script('/js/jquery.min.js') !!}
        {!! Html::script('/bootstrap/js/bootstrap.min.js') !!}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.js"></script>
        {!! Html::script('/js/script.min.js') !!}
        @yield('script-content')
        <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });

        $('.search-menu a').click(function() {
            val = $(this).attr('data-value')
            $('.search-by-wrapper').html(val.toUpperCase())
            $('.src-by').val(val);
        })

        $('.src-form').submit(function(e) {
            e.preventDefault();
            searchAction(e, $(this))
        })

        $('.tombolTemukan').click(function(e) {
            e.preventDefault;
            searchAction(e, $(this))
        })

        $(".src-location").on('input', function () {
            var requestData = {
                'city': $(this).val(),
            };
            var urlAPI = '{{ url('/city') }}';
            var obj = $(this);

            $.ajax({
                headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                data: requestData,
                url: urlAPI,
                type: 'post',
                success: function (data) {
                    obj.autocomplete({
                        source: data
                    });
                }
            });
        })

        @if(isset($search_page) && $search_page)
        window.app_search = {
            'base_search': '{{ route('search-full', ['search_by' => '', 'keyword' => '', 'location' => '']) }}/',
            'search_by': '{{$search_by}}',
            'keyword': '{{str_replace(' ', '-', $keyword)}}',
            'location': '{{str_replace(' ', '-', $address)}}'
        }
        @endif

        $('#form-filter-area').submit(function(e) {
            if( typeof(window.app_search) == 'undefined' )
                return true;

            e.preventDefault()
            s = app_search;
            _location = $(this).find('#address').val().replace(' ', '-')
            window.location.href=s.base_search + s.search_by + '/' + s.keyword + '/' + _location;
        })

        function searchAction(e, obj) {
            e.preventDefault()
            kw = $(obj).closest('form').find('.src-keyword').val();
	if(!kw){kw = 'hotel'}
            loc = $(obj).closest('form').find('.src-location').val();
            if(loc === ''){
                //if empty get localcity
                if(typeof localStorage.getItem('localCity') !== 'undefined' && localStorage.getItem('localCity')){
                    loc = localStorage.getItem('localCity')
                }else{
                    loc = '';
                }
            }

            by = $(obj).closest('form').find('.src-by').val();

            if (!by) {
                by = 'BISNIS';
            }

            $('.src-keyword').val(kw);
            $('.src-location').val(loc);
            $('.src-by').val(by);

            if(kw || location) {
                url = '{{ route('search-full', ['search_by' => '', 'keyword' => '', 'location' => '']) }}' + '/' +
                        by.toLowerCase() + '/' + kw.replace(' ', '-') + '/' + loc.replace(' ', '-');
                window.location.href=url
            }
        }
        </script>
    </body>
</html>
