<header class="headerNav">
    <div class="row rowHeaderNor">
        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-2">
            <div>
                <a href="{{ route('index') }}">
                    <img src="{{URL::asset('uploads/images/logo-yp.png')}}" class="logoWebsite" />
                </a>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-9 col-xs-10 navbarMenu">
            <div class="navMenu">
                <a class="text-uppercase navbarLink" href="{{ route('static-page', ['url' => 'cara-kerja']) }}">Cara Kerja</a>
                @if (Auth::check())
                <a class="text-uppercase navbarLink" href="{{ route('dashboard') }}">dashboard </a>
                <a class="text-uppercase navbarLink" href="{{ route('signout') }}">keluar </a>
                @else
                <a class="text-uppercase navbarLink" href="{{ route('signin') }}">masuk </a>
                <a class="text-uppercase navbarLink" href="{{ route('signup') }}">daftar </a>
                @endif
            </div>
            <div class="mobileMenu"><a id="trigger" class="tombolMobile"><i class="material-icons ikonMenuMobile">menu</i></a></div>
        </div>
    </div>
</header>

<div class="container-fluid heroWebsite">
    <div class="headingHero">
        <h1 class="text-center typewriter heroText">
            <span class='banner-main-text0'> </span> 
            <a href="" class="typewrite banner-dynamic-text0" data-period="2000" data-type=''></a> 
            <span class='banner-main-text1'> </span>
            <a href="" class="typewrite banner-dynamic-text1" data-period="2000" data-type='["UMKM","TEST","DETES"]'></a></h1>
        <button class="btn btn-default buttonHero" type="button" onclick="window.location.href='{{ route('signin') }}'">DAFTAR GRATIS</button>
    </div>
</div>
<form class="hidden-xs barBawahHero home-src-form">
    <div class="inputDiv">
        <div class="input-group input-TemukanBisnis">
            <input class="form-control formInputBisnis src-by" type="text" placeholder="Temukan Bisnis" name="src-by">
            <div class="input-group-btn">
                <div class="dropdown btn-group tblDropdown-Bisnis" role="group">
                    <button class="btn btn-default dropdown-toggle tblDropdownBisnis" data-toggle="dropdown" aria-expanded="false" type="button"> <span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right dropdown-MenuBisnis" role="menu">
                        <li role="presentation"><a href="javascript:void(0)" data-value="bisnis">Bisnis</a></li>
                        <li role="presentation"><a href="javascript:void(0)" data-value="produk">Produk</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="divInput-servis">
        <input type="text" placeholder="Misal: hotel" class="textInputServis src-keyword">
    </div>
    <div class="divInput-lokasi">
        <input type="text" placeholder="Lokasi" class="textInputLokasi src-location">
    </div>
    <div class="divInput-enter">
        <button class="btn btn-default btn-block tombolTemukan" type="submit">TEMUKAN </button>
    </div>
</form>
