@extends('frontend.layouts.homepage')

@section('seo-meta')
    @include('seo.directory_listing')
@stop

@section('header')
@include('frontend.layouts.main_header')
@stop

@section('content')
<section>
    <div class="divBodiKonten">
        <div class="row rowKontenProduk">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 padding-0">
                <div class="blog-top-area fix">
                    @if($search_page)
                    <h1>Hasil Bisnis: '{{$keyword}}' {{ ($address) ? ('di ' . $address ) : '' }}</h1>
                    <div class="page-nav fix">
                        <a href="{{route('index')}}">Beranda</a> > Bisnis: '{{$keyword}}' {{ ($address) ? ('di ' . $address ) : '' }}
                    </div>
                    @else
                    <h1>Daftar Bisnis {{$category_name}}</h1>
                    <div class="page-nav fix">
                        <a href="{{route('index')}}">Beranda</a> > {{$category_name}}
                    </div>
                    @endif
                    
                </div>
                <div class="filder-area fix">
                    <a id="filter-id" class="floatleft"><span class="glyphicon glyphicon-filter"></span> Filter</a>
                    <a id="urutkan-id" class="floatright"><span class="glyphicon glyphicon-sort-by-attributes-alt"></span> Urutkan</a>
                </div>
                <div class="filter-option fix">
                    <div class="filter-location">Filter berdasar lokasi</div>
                    <div class="filder-form">
                        <form action="{{route('directory-category-location')}}" id="form-filter-area" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="text" name="address" id="address" value='{{$address}}' />
                            @if($address !=='' && !$search_page)
                            <button type="reset" class="clear-btn" onclick="location.href='{{route("directory-category",["category"=>$category])}}'"><i class="fa fa-times"></i></button>
                            @elseif($search_page)
                            <button type="reset" class="clear-btn" onclick="location.href='{{route("search", ['search_by' => 'bisnis', 'keyword' => str_replace(' ', '-', $keyword)])}}'"><i class="fa fa-times"></i></button>
                            @endif
                            <input type="hidden" name="category" value="{{$category}}">
                            <input type="hidden" name="urutkan" id="urutkan" value='{{$urutkan}}' />
                            <input type="submit" value="Apply" />
                        </form>
                    </div>
                </div>
                <div class="urutkan-area fix">
                    <div class="urntkan-text fix"> Urutkan Berdasar</div>
                    @if(!$search_page)
                    <form action="{{route('directory-category-ord')}}" method="post">
                    @else
                    <form class="src-form-order" method="get">
                    @endif

                        @if($urutkan === '' || $urutkan === 'rating')
                        @php
                        $rating = 'checked="true"';
                        $id = '';
                        $name_asc = '';
                        $name_desc = '';
                        @endphp
                        @elseif($urutkan === 'id')
                        @php
                        $id = 'checked="true"';
                        $rating = '';
                        $name_desc = '';
                        $name_asc = '';
                        @endphp    
                        @elseif($urutkan === 'name_asc')
                        @php
                        $name_asc = 'checked="true"';
                        $name_desc = '';
                        $rating = '';
                        $id = '';
                        @endphp   
                        @elseif($urutkan === 'name_desc')
                        @php
                        $name_desc = 'checked="true"';
                        $name_asc = '';
                        $rating = '';
                        $id = '';
                        @endphp 
                        @endif
                        <input type="hidden" name="address" id="address" value='{{$address}}' />
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="category" value="{{$category}}">
                        <input type="radio" name="urutkan" id="option-1" value="name_asc" {{$name_asc}} />
                        <label for="option-1">Nama (asc)</label>
                        <input type="radio" name="urutkan" id="option-2" value="name_desc" {{$name_desc}} />
                        <label for="option-2">Nama (desc)</label>
                        <input type="radio" name="urutkan" id="option-3" value="id" {{$id}} />
                        <label for="option-3">Terbaru</label>
                        <input type="radio" name="urutkan" id="option-4" value="rating" {{$rating}} />
                        <label for="option-4">Rating</label>
                        <input type="submit" value="Apply" />
                    </form>
                </div>
                <div class="blog-area fix">
                    @if($address !=='')
                    <div class="filter-wrapper">
                        <b>Difilter berdasar:</b> <span>Lokasi {{$address}}</span>
                    </div>
                    @endif
                    @forelse($data as $data_detail)
                    <a href="{{route('directory',$data_detail['directory_slug'])}}">
                    <article class="single-post fix">
                        <div class="post-tham floatleft">
                            @if($data_detail['directory_logo'])
                            <img src="{{URL::asset('uploads/directories/thumb_'.$data_detail['directory_logo'])}}"
                                 onerror="this.src='{{URL::asset('uploads/directories/'.$data_detail['directory_logo']) }}'; this.onerror=null;" alt="{{ $data_detail['directory_name']}}" />
                            @else
                            <img src="{{URL::asset('img/img-default.jpg')}}" alt="{{ $data_detail['directory_name']}}" />
                            @endif
                        </div>
                        <div class="post-text floatleft">
                            <h3>{{$data_detail['directory_name']}} @if($data_detail['is_verified'])<i class="mdi mdi-check-circle verified" aria-hidden="true"></i>@endif</h3>
                            <p>{{$data_detail['province_name']}} > {{$data_detail['city_name']}}</p>
                            <p>{{$data_detail['directory_address']}}</p>
                        </div>
                        @if(count($data_detail['product']) > 0)
                        <div class="listing-image-tn floatleft">
                        <ul>
                            @foreach($data_detail['product'] as $product_detail)
                            <li><a href="{{route('product-detail',$product_detail->slug)}}">
                                @if($product_detail->picture)
                                <img src="{{URL::asset('uploads/products/thumb_'.$product_detail->picture)}}" title='{{$product_detail->name}}'
                                     onerror="this.src='{{URL::asset('uploads/products/'.$product_detail->picture)}}';this.onerror=null;" alt="{{$product_detail->name}}" />
                                @else
                                <img src="{{URL::asset('img/img-default.jpg')}}" title='{{$product_detail->name}}' />
                                @endif
                            </a></li>
                            @endforeach
                        </ul>
                        </div>
                        @endif
                    </article>
                    </a>
                    @empty
                    <article class="single-post fix">
                        <center>Data tidak ditemukan</center>
                    </article>
                    @endforelse

                    <!--article class="single-post fix sponsored">
                        
                        <div class="post-tham floatleft">
                            <img src="images/tham-1.png" alt="" />
                        </div>
                        <div class="post-text floatleft">
                            <h3>Bebek Kaleyo Tebet</h3>
                            <p>DKI Jakarta > Jakarta Selatan</p>
                            <p>Jalan Lapangan Roos No.49, Bukit Duri, Tebet, RT.4/RW.1, Tebet Timur,  Jakarta Selatan</p>
                            
                            <a href="#" class="readmore">SPONSORED</a>
                        </div>
                        <div class="listing-image-tn floatleft">
                            <ul>
                                <li><a href="#"><img src="images/p-1.png" alt="" /></a></li>
                                <li><a href="#"><img src="images/p-2.png" alt="" /></a></li>
                                <li><a href="#"><img src="images/p-3.png" alt="" /></a></li>
                                <li><a href="#"><img src="images/p-4.png" alt="" /></a></li>
                            </ul>
                        </div>
                    </article-->

                    <div class="post-nav fix">
                        <ul>
                            @php
                            $limit = config('view.pagesize');
                            $total_page = ceil($total_category_listing/$limit);
                            @endphp
                            @if(!$search_page)
                            @if($page - 2 > 1)
                                @if($urutkan !=='' && $address !== '')
                                    @php
                                    $url = route('directory-category-with-location-ord',['category'=>$category,'location'=>$address,'ord'=>$urutkan,'page'=>1]);
                                    @endphp
                                @elseif($urutkan !=='')
                                    @php
                                    $url = route('directory-category-with-ord',['category'=>$category,'ord'=>$urutkan,'page'=>1]);
                                    @endphp
                                @elseif($address !=='')
                                    @php
                                    $url = route('directory-category-with-location',['category'=>$category,'location'=>$address,'page'=>1]);
                                    @endphp
                                @else
                                    @php
                                    $url = route('directory-category-with-page',['category'=>$category,'page'=>1]);
                                    @endphp
                                @endif
                                    <li><a href="{{$url}}">{{1}}</a></li>
                                    <li> ... &nbsp;</li>
                            @endif
                            
                            @if($page - 2 > 0)
                            
                                @if($urutkan !=='' && $address !== '')
                                    @php
                                    $url = route('directory-category-with-location-ord',['category'=>$category,'location'=>$address,'ord'=>$urutkan,'page'=>$page - 2]);
                                    @endphp
                                @elseif($urutkan !=='')
                                    @php
                                    $url = route('directory-category-with-ord',['category'=>$category,'ord'=>$urutkan,'page'=>$page - 2]);
                                    @endphp
                                @elseif($address !=='')
                                    @php
                                    $url = route('directory-category-with-location',['category'=>$category,'location'=>$address,'page'=>$page - 2]);
                                    @endphp
                                @else
                                    @php
                                    $url = route('directory-category-with-page',['category'=>$category,'page'=>$page - 2]);
                                    @endphp
                                @endif    
                                    <li> <a href="{{$url}}">{{$page - 2}}</a></li>
                                
                            @endif
                            
                            @if($page - 1>0)
                                @if($urutkan !=='' && $address !== '')
                                    @php
                                    $url = route('directory-category-with-location-ord',['category'=>$category,'location'=>$address,'ord'=>$urutkan,'page'=>$page - 1]);
                                    @endphp
                                @elseif($urutkan !=='')
                                    @php
                                    $url = route('directory-category-with-ord',['category'=>$category,'ord'=>$urutkan,'page'=>$page - 1]);
                                    @endphp
                                @elseif($address !=='')
                                    @php
                                    $url = route('directory-category-with-location',['category'=>$category,'location'=>$address,'page'=>$page - 1]);
                                    @endphp
                                @else
                                    @php
                                    $url = route('directory-category-with-page',['category'=>$category,'page'=>$page - 1]);
                                    @endphp
                                @endif                            
                                <li><a href="{{$url}}">{{$page - 1}}</a></li>
                            @endif
                            
                            @php
                                $totalnext = 0;
                            @endphp
                            @for($page_list=$page;$page_list<=$total_page;$page_list++)
                            
                            @php 
                                $totalnext = $totalnext+1;
                                
                            @endphp
                            
                            @if($totalnext === 4)
                                @if($urutkan !=='' && $address !== '')
                                    @php
                                    $url = route('directory-category-with-location-ord',['category'=>$category,'location'=>$address,'ord'=>$urutkan,'page'=>$total_page]);
                                    @endphp
                                @elseif($urutkan !=='')
                                    @php
                                    $url = route('directory-category-with-ord',['category'=>$category,'ord'=>$urutkan,'page'=>$total_page]);
                                    @endphp
                                @elseif($address !=='')
                                    @php
                                    $url = route('directory-category-with-location',['category'=>$category,'location'=>$address,'page'=>$total_page]);
                                    @endphp
                                @else
                                    @php
                                    $url = route('directory-category-with-page',['category'=>$category,'page'=>$total_page]);
                                    @endphp
                                @endif
                            
                                @if($totalnext < $total_page)
                                    <li> ... &nbsp;</li>
                                    <li><a href="{{$url}}">{{$total_page}}</a></li>
                                    @php
                                        break;
                                    @endphp
                                @endif
                            
                            @else
                                @if($urutkan !=='' && $address !== '')
                                    @php
                                    $url = route('directory-category-with-location-ord',['category'=>$category,'location'=>$address,'ord'=>$urutkan,'page'=>$page_list]);
                                    @endphp
                                @elseif($urutkan !=='')
                                    @php
                                    $url = route('directory-category-with-ord',['category'=>$category,'ord'=>$urutkan,'page'=>$page_list]);
                                    @endphp
                                @elseif($address !=='')
                                    @php
                                    $url = route('directory-category-with-location',['category'=>$category,'location'=>$address,'page'=>$page_list]);
                                    @endphp
                                @else
                                    @php
                                    $url = route('directory-category-with-page',['category'=>$category,'page'=>$page_list]);
                                    @endphp
                                @endif
                            
                                @if($page_list == $page)
                                    <li><a href="{{$url}}" class="current">{{$page_list}}</a></li>
                                @else
                                    <li><a href="{{$url}}">{{$page_list}}</a></li>
                                @endif
                                
                            @endif
                            @endfor
                            @else
                                @if($page - 2 > 1)
                                    @php
                                    $url = route('search-full',[
                                        'search_by' => $search_by, 'keyword' => str_replace(' ', '-', $keyword),
                                        'location' => str_replace(' ', '-', $address), 'sort' => $urutkan
                                    ]);
                                    @endphp
                                    <li><a href="{{$url}}">{{1}}</a></li>
                                    <li> ... &nbsp;</li>
                                @endif

                                @if($page - 2 > 0)
                                    @php
                                    $url = route('search-full',[
                                        'search_by' => $search_by, 'keyword' => str_replace(' ', '-', $keyword),
                                        'location' => str_replace(' ', '-', $address), 'page' => $page - 2, 'sort' => $urutkan
                                    ]);
                                    @endphp
                                    <li> <a href="{{$url}}">{{$page - 2}}</a></li>
                                @endif

                                @if($page - 1>0)
                                    @php
                                    $url = route('search-full',[
                                        'search_by' => $search_by, 'keyword' => str_replace(' ', '-', $keyword),
                                        'location' => str_replace(' ', '-', $address), 'page' => $page - 1, 'sort' => $urutkan
                                    ]);
                                    @endphp
                                    <li><a href="{{$url}}">{{$page - 1}}</a></li>
                                @endif

                                @php
                                $totalnext = 0;
                                @endphp
                                @for($page_list=$page;$page_list<=$total_page;$page_list++)
                                    @php
                                    $totalnext = $totalnext+1;
                                    @endphp

                                    @if($totalnext === 4)
                                        @php
                                        $url = route('search-full',[
                                            'search_by' => $search_by, 'keyword' => str_replace(' ', '-', $keyword),
                                            'location' => str_replace(' ', '-', $address), 'page' => $total_page, 'sort' => $urutkan
                                        ]);
                                        @endphp

                                        @if($totalnext < $total_page)
                                            <li> ... &nbsp;</li>
                                            <li><a href="{{$url}}">{{$total_page}}</a></li>
                                            @php
                                            break;
                                            @endphp
                                        @endif

                                    @else
                                        @php
                                        $url = route('search-full',[
                                            'search_by' => $search_by, 'keyword' => str_replace(' ', '-', $keyword),
                                            'location' => str_replace(' ', '-', $address), 'page' => $page_list, 'sort' => $urutkan
                                        ]);
                                        @endphp
                                        @if($page_list == $page)
                                            <li><a href="{{$url}}" class="current">{{$page_list}}</a></li>
                                        @else
                                            <li><a href="{{$url}}">{{$page_list}}</a></li>
                                        @endif
                                    @endif
                                @endfor
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding-0">
                <!--div class="map-area fix">
                    <iframe allowfullscreen="" frameborder="0" width="100%" height="300" src="https://www.google.com/maps/embed/v1/view?key=AIzaSyDTirAXlwdsHq7L0Y_h0BN98Ztu-1FVjLw&amp;center=-6.2297264%2C106.6894312&amp;zoom=15" class="petaLokasi"></iframe>
                </div-->

                <div class="bannerWidget">
                    <div class="bannerKode">
                        <a href="{{route('signup')}}"><img src="{{URL::asset('img/').'/banner_yp2.png'}}" alt="" /></a>
                    </div>
                </div>

                @if(count($newest_bis)>0)
                <div class="widgetArtikelTerkait">
                    <div class="divHeader-ArtikelTerkait">
                        <h3 class="text-uppercase headingArtikelTerkait">Bisnis Terbaru</h3>
                    </div>
                    @foreach($newest_bis as $bis)
                    <a href="{{ route('directory', $bis->slug) }}">
                        <div class="listProdukSerupa">
                            <p class="namaBisnis-slide">{{$bis->name}}</p>
                            <p class="lokasiBisnis-slide">{{$bis->address_1}}</p>
                        </div>
                    </a>
                    @endforeach
                </div>
                @endif
                <div class="widgetArtikelTerkait">
                    @if(count($affiliate_article)>0)
                    <div class="divHeader-ArtikelTerkait">
                        <h3 class="text-uppercase headingArtikelTerkait">artikel terkait</h3>
                    </div>
                    @foreach($affiliate_article as $affiliate_article_detail)
                    <a href="{{$affiliate_article_detail->full_url}}">
                        <div class="listProdukSerupa">
                            <p class="namaBisnis-slide">{{$affiliate_article_detail->title}}</p>
                        </div>
                    </a>
                    @endforeach
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('footer-company-profile')
@stop

@section('script-content')
{!! Html::script('/js/location.js') !!}
<script>

$("#address").on('input', function () {
    var requestData = {
        'city': $("#address").val(),
    };
    var urlAPI = "{{route('getAllCity')}}";
    $.ajax({
        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
        data: requestData,
        url: urlAPI,
        type: 'post',
        beforeSend: function () {
            //beginTransition();
        },
        complete: function () {
            //endLoadingMapTransition();
        },
        success: function (data) {
            $("#address").autocomplete({
                source: data
            });
        }
    });
})



jQuery(document).ready(function () {
    jQuery('#filter-id').click(function () {
        jQuery('.filter-option').toggleClass('show');
    });
    jQuery('#urutkan-id').click(function () {
        jQuery('.urutkan-area').toggleClass('show');
    });
});

@if($search_page)
$('.src-form-order').submit(function(e) {
    e.preventDefault()
    base_url = '/search/bisnis/{{$keyword}}';
    val = $(this).closest('form').find('input[type=radio]:checked').val()
    window.location.href="?sort="+val;
})
@endif
</script>

@stop
