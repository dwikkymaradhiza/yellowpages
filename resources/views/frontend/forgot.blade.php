@extends('frontend.layouts.homepage')

@section('header')
{!! Html::style('/css/main.min.css') !!}
@include('frontend.layouts.main_header')
@stop

@section('content')
<!-- content -->
<div class="daftar-data">
    <h2>KEMBANGKAN BISNIS ANDA BERSAMA KAMI</h2>
    <p align="center">Belum punya akun Yellow Pages? <a href="{{ route('signup') }}">Daftar disini</a></p>
    <div class="container">
        <div class="row  pad-top">
            <div class="col-md-6 col-md-offset-3" >
                <div class="panel panel-default login">
                    <div class="strike" style="margin-top:0px;">
                        <span>Masukkan alamat email Anda</span>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form role="form" action="{{ url('/password/email') }}" method="POST" id="main-form">
                        {!! csrf_field() !!}
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Alamat Email" name="email" value="{{ old('email') }}">
                            <span class="fa fa-envelope-o fa-2x form-control-feedback"></span>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </form>
                    <button type="submit" class="btn btn-yellow btn-login" form="main-form">Reset Password</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="/js/messages_id.js"></script>
{!! Html::script('/js/location.js') !!}
<script>
$("#main-form").validate({
    rules: {
        email: {
            required: true,
            email: true,
            remote: {
                url: '/ajax/check_email',
                type: 'post',
                data: {
                    _token: window.Laravel.csrfToken,
                    must_exists: true
                }
            }
        }
    },
    messages: {
        email: {
            remote: "Email tidak terdaftar"
        }
    }
});
</script>
@endsection
