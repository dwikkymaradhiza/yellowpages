@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Update Status Anda</h2>
    <div class="row rowKontenProduk2 ">
        <div class="table-responsive">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->
            <table class="table-fill">
                <thead>
                    <tr>
                        <th class="text-center"><input type="checkbox" id="check-all" /></th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Tanggal</th>
                        <th class="text-center">Bisnis</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @foreach($infos as $k => $v)
                    <tr>
                        <td class="text-center"><input type="checkbox" class="row-check" /></td>
                        <td class="text-left">{{ str_limit($v->message, 25) }}</td>
                        <td class="text-left">{{ $v->created_at->format('d/m/Y H:i:s') }}</td>
                        <td class="text-left">{{ $v->directory->name or "-" }}</td>
                        <td class="text-center">
                            <div class="">
                                <div class="col-md-4" style="padding:0px">
                                    <form onclick="return false;">
                                    <button data-directory-id="{{ $v->directory_id }}" data-id="{{ $v->id }}" 
                                            data-message ="{{ $v->message }}"
                                            data-toggle="modal" data-target="#myModal--effect-zoomOutIn" class="btn-yellow btn-edit" style="border:none">    
                                    <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit" data-placement="right"></i>
                                    </button>
                                    <!--</button>-->
                                </form>
                                </div>
                                <div class="col-md-4" style="padding:0px">
                                    <form method="POST" action="{{ route('dashboard-status-delete', ['id' => $v->id]) }}">
                                        {{ csrf_field() }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        <button onclick="return window.confirm('Apakah anda ingin menghapus data ini?')" class="btn-yellow btn-hapus" style="border:none">
                                            <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" title="Hapus" data-placement="right"></i>
                                        </button>
                                    </form>
                                </div>
                                <div class="col-md-4" style="padding:5px 0px;">
                                    <a href="{{ (isset($v->directory->slug)) ? route('directory', ['slug' => $v->directory->slug]) : '#' }}" class="btn-yellow btn-edit" target="_blank"><i class="fa fa-eye" aria-hidden="true" title="Lihat" data-placement="right"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>   
        </div>
        <div class="col-md-6">
            {{ $infos->appends(['size' => $size])->links() }}
        </div>
        <div class="col-md-6 pull-right">
            <div class="btn-produk">
                <div class="pull-right">
                    <form class="form-horizontal" id="page-rows-form">
                        <label class="pull-left control-label">Show</label>
                        <div class="pull-left">
                            <select id="pagesize" class="form-control">
                                <option value="5" {!! $size && $size === 5 ? "selected='selected'" : ""  !!}>5</option>
                                <option value="10" {!! $size && $size === 10 ? "selected='selected'" : ""  !!}>10</option>
                                <option value="15" {!! $size && $size === 15 ? "selected='selected'" : ""  !!}>15</option>
                                <option value="20" {!! $size && $size === 20 ? "selected='selected'" : ""  !!}>20</option>
                                <option value="25" {!! $size && $size === 25 ? "selected='selected'" : ""  !!}>25</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                <button type="submit" class="btn btn-yellow btn-bisnis" data-toggle="modal" data-target="#myModal--effect-zoomOutIn">TAMBAH STATUS</button>
                <button type="submit" class="btn btn-yellow btn-bisnis">HAPUS STATUS</button>
            </div>
        </div>
    </div>
</div>

    <!-- modal -->
    <div class="modal fade" id="myModal--effect-zoomOutIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">TAMBAH STATUS</h4>
                </div>
                <div class="modal-body">
                    <div class="row rowKontenProduk">
                        <form class="form-horizontal" method="POST" action="{{ route('dashboard-status-create') }}" id="statusForm">
                            <input type='hidden' id='status_id' name='status_id'>
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>TULIS STATUS</label>
                                            <select class="form-control" name="directory_id" id='directory_id'>
                                                @foreach($directories as $dk => $dv)
                                                    <option value="{{ $dv->id }}">{{ $dv->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label> </label>
                                            <textarea required id="message" name="message" class="form-control" style="height: 280px;"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <hr/>
                                <p class="text-center">
                                    <button type="submit" class="btn btn-yellow btn-simpan">SIMPAN</button>
                                </p>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min') }}"></script>
<script>
    $('#myModal--effect-zoomOutIn').on('show.bs.modal', function (e) {
        //clear all error remain
        $(".error").html('');
        $(".form-control").removeClass('error');
        
        var directory_id = $(e.relatedTarget).data('directory-id');
        $('#directory_id option[value='+directory_id +']').attr('selected','selected');
        
        var message = $(e.relatedTarget).data('message');
        $('#message').val(message);
            
        var id = $(e.relatedTarget).data('id');
        $('#status_id').val(id);
        
    });
    
    let pageSize = document.getElementById("pagesize");
    pageSize.onchange = function () {
        let value = this.value;
        let reloadUri = '{{ route("dashboard-inquiry-index") }}';

        window.location = reloadUri + "?size=" + value;
    };
    
    $(document).ready(function(){
        $("#statusForm").validate();
        $('[data-toggle="tooltip"]').tooltip();   
    });

    $(document).on('change', 'table thead [type="checkbox"]', function(e){
        e && e.preventDefault();
        var $table = $(e.target).closest('table'), $checked = $(e.target).is(':checked');
        $('tbody [type="checkbox"]',$table).prop('checked', $checked);
        $("#btn-del-reports").toggle();
    });

    function initMap() {
        var map = new google.maps.Map(document.getElementById('maps'), {
          center: {lat: -34.397, lng: 150.644},
          scrollwheel: false,
          zoom: 8
        });
    }
</script>
@endsection
