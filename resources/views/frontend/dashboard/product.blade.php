@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Produk Bisnis Anda</h2>

    <div class="row rowKontenProduk2 ">
        <div class="table-responsive">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->
            <table class="table-fill" width="">
                <thead>
                    <tr>
                        <th class="text-center"><input type="checkbox" id="check-all" /></th>
                        <th class="text-center">Nama Produk</th>
                        <th class="text-center">Kategori</th>
                        <th class="text-center">Bisnis</th>
                        <th class="text-center">Stok</th>
                        <th class="text-center" width="">Opsi</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @foreach($products as $k => $v)
                    <tr>
                        <td class="text-center"><input type="checkbox" class="row-check" /></td>
                        <td class="text-left">{{ $v->name }}</td>
                        <td class="text-left">{{ str_limit($v->directory->categories->pluck('name_lid')->implode(', '), 20) }}</td>
                        <td class="text-left">{{ $v->directory->name }}</td>
                        <td class="text-center">{{ $v->stock }}</td>
                        <td class="text-center">
                            <div class="">
                                <div class="col-md-4" style="padding:5px 0px;">
                                    <a href="{{ route('edit_product', ['id' => $v->id]) }}" class="btn-yellow btn-edit" >
                                        <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit" data-placement="right"></i>
                                    </a>
                                </div>
                                <div class="col-md-4" style="padding:0px">
                                    <form class="form-horizontal" action="{{route('dashboard-product-delete')}}" method="post">
                                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="id" value="{{$v->id}}">
                                        <button onclick="return window.confirm('Apakah anda ingin menghapus data ini?')" class="btn-yellow btn-hapus" style="border:none">
                                            <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" title="Hapus" data-placement="right"></i>
                                        </button>
                                    </form>
                                </div>
                                <div class="col-md-4" style="padding:5px 0px;">
                                    <a href="{{ route('product-detail', $v->slug) }}" target="_blank" class=" btn-yellow btn-edit"> <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>   
        </div>
        <div class="col-md-6">
            {{ $products->appends(['size' => $size])->links() }}
        </div>
        <div class="col-md-6 pull-right">
            <div class="btn-produk">
                <div class="pull-right">
                    <form class="form-horizontal" id="page-rows-form">
                        <label class="pull-left control-label">Show</label>
                        <div class="pull-left">
                            <select id="pagesize" class="form-control">
                                <option value="5" {!! $size && $size === 5 ? "selected='selected'" : ""  !!}>5</option>
                                <option value="10" {!! $size && $size === 10 ? "selected='selected'" : ""  !!}>10</option>
                                <option value="15" {!! $size && $size === 15 ? "selected='selected'" : ""  !!}>15</option>
                                <option value="20" {!! $size && $size === 20 ? "selected='selected'" : ""  !!}>20</option>
                                <option value="25" {!! $size && $size === 25 ? "selected='selected'" : ""  !!}>25</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                <button type="submit" class="btn btn-yellow btn-bisnis" data-toggle="modal" data-target="#myModal--effect-zoomOutIn">TAMBAH PRODUK</button>
                <button type="submit" class="btn btn-yellow btn-bisnis">HAPUS PRODUK</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }
</style>
<div class="modal fade" id="myModal--effect-zoomOutIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">TAMBAH PRODUK</h4>
            </div>
            <div class="modal-body">
                <div class="row rowKontenProduk">
                    <form id="main-form" class="form-horizontal" action="{{ route('create_product') }}" method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input id="product_id" name="product_id" type="hidden">
                        <fieldset>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Nama Produk</label>
                                    <input type="text" required id="name" class="form-control" name="name">
                                </div>
                                <div class="col-md-6">
                                    <label for="firstName">Stock </label>
                                    <input type="text" id="stock" class="form-control" name="stock">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="firstName">Gambar </label>
                                    <input type="hidden" id="picture" name="picture">
                                    <input type="file" multiple class="form-control" name="image[]" />
                                    <span>Max size: 2MB</span>
                                    <br>
                                    <div id="pic"></div>
                                    <br>
                                    <label for="firstName">Bisnis </label>
                                    <select required name="directory_id" id="directory_id" class="form-control">
                                        @foreach($directoriesList as $directoriesListDetail)
                                        <option value="{{$directoriesListDetail->id}}">{{$directoriesListDetail->name}}</option>
                                        @endforeach
                                    </select>    
                                </div>
                                <div class="col-md-6">
                                    <label>Deskripsi Produk</label>
                                    <textarea required class="form-control" id="description" style="height: 280px;" name="description"></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group row">

                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                </div>
                            </div>
                            <p class="text-center">
                                <button type="submit" class="btn btn-yellow btn-simpan">SIMPAN</button>
                            </p>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script>
    function showModal(){
        $('#myModal--effect-zoomOutIn').modal('show');
        return false;
    }
    
    $(document).ready(function() {
        $("#main-form").validate();
    });
    
    $('#myModal--effect-zoomOutIn').on('show.bs.modal', function (e) {
        //clear all error
        $(".error").html('');
        $(".form-control").removeClass('error');
    });
        
    let pageSize = document.getElementById("pagesize");
    pageSize.onchange = function() {
    let value = this.value;
            let reloadUri = '{{ route("dashboard-directory-index") }}';
            window.location = reloadUri + "?size=" + value;
    };
    
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    
    $(document).on('change', 'table thead [type="checkbox"]', function(e){
        e && e.preventDefault();
        var $table = $(e.target).closest('table'), $checked = $(e.target).is(':checked');
        $('tbody [type="checkbox"]', $table).prop('checked', $checked);
        $("#btn-del-reports").toggle();
    });
    
    // upload foto
    jQuery(document).ready(function($) {
    var outputHandlerFunc = function(imgObj) {
    var sizeInKB = function(bytes) {return (typeof bytes == 'number') ? (bytes / 1024).toFixed(2) + 'Kb' : bytes; };
            var getThumbnail = function(original, maxWidth, maxHeight, upscale) {
            var canvas = document.createElement("canvas"), width, height;
                    if (original.width < maxWidth && original.height < maxHeight && upscale == undefined) {
            width = original.width;
                    height = original.height;
            }
            else {
            width = maxWidth;
                    height = parseInt(original.height * (maxWidth / original.width));
                    if (height > maxHeight) {
            height = maxHeight;
                    width = parseInt(original.width * (maxHeight / original.height));
            }
            }
            canvas.width = width;
                    canvas.height = height;
                    canvas.getContext("2d").drawImage(original, 0, 0, width, height);
                    $(canvas).attr('title', 'Original size: ' + original.width + 'x' + original.height);
                    return canvas;
            }

    $(new Image()).on('load', function(e) {
            var $wrapper = $('<li class="new-item"><div class="list-content"><span class="preview"></span><span class="type">' + imgObj.type + '<br>' + (e.target.width + '&times;' + e.target.height) + '<br>' + sizeInKB(imgObj.size) + '</span>Berhasil upload  <span class="name">' + imgObj.name + '</span><span class="options"><span class="imagedelete" title="Remove image"><i class="fa fa-times" aria-hidden="true"></i></span></span></div></li>').appendTo('#output ul');
            $('.imagedelete', $wrapper).one('click', function(e) {
    $wrapper.toggleClass('new-item').addClass('removed-item');
            $wrapper.one('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(e) {
            $wrapper.remove();
            });
    });
            var thumb = getThumbnail(e.target, 50, 50);
            var $link = $('<a rel="fancybox">').attr({
    target:"_blank",
            href: imgObj.imgSrc
    }).append(thumb).appendTo($('.preview', $wrapper));
    }).attr('src', imgObj.imgSrc);
    }

    $("a[rel=fancybox]").fancybox();
            var fileReaderAvailable = (typeof FileReader !== "undefined");
            var clipBoardAvailable = (window.clipboardData !== false);
            var pasteAvailable = Boolean(clipBoardAvailable & fileReaderAvailable & !eval('/*@cc_on !@*/false'));
            if (fileReaderAvailable) {
    $('#dropzones').imageUpload({
    errorContainer: $('span', '#errormessages'),
            trigger: 'click',
            enableCliboardCapture: pasteAvailable,
            onBeforeUpload: function() {$('body').css('background-color', 'green'); console.log('start', Date.now()); },
            onAfterUpload: function() {$('body').css('background-color', '#eee'); console.log('end', Date.now()); },
            outputHandler:outputHandlerFunc
    })
            $('#dropzones').prev('#textbox-wrapper').find('#textbox').append('<i class="fa fa-camera fa-3x" aria-hidden="true"></i>');
    }
    else {
    $('body').addClass('nofilereader');
    }
    if (!pasteAvailable) {
    $('body').addClass('nopaste');
    }
    });
            $(".js-example-basic-multiple").select2();
</script>
@endsection
