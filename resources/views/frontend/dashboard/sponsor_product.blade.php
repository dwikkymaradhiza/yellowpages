@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Pilih Bisnis Atau Produk</h2>

    <p>Pilih bisnis atau produk yang Anda akan iklankan di Yellowpages Sponsored Business</p>
    
    <div class="row rowKontenProduk2 ">
        <div class="table-responsive">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->

            <table class="table-fill" width="">
                <thead>
                    <tr>
                        <th class="text-center"><input type="checkbox" id="check-all" /></th>
                        <th class="text-center">Nama Produk/Bisnis</th>
                        <th class="text-center">Deskripsi</th>
                        
                    </tr>
                </thead>
                <tbody class="table-hover">
            @if(count($directories)>0)
            @foreach($directories as $directory_detail)
            <tr>
                <td class="text-center">
                    <input type="checkbox" data-type="{{$directory_detail->type}}" id="{{$directory_detail->type.'_'.$directory_detail->id}}" value="{{$directory_detail->id}}" name="productcb">
                </td>
                <td>
                    {{$directory_detail->product_name}}
                </td>
                <td>
                    {{$directory_detail->product_caption}}
                </td>
            </tr>
            
            @endforeach
            @else
                <tr>
                    <td colspan='2'>Anda belum mempunyai bisnis atau produk. Silahkan buat bisnis anda terlebih dahulu.</td>
                </tr>
            @endif
                </tbody>
            </table>   
        </div>
        <div class="col-md-6">
            
        </div>
        <div class="col-md-6 pull-right">
            <div class="btn-produk">
                <div class="pull-right">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                @if(count($directories)>0)
                    <button class="btn btn-yellow btn-bisnis" onclick="saveSponsorItem();return false;">Lanjutkan</button>
                @else
                    <button class="btn btn-yellow btn-bisnis" onclick="createDirectory();return false;">Buat Bisnis</button>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }
</style>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script>
    $(document).on('ready',function(){
        var product_id = localStorage.getItem('product_sponsor');
        var directory_id = localStorage.getItem('directory_sponsor');
        

        var product_id_array = product_id.split(',');
        var directory_id_array = directory_id.split(',');

        if(product_id_array.length > 0){
            for (var index in product_id_array){
                $("#product_"+product_id_array[index]).prop('checked',true);
            }            
        }
        
        if(directory_id_array.length > 0){
            for (var index in directory_id_array){
                $("#directory_"+directory_id_array[index]).prop('checked',true);
            }            
        }
        
    });
    
    function createDirectory(){
        var url = "{{route('dashboard-directory-index')}}"
        
        window.location = url;
    }
function saveSponsorItem() {
    var product_id_array = $("input[name=productcb]:checked").map(function () {
        if ($(this).data('type') === 'product') {
            return $(this).val();
        }
    }).get().join(',');


    var directory_id_array = $("input[name=productcb]:checked").map(function () {
        if ($(this).data('type') === 'directory') {
            return $(this).val();
        }
    }).get().join(',');
    
    localStorage.setItem('product_sponsor', product_id_array);
    localStorage.setItem('directory_sponsor', directory_id_array);
    
    window.location = "{{route('sponsor-package')}}";
    
    return false;
}
</script>
@endsection
