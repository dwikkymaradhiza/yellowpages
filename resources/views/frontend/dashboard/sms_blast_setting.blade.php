@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Pengaturan SMS Blast</h2>
    <p>Silakan pilih jenis paket Targeted SMS yang sesuai dengan promo Anda.</p>

    <div class="row rowKontenProduk2 ">
        
        <form>
            {{ csrf_field() }}
            <div class="col-xs-12">
                <div class="col-md-4">
                    <textarea id="text_sms" name="text" class="form-control sms-number-textarea" placeholder="Isi pesan, maksimum 160 huruf termasuk spasi dan tanda baca."></textarea>
                </div>
                <div class="col-md-8 box_1">
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <label>Jumlah SMS</label>                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <select name="packages" class="form-control" id="packages">
                                    @foreach($packages as $packages_detail)
                                        <option value="{{$packages_detail->id}}" data-price="{{$packages_detail->price}}">
                                            {{$packages_detail->package_name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-md-3">
                                <input type="text" required id="total_sms" class="form-control" name="total_sms" id="total_sms" value="1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <label>Jadwal SMS</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-md-6">
                                <input type='text' class="form-control" id="start_date" name="start_date" />
                            </div>
                            <div class="col-xs-10 col-md-6">
                                <input type="date" required id="end_date" class="form-control" name="end_date" value="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <label>Custom Sender ID</label>
                            </div>
                            <div class="col-xs-12 col-md-12">
                                <input type="date" required id="sender_id" value="{{$senders[0]->id}}" class="form-control" name="sender_id" readonly="true">
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </form>
            <div class="col-xs-12 error-box">
                
            </div> 
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                <button onclick="javascript: history.go(-1)" class="btn btn-yellow btn-bisnis">Kembali</button>
                <button id="save" class="btn btn-yellow btn-bisnis" onclick="return false;">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }
</style>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script src="{{ asset('js/datetimepicker-master/build/jquery.datetimepicker.full.js')}}"></script>
{!! Html::style('js/datetimepicker-master/jquery.datetimepicker.css') !!}
<script>
            
    $(document).ready(function() {
        $.datetimepicker.setLocale('en');
        $('#start_date').datetimepicker({
            timepicker:false,
            format:'Y/m/d',
            formatDate:'Y/m/d',
        });
        $('#end_date').datetimepicker({
            timepicker:false,
            format:'Y/m/d',
            formatDate:'Y/m/d',
        });
        
        var customer_id = {{ $user->id }};
        var uuid = localStorage.getItem('uuid');
        var content = $("#text_sms").val();
        var count_sms = $("#total_sms").val();
        var date_start = $("#start_date").val();
        var date_end = $("#end_date").val();
        var sender_id = $('#sender_id').val();
        var sms_type = localStorage.getItem('sms_type');
        
        $("#save").click(function(){
            $(".error-box").html("");
            localStorage.setItem('package',$("#packages").val());
            localStorage.setItem('amount',$("#total_sms").val());
            
            var total_price = parseFloat($("#packages :selected").data('price')) * parseFloat($("#total_sms").val());
             localStorage.setItem('total_price', total_price);
 
            if(date_start == null && date_end == null)
            {
                date_start = new Date();
                date_end = new Date();
                localStorage.setItem('date_start', date_start);
                localStorage.setItem('date_end', date_end);
            }

            if(content == null)
            {
                $("#li-sms").addClass("li-error");
                $(".error-box").append("<p>Isi SMS harus diisi.</p>");
            }
            else if(count_sms == null)
            {
                $("#li-quantity").addClass("li-error");
                $(".error-box").append("<p>Jumlah SMS harus diisi.</p>");
            }
            else if(sender_id == null)
            {
                $("#li-sender").addClass("li-error");
                $(".error-box").append("<p>Sender ID harus dipilih.</p>");
            }
            else
            {
                window.location = "{{ route('dashboard-sms-blast-confirmation') }}";
            }
        });

        if(content != null)
            $("#sms_preview").text(content);
    });
</script>
@endsection
