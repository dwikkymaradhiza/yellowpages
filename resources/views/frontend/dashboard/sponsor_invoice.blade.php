@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Konfirmasi Pemasangan Iklan</h2>

    <p>Berikut adalah ringkasan pembelian iklan Anda. Mohon periksa terlebih dahulu sebelum melanjutkan.</p>
    
    <div class="row rowKontenProduk2 ">
        <div class="table-responsive">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->

            <div class="promote-main-content">
            <ul class="prd-ls">
                <li>
                    <div class="col-xs-8">
                        <div class="row">Biaya Iklan</div>
                    </div>
                    <div class="col-xs-4">
                        <div class="row text-right promote-credit"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-8">
                        <div class="row">Saldo Iklan YP</div>
                    </div>
                    <div class="col-xs-4">
                        <div class="row text-right promote-saldo" data-saldo="{{$saldo}}">Rp {{$saldo}}</div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-8">
                        <div class="row">TOTAL</div>
                    </div>
                    <div class="col-xs-4">
                        <div class="row text-right insufficient"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                
                <div class="insufficient-warning">
                    <p>Maaf, saldo Anda tidak mencukupi untuk melakukan transaksi ini.</p>
                    <p>Silakan ketuk tombol LANJUTKAN untuk melakukan topup saldo iklan Anda terlebih dahulu.</p>
                </div>
            </ul>
        </div> 
        </div>
        <div class="col-md-6">
            
        </div>
        <div class="col-md-6 pull-right">
            <div class="btn-produk">
                <div class="pull-right">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                    <button class="btn btn-yellow btn-bisnis" onclick="goToSponsorPackage();return false;">Kembali</button>
                    <button class="btn btn-yellow btn-bisnis" onclick="createPackagesOrder();return false;">Lanjutkan</button>
                
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }
</style>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script>
    var insufficient;
    
    $(document).on('ready',function(){
        var package_price = localStorage.getItem('package_price');
        if(typeof package_price !== 'undefined'){
            $('.promote-credit').html('Rp '+ package_price);
        }        
            var saldo = $('.promote-saldo').data('saldo');
        
        if(saldo >= package_price){
            insufficient = false;
            $(".insufficient-warning").hide();
        }else{
            insufficient = true;
            $(".insufficient-warning").show();
        }
        
        var total = parseInt(saldo) - parseInt(package_price);
        $(".insufficient").html('Rp ' + total);
    });
    
    function createPackagesOrder(){
        if(insufficient){
            insertPackagesOrder(0);
        }else{
            insertPackagesOrder(1);
        }
    }
    
    function insertPackagesOrder(status){
        var requestData = {
            'price' : localStorage.getItem('package_price'),
            'package_id' : localStorage.getItem('package_id'),
            'status' : status,
            'directories_id' : localStorage.getItem('directory_sponsor'),
            'products_id' : localStorage.getItem('product_sponsor'),
        };
        var urlAPI = "{{route('sponsor-invoice-update')}}";
        if(status === 0){
            var nextUrl = "{{route('topup')}}";
        }else{
            var nextUrl = "{{route('sponsor-thanks')}}"; 
        }
        
        $.ajax({
            headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
            data: requestData,
            url: urlAPI,
            type: 'post',
            beforeSend: function () {
                //beginTransition();
            },
            complete: function () {
                //endLoadingMapTransition();
            },
            success: function (data) {
                var result = JSON.parse(data);
                localStorage.setItem('order_id', result.order_id);
                
                if(result.message ==='success'){
                    localStorage.setItem('order_id', result.order_id);
                    //clear all local storage
                    localStorage.clear();
                    window.location = nextUrl;
                }else{
                    localStorage.setItem('order_id', result.order_id);
                    window.location = nextUrl;
                }                    
            },
            error: function (data){
                error.log('Email not sent');
            }
        });
    }
    
    function goToSponsorPackage(){
        var url = "{{route('sponsor-package')}}"
        window.location = url;
    }
</script>
@endsection
