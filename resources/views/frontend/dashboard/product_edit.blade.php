@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Ubah Produk</h2>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

    <div class="row rowKontenProduk">
        <div class="table-responsive">
            <form id="main-form" class="form-horizontal" action="{{ route('update_product', ['id' => $product->id]) }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input id="product_id" name="product_id" type="hidden">
                <fieldset>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Nama Produk</label>
                            <input type="text" required id="name" class="form-control" name="name" value="{{ $product->name }}">
                        </div>
                        <div class="col-md-6">
                            <label for="firstName">Stock </label>
                            <input type="text" id="stock" class="form-control" name="stock" value="{{ $product->stock }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="firstName">Tambah Gambar </label>
                            <input type="hidden" id="picture" name="picture">
                            <input type="file" class="form-control" multiple name="image[]" />
                            <span>Max size: 2MB</span>
                            <br>
                            <div id="pic"></div>
                            <br>
                            <label for="firstName">Bisnis </label>
                            <select required name="directory_id" id="directory_id" class="form-control">
                                @foreach($directoriesList as $directoriesListDetail)
                                    @if($product->directory_id == $directoriesListDetail->id)
                                        <option selected="selected" value="{{$directoriesListDetail->id}}">{{$directoriesListDetail->name}}</option>
                                    @else
                                        <option value="{{$directoriesListDetail->id}}">{{$directoriesListDetail->name}}</option>
                                    @endif
                                @endforeach
                            </select>    
                        </div>
                        <div class="col-md-6">
                            <label>Deskripsi Produk</label>
                            <textarea required class="form-control" id="description" style="height: 280px;" name="description">{{ $product->description }}</textarea>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group row">

                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        </div>
                    </div>
                    <hr/>
                    <p class="text-center">
                        <button type="submit" class="btn btn-yellow btn-simpan">SIMPAN</button>
                    </p>
                </fieldset>
            </form>
        </div>
        <div class="table-responsive">
            <table class="table-fill">
                <thead>
                    <tr>
                        <th class="text-center">Gambar</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @if(count($pictures) > 0)
                        @foreach($pictures as $k => $v)
                        <tr>
                            <td class="text-center" width="80%"><img width="30%" src="{{ URL('/') . '/uploads/products/' . $v->picture }}" /></td>
                            <td class="text-center">
                                <div class="col-md-4" style="padding: 0px">
                                    <form method="POST" action="{{ route('dashboard-product-picture-delete', ['id' => $v->id]) }}">
                                        {{ csrf_field() }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        <button onclick="return window.confirm('Apakah anda ingin menghapus gambar ini?')" class="btn-yellow btn-hapus" style="border:none">
                                            <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" title="Hapus" data-placement="right"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="2" style="text-align:center">Belum ada gambar</td>
                        </tr>
                    @endif
                </tbody>
            </table>   
        </div>
    </div>
</div>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script>
function showModal() {
    $('#myModal--effect-zoomOutIn').modal('show');
    return false;
}

$(document).ready(function () {
    $("#main-form").validate();
});

</script>
@endsection
