@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Produk Bisnis Anda</h2>
    <div class="row rowKontenProduk2 ">
        <div class="table-responsive">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->
            <table class="table-fill">
                <thead>
                    <tr>
                        <th class="text-center"><input type="checkbox" id="check-all" /></th>
                        <th class="text-center">Nama Bisnis</th>
                        <th class="text-center">Kategori</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center">Produk</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @foreach($directories as $k => $v)
                    <tr>
                        <td class="text-center"><input type="checkbox" class="row-check" name="directory-cb" data-id="{{ $v->id }}"/></td>
                        <td class="text-left">{{ $v->name }}</td>
                        <td class="text-left">{{ str_limit($v->categories->pluck('name_lid')->implode(', '), 20) }}</td>
                        <td class="text-left">{{ str_limit($v->address_1, 20) }}</td>
                        <td class="text-center"><a href="{{ route('dashboard-product-index') }}">{{ count($v->products) }}</a></td>
                        <td class="text-center">
                            <div class="">
                                <div class="col-md-3" style="padding:5px 0px;">
                                    <a href="{{ route('dashboard-directory-edit', ['id' => $v->id]) }}"                                              
                                            class="btn-yellow btn-edit" style="border:none">    
                                        <i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit" data-placement="right"></i>
                                    </a>
                                </div>
                                <div class="col-md-3" style="padding: 0px">
                                    @if($total_dir == 1)
                                        <button onclick="return alert('Anda harus mempunyai minimal 1 bisnis')" class="btn-yellow btn-hapus" style="border:none">
                                            <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" title="Hapus" data-placement="right"></i>
                                        </button>
                                    @else
                                    <form method="POST" action="{{ route('dashboard-directory-delete', ['id' => $v->id]) }}">
                                        {{ csrf_field() }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        <button onclick="return window.confirm('Apakah anda ingin menghapus data ini?')" class="btn-yellow btn-hapus" style="border:none">
                                            <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" title="Hapus" data-placement="right"></i>
                                        </button>
                                    </form>
                                    @endif
                                </div>
                                <div class="col-md-3" style="padding:5px 0px;">
                                    <a href="{{ route('directory', $v->slug) }}" target="_blank" class=" btn-yellow btn-edit"> <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i></a>
                                </div>
                                <div class="col-md-3" style="padding:5px 0px;">
                                    <a href="{{ route('verify-form', $v->slug) }}" class=" btn-yellow btn-edit">
                                        <i class="mdi mdi-briefcase-check" aria-hidden="true"></i></a>
                                </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>   
        </div>
        <div class="col-md-6">
            {{ $directories->appends(['size' => $size])->links() }}
        </div>
        <div class="col-md-6 pull-right">
            <div class="btn-produk">
                <div class="pull-right">
                    <form class="form-horizontal" id="page-rows-form">
                        <label class="pull-left control-label">Show</label>
                        <div class="pull-left">
                            <select id="pagesize" class="form-control">
                                <option value="5" {!! $size && $size === 5 ? "selected='selected'" : ""  !!}>5</option>
                                <option value="10" {!! $size && $size === 10 ? "selected='selected'" : ""  !!}>10</option>
                                <option value="15" {!! $size && $size === 15 ? "selected='selected'" : ""  !!}>15</option>
                                <option value="20" {!! $size && $size === 20 ? "selected='selected'" : ""  !!}>20</option>
                                <option value="25" {!! $size && $size === 25 ? "selected='selected'" : ""  !!}>25</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                <button type="button" class="btn btn-yellow btn-bisnis" data-toggle="modal" data-target="#myModal--effect-zoomOutIn">TAMBAH BISNIS</button>
                <button type="button" id="delete-bulk-btn" class="btn btn-yellow btn-bisnis">HAPUS BISNIS</button>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }

    .pac-container {
        z-index: 1051 !important;
    }
</style>
<div class="modal fade" id="myModal--effect-zoomOutIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">TAMBAH BISNIS</h4>
            </div>
            <div class="modal-body">
                <div class="row rowKontenProduk">
                    <form class="form-horizontal" method="POST" action="{{ route('dashboard-directory-create') }}" id="directoryForm" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="latlng" id="latlng" />
                        <fieldset>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Nama Bisnis</label>
                                    <input type="text" id="name" name="name" value="{{ old('name') }}" required class="form-control">
                                </div>
                                <div class="col-md-6 no-label">
                                    <label>Kategori</label>
                                    <input name="categories" id="categories" value="{{ old('categories') }}" type="hidden"/>
                                    <select required class="form-control category-directory" id="categories-id" multiple>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="firstName">Nomor Telepon/Mobile </label>
                                    <input required id="phone" name="phone" value="{{ old('phone') }}" type="text" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label for="firstName">Gambar <span style="font-size:.8em;">(max: 2MB)</span></label>
                                    <input type="file"multiple id="image" class="form-control" name="image[]" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="firstName">Gambar Utama <span style="font-size:.8em;">(max: 2MB)</span></label>
                                    <input type="file" id="image" class="form-control" name="logo" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Deskripsi Bisnis</label>
                                    <textarea required id="description" id="description" name="description" class="form-control">{{ old('description') }}</textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="firstName">Alamat </label>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <select required class="form-control auto-province" name="province_id">
                                                <option value="">Provinsi</option>
                                                @foreach($provinces as $province)<option value="{{$province['id']}}">{{$province['name']}}</option>@endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <select required class="form-control auto-city" id="city_id" name="city_id">
                                                <option value="">Kota</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="position:relative">
                                            <input  required placeholder="Enter a location" type="text" id="pac-input" name="address" value="{{ old('address') }}" class="form-control">
                                            <div id="map" style="height:300px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <p class="text-center">
                                <button type="submit" class="btn btn-yellow btn-simpan">SIMPAN</button>
                            </p>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 


@endsection

@section('script-content')
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GMAPS_KEY') }}&libraries=places&callback=initAutocomplete&language=id" async defer></script>
<script src="{{ asset('js/messages_id.js') }}"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript">
    var map;
    let pageSize = document.getElementById("pagesize");
    pageSize.onchange = function () {
        let value = this.value;
        let reloadUri = '{{ route("dashboard-directory-index") }}';
        window.location = reloadUri + "?size=" + value;
    };
    
    $("#delete-bulk-btn").on('click', function () {
        var reply = confirm('Apakah anda akan menghapus semua data ini?')
        if (reply === false) {
            return false;
        }
        var id = $("input[name='directory-cb']:checked").map(function () {
            return $(this).data("id");
        }).get().join(',');

        if (id === '') {
            alert('Please check the product');
            return false;

        }

        var requestData = {
            'id': id,
        };
        var urlAPI = "{{route('dashboard-directory-delete-bulk')}}";
        $.ajax({
            headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
            data: requestData,
            url: urlAPI,
            type: 'post',
            beforeSend: function () {
                //beginTransition();
            },
            complete: function () {
                //endLoadingMapTransition();
            },
            success: function (data) {
                if (data === 'success') {
                    location.reload();
                } else {
                    alert('failed');
                }

            }
        });
    });

    
    $(document).ready(function () {
        $("#directoryForm").validate();
        $(".auto-category, .auto-province, .auto-city").select2();
        
        $('.auto-city').on('select2:select', function(e) {
            $("#city_id").val($('.auto-city').val());
        });
        
        $('.auto-province').on('select2:select', function(e) {
            $.ajax({
                url: '/ajax/city/' + $('.auto-province').val() + '/option',
                dataType: 'json',
                success: function(d) {
                    $('.auto-city').select2('destroy').html(d.data).select2()
                }
            });
        });
        
        $(".category-directory").on("select2:select select2:unselect", function (e) {
            //this returns all the selected item
            var items = $(this).val();
            $("#categories").val(items.join(','));
        });
        
        $('#myModal--effect-zoomOutIn').on('show.bs.modal', function (e) {
            //clear all error
            $(".error").html('');
            $(".form-control").removeClass('error');
            
            //get data-id attribute of the clicked element
            var id_update = $(e.relatedTarget).data('id');
            $('.error').html('');
            
            $("#myModalLabel").html('TAMBAH BISNIS');
            $("#image-display").html("");
        });
    });
    
    $(document).on('change', 'table thead [type="checkbox"]', function (e) {
        e && e.preventDefault();
        var $table = $(e.target).closest('table'), $checked = $(e.target).is(':checked');
        $('tbody [type="checkbox"]', $table).prop('checked', $checked);
        $("#btn-del-reports").toggle();
    });
    
    var urlApiCategory = "{{route('directory-categories')}}";
$(".category-directory").select2({
  ajax: {
    url: function (params) {
      return urlApiCategory + '/' + params.term;
    },
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        q: params.term, // search term
        page: params.page
      };
    },
    processResults: function (data) {
        return {
                results: $.map(data.items, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                })
            };
    },
    cache: true
  },
  minimumInputLength: 1,
});

    function initAutocomplete() {
        function placeMarker(location) {
            $('#latlng').val(location.lat() + ';' + location.lng());
            markers.push(new google.maps.Marker({
                position: location, 
                map: map
            }));
        }
        
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -6.1753924, lng: 106.8249587},
            zoom: 13,
            mapTypeId: 'roadmap',
            mapTypeControl: false
        });
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };
                // Create a marker for each place.
                $('#latlng').val(place.geometry.location.lat() + ';' + place.geometry.location.lng());
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
            
            google.maps.event.addListener(map, 'click', function(event){
                markers.forEach(function(marker) {
                  marker.setMap(null);
                });
                markers = [];
                placeMarker(event.latLng);
            });
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcE2BDUnr6CkeHkVnUZcayxGqdACDTgWw&libraries=places&callback=initAutocomplete" async defer></script>
@endsection
