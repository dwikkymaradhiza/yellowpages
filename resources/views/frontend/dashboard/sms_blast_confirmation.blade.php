@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Konfirmasi SMS <font name="sms_type_label"></font></h2>
    <p>Berikut rincian SMS <font name="sms_type_label"></font> dan biaya yang harus Anda bayarkan.</p>

     @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    
    <div class="row rowKontenProduk2 ">    
        <div class="container insuffficient-container">
            <div class="col-md-4 box_1">
                <div class='row'>
                    <div class='col-md-6'>
                        <b>Kredit YP anda</b>
                    </div>
                    <div class='col-md-6' id='saldo' data-saldo="{{$saldo}}">Rp {{number_format($saldo,'2','.',',')}}
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class='row'>
                    <div class='col-md-6'>
                        <b>Biaya SMS</b>
                    </div>
                    <div class='col-md-6 biaya_sms' id='biaya_sms'>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class='row'>
                    <div class='col-md-6'>
                        <b>TOTAL</b>
                    </div>
                    <div class='col-md-6 total' id='total'>
                    </div>
                </div>
                
                
                <div class='row insufficient-warning-desktop' >
                    <div class='col-md-12'>
                        <p>
                            Maaf, saldo YP Anda tidak mencukupi untuk mengirim SMS blast ini.
                            <br>
                            Silakan melakukan pengisian kredit untuk bisa melanjutkannya.
                        </p>
                    </div>                    
                </div>
                
            </div>

            <div class="col-md-5 box_1">
                <div class='row'>
                    <div class='col-md-10'>
                        <b>Paket Kredit</b>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-10'>
                        <select name="nominal_payment" class="form-control" id="nominal_payment">
                            <option value="100000">IDR 100,000</option>
                            <option value="500000">IDR 500,000</option>
                            <option value="1000000">IDR 1,000,000</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class='row'>
                    <div class='col-md-10'>
                        <b>Jumlah Paket</b>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-10'>
                        <input type="text" required id="total_packages" class="form-control" name="total_packages" value="1">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class='row'>
                    <div class='col-md-10'>
                        <b>Metode Pembayaran</b>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-10'>
                        <select name="payment_method" class="form-control" id="payment_method">
                            <option value='CC'>Credit Card</option>
                            <option value='BT'>Bank Transfer</option>
                            <option value='IB'>Internet Banking</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container suffficient-container" align="center">

            <div class="col-md-4 box_1">
                <div class='row'>
                    <div class='col-md-6'>
                        <b>Kredit YP anda</b>
                    </div>
                    <div class='col-md-6' id='saldo' data-saldo="{{$saldo}}">Rp {{number_format($saldo,'2','.',',')}}
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class='row'>
                    <div class='col-md-6'>
                        <b>Biaya SMS</b>
                    </div>
                    <div class='col-md-6 biaya_sms' id='biaya_sms'>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class='row'>
                    <div class='col-md-6'>
                        <b>TOTAL</b>
                    </div>
                    <div class='col-md-6 total' id='total'>
                    </div>
                </div>
                
            </div>

        </div>
            <div class="col-xs-12 error-box">
                
            </div> 
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                <button onclick="javascript: history.go(-1)" class="btn btn-yellow btn-bisnis">Kembali</button>
                <button id="create_campaign" class="btn btn-yellow btn-bisnis" onclick="return false;">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }
</style>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script src="{{ asset('js/datetimepicker-master/build/jquery.datetimepicker.full.js')}}"></script>
{!! Html::style('js/datetimepicker-master/jquery.datetimepicker.css') !!}
<script>
    var insufficient;        
    $(document).ready(function() {
        var total_price = localStorage.getItem('total_price');
        
        $(".biaya_sms").html('Rp ' + parseFloat(total_price, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
        
        var total_saldo = parseFloat($("#saldo").data('saldo')) - parseFloat(total_price);
        
        $(".total").html('Rp ' + parseFloat(total_saldo, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());

        if(total_saldo < 0){
            insufficient = true;
            $(".insuffficient-container").show();
        }else{
            $(".suffficient-container").show();
            insufficient = false;
        }
        
        var sms_type = localStorage.getItem('sms_type');
        
        var uuid = localStorage.getItem('uuid');
        var content = localStorage.getItem('content');
        var count_sms = localStorage.getItem('count_sms');
        var total_price = localStorage.getItem('total_price');
        var date_start = localStorage.getItem('date_start');
        var date_end = localStorage.getItem('date_end');
        var sender_id = localStorage.getItem('sender_id');
        
        var sms_type = localStorage.getItem('sms_type');
        
        if(sms_type == "BLAST")
        {
            $( "#create_campaign" ).click(function() {
                if(saldo_akhir >= 0)
                {
                    var requestData = {
                        data : {
                            "uuid" : uuid,
                            "content" : content,
                            "quantity": count_sms,
                            "period": {
                                "start": date_start,
                                "end": date_end
                            },
                            "sender_id": {
                                "id" : sender_id,
                                "is_new" : 0
                            }
                        },
                        price : total_price
                    };
                    
                    var urlAPI = "{{route('dashboard-sms-create-campaign')}}";
                    
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: requestData,
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });

//                    console.log(localStorage.getItem('count_package'));

                    var requestData = [];
                    var newData = { "id" : localStorage.getItem('package'), "quantity" : localStorage.getItem('amount') };
                    requestData.push(newData);
                    
//                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
//                        console.log(i);
//                        if(localStorage.getItem('amount' + i) > 0)
//                        {
//                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
//                            requestData.push(newData);
//                        }
//                    }

//                    console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 1, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/sms/blast/thankyou";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                }
                else
                {
                    console.log(localStorage.getItem('count_package'));

                     var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 0, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/topup";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                    
                }
            });
        }
        else if(sms_type == "TARGETED")
        {
            $( "#create_campaign" ).click(function() {
                if(saldo_akhir >= 0)
                {
                    var requestData = {
                        data : {
                            "customer_id" : {{ $user->id }},
                            "customer_name" : "{{ $user->first_name." ".$user->last_name}}",
                            "uuid" : uuid,
                            "content" : content,
                            "quantity": count_sms,
                            "period": {
                                "start": date_start,
                                "end": date_end
                            },
                            "sender_id": {
                                "id" : sender_id,
                                "is_new" : 0
                            },
                            "client_id" : 83,
                            "type" : "segmented",
                            "profiles" : profiles,
                            "locations" : location,
                            "providers" : provider
                        },
                        price : total_price
                    };
                    
                    var urlAPI = "{{route('dashboard-sms-targeted-create-campaign')}}";
                    
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: requestData,
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                            
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });

                    console.log(localStorage.getItem('count_package'));

                    var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 1, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/sms/blast/thankyou";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                }
                else
                {
                    console.log(localStorage.getItem('count_package'));

                     var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 0, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/topup";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                    
                }
            });
        }
        if(sms_type == "CALL_HISTORY")
        {
            $( "#create_campaign" ).click(function() {
                if(saldo_akhir >= 0)
                {
                    var requestData = {
                        data : {
                            "customer_id" : {{ $user->id }},
                            "customer_name" : "{{ $user->first_name." ".$user->last_name}}",
                            "uuid" : uuid,
                            "content" : content,
                            "quantity": count_sms,
                            "period": {
                                "start": date_start,
                                "end": date_end
                            },
                            "sender_id": {
                                "id" : sender_id,
                                "is_new" : 0
                            },
                            "client_id" : 83,
                            "type" : "call_pattern",
                            "profiles" : profiles,
                            "locations" : location,
                            "providers" : provider,
                            "patterns" : pattern,
                            "call_history" : {}
                        },
                        price : total_price
                    };
                    
                    var urlAPI = "{{route('dashboard-sms-call-history-create-campaign')}}";
                    
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: requestData,
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });

                    //console.log(localStorage.getItem('count_package'));

                    var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    //console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 1, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/sms/blast/thankyou";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                }
                else
                {
                    console.log(localStorage.getItem('count_package'));

                     var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    //console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 0, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/topup";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                    
                }
            });
        }
    });
</script>
@endsection
