@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Ubah Direktori</h2>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="row rowKontenProduk ">
        <div class="table-responsive">
            <form class="form-horizontal" method="POST" action="{{ route('dashboard-directory-update', ['id' => $directory->id]) }}" id="directoryForm" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="latlng" id="latlng" value="{{ $directory->latitude }};{{ $directory->longitude }}" />
                <input type="hidden" id="id" name="id" value="{{ $directory->id }}" />
                <fieldset>
                    <input type="hidden" id="id" name="id">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Nama Bisnis</label>
                            <input type="text" id="name" name="name" value="{{ $directory->name }}" required class="form-control">
                        </div>
                        <div class="col-md-6 no-label">
                            <label>Kategori</label>
                            <input name="categories" id="categories" value="{{ $directory->categories->implode('id', ",") }}" type="hidden"/>
                            <select required class="form-control category-directory" id="categories-id" multiple>
                                @foreach($categories as $ck => $cv)<option value="{{ $cv->id }}">{{ $cv->name_lid }}</option>@endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="firstName">Nomor Telepon/Mobile </label>
                            <input required id="phone" name="phone" value="{{ $directory->phone_1 }}" type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="firstName">Tambah Gambar <span style="font-size:.8em;">(max: 2MB)</span></label>
                            <div id="image-display"></div>
                            <input type="file"multiple id="image" class="form-control" name="image[]" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="firstName">Ubah Gambar Utama <span style="font-size:.8em;">(max: 2MB)</span></label>
                            <div id="image-display"></div>
                            <input type="file" id="image" class="form-control" name="logo" />
                        </div>
                        <div class="col-md-6">
                            <img src="{{ URL::to('/') . "/uploads/directories/thumb_" . $directory->logo }}" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Deskripsi Bisnis</label>
                            <textarea required id="description" id="description" name="description" class="form-control">{{ $directory->description_lid }}</textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="firstName">Alamat </label>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <select required class="form-control auto-province" name="province_id">
                                        <option value="">Provinsi</option>
                                        @foreach($provinces as $province)<option value="{{$province['id']}}">{{$province['name']}}</option>@endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select required class="form-control auto-city" id="city_id" name="city_id">
                                        <option value="">Kota</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="position:relative">
                                    <input  required placeholder="Enter a location" type="text" id="pac-input" name="address" class="form-control" value="{{ $directory->address_1 }}">
                                    <div id="map" style="height:300px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <p class="text-center">
                        <button type="submit" class="btn btn-yellow btn-simpan">SIMPAN</button>
                    </p>
                </fieldset>
            </form>
        </div>
        <div class="table-responsive">
            <table class="table-fill">
                <thead>
                    <tr>
                        <th class="text-center">Gambar</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @if(count($pictures) > 0)
                        @foreach($pictures as $k => $v)
                        <tr>
                            <td class="text-center" width="80%"><img width="30%" src="{{ URL('/') . '/uploads/directories/' . $v->picture_file }}" /></td>
                            <td class="text-center">
                                <div class="col-md-4" style="padding: 0px">
                                    <form method="POST" action="{{ route('dashboard-directory-picture-delete', ['id' => $v->id]) }}">
                                        {{ csrf_field() }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        <button onclick="return window.confirm('Apakah anda ingin menghapus gambar ini?')" class="btn-yellow btn-hapus" style="border:none">
                                            <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" title="Hapus" data-placement="right"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="2" style="text-align:center">Belum ada gambar</td>
                        </tr>
                    @endif
                </tbody>
            </table>   
        </div>
    </div>
</div>
<div class="clear"></div>
<!-- modal -->
<style>
    .pac-container {
        z-index: 1051 !important;
    }
</style>
@endsection

@section('script-content')
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<script src="{{ asset('js/messages_id.js') }}"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript">
    var map;
    
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $("#directoryForm").validate();
        
        var urlApiCategory = "{{route('directory-categories')}}";
        $(".category-directory").select2({
        ajax: {
          url: function (params) {
            return urlApiCategory + '/' + params.term;
          },
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
          },
          processResults: function (data) {
              return {
                      results: $.map(data.items, function (item) {
                          return {
                              text: item.name,
                              id: item.id
                          }
                      })
                  };
          },
          cache: true
        },
        minimumInputLength: 1,
      });
        $(".auto-category, .auto-province, .auto-city").select2();
        
        $('.auto-city').on('select2:select', function(e) {
            $("#city_id").val($('.auto-city').val());
        });
        
        $('.auto-province').on('select2:select', function(e) {
            $.ajax({
                url: '/ajax/city/' + $('.auto-province').val() + '/option',
                dataType: 'json',
                success: function(d) {
                    $('.auto-city').select2('destroy').html(d.data).select2();
                }
            });
        });
        
        //Set default city
        $.ajax({
            url: '/ajax/city/' + '{{ $directory->city->province_id }}' + '/option',
            dataType: 'json',
            success: function(d) {
                $('.auto-city').select2('destroy').html(d.data).select2();
                $('.auto-city').val('{{ $directory->city_id }}').trigger("change");
            }
        });
        
        //Set default province
        $('.auto-province').val('{{ $directory->city->province_id }}').trigger("change");
        
        //Set default categories
        var categoriesValue = '{{ $directory->categories->implode("id",",") }}';
        try {
            var selectedValues = categoriesValue.split(',');
        } catch(e) {
            var selectedValues = "" + categoriesValue + "";
        }
        
        $("#categories-id").val(selectedValues).trigger("change");
        
        $(".category-directory").on("select2:select select2:unselect", function (e) {
            //this returns all the selected item
            var items = $(this).val();
            $("#categories").val(items.join(','));
        });
    });
    
    function initAutocomplete() {
        function placeMarker(location) {
            $('#latlng').val(location.lat() + ';' + location.lng());
            markers.push(new google.maps.Marker({
                position: location, 
                map: map
            }));
        }
        
        //Set default map point
        var latlngVal = $("#latlng").val().split(';');
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(latlngVal[0], latlngVal[1]),
            zoom: 13,
            mapTypeId: 'roadmap',
            mapTypeControl: false
        });
        
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(latlngVal[0], latlngVal[1]),
            map: map,
            title: '{{ $directory->address_1 }}'
        });
        
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };
                // Create a marker for each place.
                $('#latlng').val(place.geometry.location.lat() + ';' + place.geometry.location.lng());
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
            
            google.maps.event.addListener(map, 'click', function(event){
                markers.forEach(function(marker) {
                  marker.setMap(null);
                });
                markers = [];
                placeMarker(event.latLng);
            });
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GMAPS_KEY') }}&libraries=places&callback=initAutocomplete&language=id" async defer></script>
@endsection
