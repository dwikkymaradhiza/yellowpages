@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Terimakasih telah mendaftar Paket <span id='membership'></span>.</h2>
        <div class="row paket">
            <p>Silahkan melakukan pembayaran melalui transfer bank berikut.</p>
            <div class="ket">
                <div class="col-md-6 bank text-center">
                    <img src="{{asset('img/mandiri_logo.png')}}" class="img-responsive"><br/>
                    <p>PT. Metra Digital Media</p>
                    <strong>127 0000 990026</strong><br/>
                    <strong>Bank Mandiri Cabang Cipete</strong>
                </div>
                <div class="col-md-6 bank text-center">
                    <img src="{{asset('img/bca_logo.png')}}" class="img-responsive"><br/>
                    <p>PT. Metra Digital Media</p>
                    <strong>218 549 9900</strong><br/>
                    <strong>Bank BCA Cabang Cipete</strong>
                </div>
            </div>
            <div class="clearfix"></div>
            <p class="text-center">
                <a href="{{route('confirmation-page')}}"><button type="button" class="btn btn-yellow btn-simpan">LANJUTKAN</button></a>
            </p>
        </div>
    </div>


@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<script src="{{ asset('js/script.min') }}"></script>
<script>
    var membership_id = localStorage.getItem('membership_id');
    var membership_name = localStorage.getItem('membership_name');
    var membership_price = localStorage.getItem('membership_price');
    
    $("#membership").text(membership_name.toUpperCase());
    
    let pageSize = document.getElementById("pagesize");
    pageSize.onchange = function () {
        let value = this.value;
        let reloadUri = '{{ route("dashboard-inquiry-index") }}';

        window.location = reloadUri + "?size=" + value;
    };
    
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });


</script>
@endsection