@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Penerima SMS</h2>
    <p>Tentukan penerima SMS Targeted dengan mengisi informasi di bawah</p>

    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="row rowKontenProduk2">
        <div class='col-md-6 m-maincontent' style='margin-top:40px; '>
            <h2 style="text-align:left">Operator</h2>
            <input style="width:100%" class="ac-operators" name="operators-ac" type="text" />
            <input id="operators" name="operators" type="hidden" />
        </div>
        <div class='col-md-6 m-maincontent' style='margin-top:40px; '>
            <h2 style="text-align:left">Lokasi</h2>
            <input style="width:100%" class="ac-locations" name="locations-ac" type="text" />
            <input id="locations" name="locations" type="hidden" />
        </div>
        <div class='col-md-6 m-maincontent' style='margin-top:40px; '>
            <h2 style="text-align:left">Minat</h2>
            <input style="width:100%" class="ac-interests" name="interests-ac" type="text" />
            <input id="interests" name="interests" type="hidden" />
        </div>
        <div style="margin-top:50px" class="col-md-12 text-center">
            <div class="btn-produk">
                <button onclick="javascript: history.go(-1)" class="btn btn-yellow btn-bisnis">Kembali</button>
                <button id="sms-button-post" class="btn btn-yellow btn-bisnis">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    .blur
    {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(1,1,1,0.5);
    }
    .loader {
        position: absolute;
        top: 43%;
        left: 40%;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 80px;
        height: 80px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        z-index: 20;
    }

    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
</style>
<script>
    $(document).ready(function() {
        var count_phone_number = 0;
        localStorage.setItem('count_phone_number', count_phone_number);
        localStorage.setItem('uuid', "{{ $uuid }}");
        localStorage.setItem('sms_type', "TARGETED");

        var segment = [];
        var location = [];
        var provider = [];
        
        localStorage.setItem('profiles', JSON.stringify(segment));
        
        $("#save").on('click', function() {
            $(".error-box").html("");
            localStorage.setItem('provider', JSON.stringify(provider));
            provider = JSON.parse(localStorage.getItem('provider'));

            localStorage.setItem('location', JSON.stringify(location));
            location = JSON.parse(localStorage.getItem('location'));

            localStorage.setItem('segment', JSON.stringify(segment));
            segment = JSON.parse(localStorage.getItem('segment'));
            
            if(segment.length < 1)
            {
                $("#li-interest").addClass("li-error");
                $(".error-box").append("<p>Minat Penerima harus diisi.</p>");
            }
            else if(location.length < 1)
            {
                $("#li-location").addClass("li-error");
                $(".error-box").append("<p>Lokasi Penerima harus diisi.</p>");
            }
            else if(provider.length < 1)
            {
                $("#li-operator").addClass("li-error");
                $(".error-box").append("<p>Operator Telekomunikasi harus dipilih.</p>");
            }
            else
            {
                window.location = "{{ route('dashboard-sms-targeted-setting') }}";
            }
        });
        
        var segment_lists = {!! $interests !!};
        console.log(segment_lists);
        $( ".ac-interests" ).autocomplete({
            minLength: 0,
            delay: 200,
            source: JSON.parse(segment_lists),
            focus: function( event, ui ) {
                $( ".ac-interests" ).val( ui.item.name);
                return false;
            },
            select: function( event, ui ) {
                var terms = split( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( ", " );
                
//                $( ".ac-interests" ).val( ui.item.name);
//                $( "#interests" ).val( ui.item.id);
                
                return false;
            },
            search: function(event, ui) { console.log(event); console.log(ui); }
        })
        .data( "uiAutocomplete" )._renderItem = function( ul, item ) {
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + item.name+"</a>" )
                .appendTo( ul );
        };
    });
</script>
@endsection
