@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <h2>Selamat Datang, {{ Auth::user()->first_name }}.</h2>
    
    <div class="m-maincontent" style="margin: 50px 0px;">
        <h3 class="text-uppercase text-center m-judulmain">BERIKLAN SEKARANG</h3>
        <div class="col-xs-12" style="margin-top:20px;">
            <div class="block-promote-btn">
                <a href="{{route('sponsor-business')}}">
                    <div class="col-xs-4" align='center'>
                        <div class="promote-btn">
                            <i class="mdi mdi-briefcase-check fa-3x text-center"></i></div>
                        <span class="promote-btn-text">SPONSORED BUSINESS</span>
                    </div>
                </a>
                <a href="#" class="disabled">
                    <div class="col-xs-4" align='center'>
                        <div class="promote-btn"><i class="mdi mdi-google fa-3x text-center"></i></div>
                        <span class="promote-btn-text">GOOGLE ADWORDS</span>
                    </div>
                </a>
                <a href="{{route('dashboard-sms-index')}}">
                    <div class="col-xs-4" align='center'>
                        <div class="promote-btn"><i class="mdi mdi-message-reply-text fa-3x text-center"></i></div>
                        <span class="promote-btn-text">SMS TARGETED & BLAST</span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
     
    
    
    
    <div class="row rowKontenProduk3">
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
</div>
<div class="clear"></div>
<div class="jumbotron2">
    <div class="container">
        <form class="form-horizontal" action="{{ route('dashboard-status-create') }}" method="POST">
            {{ csrf_field() }}
            <fieldset>
                <div class="col-md-6">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label>TULIS STATUS</label>
                            <select class="form-control" name="directory_id">
                                @foreach($directories as $dk => $dv)
                                <option value="{{ $dv->id }}">{{ $dv->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label></label>
                            <textarea class="form-control" style="height: 280px;" name="message"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <p class="text-right">
                                <button type="submit" class="btn btn-yellow btn-dash">KIRIM</button>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="listfooter">
                        @if(count($last_activity) > 0)
                        <h2>INFORMASI TERBARU</h2>
                        @endif
                        <div id="layanan">
                            <ul class="list tick">
                                @foreach($last_activity as $k => $last_activity_detail)
                                <li>
                                    <div class="title">
                                        <a href="#">{{$last_activity_detail['activity']}}</a><span class="date" style="float:right">{{\Carbon\Carbon::parse($last_activity_detail['time'])->diffForHumans()}}</span>
                                    </div>
                                </li>  
                                <li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="clear"></div>
@endsection

@section('script-content')
<script src="{{ asset('js/script.min.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
var chartInfo; // global
function requestData() {
    $.ajax({
        url: '/ajax/info_counter',
        method: 'GET',
        dataType: 'JSON',
        success: function (e) {
            // add the point
            chartInfo.addSeries({
                name: 'Inquiry',
                data: e.data.inquiry
            });
            chartInfo.addSeries({
                name: 'SMS',
                data: e.data.sms
            });
        },
        cache: false
    });
}

$(document).ready(function () {

    chartInfo = new Highcharts.chart({
        colors: ['#000', '#ffd600'],
        chart: {
            defaultSeriesType: "line",
            renderTo: 'container',
            events: {
                load: requestData
            }
        },
        title: {
            text: 'Informasi Inquiry dan SMS ',
            align: 'left'
        },
        subtitle: {
            text: ' '
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'],
            lineColor: '#e0e0e0',
            gridLineColor: '#fff',
            tickColor: '#fff'
        },
        yAxis: {
            gridLineColor: '#fff',
            tickColor: '#fff',
            title: {
                text: ' ',
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        }
    });

    var date_array = '{!! json_encode($date_array) !!}';
    var total_view = '{!! json_encode($total_view) !!}';
    var total_click = '{!! json_encode($total_click) !!}';
    
    Highcharts.chart('container2', {
        colors: ['#000', '#ffd600'],
        chart: {
            type: 'line'
        },
        title: {
            text: 'Tayangan & Klik ',
            align: 'left'
        },
        subtitle: {
            text: ' '
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: JSON.parse(date_array),
            lineColor: '#e0e0e0',
            gridLineColor: '#fff',
            tickColor: '#fff'
        },
        yAxis: {
            gridLineColor: '#fff',
            tickColor: '#fff',
            title: {
                text: ' ',
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
                name: 'View',
                data: JSON.parse(total_view)
            }, {
                name: 'Click',
                data: JSON.parse(total_click)
            }]
    });
});
</script>
@endsection