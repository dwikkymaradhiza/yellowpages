@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
        
    <div class="m-maincontent">
        <h3 class="text-uppercase text-center m-judulmain">Jenis Paket SMS</h3>
        <p>Silakan pilih jenis paket Targeted SMS yang sesuai dengan promo Anda.</p>
        <div class="col-xs-12">
            <div class="block-promote-btn">
                <a href="{{route('dashboard-sms-blast')}}">
                    <div class="col-xs-4" align='center'>
                        <div class="promote-btn" align='center'>
                            <i class="mdi mdi-bullhorn fa-3x text-center"></i></div>
                        <span class="promote-btn-text">SMS Blast</span>
                    </div>
                </a>
                <a href="{{route('dashboard-sms-targeted')}}">
                    <div class="col-xs-4" align='center'>
                        <div class="promote-btn"><i class="mdi mdi-crosshairs-gps fa-3x text-center"></i></div>
                        <span class="promote-btn-text">SMS Targeted</span>
                    </div>
                </a>
                <a href="{{route('dashboard-sms-call-history')}}">
                    <div class="col-xs-4" align='center'>
                        <div class="promote-btn"><i class="mdi mdi-phone-incoming fa-3x text-center"></i></div>
                        <span class="promote-btn-text">SMS Call History</span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
     
    
    
    
    
</div>
<div class="clear"></div>

@endsection

@section('script-content')
<script src="{{ asset('js/script.min.js') }}"></script>

@endsection