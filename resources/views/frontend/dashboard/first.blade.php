@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <div class="row rowKontenProduk3">
        <h2>Selamat Datang {{ Auth::user()->first_name }}, Silahkan pilih paket keanggotaan anda.</h2>
        <div class="row opsi">
            <div class="col-md-4">
                <div class="opsis">
                    <div class="opsis-judul">
                        <h5>FREE</h5>
                    </div>
                    <div class="opsis-isi free">
                        <ul>
                            <li>Listing hingga 50 produk</li>
                            <li>SMS hingga 30/bulan</li>
                        </ul>
                    </div>
                    <p class="text-center">
                        <button type="submit" class="btn btn-yellow btn-simpan">PILIH</button>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="opsis">
                    <div class="opsis-judul">
                        <h5>STANDAR</h5>
                    </div>
                    <div class="opsis-isi standar">
                        <ul>
                            <li>Listing hingga 500 produk</li>
                            <li>SMS Update</li>
                            <li>Boosting credit 10K</li>
                        </ul>
                    </div>
                    <p class="text-center">
                        <button type="submit" class="btn btn-yellow btn-simpan">PILIH</button>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="opsis">
                    <div class="opsis-judul ">
                        <h5>PREMIUM</h5>
                    </div>
                    <div class="opsis-isi premium"> 
                        <ul>
                            <li>Listing Unlimited</li>
                            <li>SMS Update</li>
                            <li>Boosting credit 10k</li>
                            <li>Boosting di network site</li>
                        </ul>
                    </div>
                    <p class="text-center">
                        <button type="submit" class="btn btn-yellow btn-simpan">PILIH</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
$(function () {
    Highcharts.chart('container', {
        colors: ['#000', '#ffd600'],
        chart: {
            type: 'line'
        },
        title: {
            text: 'Informasi Inquiry dan SMS ',
            align: 'left'
        },
        subtitle: {
            text: ' '
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'],
            lineColor: '#e0e0e0',
            gridLineColor: '#fff',
            tickColor: '#fff'
        },
        yAxis: {
            gridLineColor: '#fff',
            tickColor: '#fff',
            title: {
                text: ' ',
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
                name: 'Inquiry',
                data: [190.0, 85.0, 97.0, 250.0, 97.0, 250.0, 150.0]
            }, {
                name: 'SMS',
                data: [97.0, 30.0, 32.0, 55.0, 265.0, 300.0, 275.0]
            }]
    });

    Highcharts.chart('container2', {
        colors: ['#000', '#ffd600'],
        chart: {
            type: 'line'
        },
        title: {
            text: 'Tayangan & Klik ',
            align: 'left'
        },
        subtitle: {
            text: ' '
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'],
            lineColor: '#e0e0e0',
            gridLineColor: '#fff',
            tickColor: '#fff'
        },
        yAxis: {
            gridLineColor: '#fff',
            tickColor: '#fff',
            title: {
                text: ' ',
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
                name: 'Inquiry',
                data: [190.0, 85.0, 97.0, 250.0, 97.0, 250.0, 150.0]
            }, {
                name: 'SMS',
                data: [97.0, 30.0, 32.0, 55.0, 265.0, 300.0, 275.0]
            }]
    });
});
</script>
@endsection