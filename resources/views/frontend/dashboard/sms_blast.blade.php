@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Target Penerima SMS</h2>
    <p>Masukkan nomor penerima secara manual di bawah atau unggah file yang berisi daftar penerima.</p>

    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    
    <div class="row rowKontenProduk2 ">
        
        <form id="sms-blast">
            {{ csrf_field() }}
            <input type="hidden" name="uuid" value="{{ $uuid }}">
            <div class="col-xs-12"  align='center'>
                <textarea id="numbers" name="numbers" type="number" class="form-control sms-number-textarea" placeholder="Ketik nomor penerima, satu nomor per baris."></textarea>
            </div>
            <div class="col-xs-12"  align='center'>
                <label for="file-upload" class="custom-file-upload">
                    UNGGAH FILE
                </label>
                <input id="file-upload" name="file_upload" type="file"/>
            </div>
        </form>
        <div class="col-xs-12" style="padding-top: 15px;">
            <p id="phone_required" class="text-error">Nomor telepon harus diisi.</p>
            <p id="must_numeric" class="text-error">Nomor telepon harus angka.</p>
        </div>
        <div class="col-md-6 pull-right">
            
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                <button onclick="javascript: history.go(-1)" class="btn btn-yellow btn-bisnis">Kembali</button>
                <button id="sms-button-post" class="btn btn-yellow btn-bisnis">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }
</style>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script>
    $(document).ready(function() 
    {
        $("#phone_required").hide();
        $("#must_numeric").hide();
        $(".loader").hide();
        $(".blur").hide();

        var count_phone_number = 0;
        
        localStorage.setItem('uuid', "{{ $uuid }}");
        localStorage.setItem('sms_type', "BLAST");

        $( "#sms-button-post" ).click(function() {
            //$( "#sms-blast" ).submit();
            var is_required = true;

            var length = $("#numbers").val().length;
            if(length < 1)
            {
                $("#numbers").css("border-color", "red");
                $("#phone_required").show();
                is_required = false;
            }
            else
            {
                $("#numbers").css("border-color", "rgb(169, 169, 169)");
                $("#phone_required").hide();
                is_required = true;
            }

            var content = $("#numbers").val();

            var is_number = true;

            if(content.length > 0)
            {
                for(i = 0; i < content.length; i++)
                {
                    if(content[i] != "\n" && content[i] != " ")
                    {
                        if(!$.isNumeric(content[i]))
                        {
                            $("#numbers").css("border-color", "red");
                            $("#must_numeric").show();

                            is_number = is_number && false;
                        }
                        else
                        {
                            $("#numbers").css("border-color", "rgb(169, 169, 169)");
                            $("#must_numeric").hide();
                            is_number = is_number && true;
                        }
                    } 
                }
            }

            if(is_required && is_number)
            {
                $(".loader").show();
                $(".blur").show();
                
                var requestData = {
                    "uuid" : "{{ $uuid }}",
                    "numbers" : $("#numbers").val()
                };
                
                var urlAPI = "{{route('phone-number-store')}}";
                
                $.ajax({
                    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                    data: requestData,
                    url: urlAPI,
                    type: 'post',
                    beforeSend: function () {
                        //beginTransition();
                    },
                    complete: function () {
                        //endLoadingMapTransition();
                    },
                    success: function (data) {
                        console.log(data);
                        localStorage.setItem('count_phone_number', data);
                        window.location = "{{ route('dashboard-sms-targeted-setting') }}";
                    },
                    error: function (data){
                        console.log(data);
                    }
                });
            }
        });

        $('#file-upload').on("change", function(){ 
            var requestData = {
                "uuid" : "{{ $uuid }}",
                "file-upload" : $("#file-upload").val()
            };
            
            var urlAPI = "{{route('phone-number-store')}}";
            
            $.ajax({
                headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                data: requestData,
                url: urlAPI,
                type: 'post',
                beforeSend: function () {
                    //beginTransition();
                },
                complete: function () {
                    //endLoadingMapTransition();
                },
                success: function (data) {
                    console.log(data);
                    localStorage.setItem('count_phone_number', data);
                    window.location = "{{ route('dashboard-sms-targeted-setting') }}";
                },
                error: function (data){
                    console.log(data);
                }
            });
        });
    });
</script>
@endsection
