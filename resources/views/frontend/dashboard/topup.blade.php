@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Topup Saldo Iklan</h1>
        <p>Silahkan memilih nominal dan jumlah saldo iklan yang ingin anda tambahkan ke akun.</p>
    </div>
    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content">
        <form method="POST" action="{{ route('topup-payment-method') }}">
            {{ csrf_field() }}
            <ul class="prd-ls">
                <li>
                    <div class="col-xs-9">
                        <div class="row">Rp 100.000</div>
                    </div>
                    <div class="col-xs-3">
                        <div class="row"><input data-amount="100000" type="number" placeholder="x 0" class="topup-input" value="0"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-9">
                        <div class="row">Rp 200.000</div>
                    </div>
                    <div class="col-xs-3">
                        <div class="row"><input data-amount="200000" type="number" placeholder="x 0" class="topup-input" value="0"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-9">
                        <div class="row">Rp 500.000</div>
                    </div>
                    <div class="col-xs-3">
                        <div class="row"><input data-amount="500000" type="number" placeholder="x 1" class="topup-input" value="0"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-9">
                        <div class="row">Rp 1000.000</div>
                    </div>
                    <div class="col-xs-3">
                        <div class="row"><input data-amount="1000000" type="number" placeholder="x 0" class="topup-input" value="0"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-9">
                        <div class="row">Rp 5.000.000</div>
                    </div>
                    <div class="col-xs-3">
                        <div class="row"><input data-amount="5000000" type="number" placeholder="x 0" class="topup-input" value="0"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

            <div class="insufficient-warning" style="display: block">
                <input type="hidden" class="order_balance_input" name="order_balance" value="0" />
                <input type="hidden" class="order_id" name="order_id" />
                <p class="text-center">Total Pembelian Saldo</p>
                <p class="total-credit text-center">Rp <span class="order-balance">0</span></p>
            </div>

            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a href="#">
                    <div class="col-xs-6">
                        <input style="background: none;width: 100%;border:none" type="submit" value="BAYAR" />
                    </div>
                </a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    $(document).ready(function() {
        $(".order_id").val(localStorage.getItem('order_id'));
        
        var calculate = function() {
            var totalOrderAmount = 0;
            $('.topup-input').each(function() {
                var value = parseInt($(this).val());
                var amount = parseInt($(this).data('amount'));
                totalOrderAmount += parseInt(amount * value);
            });
            
            $('.order_balance_input').val(totalOrderAmount);
            $('.order-balance').html(totalOrderAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
        };
        
        calculate();
        $('.topup-input').on('keyup change', function(e) {
            if(parseInt($(this).val()) < 0){
                $(this).val(0);
            }
            
            if($(this).val() == ''){
                $(this).val(0);
            }
            
            calculate();
        });
    });
</script>
@endsection
