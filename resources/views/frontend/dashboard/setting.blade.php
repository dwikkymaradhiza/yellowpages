@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<style type="text/css">
.btn-register{
  width: 60%;
  margin-left: 15% !important;
}
</style>
<!-- content -->
<div class="divBodiKonten">
    <h2>Pengaturan</h2>
    <div class="row rowKontenProduk">
        
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
            @endforeach
        </div> <!-- end .flash-message -->
    
        <form id="settings-form" class="form-horizontal" action="{{route('dashboard-setting-edit')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}">
            <fieldset>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Nama Depan</label>
                        <input type="text" required class="form-control" name="first_name" value="{{ $user->first_name }}">
                    </div>
                    <div class="col-md-6 Kategori-bisnis">
                        <label>Nama Belakang</label>
                        <input type="text" required class="form-control" name="last_name" value="{{ $user->last_name }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="firstName">Nomor Telepon/Mobile </label>
                        <input type="text" class="form-control" name="handphone" value="{{ $user->handphone }}">
                    </div>
                    <div class="col-md-6">
                        <label for="firstName Kategori-bisnis">Email </label>
                        <input required type="text" class="form-control" name="email" value="{{ $user->email }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="firstName">Provinsi </label>
                        <select class="form-control" id="province" name='province'>
                            @foreach ($province_all as $province_all_detail) 
                                <option value="{{$province_all_detail->id}}"
                                        @if($province_all_detail->id === $user->province_id)
                                         {{'selected'}}
                                        @endif
                                        >{{$province_all_detail->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="firstName Kategori-bisnis">Kota </label>
                        <select class="form-control" id="city" name='city'>
                            @foreach ($city_all as $city_all_detail) 
                                <option value="{{$city_all_detail->id}}"
                                        @if($city_all_detail->id === $user->city_id)
                                         {{'selected'}}
                                        @endif
                                        >{{$city_all_detail->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row alamat-pengaturan">
                    <div class="col-md-12">
                        <label for="firstName">Alamat </label>
                        <textarea class="form-control" style="height: 100px" name="address">{{ $user->address }}</textarea>
                    </div>
                </div>
                <hr/>
                <div class="hidden col-md-6 anggota">
                    @if(count($package_order)>0)
                    <p>Paket Keanggotaan : <strong id='membership'>{{$package_order->package_name}}</strong></p><br/>
                    <p>Status Keanggotaan : <strong id='membershipstatus'>@if($package_order->status === 0) Tidak Aktif @else Aktif @endif</strong></p><br/>
                    @else
                    <p>Paket Keanggotaan : <strong id='membership'>Free</strong></p><br/>
                    @endif
                    <!--<p>Priode Keanggotaan : <strong>Desember 2017</strong></p>-->
                    <br/>
                    <h5><a data-toggle="modal" href="#myModal--effect-zoomOutIn">Ubah Paket Keanggotaan</a></h5>
                </div>
                <div class="col-md-12">
                    <hr/>
                    <p class="text-center">
                        <button type="submit" class="btn btn-yellow btn-simpan">SIMPAN</button>
                    </p>
                </div>
            </fieldset>
        </form>
    </div>
</div>
</div>
<div class="clear"></div>
   <!-- modal -->
    <style>
        .modal-dialog{
            width: 900px;
        }
        @media (max-width:991px){
            .modal-dialog{
                width: 100%;
                padding: 10px;
            }
        }
    </style>
    <div class="modal fade" id="myModal--effect-zoomOutIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Pilih Paket Keanggotaan</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        
                        @foreach($package_all as $package_all_detail)
                        
                        <div class="col-md-4">
                            <div class="opsis">
                                <div class="opsis-judul">
                                    <h5>{{strtoupper($package_all_detail->package_name)}}</h5>
                                </div>
                                <div class="opsis-isi {{$package_all_detail->package_name}}">
                                    {!!html_entity_decode($package_all_detail->description)!!}
                                </div>
                                <p class="">
                                    <button type="submit" 
                                            data-package-name ='{{$package_all_detail->package_name}}'
                                            data-package-id='{{$package_all_detail->id}}'
                                            data-package-price='{{$package_all_detail->price}}'
                                            class="btn btn-yellow btn-register">PILIH</button>
                                </p>
                            </div>
                        </div>
                        
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
        
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    
    $('.btn-register').on('click', function () {
        
        var id = $(this).data('package-id');
        var name = $(this).data('package-name');
        var price = $(this).data('package-price');
        
        localStorage.setItem('membership_id', id);
        localStorage.setItem('membership_name', name);
        localStorage.setItem('membership_price', price);
        
        if(id === '1'){
            $(".content").html('Anda telah mengubah paket member '+name);
        }else{
            
            var requestData = {
                'name': name,
                'price' : price,
                'id' : id,
                'user_id' : $("#user_id").val(),
                'payment_id' : '1'
            };
            var urlAPI = "{{route('dashboard-membership-update')}}";
            var thanksUrl = "{{route('thanks-page')}}";
            $.ajax({
                headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                data: requestData,
                url: urlAPI,
                type: 'post',
                beforeSend: function () {
                    //beginTransition();
                },
                complete: function () {
                    //endLoadingMapTransition();
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.message ==='success'){
                        localStorage.setItem('order_id', result.order_id);
                        window.location = thanksUrl;
                    }                    
                },
                error: function (data){
                    error.log('Email not sent');
                }
            });
        
                 
        }
         
    });
    
    
    $("#province").on('change', function () {
        var requestData = {
            'province': $("#province").val(),
        };
        var urlAPI = "{{route('getAllCityByProvince')}}";
        $.ajax({
            headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
            data: requestData,
            url: urlAPI,
            type: 'post',
            beforeSend: function () {
                //beginTransition();
            },
            complete: function () {
                //endLoadingMapTransition();
            },
            success: function (data) {
                var template_html = '';
                
                for (index in data){
                    template_html += '<option value="'+data[index]['id']+'">'+ data[index]['name'] +'</option>';
                }
                
                $("#city").html(template_html);
                
            }
        });
    });
    
    $(document).ready(function() {
        $("#settings-form").validate();
    });
    
    let pageSize = document.getElementById("pagesize");
    pageSize.onchange = function() {
        let value = this.value; 
        let reloadUri = '{{ route("dashboard-directory-index") }}';
        
        window.location = reloadUri + "?size=" + value;
    };
    
    
</script>
@endsection
