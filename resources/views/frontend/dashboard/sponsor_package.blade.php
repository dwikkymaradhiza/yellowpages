@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Pilih Paket Sponsored Business</h2>

    <p>Silakan pilih paket sponsored business yang Anda kehendaki.</p>
    
    <div class="row rowKontenProduk2 ">
        <div class="table-responsive">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->

            <table class="table-fill" width="">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Nama Produk/Bisnis</th>
                        <th class="text-center">Deskripsi</th>
                        
                    </tr>
                </thead>
                <tbody class="table-hover">
            @if(count($packages)>0)
            @foreach($packages as $packages_detail)
            <tr>
                <td class="text-center">
                    <input type="radio" data-price="{{$packages_detail->price}}" name="productcb" id="{{$packages_detail->id}}" value="{{$packages_detail->id}}">
                </td>
                <td>
                    {{$packages_detail->package_name}}
                </td>
                <td>
                    {{amount_format($packages_detail->price)}}
                </td>
            </tr>
            
            @endforeach
            @else
                <!--nothing todo-->
            @endif
                </tbody>
            </table>   
        </div>
        <div class="col-md-6">
            
        </div>
        <div class="col-md-6 pull-right">
            <div class="btn-produk">
                <div class="pull-right">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                @if(count($packages)>0)
                    <button class="btn btn-yellow btn-bisnis" onclick="goToSponsorProduct();return false;">Kembali</button>
                    <button class="btn btn-yellow btn-bisnis" onclick="saveSponsorPackages();return false;">Lanjutkan</button>
                @else
                    <!--Nothing todo-->
                @endif
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }
</style>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script>
    $(document).on('ready',function(){
        var package_id = localStorage.getItem('package_id');
        if(typeof package_id !== 'undefined' && package_id !== null){
            $("#"+package_id).prop('checked',true);
        }else{
            $("input[name=productcb]:first").prop("checked",true)
        }
    });
    function saveSponsorPackages() {
        var package_id = $("input[name=productcb]:checked").val();
        var package_price = $("input[name=productcb]:checked").data('price');

        localStorage.setItem('package_id', package_id);
        localStorage.setItem('package_price', package_price);

        window.location = "{{route('sponsor-invoice')}}";

        return false;
    }
    
    function goToSponsorProduct(){
        var url = "{{route('sponsor-business')}}"
        window.location = url;
    }
</script>
@endsection
