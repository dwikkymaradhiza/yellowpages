@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Inquiri Anda</h2>
    <div class="row rowKontenProduk2 ">
        <div class="table-responsive">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->
            <table class="table-fill">
                <thead>
                    <tr>
                        <th class="text-center"><input type="checkbox" id="check-all" /></th>
                        <th class="text-center">Tanggal</th>
                        <th class="text-center">Inquiry</th>
                        <th class="text-center">Pengirim</th>
                        <th class="text-center">Mobile</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @foreach($inquiries as $k => $v)
                    <tr>
                        <td class="text-center"><input type="checkbox" class="row-check" /></td>
                        <td class="text-left">{{ $v->created_at->format('d/m/Y') }}</td>
                        <td class="text-left">{{ str_limit($v->inquiry, 20) }}</td>
                        <td class="text-left">{{ str_limit($v->name, 15) }}</td>
                        <td class="text-left">{{ str_limit($v->phone, 13) }}</td>
                        <td class="text-center">{{ $v->email or "-" }}</td>
                        <td class="text-center">
                            <div class="">
                                <div class="col-md-6" align="right" style="padding:0px 5px">
                                    <form onclick="return false;">
                                    <button 
                                        data-inquiry="{{ $v->inquiry }}" 
                                        data-name="{{ $v->name }}" 
                                        data-phone="{{ $v->phone }}" 
                                        data-email="{{ $v->email or "-" }}" 
                                        data-toggle="modal" data-target="#myModal--effect-zoomOutIn" class="btn-yellow btn-edit" style="border:none">    
                                    <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Edit" data-placement="right"></i>
                                    </button>
                                    <!--</button>-->
                                </form>
                                </div>
                                <div class="col-md-6" align="left" style="padding:0px 5px">
                            <form method="POST" action="{{ route('dashboard-inquiry-delete', ['id' => $v->id]) }}">
                                {{ csrf_field() }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button onclick="return window.confirm('Apakah anda ingin menghapus data ini?')" class="btn-yellow btn-hapus" style="border:none">
                                    <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" title="Hapus" data-placement="right"></i>
                                </button>
                            </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>   
        </div>
        <div class="col-md-6">
            {{ $inquiries->appends(['size' => $size])->links() }}
        </div>
        <div class="col-md-6 pull-right">
            <div class="btn-produk">
                <div class="pull-right">
                    <form class="form-horizontal" id="page-rows-form">
                        <label class="pull-left control-label">Show</label>
                        <div class="pull-left">
                            <select id="pagesize" class="form-control">
                                <option value="5" {!! $size && $size === 5 ? "selected='selected'" : ""  !!}>5</option>
                                <option value="10" {!! $size && $size === 10 ? "selected='selected'" : ""  !!}>10</option>
                                <option value="15" {!! $size && $size === 15 ? "selected='selected'" : ""  !!}>15</option>
                                <option value="20" {!! $size && $size === 20 ? "selected='selected'" : ""  !!}>20</option>
                                <option value="25" {!! $size && $size === 25 ? "selected='selected'" : ""  !!}>25</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>

<div class="modal fade" id="myModal--effect-zoomOutIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Inquiry</h4>
                </div>
                <div class="modal-body">
                    <div class="row rowKontenProduk">
                        <form class="form-horizontal" method="POST" action="{{ route('dashboard-status-create') }}">
                            <input type='hidden' id='status_id' name='status_id'>
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="col-md-12">                                    
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Nama</label>
                                            <input type="text" id="user_name" name="inquiry" class="form-control" readonly="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">                                    
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Nomor Handphone</label>
                                            <input type="text" id="user_handphone" name="inquiry" class="form-control" readonly="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">                                    
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Email</label>
                                            <input type="text" id="user_email" name="inquiry" class="form-control" readonly="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">                                    
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Inkuiri</label>
                                            <textarea  id="inquiry" name="inquiry" class="form-control" style="height: 280px;" readonly="true"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <hr/>
                                <p class="text-center">
                                    <button type="button" class="btn btn-yellow btn-simpan" data-dismiss="modal">close</button>
                                </p>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection

@section('script-content')
<script>
    $('#myModal--effect-zoomOutIn').on('show.bs.modal', function (e) {
       
        var inquiry = $(e.relatedTarget).data('inquiry');
        $('#inquiry').val(inquiry);
            
        var phone = $(e.relatedTarget).data('phone');
        $('#user_handphone').val(phone);
            
        var name = $(e.relatedTarget).data('name');
        $('#user_name').val(name);
            
        var email = $(e.relatedTarget).data('email');
        $('#user_email').val(email);
       
    });
    
    let pageSize = document.getElementById("pagesize");
    pageSize.onchange = function() {
        let value = this.value; 
        let reloadUri = '{{ route("dashboard-inquiry-index") }}';
        
        window.location = reloadUri + "?size=" + value;
    };
</script>
@endsection
