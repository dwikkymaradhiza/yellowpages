@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Terima kasih</h2>

    <div class="row rowKontenProduk2 ">
        <div class="table-responsive">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->

            <div class="promote-main-content">
            <div class="top-success"><i class="mdi mdi-check"></i></div>

            <p>Terima kasih telah melakukan pengisian ulang saldo iklan YP. Saldo iklan Anda akan otomatis bertambah ketika pembayaran telah kami terima.</p>

            <p>Secara bersamaan, iklan bisnis Anda pun akan otomatis kami aktifkan setelah saldo mencukupi.</p>

            <p>Ketuk tombol di bawah untuk mengakhiri transaksi dan kembali ke halaman dashboard Anda.</p>
        </div> 
        </div>
        <div class="col-md-6">
            
        </div>
        <div class="col-md-6 pull-right">
            <div class="btn-produk">
                <div class="pull-right">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6 pull-left">
            <div class="btn-produk">
                    <button class="btn btn-yellow btn-bisnis" onclick="goToDashboard();return false;">DASHBOARD</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }
</style>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script>
    function goToDashboard(){
        var url = "{{route('dashboard')}}"
        window.location = url;
    }
    $(document).on('ready',function(){
        localStorage.clear();
    });    
</script>
@endsection
