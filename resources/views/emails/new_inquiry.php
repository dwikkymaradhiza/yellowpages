<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="format-detection" content="telephone=no"/>
<title>YP Reset Pass</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,300&subset=latin,cyrillic,greek" rel="stylesheet" type="text/css">
<style type="text/css">

    /* Resets: see reset.css for details */
    .ReadMsgBody { width: 100%; background-color: #ffffff;}
    .ExternalClass {width: 100%; background-color: #ffffff;}
    .ExternalClass, .ExternalClass p, .ExternalClass span,
    .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
    #outlook a{ padding:0;}
    body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0;}
    body{ -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    html{width:100%;}
    table {mso-table-lspace:0pt; mso-table-rspace:0pt; border-spacing:0;}
    table td {border-collapse:collapse;}
    table p{margin:0;}
    br, strong br, b br, em br, i br { line-height:100%; }
    div, p, a, li, td { -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
    h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
    span a { text-decoration: none !important;}
    a{ text-decoration: none !important; }
    img{height: auto !important; line-height: 100%; outline: none; text-decoration: none;  -ms-interpolation-mode:bicubic;}
    .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited,
    .yshortcuts a:hover, .yshortcuts a span { text-decoration: none !important; border-bottom: none !important;}
    /*mailChimp class*/
    .default-edit-image{
    height:20px;
    }
    ul{padding-left:10px; margin:0;}
    .tpl-repeatblock {
    padding: 0px !important;
    border: 1px dotted rgba(0,0,0,0.2);
    }
    .tpl-content{
    padding:0px !important;
    }
    @media only screen and (max-width:800px){
    table[style*="max-width:800px"]{width:100%!important; max-width:100%!important; min-width:100%!important; clear: both;}
    table[style*="max-width:800px"] img{width:100% !important; height:auto !important; max-width:100% !important;}
    }
    @media only screen and (max-width: 640px){
    /* mobile setting */
    table[class="container"]{width:100%!important; max-width:100%!important; min-width:100%!important;
    padding-left:20px!important; padding-right:20px!important; text-align: center!important; clear: both;}
    td[class="container"]{width:100%!important; padding-left:20px!important; padding-right:20px!important; clear: both;}
    table[class="full-width"]{width:100%!important; max-width:100%!important; min-width:100%!important; clear: both;}
    table[class="full-width-center"] {width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    table[class="force-240-center"]{width:240px !important; clear: both; margin:0 auto; float:none;}
    table[class="auto-center"] {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    *[class="auto-center-all"]{width: auto!important; max-width:75%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    *[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    table[class="col-3"],table[class="col-3-not-full"]{width:30.35%!important; max-width:100%!important;}
    table[class="col-2"]{width:47.3%!important; max-width:100%!important;}
    *[class="full-block"]{width:100% !important; display:block !important; clear: both; padding-top:10px; padding-bottom:10px;}
    /* image */
    td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important;}
    /* helper */
    table[class="space-w-20"]{width:3.57%!important; max-width:20px!important; min-width:3.5% !important;}
    table[class="space-w-20"] td:first-child{width:3.5%!important; max-width:20px!important; min-width:3.5% !important;}
    table[class="space-w-25"]{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
    table[class="space-w-25"] td:first-child{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
    table[class="space-w-30"] td:first-child{width:5.35%!important; max-width:30px!important; min-width:5.35% !important;}
    table[class="fix-w-20"]{width:20px!important; max-width:20px!important; min-width:20px!important;}
    table[class="fix-w-20"] td:first-child{width:20px!important; max-width:20px!important; min-width:20px !important;}
    *[class="h-10"]{display:block !important;  height:10px !important;}
    *[class="h-20"]{display:block !important;  height:20px !important;}
    *[class="h-30"]{display:block !important; height:30px !important;}
    *[class="h-40"]{display:block !important;  height:40px !important;}
    *[class="remove-640"]{display:none !important;}
    *[class="text-left"]{text-align:left !important;}
    *[class="clear-pad"]{padding:0 !important;}
    }
    @media only screen and (max-width: 479px){
    /* mobile setting */
    table[class="container"]{width:100%!important; max-width:100%!important; min-width:124px!important;
    padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
    td[class="container"]{width:100%!important; padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
    table[class="full-width"],table[class="full-width-479"]{width:100%!important; max-width:100%!important; min-width:124px!important; clear: both;}
    table[class="full-width-center"] {width: 100%!important; max-width:100%!important; min-width:124px!important; text-align: center!important; clear: both; margin:0 auto; float:none;}
    *[class="auto-center-all"]{width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    *[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    table[class="col-3"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
    table[class="col-3-not-full"]{width:30.35%!important; max-width:100%!important; }
    table[class="col-2"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
    *[class="full-block-479"]{display:block !important; width:100% !important; clear: both; padding-top:10px; padding-bottom:10px; }
    /* image */
    td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:124px !important;}
    td[class="image-min-80"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:80px !important;}
    td[class="image-min-100"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:100px !important;}
    /* halper */
    table[class="space-w-20"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
    table[class="space-w-20"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
    table[class="space-w-25"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
    table[class="space-w-25"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
    table[class="space-w-30"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
    table[class="space-w-30"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
    *[class="remove-479"]{display:none !important;}
    table[width="595"]{width:100% !important;}
    img{max-width:280px !important;}
    .resize-font, .resize-font *{
      font-size: 37px !important;
      line-height: 48px !important;
    }
    }
    
    td ul{list-style: initial; margin:0; padding-left:20px;}

	@media only screen and (max-width: 640px){ .image-100-percent{ width:100%!important; height: auto !important; max-width: 100% !important; min-width: 124px !important;}}body{background-color:#efefef;} .default-edit-image{height:20px;} tr.tpl-repeatblock , tr.tpl-repeatblock > td{ display:block !important;} .tpl-repeatblock {padding: 0px !important;border: 1px dotted rgba(0,0,0,0.2);} table[width="595"]{width:100% !important;}a img{ border: 0 !important;}
a:active{color:initial } a:visited{color:initial }
.tpl-content{padding:0 !important;}
.full-mb,*[fix="full-mb"]{width:100%!important;} .auto-mb,*[fix="auto-mb"]{width:auto!important;}
</style>
<!--[if gte mso 15]>
<style type="text/css">
a{text-decoration: none !important;}
body { font-size: 0; line-height: 0; }
tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }
table { font-size:1px; line-height:0; mso-margin-top-alt:1px; }
body,table,td,span,a,font{font-family: Arial, Helvetica, sans-serif !important;}
a img{ border: 0 !important;}
</style>
<![endif]-->
<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
</head>
<body  style="font-size:12px; width:100%; height:100%;">
<table id="mainStructure" width="800" class="full-width" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: #efefef; width: 800px; max-width: 800px; outline: rgb(239, 239, 239) solid 1px; box-shadow: rgb(224, 224, 224) 0px 0px 5px; margin: 0px auto;"><!--START LAYOUT-2 ( LOGO / MENU )--><tbody><tr><td align="center" valign="top" class="container" style="background-color: #ffffff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="min-width: 600px; background-color: #ffffff; padding-left: 20px; padding-right: 20px; width: 600px; margin: 0px auto;"><tbody><tr><td valign="top">
                  <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width" style="width: 560px; margin: 0px auto;"><!-- start space --><tbody><tr><td valign="top" height="30" style="height: 30px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --><tr><td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: 0px auto;"><tbody><tr><td valign="middle">
                              
                              <table width="auto" align="left" border="0" cellspacing="0" cellpadding="0" class="full-width-center" style="mso-table-lspace:0pt; mso-table-rspace:0pt;"><tbody><tr><td align="center" valign="top" width="136" style="width: 136px;">
                                    <a href="#" style="text-decoration: none !important; font-size: inherit; border-style: none;" border="0">
                                      <img src="https://yellowpages.co.id/email/20170207215618_yp-logo.png" width="136" style="max-width: 240px; display: block !important; width: 136px; height: auto;" alt="logo-top" border="0" hspace="0" vspace="0" height="auto"></a>
                                  </td>
                                </tr></tbody></table><!--[if (gte mso 9)|(IE)]></td><td valign="top" ><![endif]--><table width="20" border="0" cellpadding="0" cellspacing="0" align="left" class="full-width" style="height: 1px; border-spacing: 0px; width: 20px;mso-table-lspace:0pt; mso-table-rspace:0pt;"><tbody><tr><td height="1" class="h-30" style="height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                  </td>
                                </tr></tbody></table><!--[if (gte mso 9)|(IE)]></td><td valign="top" ><![endif]--><table width="auto" align="right" border="0" cellpadding="0" cellspacing="0" class="full-width-center" style="mso-table-lspace:0pt; mso-table-rspace:0pt;"><tbody><tr><td valign="top">
                                    <table width="auto" align="left" border="0" cellpadding="0" cellspacing="0" class="full-width-center" style="mso-table-lspace:0pt; mso-table-rspace:0pt;"><tbody><tr><td align="center" style="font-size: 14px; color: #888888; font-weight: normal; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 22px;"><br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"></td>
                                      </tr></tbody></table></td>
                                </tr></tbody></table></td>
                          </tr></tbody></table></td>
                    </tr><!-- start space --><tr><td valign="top" height="30" style="height: 30px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --></tbody></table></td>
              </tr></tbody></table><!-- end container --></td>
        </tr><!--END LAYOUT-2 ( LOGO / MENU )--></tbody><!-- START LAYOUT-6 ( 3-COL IMAGE-ICON / TEXT / BUTTON ) --><tbody><tr><td valign="top" align="center" class="container" style="background-color: #ffffff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; min-width: 600px; width: 600px; margin: 0px auto;"><tbody><tr><td valign="top" align="center">
                  <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="full-width" style="margin: 0px auto;"><!-- start space --><tbody><tr><td valign="top" height="38" style="height: 38px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --><!-- start heading text --><tr dup="0"><td valign="top" align="center" class="clear-pad" style="padding-left:20px; padding-right:20px;">
                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><!-- start title --><tbody><tr dup="0"><td valign="top">
                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tbody><tr><td align="center" style="font-size: 26px; color: #333333; font-weight: normal; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 34px;"><span style="color: #333333; text-decoration: none; line-height: 34px; font-size: 26px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif">Inquiry Baru</font></span></td>
                                </tr><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                </tr><!-- end space --></tbody></table></td>
                          </tr><!-- end title --><!-- start divider line  --><!-- end divider line --></tbody></table></td>
                    </tr><!-- end heading text --><!-- start 3-col --><!-- end 3-col --><!-- start content --><tr dup="0"><td valign="top" align="center" class="clear-pad" style="padding-left:20px; padding-right:20px;">
                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tbody><tr dup="0"><td valign="top" align="center">
                                        <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tbody><tr><td align="left" style="font-size: 14px; color: #888888; font-weight: normal; font-family: 'Open Sans', Arial, Helvetica, sans-serif; text-align: left; word-break: break-word; line-height: 22px;"><span style="line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif"><div style="text-align: left; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif">Ada Inquiry baru Untuk Anda:<br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                                Nama : <?php echo $name ?><br />
                                                                                Telepon : <?php echo $phone ?><br />
                                                                                Email : <?php echo $email ?><br />
                                                                                Pesan : <?php echo $msg ?>
                                                                    </font></div></font></span></td>
                                </tr><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                </tr><!-- end space --></tbody></table></td>
                                    
                          </tr></tbody></table></td>
                          </tr><!-- start space --><!-- end space --></tbody></table></td>
                    </tr><tr dup="0"><td valign="top" align="center" class="clear-pad" style="padding-left:20px; padding-right:20px;">
                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tbody><tr dup="0">
                          </tr><tr dup="0"><td valign="top" align="center">
                              <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tbody><tr><td align="left" style="font-size: 14px; color: #888888; font-weight: normal; font-family: 'Open Sans', Arial, Helvetica, sans-serif; text-align: left; word-break: break-word; line-height: 22px;"><span style="line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif"><div style="text-align: left; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif"><br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">Jika&nbsp;anda mengalami kendala atau memiliki&nbsp;pertanyaan tertentu, silakan hubungi call center kami di:<br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"></font></div></font></span></td>
                                </tr></tbody></table></td>
                          </tr><tr dup="0"><td valign="top" align="center">
                              <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tbody><tr><td align="center" style="font-size: 14px; color: #888888; font-weight: normal; font-family: 'Open Sans', Arial, Helvetica, sans-serif; text-align: center; word-break: break-word; line-height: 22px;"><div style="text-align: center; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif"><span style="line-height: 26px; font-size: 18px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif">1500057</font></span></font></div></td>
                                </tr><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                </tr><!-- end space --></tbody></table></td>
                          </tr><tr dup="0"><td valign="top" align="center">
                              <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tbody><tr><td align="left" style="font-size: 14px; color: #888888; font-weight: normal; font-family: 'Open Sans', Arial, Helvetica, sans-serif; text-align: left; word-break: break-word; line-height: 22px;"><span style="line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif"><div style="text-align: left; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif">Mohon tidak membalas email ini.<br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"></font></div></font></span></td>
                                </tr><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                </tr><!-- end space --></tbody></table></td>
                          </tr><tr><td valign="top" align="center" class="clear-pad" style="padding-left:20px; padding-right:20px;">
                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><!--start button--><tbody><tr><td valign="top" align="center">
                                    <table width="auto" align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 0px auto;"><tbody><tr><!-- start duplicate button -->
                                        <!-- end duplicate button -->
                                      </tr></tbody></table></td>
                                </tr><!--end button--></tbody></table></td>
                          </tr><!-- start space --><tr><td valign="top" height="3" style="height: 3px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                            </td>
                          </tr><!-- end space --></tbody></table></td>
                    </tr><!-- end content --><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --></tbody></table></td>
              </tr></tbody></table><!-- end container --></td>
        </tr><!-- END LAYOUT-6 ( 3-COL IMAGE-ICON / TEXT / BUTTON ) --></tbody><!-- START LAYOUT-20 ( UNSUBSCRIBE )--><tbody><tr><td valign="top" align="center" style="background-color: #2f2f2f;" class="container" bgcolor="#2f2f2f">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: #2f2f2f; min-width: 600px; padding-left: 20px; padding-right: 20px; width: 600px; margin: 0px auto;" class="container"><tbody><tr><td valign="top" align="center">
                  <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width" style="width: 560px; margin: 0px auto;"><!-- start space --><tbody><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --><tr><td valign="top" align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: 0px auto;"><tbody><tr><td valign="middle" align="center">
                              
                              <table width="auto" align="left" border="0" cellspacing="0" cellpadding="0" class="full-width-center" style="mso-table-lspace:0pt; mso-table-rspace:0pt;"><tbody><tr><td align="center" style="font-size: 14px; color: #ffffff; font-weight: normal; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 22px;"><span style="color: #ffffff; text-decoration: none; line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif"> <a href="#" style="color: #ffffff; text-decoration: none !important; border-style: none; line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;" data-mce-href="#" border="0"><font face="'Open Sans', Arial, Helvetica, sans-serif">©&nbsp;<span style="line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif">Copyright 1996-2017 MD Media</font></span></font></a></font></span></td>
                                </tr></tbody></table><!--[if (gte mso 9)|(IE)]></td><td valign="top" ><![endif]--><table width="20" border="0" cellpadding="0" cellspacing="0" align="left" class="full-width" style="height: 1px; border-spacing: 0px; width: 20px;mso-table-lspace:0pt; mso-table-rspace:0pt;"><tbody><tr><td height="1" class="h-20" style="height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                  </td>
                                </tr></tbody></table><!--[if (gte mso 9)|(IE)]></td><td valign="top" ><![endif]--><table width="auto" align="right" border="0" cellspacing="0" cellpadding="0" class="full-width-center" style="mso-table-lspace:0pt; mso-table-rspace:0pt;"><tbody><tr><td align="center" style="font-size: 14px; color: #ffffff; font-weight: normal; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 22px;">
                                    <!--span style="text-decoration: none; color: #ffffff; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;"><font face="'Open Sans', Arial, Helvetica, sans-serif">
                                      <a href="#" style="color: #ffffff; text-decoration: none !important; border-style: none; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;" border="0"><font face="'Open Sans', Arial, Helvetica, sans-serif">Unsubscribe</font></a>
                                    </font></span-->
                                  </td>
                                </tr></tbody></table></td>
                          </tr></tbody></table></td>
                    </tr><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --></tbody></table></td>
              </tr></tbody></table><!-- end container --></td>
        </tr><!-- END LAYOUT-20 ( UNSUBSCRIBE )--></tbody></table></body>
</html>