@extends('mobile.layouts.general')

@section('seo-meta')
    @include('seo.product')
@stop

@section('header')
@include('mobile.layouts.nav_general')
@stop

@section('content')
<!-- map -->
<div class="modal fade" role="dialog" tabindex="-1" id="pop-map">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <header class="m-header-flow">
                    <div class="m-arrow-back"><a href="#" class="m-li-back" data-dismiss="modal"><i class="material-icons m-ic-arrow">keyboard_arrow_left</i></a></div>
                    <h3 class="m-title-flow">Peta Bisnis</h3></header>
                    <iframe allowfullscreen="" frameborder="0" width="100%" height="500px" src="//maps.google.com/maps?q={{ $directory->latitude }},{{ $directory->longitude }}&z=17&output=embed" class="m-map-bisnis"></iframe>
            </div>
        </div>
    </div>
</div>
<!-- all review -->
<div class="modal fade" role="dialog" tabindex="-1" id="pop-allreview">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <header class="m-header-flow">
                    <div class="m-arrow-back"><a href="#" class="m-li-back" data-dismiss="modal"><i class="material-icons m-ic-arrow">keyboard_arrow_left</i></a></div>
                    <h3 class="m-title-flow">Semua Ulasan</h3></header>
                <div class="m-modal-ulasan">
                    @foreach($ratings as $k => $v) 
                        @php
                            $reviewerName = (!empty($v->name)) ? $v->name : "Anonymous";
                        @endphp
                        @if($v->uf_name && $v->ul_name)
                            @php 
                                $userUri = '';
                                $reviewerName = $v->uf_name . " " . $v->ul_name;
                            @endphp
                        @endif
                        
                        <div class="m-ulasan-pro">
                            <div class="m-headeKomen">
                                <div class="m-owner">
                                    <h4 class="text-uppercase m-nama-user">{{ $reviewerName }}  </h4>
                                    <p class="m-time-post">{{ $v->rating_date }}</p>
                                </div>
                                <div class="m-star-komen">
                                    <div>
                                        @php $rate = (int) $v->rate @endphp
                                        @for($r = 0; $r < 5; $r++)
                                            <i class="material-icons {!! $rate > 0 ? 'm-ic-star' : 'm-ic-star-inactive' !!}">star</i>
                                            @php $rate-- @endphp
                                        @endfor   
                                     </div>
                                </div>
                            </div>
                            <div class="m-open-coment">
                                <p class="m-text-komen">{{ $v->comment }}</p>
                            </div>
                        </div>
                    @endforeach
                    
                    <div class="m-modal-btn">
                        <button class="btn btn-info btn-block m-btn-create" type="button" data-toggle="modal" data-target="#pop-review"><i class="material-icons m-ic-komen">chat</i>TULIS ULASAN ANDA</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- review form -->
<div class="modal fade" role="dialog" tabindex="-1" id="pop-review">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="ratingForm" method="POST" action="{{ route('product-rating-create') }}" >
                {{ csrf_field() }}
                <input type="hidden" name="product_id" value="{{ $product->id }}" />
                <div class="modal-body">
                    <header class="m-header-flow">
                        <div class="m-arrow-back"><a href="#" class="m-li-back" data-dismiss="modal"><i class="material-icons m-ic-arrow">keyboard_arrow_left</i></a></div>
                        <h3 class="m-title-flow">Tulis Ulasan</h3>
                    </header>
                    @php $enable_review = true; @endphp
                    @if(Auth::check())
                    <div class="m-pop-padding">
                        <div class="form-group">
                            @if($directory->user_id == Auth::user()->id)
                            @php $enable_review = false; @endphp
                            <p class="m-label-input">Pemilik bisnis tidak dapat melakukan review untuk bisnisnya sendiri</p>
                            @else
                            <div class="m-div-form">
                                <input type="hidden" value="4" name="rate" class="rate-star" />
                                <div>
                                    <i data-rate="1" class="rate-select-1 material-icons m-ic-star">star</i>
                                    <i data-rate="2" class="rate-select-2 material-icons m-ic-star">star</i>
                                    <i data-rate="3" class="rate-select-3 material-icons m-ic-star">star</i>
                                    <i data-rate="4" class="rate-select-4 material-icons m-ic-star">star</i>
                                    <i data-rate="5" class="rate-select-5 material-icons m-ic-star m-ic-star-inactive">star</i>
                                </div>
                            </div>
                            <div class="m-div-form">
                                <p class="m-label-input">Ulasan anda <span class="m-required">* </span></p>
                                <textarea required minlength="50" rows="3" name="review" placeholder="Ulasan anda" class="m-input-ulasan"></textarea>
                            </div>
                            @endif
                        </div>
                    </div>
                    @else
                    <div class="m-pop-padding">
                        <div class="form-group">
                            <div class="m-div-form">
                                <p class="m-label-input">Nama <span class="m-required">* </span></p>
                                <input type="text" name="name" required="" placeholder="Nama anda" class="m-input-name">
                            </div>
                            <div class="m-div-form">
                                <p class="m-label-input">Nomor HP <span class="m-required">* </span></p>
                                <input type="number" name="phone" required="" placeholder="Cth: 0812345678" inputmode="tel" class="m-input-name">
                            </div>
                            <div class="m-div-form">
                                <p class="m-label-input">Email <span class="m-required">* </span></p>
                                <input type="email" name="email" required="" placeholder="Cth: name@mail.com" inputmode="email" class="m-input-email">
                            </div>
                            <div class="m-div-form">
                                <input type="hidden" value="4" name="rate" class="rate-star" />
                                <div>
                                    <i data-rate="1" class="rate-select-1 material-icons m-ic-star">star</i>
                                    <i data-rate="2" class="rate-select-2 material-icons m-ic-star">star</i>
                                    <i data-rate="3" class="rate-select-3 material-icons m-ic-star">star</i>
                                    <i data-rate="4" class="rate-select-4 material-icons m-ic-star">star</i>
                                    <i data-rate="5" class="rate-select-5 material-icons m-ic-star m-ic-star-inactive">star</i>
                                </div>
                            </div>
                            <div class="m-div-form">
                                <p class="m-label-input">Ulasan anda <span class="m-required">* </span></p>
                                <textarea rows="3" minlength="50" name="review" placeholder="Ulasan anda" class="m-input-ulasan" required></textarea>
                            </div>
                            <div style="border:none" id="RecaptchaField1"></div>
                        </div>
                    </div>
                    @endif
                </div>
                @if(isset($enable_review) && $enable_review)
                <div class="modal-footer">
                    <button class="btn btn-info btn-block m-btn-kirim" type="submit"><i class="material-icons m-ic-komen">chat</i>SUBMIT</button>
                </div>
                @endif
            </form>
        </div>
    </div>
</div>
<!-- inquiry / sms form -->
<div class="modal fade" role="dialog" tabindex="-1" id="pop-inquiry">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="inquiryForm" method="POST" action="{{ route('inquiry') }}">
                {{ csrf_field() }}
                <input type="hidden" name="for" id="for" />
                <input type="hidden" name="directory_id" value="{{ $directory->id }}" />
                <div class="modal-body">
                    <header class="m-header-flow">
                        <div class="m-arrow-back"><a href="#" class="m-li-back" data-dismiss="modal"><i class="material-icons m-ic-arrow">keyboard_arrow_left</i></a></div>
                        <h3 class="m-title-flow inquiry-form-title">Kirim Inquiry</h3>
                    </header>
                    <div class="m-pop-padding">
                        <div class="form-group">
                            @if($directory->user_id)
                            <div class="m-div-form">
                                <p class="m-label-input">Nama <span class="m-required">* </span></p>
                                <input type="text" name="name" required="" placeholder="Nama anda" class="m-input-name" value="{{ (Auth::check()) ? Auth::user()->first_name . " " . Auth::user()->last_name : old('name') }}">
                            </div>
                            <div class="m-div-form">
                                <p class="m-label-input">Nomor HP <span class="m-required">* </span></p>
                                <input type="number" name="phone" required="" placeholder="Cth: 0812345678" inputmode="tel" class="m-input-name"  value="{{ (Auth::check()) ? Auth::user()->handphone : old('phone') }}">
                            </div>
                            <div class="m-div-form">
                                <p class="m-label-input">Email <span class="m-required">* </span></p>
                                <input type="email" name="email" required="" placeholder="Cth: name@mail.com" inputmode="email" class="m-input-email" value="{{ (Auth::check()) ? Auth::user()->email : old('email') }}">
                            </div>
                            <div class="m-div-form">
                                <p class="m-label-input">Isi Pesan <span class="m-required">* </span></p>
                                <textarea rows="3" minlength="20" required name="message" id="message-area" placeholder="Pesan SMS" class="m-input-ulasan">{{ old('message') }}</textarea>
                            </div>
                            <div class="m-div-captcha" style="border:0" id="RecaptchaField2"></div>
                            @else
                                <div class="m-div-form">
                                    <p class="m-label-input">
                                    <strong>{{$directory->name}}</strong> belum mengklaim listing ini. Silahkan menghubungi via telfon dibawah.<br />
                                    Anda pemilik Bisnis ini? <a href="{{route('claim', $directory->slug)}}">Klaim sekarang!</a>
                                </p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    @if($directory->user_id)
                    <button class="btn btn-info btn-block m-btn-kirim inquiry-form-btn" type="submit"><i class="material-icons m-ic-komen">chat</i>KIRIM INQUIRY</button>
                    @else
                        <a href="tel:{{$directory->phone_1}}" class="btn btn-info m-btn-query" type="button">TELEPON BISNIS</a>
                        <a href="{{route('claim', $directory->slug)}}" class="btn btn-info m-btn-query" type="button">KLAIM BISNIS</a>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
<div>
    @if (count($errors) > 0)
        <div class="alert alert-danger no-margin">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="flash-message no-margin">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }} no-margin">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->

    <div class="m-div-slide">
        <div class="m-swip-img">
            <div class="carousel slide" data-ride="carousel" id="carousel-1">
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        @if($product->picture)
                            <img src="{{ asset('uploads/products/' . $product->picture) }}" class="img-responsive sld-img m-imgProduk"
                                 onerror="this.src='{{ asset('img/img-default.jpg') }}'; this.onerror=null;" alt="{{$product->name}}">
                        @else
                            <img src="{{ asset('img/img-default.jpg') }}" class="img-responsive sld-img m-imgProduk">
                        @endif
                    </div>
                    @foreach($pictures as $pk => $pv)
                        <div class="item">
                            <img class="img-responsive sld-img" src="{{ asset('uploads/products/' . $pv->picture) }}" alt="Slide Image">
                        </div>
                    @endforeach
                </div>
                <ol class="carousel-indicators">
                    @if(count($pictures))
                        <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                        @for($i=0; $i<count($pictures); $i++)
                            <li data-target="#carousel-1" data-slide-to="1"></li>
                        @endfor
                    @endif

                </ol>
            </div>
            @if($directory->phone_1)
            <div class="m-div-btn">
                <a href="tel:{{$directory->phone_1}}" class="btn btn-default m-btnProduk" type="button"><i class="material-icons m-ic-telp">call</i>TELEPON </a>
            </div>
            @endif
        </div>
    </div>
    <div class="m-block-div">
        <div class="m-desk-produk">
            <div class="m-div-header">
                <div class="m-g-judul">
                    <h1 class="m-h-produk">{{ $product->name }}</h1>
                    <h4 class="text-uppercase m-shop-name"><a href="{{ route('directory', $directory->slug) }}">{{ $directory->name }}</a></h4>
                </div>
                <div class="m-g-share">
                    <i id="ic-suka" class="material-icons m-ic-share {{ ($isFavorited) ? "red" : "" }}">favorite</i>
                    <div>
                        <a id="pop-share" class="m-ic-share" rel="popover" data-placement="bottom" data-toggle="pop-share"> <i class="material-icons">share</i></a>
                        <div id="share-konten" class="hide">
                            <a href="{{ Share::load(Request::url(), $product->name)->facebook() }}" target="_blank" class="li-share-facebook"><i class="fa fa-facebook-official ic-share-facebook"></i></a>
                            <a href="{{ Share::load(Request::url(), $product->name)->linkedin() }}" target="_blank" class="li-share-facebook"><i class="fa fa-linkedin ic-share-inked"></i></a>
                            <a href="{{ Share::load(Request::url(), $product->name)->twitter() }}" target="_blank" class="li-share-facebook"><i class="fa fa-twitter ic-share-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-kol-star">
                <div class="m-star">
                    <div>
                        @if($average_rate > 0)
                            @for($r = 0; $r < 5; $r++)
                                <i class="material-icons {!! $average_rate > 0 ? "m-ic-star" : "m-ic-star-inactive" !!}">star</i>
                                @php $average_rate-- @endphp
                            @endfor
                        @else
                            @for($r = 0; $r < 5; $r++)
                                <i class="material-icons m-ic-star-inactive">star</i>
                                @php $average_rate-- @endphp
                            @endfor
                        @endif
                    </div>
                    <div
                    class="m-div-info">
                        @if(!$average_rate)
                            <p class="m-p-info">Berdasarkan <span class="m-con">{{ count($ratings) }} </span> Ulasan</p>
                        @else
                            <p class="m-p-info">Belum Ada Ulasan</p>
                        @endif
                    </div>
                </div>
                <div class="m-kolaps"><a href="#deskripsi-produk" class="m-link-kolaps" data-toggle="collapse" aria-expanded="false" aria-controls="deskripsi-produk" role="button"><i class="material-icons m-ic-kolaps">arrow_downward</i></a></div>
            </div>
            <div>
                <div class="collapse m-pro-kolapse" id="deskripsi-produk">
                    <p class="m-p-deskripsi-pro">
                        {!! nl2br($product->description) !!}
                    </p>
                </div>
            </div>
        </div>
        <div class="m-div-produkBtn">
            <button class="btn btn-info m-btn-query inquiry-sms" type="button" data-toggle="modal" data-target="#pop-inquiry">KIRIM SMS</button>
            <button class="btn btn-info m-btn-query-sms inquiry-inquiry" type="button" data-toggle="modal" data-target="#pop-inquiry">KIRIM INQUIRY</button>
        </div>
    </div>
    <div class="m-block-div">
        <div class="m-div-map"><a href="#pop-map" class="m-direct-map" data-toggle="modal" data-target="#pop-map">{{ $directory->address_1 }}</a></div>
    </div>
    <div class="m-block-div">
        <div class="m-ulasan-pro">
            @if(count($ratings) > 0)
                @php
                    $reviewerName = (!empty($ratings[0]->name)) ? $ratings[0]->name : "Anonymous";
                @endphp
                @if($ratings[0]->uf_name && $ratings[0]->ul_name)
                    @php 
                        $userUri = '';
                        $reviewerName = $ratings[0]->uf_name . " " . $ratings[0]->ul_name;
                    @endphp
                @endif
                <div class="m-headeKomen">
                    <div class="m-owner">
                        <h4 class="text-uppercase m-nama-user">{{ $reviewerName }} </h4>
                        <p class="m-time-post">{{ $ratings[0]->rating_date }}</p>
                    </div>
                    <div class="m-star-komen">
                        <div>
                            @php $rate = (int) $ratings[0]->rate @endphp
                            @for($r = 0; $r < 5; $r++)
                                <i class="material-icons {!! $rate > 0 ? 'm-ic-star' : 'm-ic-star-inactive' !!}">star</i>
                                @php $rate-- @endphp
                            @endfor
                        </div>
                    </div>
                </div>
                <div class="m-open-coment">
                    <p class="m-text-komen">
                        {{ str_limit($ratings[0]->comment, 200) }}
                        <a href="#" class="m-li-selenglapnya" data-toggle="modal" data-target="#pop-allreview">Selengkapnya </a> </p>
                </div>
            @else
                <center>Belum ada ulasan.</center>
            @endif
        </div>
        <div>
            <button class="btn btn-info btn-block m-btn-create" type="button" data-toggle="modal" data-target="#pop-review"><i class="material-icons m-ic-komen">chat</i>TULIS ULASAN ANDA</button>
        </div>
    </div>
    @if(count($others))
    <div class="m-block-div">
        <h3 class="text-uppercase m-title-tumb">Produk Lainnya</h3>
        <div class="m-list-produk">
            @foreach($others as $k => $v)
            <div class="m-produk-rid">
                <a href="{{ route('product-detail', ['slug' => $v->slug]) }}">
                    <div class="m-div-tumb">
                        @if($v->picture)
                        <img src="{!! asset('uploads/products/thumb_' . $v->picture) !!}"
                             onerror="this.src='{{URL::asset('uploads/products/'.$v->picture) }}'; this.onerror=null;" alt="{{$v->name}}" class="m-tumb-produk">
                        @else
                        <img src="{!! asset('img/img-default.jpg') !!}" class="m-tumb-produk">
                        @endif
                    </div>
                    <h4 class="m-tumb-title">{{ $v->name }}</h4>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    @endif
    @if(count($articles))
    <div class="m-block-div">
        <h3 class="text-uppercase m-title-tumb">artikel terkait</h3>
        <div class="m-list-artikel">
            @foreach($articles as $ak => $vk)
            <div class="m-recent-artikel">
                <div class="m-tumb-img">
                    <a href="{{ $vk->full_url }}" target="_blank">
                        <img src="{{ $vk->image_url }}" class="m-tumb-artikel">
                    </a>
                </div>
                <div class="m-desk-tum">
                    <a href="{{ $vk->full_url }}" target="_blank">
                        <h4 class="m-title-artikel">{{ $vk->title }}</h4>
                    </a>
                    <p class="m-par-artikel">{{ str_limit($vk->headline, 100) }}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
    </div>
@stop

@section('script-content')
<style>
    .g-recaptcha {
        transform:scale(0.9);
        transform-origin:0 0;
    }
    .red{
        color: red;
    }
    .m-div-map{
        background-image: url('//maps.googleapis.com/maps/api/staticmap?center={{ $directory->latitude }},{{ $directory->longitude }}&zoom=16&size=400x200&maptype=roadmap&markers={{ $directory->latitude }},{{ $directory->longitude }}&key={{env('GMAPS_KEY')}}')
    }
</style>
<script>
    var CaptchaCallback = function(){
        if($('#RecaptchaField1').length) {
            grecaptcha.render('RecaptchaField1', {'sitekey' : "{{ config('recaptcha.public_key') }}" });
        }

        if($('#RecaptchaField2').length) {
            grecaptcha.render('RecaptchaField2', {'sitekey' : "{{ config('recaptcha.public_key') }}" });
        }
    };
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit{{ (isset($lang) ? '&hl='.$lang : '') }}' async defer></script>
<script src="{{ asset('js/messages_id.js') }}"></script>
<script>
$(document).ready(function() {
    $("#ratingForm").validate();
    $("#inquiryForm").validate();
    
    $("[class*='rate-select-']").on('click', function() {
        var rate = $(this).data('rate');
        $('.rate-star').val(rate);

        $("[class*='rate-select-']").addClass("m-ic-star-inactive");
        for(let r = 1; r <= rate; r++) {
            $(".rate-select-" + r).removeClass('m-ic-star-inactive');
        }
    });
    
    $('.inquiry-sms').click(function() {
        $('.inquiry-form-title').html('Kirim SMS');
        $('.inquiry-form-btn').html('<i class="material-icons m-ic-komen">chat</i>KIRIM SMS');
        $('#message-area').attr('placeholder', 'Pesan SMS')
        $('#for').val('sms');
    });

    $('.inquiry-inquiry').click(function() {
        $('.inquiry-form-title').html('Kirim Inquiry');
        $('.inquiry-form-btn').html('<i class="material-icons m-ic-komen">chat</i>KIRIM INQUIRY');
        $('#message-area').attr('placeholder', 'Pesan Inquiry')
        $('#for').val('inq');
    });
    
    $('#ic-suka').on('click', function() {
        var _t = $(this);
        $.ajax({
            url: '{{ route("product-favorite") }}',
            method: 'POST',
            data: "product_id={{ $product->id }}&_token={{ csrf_token() }}",
            success: function(e) {
                if(e.error) {
                    window.location.href='{{ route('signin') }}';
                }else if(e.stat) {
                    _t.toggleClass('red');
                } 
            }
        });
    });
});
</script>
@stop
