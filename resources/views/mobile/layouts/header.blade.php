<div id="menuMobile">
    <div class="m-mobileMenuFloat-side"><a id="triggerSlide" class="m-tombolMobileFloat-side"><i class="material-icons ikonMenuMobile-f">menu</i></a></div>
    <div class="m-navMenu-side">
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('static-page', ['url' => 'cara-kerja']) }}">Cara Kerja</a>
        @if (Auth::check())
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('dashboard') }}">dashboard </a>
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('signout') }}">keluar </a>
        @else
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('signin') }}">masuk </a>
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('signup') }}">daftar </a>
        @endif
    </div>
    <form class="m-divInput-Side src-form">
        <div class="m-divHeaderInput">
            <div class="dropdown open m-dropTemukan-side">
                <a class="btn btn-default dropdown-toggle tombolDropdownSide" data-toggle="dropdown" aria-expanded="true" role="button" href="#">TEMUKAN BISNIS<span class="caret"></span></a>
                <input type='hidden' name='src-by' id='src-by' class='src-by' value='{{$search_by or ''}}'>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <li role="presentation" class='presentation'><a class="text-uppercase" href="javascript:void(0)" data-value="bisnis">Produk</a></li>
                    <li role="presentation" class='presentation'><a class="text-uppercase" href="javascript:void(0)" data-value="produk">Bisnis</a></li>
                </ul>
            </div>
        </div>
        <div class="m-divInputSide-produk">
            <input type="text" placeholder="Misal: hotel" class="m-textInputServis-side src-keyword" required value="{{$keyword or ''}}">
        </div>
        <div class="divInputSide-lokasiProduk">
            <input type="text" placeholder="Lokasi" class="m-textInputLokasi-side src-location" value="{{$address or ''}}">
        </div>
        <div class="m-divInput-cariSide">
            <button class="btn btn-default btn-block m-tombolTemukanSide" type="submit"> 
                <span><i class="icon ion-android-search ikonSearch"></i>TEMUKAN </span>
            </button>
        </div>
    </form>
</div>
