<!DOCTYPE html>
<html lang="id-ID">

    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
        <link rel="shortcut icon" type="image/png" href="/favicon.png"/>

        <link rel="canonical" href="{{Request::url()}}" />
        <meta name="robots" content="{{(isset($search_page)) ? 'nofollow, noindex': 'index, follow'}}" />
        @yield('seo-meta')

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        {!! Html::style('/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('/bootstrap/css/responsive.css') !!}
        {!! Html::style('/css/styles.min.css') !!}
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        {!! Html::style('/fonts/font-awesome.min.css') !!}
        {!! Html::style('/fonts/material-icons.css') !!}
        {!! Html::style('/mobile_assets/css/produk.css') !!}
        {!! Html::style('/mobile_assets/css/styles.css') !!}
        {!! Html::style('/mobile_assets/css/global.css') !!}

        <link rel="stylesheet" href="//cdn.materialdesignicons.com/1.8.36/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.css">
        @yield('style-content')
        
        <script>
            window.Laravel = <?php
            echo json_encode([
            'csrfToken' => csrf_token(),
            ]);
            ?>
        </script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', '{{env('GA')}}', 'auto');
            ga('send', 'pageview');

        </script>
        <!-- Piwik -->
        <script type="text/javascript">
          var _paq = _paq || [];
          /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
          _paq.push(['trackPageView']);
          _paq.push(['enableLinkTracking']);
          (function() {
            var u="//192.168.33.10/piwik/";
            _paq.push(['setTrackerUrl', u+'piwik.php']);
            _paq.push(['setSiteId', '1']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
          })();
        </script>
        <!-- End Piwik Code -->
    </head>
    <body>
        

        @yield('header')   
        @yield('content')

        <div class="divArtikelFooter-produk">
            <div class="container-fluid footer-introProduk">
                @yield('footer-company-profile')
                @include('frontend.layouts.main_footer')
            </div>
        </div>

        @include('frontend.layouts.mobile_footer')

        {!! Html::script('/js/jquery.min.js') !!}
        {!! Html::script('/bootstrap/js/bootstrap.min.js') !!}
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
        {!! Html::script('/mobile_assets/js/jquery.mobile.js') !!}
        {!! Html::script('/mobile_assets/js/kostum.js') !!}
        @yield('script-content')
    </body>
</html>
