<header class="m-headerNav">
    <div class="row m-rowHeaderNor">
        <div class="m-div-logo">
            <div>
                <a href="{{ route('index') }}">
                    <img src="{!! asset('mobile_assets/img/logo-yp.png ') !!}" class="logoWebsite" />
                </a>
            </div>
        </div>
        <div class="m-navbarMenu">
            <div class="m-mobileMenu"><a id="trigger" class="m-tombolMobile"><i class="material-icons ikonMenuMobile-f">menu</i></a></div>
        </div>
    </div>
</header>
