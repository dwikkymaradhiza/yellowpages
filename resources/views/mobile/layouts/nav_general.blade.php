<div class="m-menuNavbar">
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
            <div>
                <a href="{{ URL('/') }}">
                    <img src="{!! asset('mobile_assets/img/logo-yp.png ') !!}" class="logoWebsiteFloat" />
                </a>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-10 col-xs-10 m-navbarMenuFloat">
            <div class="m-mobileMenuFloat"><a id="trigger-normal" class="m-tombolMobileFloat"><i class="material-icons m-ikonMenuMobile-f">menu</i></a></div>
        </div>
    </div>
</div>
