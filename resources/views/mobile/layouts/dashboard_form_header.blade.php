@section('style-content')
{!! Html::style('/css/main.min.css') !!}
{!! Html::style('/css/mobile/styles.css') !!}
{!! Html::style('/mobile_assets/css/produk.css') !!}
{!! Html::style('/css/global.css') !!}
{!! Html::style('/css/select2.css') !!}

@endsection
<style>
    body {
        background: #FFF;
    }
</style>
<!-- header -->

<header class="nav_header2" style="position: fixed; top: 0; width: 100%; z-index: 1;">
    <div class='row m-bungkus'>
        <div class='col-xs-1 col-sm-1' onclick="javascript:history.back()">
            <img src='{{ asset('mobile_assets/img/arrow-kiri.png') }}' width='12px'style='margin-top:10px;'>
        </div>
        <div class='col-xs-10 col-sm-10'>
            <h2 class="text-uppercase text-center m-judulmain">{{ $form_title }}</h2>
        </div>
    </div>
</header>