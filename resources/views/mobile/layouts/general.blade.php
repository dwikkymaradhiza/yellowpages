<!DOCTYPE html>
<html lang="id-ID">

<head>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#2f2f2f">
    <meta charset="utf-8">
    <link rel="canonical" href="{{Request::url()}}" />
    <meta name="robots" content="{{(isset($search_page)) ? 'nofollow, noindex': 'index, follow'}}" />
    @yield('seo-meta')
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    {!! Html::style('/assets/css/bootstrap.min.css') !!}
    {!! Html::style('/assets/css/font-awesome.min.css') !!}
    {!! Html::style('/assets/css/fonts/ionicons.min.css') !!}
    {!! Html::style('/assets/css/fonts/material-icons.css') !!}
    {!! Html::style('/assets/css/materialdesignicons.min.css') !!}
    {!! Html::style('/assets/css/lightbox.min.css') !!}
    {!! Html::style('/assets/css/swiper.min.css') !!}
    {!! Html::style('/assets/css/flowpage.css') !!}
    {!! Html::style('/assets/css/produk.css?v=1') !!}
    {!! Html::style('/assets/css/mobile_styles.css') !!}
    {!! Html::style('/assets/css/additional.css') !!}
    @yield('style-content')

    <script>
        window.Laravel = <?php
        echo json_encode([
                'csrfToken' => csrf_token(),
        ]);
        ?>
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '{{env('GA')}}', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Piwik -->
    <script type="text/javascript">
      var _paq = _paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="//192.168.33.10/piwik/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <!-- End Piwik Code -->
</head>

<body {!! isset($is_homepage) ? 'class="homepage"' : ''  !!}>
@include('mobile.layouts.header')

@yield('header')
@yield('content')

@include('mobile.layouts.footer')
{!! Html::script('/assets/js/jquery-1.12.4.min.js') !!}
{!! Html::script('/assets/js/lightbox.min.js') !!}
{!! Html::script('/assets/js/swiper.jquery.min.js') !!}
{!! Html::script('/assets/js/bootstrap.min.js') !!}
{!! Html::script('/assets/js/jquery.mobile.js') !!}
{!! Html::script('/assets/js/kostum.js') !!}
{!! Html::script('/assets/js/location.js') !!}
{!! Html::script('/assets/js/jquery-ui.min.js') !!}
<!--script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.js"></script-->

<script>
    
    $(".presentation").on('click',function(){
        $("#src-by").val($(this).text());
        $(".tombolDropdownSide").html($(this).text());
    })

    $('.src-form').submit(function(e) {
            e.preventDefault();
            searchAction(e, $(this))
        })
        
    $('.m-tombolTemukanSide').click(function(e) {
            e.preventDefault;
            searchAction(e, $(this))
        })
    
    function searchAction(e, obj) {
        e.preventDefault()
        kw = $(obj).closest('form').find('.src-keyword').val();
	if(!kw){kw = 'hotel'}
        loc = $(obj).closest('form').find('.src-location').val();
        if(loc === ''){
            //if empty get localcity
            if(typeof localStorage.getItem('localCity') !== 'undefined' && localStorage.getItem('localCity')){
                loc = localStorage.getItem('localCity')
            }else{
                loc = '';
            }
        }

        by = $(obj).closest('form').find('.src-by').val();

        if(kw == ''){
            return false;
        }
        
        if (!by) {
            by = 'BISNIS';
        }

        $('.src-keyword').val(kw);
        $('.src-location').val(loc);
        $('.src-by').val(by);

        if(kw || location) {
            url = '{{ route('search-full', ['search_by' => '', 'keyword' => '', 'location' => '']) }}' + '/' +
                    by.toLowerCase() + '/' + kw.replace(' ', '-') + '/' + loc.replace(' ', '-');
            window.location.href=url
        }
    }

    $(".src-location").on('input', function () {
        var requestData = {
            'city': $(".src-location").val(),
        };
        var urlAPI = "{{route('getAllCity')}}";
        $.ajax({
            headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
            data: requestData,
            url: urlAPI,
            type: 'post',
            beforeSend: function () {
                //beginTransition();
            },
            complete: function () {
                //endLoadingMapTransition();
            },
            success: function (data) {
                $(".src-location").autocomplete({
                    source: data
                });
            }
        });
    })

    $( ".src-location" ).autocomplete({
        open: function( event, ui ) {
            $('#ui-id-1').addClass('sidebar')
            $('#ui-id-1').width($('.src-location').width()+10)
            $('#ul-id-1').css('top', ($('.src-location').position().top + $('.src-location').height()))
            $('#ul-id-1').css('left', ($('.src-location').position().left + 40))
        }
    });
</script>

@yield('script-content')
</body>

</html>
