@section('style-content')
{!! Html::style('/css/main.min.css') !!}
{!! Html::style('/css/mobile/styles.css') !!}
{!! Html::style('/mobile_assets/css/produk.css') !!}
{!! Html::style('/css/global.css?v=1') !!}
{!! Html::style('/css/select2.css') !!}

@endsection
<style>
    body {
        background: #FFF;
    }
</style>
<!-- header -->

<div id="menuMobile">
    <div class="m-mobileMenuFloat-side"><a id="triggerSlide" class="m-tombolMobileFloat-side"><i class="material-icons ikonMenuMobile-f">menu</i></a></div>
    <div class="m-navMenu-side">
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('dashboard') }}">Dashboard</a>
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('dashboard-directory-index') }}">Bisnis</a>
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('dashboard-product-index') }}">Produk</a>
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('dashboard-inquiry-index') }}">Inquiry</a>
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('dashboard-status-index') }}">Status</a>
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('dashboard-setting-index') }}">Pengaturan</a>
        <a class="text-uppercase m-navbarLink-Sidebar" href="{{ route('signout') }}">Keluar</a>
    </div>
</div>
<header class="nav_header2">
    <div class="row m-rowHeaderNor">
        <div class="m-div-logo">
            <div>
                <a href="{{ route('index') }}">
                    <img src="{{ URL::asset('/img/logo-yp-3.png') }}" class="logoWebsite" />
                </a>
            </div>
        </div>
        <div class="m-navbarMenu">
            <div class="m-navMenu"><a class="text-uppercase m-navbarLink" href="#">How It Work?</a><a class="text-uppercase m-navbarLink" href="#">masuk </a><a class="text-uppercase m-navbarLink" href="#">daftar </a></div>
            <div class="m-mobileMenu"><a id="trigger"><i class="material-icons ikonMenuMobile-f">menu</i></a></div>
        </div>
    </div>
</header>
