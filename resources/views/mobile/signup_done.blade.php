@extends('mobile.layouts.general')

@section('style-content')
{!! Html::style('/mobile_assets/css/registration.css') !!}
@stop

@section('header')
@include('mobile.layouts.nav_general')
@stop

@section('content')
<div class="full-block-white">
    <div class="signup-mobile signup-method" style="min-height:calc(75vh - 60px);">
        <h1 class="text-uppercase title-signup">Terima Kasih</h1>
        <center class="signed-already">Terima kasih, sudah menyelesaikan proses pendaftaran.<br />Silahkan kembali ke <a href="{{route('index')}}">beranda</a> atau kunjungi halaman <a href="{{ route('directory', session('wizard_dir_slug')) }}">bisnis Anda</a></center>
    </div>
</div>
@stop

@section('script-content')
@stop