@extends('mobile.layouts.general')

@section('style-content')
{!! Html::style('/mobile_assets/css/registration.css') !!}
@stop

@section('header')
@include('mobile.layouts.nav_general')
@stop

@section('content')
<div class="full-block-white">
    <div class="signup-mobile signup-method" style="min-height:calc(75vh - 60px);">
        <h1 class="text-uppercase title-signup">Pendaftaran Berhasil</h1>
        <center class="signed-already">Silahkan cek email anda untuk melakukan proses selanjutnya.<br />Kembali ke <a href="{{route('index')}}">beranda</a></center>
    </div>
</div>
@stop

@section('script-content')
@stop