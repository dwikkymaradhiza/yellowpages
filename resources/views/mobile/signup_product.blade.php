@extends('mobile.layouts.general')

@section('style-content')
{!! Html::style('/mobile_assets/css/registration.css') !!}
@stop

@section('header')
@include('mobile.layouts.nav_general')
@stop

@section('content')
    <div class="full-block-white">
        <div class="signup-mobile signup-business">
            <h1 class="text-uppercase title-signup">TAMBAHKAN Produk ANDA</h1>
            <p class="signed-already">Tambahkan informasi produk yang Anda jual di bisnis Anda. <a href="{{ url('/directory/'.session('wizard_dir_slug')) }}"> Klik di sini</a> apabila anda ingin melewati proses ini dan menambakan produk belakangan.</p>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))

                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div> <!-- end .flash-message -->
            <div class="flex-box">
                <div class="flex-100">
                    <form id="main-form" action="{{ route('signup_product_post') }}" method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input type="hidden" name="directory_id" value="{{session('wizard_dir_id')}}" />
                        <div class="form-input-group">
                            <input type="text" class="input-text-signup input-text-space" name="name" value="{{old('name')}}" placeholder="Nama produk">
                            <!--input type="text" class="input-text-signup input-text-space" name="prod_cat" placeholder="Kategori produk"-->
                            <input type="number" class="input-text-signup input-text-space" name="stock" value="{{old('stock')}}" placeholder="Stok">
                            <textarea class="input-text-signup input-text-space textare-signup" name="description" placeholder="Tulis deskripsi produk">{{old('description')}}</textarea>
                            <a href="#" class="btn-upload" id="main-image-button"><i class="mdi mdi-camera" aria-hidden="true"></i> <span>Unggah foto utama produk</span></a>
                            <input type="file" class="hidden" name="image" id="image" />
                        </div>
                        
                        <div class="btn-bottom-signup">
                            <button type="submit" class="btn-primary-signup text-uppercase">Lanjut</button>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
@stop

@section('script-content')
    <script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
    <link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <script src="/js/messages_id.js"></script>
    <script>
        $('.btn-upload').click(function(e) {
            e.preventDefault();
            $('#image').trigger('click');
        })
        $('#image').change(function() {
            $('#main-image-button span').html($('#image').val());
        })

        $("#main-form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 10
                },
                description: {
                    required: true,
                    minlength: 100
                },
                stock: {
                    number: true
                }
            },
            messages: {}
        })
    </script>
@endsection