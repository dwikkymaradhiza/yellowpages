@extends('mobile.layouts.general')

@section('seo-meta')
    @include('seo.homepage')
@stop

@section('header')
@include('mobile.layouts.nav_homepage')

<div class="container-fluid m-heroWebsite">
    <div class="m-headingHero">
        <h1 class="text-center typewriter m-heroText">
            <span class='banner-main-text0'></span>
            <label class="typewrite banner-dynamic-text0" data-period="2000" data-type=''></label>
            <span class="banner-main-text1"></span>
            <label class="typewrite banner-dynamic-text1" data-period="2000" data-type=''></label>
        </h1>
        <button class="btn btn-default m-buttonHero" type="button" onclick="window.location.href='{{ route('signup') }}'">DAFTAR GRATIS</button>
    </div>
</div>
@stop

@section('content')

@if(count($featured_directory_array))
<div class="m-koroselProdukMobile">
    <h2 class="text-uppercase text-center m-bisnisPilihan">bisnis pilihan</h2>
    <div class="m-divBisnis-touch directory-container-impression">
        @foreach($featured_directory_array as $featured_directory_Detail)
        <div class="m-tumbMobile promote-directory" data-slug="{{ $featured_directory_Detail->slug }}">
            <a href="{{route('directory',$featured_directory_Detail->slug)}}" data-link="{{route('directory-click',$featured_directory_Detail->slug)}}" class="promote">
                <div>
                    @if($featured_directory_Detail->logo)
                        <img src="{{ asset('uploads/directories/thumb_'.$featured_directory_Detail->logo) }}" class="m-gambarMobile-tumb" alt="{{$featured_directory_Detail->name}}"
                             onerror="this.src='{{asset('uploads/directories/'.$featured_directory_Detail->logo) }}'; this.onerror=null;" />
                    @else
                        <img src="{{URL::asset('img/img-default.jpg')}}" class="m-gambarMobile-tumb" alt="{{$featured_directory_Detail->name}}" />
                    @endif
                </div>
                <div class="info">
                    <h5 class="m-namaBisnis-slide">{{$featured_directory_Detail->name}}</h5>
                    <p class="m-lokasiBisnis-slide">{{$featured_directory_Detail->address_name}}</p>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endif

@if(count($featured_product_array))
<div class="m-koroselProdukMobile">
    <div class="container m-galeriProduk">
        <div class="intro">
            <h2 class="text-uppercase text-center m-bisnisPilihan">produk pilihan</h2></div>
        <div class="m-divBisnis-touch product-container-impression" >
            @foreach($featured_product_array as $product_detail)
            <a href="{{route('product-detail',$product_detail->slug)}}" data-slug="{{ $product_detail->slug }}" class="promote-product promote" data-link="{{route('product-detail-click',$product_detail->slug)}}">
                <div class="m-tumbMobile">
                    @if($product_detail->picture)
                        <img class="m-gambarMobile-tumb" src="{{asset('uploads/products/thumb_'.$product_detail->picture)}}"
                                onerror="this.src='{{asset('uploads/products/'.$product_detail->picture) }}'; this.onerror=null;">
                    @else
                        <img class="m-gambarMobile-tumb" src="{{asset('img/img-default.jpg')}}">
                    @endif
                    <div class="info">
                        <h5 class="m-namaBisnis-slide">{{$product_detail->name}}</h5>
                        <p class="m-lokasiBisnis-slide">{{$product_detail->directory_name}}</p>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</div>
@endif

<div class="m-seksi-kategoriBisnis m-koroselProdukMobile">
    <div class="container m-div-kategoriBisnis">
        <div class="m-introKategori">
            <h2 class="text-uppercase m-judulKategoriBisnis">kategori populer</h2></div>
        <div class="row m-rowKategoriBisnis">
            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','hotel-motel')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-hotel.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Hotel </h2></a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','industry')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-industri.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Industri </h2></a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','property-building-materials')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-properti.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Properti </h2></a>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','information-technology')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-komputer.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Komputer </h2></a>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','automotive')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-otomotif.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Otomotif </h2>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','transportation-communications')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-travel.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Travel </h2></a>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','electric-electronic')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-radio.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Elektronik </h2></a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','finance')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-duit.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Keuangan </h2></a>
            </div>
            
            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','travel-bureaus')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-toga.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Edukasi </h2></a>
            </div>
            
            <div class="col-md-12 col-sm-12 col-xs-12 m-kolomKategoriBisnis">
                <a href="{{route('directory-category','restaurants-1')}}" class="m-div-ItemBisnis">
                    <div class="m-divGambarIcon"><img src="mobile_assets/img/ic-resto.png" class="m-gbr-ikon"></div>
                    <h2 class="m-nama-ikonBisnis">Restoran </h2></a>
            </div>
        </div>
    </div>
</div>

<div class="m-seksi-TigaKolom">
    <div class="m-tabMobile">
        <ul class="nav nav-tabs tab-latest">
            @php
            $sel_active = false;
            @endphp

            @if(count($newest_directory))
            <li class="active"><a class="text-uppercase" href="#tab-1" role="tab" data-toggle="tab">bisnis baru</a></li>
            @php $sel_active = true; @endphp
            @endif

            @if(count($newest_directory))
            <li {!! (!$sel_active) ? 'class="active"' : '' !!}><a class="text-uppercase" href="#tab-2" role="tab" data-toggle="tab">produk baru</a></li>
            @php $sel_active = true; @endphp
            @endif

            @if(count($newest_directory))
            <li {!! (!$sel_active) ? 'class="active"' : ''!!}><a class="text-uppercase" href="#tab-3" role="tab" data-toggle="tab">Update bisnis</a></li>
            @php $sel_active = true; @endphp
            @endif
        </ul>
        <div class="tab-content">
            @if(count($newest_directory))
            <div class="tab-pane fade in active" role="tabpanel" id="tab-1">
                <div class="m-kolomTabKonten">
                    <div class="m-ListBisnis">
                        @foreach($newest_directory as $newest_directory_detail)
                        <p class="m-NamaBisnis-A"><a href="{{route('directory',$newest_directory_detail->slug)}}">{{$newest_directory_detail->name}} </a></p>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            @if(count($newest_product))
            <div class="tab-pane fade" role="tabpanel" id="tab-2">
                <div class="m-kolomTabKonten">
                    <div class="m-listProduk">
                        @foreach($newest_product as $newest_product_detail)
                        <p class="m-NamaBisnis-A"><a href="{{route('product-detail',$newest_product_detail->slug)}}">{{$newest_product_detail->name}} </a></p>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            <div class="tab-pane fade" role="tabpanel" id="tab-3">
                <div class="m-kolomTabKonten">
                    <div class="m-listUpdate">
                        @foreach($directory_info as $directory_info_detail)
                        <p class="m-UpdateTerbaru-C"><a href="{{ route('directory', $directory_info_detail->slug) }}">{{$directory_info_detail->message}} </a></p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(count($affiliate_article))
<div class="m-koroselProdukMobile">
    <div class="m-introNewsUpdate">
        <h2 class="text-uppercase m-judulKategoriBisnis">artikel terbaru</h2></div>
    <div class="m-koroselNewArtikel">
        <div class="m-artikelBlok">
            @foreach($affiliate_article as $affiliate_article_detail)
            <a href="{{$affiliate_article_detail->full_url}}" target='_blank' class="m-NewsUpdate-mobile">
                <div class="photo m-fotoNewsUpdate">
                    <img src="{{ asset('uploads/affiliate/'.$affiliate_article_detail->image_url) }}" title="{{$affiliate_article_detail->title}}" />
                </div>
                <div class="m-info-newUpdate">
                    <h4 class="m-judulNewsUpdate">{{$affiliate_article_detail->title}} </h4>
                    <p class="m-desk-NewsUpdate">{{ strip_tags($affiliate_article_detail->headline) }} </p>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</div>
@endif

@stop

@section('script-content')
    <script>
        var main_banner = @php echo json_encode($all_banner); @endphp

            var image = JSON.parse(main_banner['image']);

        var main_banner_image_count = image.length;

        var random = Math.floor(Math.random() * main_banner_image_count);
        var random_image = image[random];

        var main_text = JSON.parse(main_banner['main_text']);
        var dynamic_text = JSON.parse(main_banner['dynamic_text']);

        for (var index in main_text) {
            $(".banner-main-text" + index).html(main_text[index]);
        }

        for (var index in main_text) {
            $(".banner-main-text" + index).html(main_text[index]);
        }

        for (var index in dynamic_text) {
            $(".banner-dynamic-text" + index).attr('data-type', JSON.stringify(dynamic_text[index]));
        }

        $(".m-heroWebsite").css('background-image', 'linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.33)), url("img/' + random_image + '")');

    $('.directory-container-impression').on("scrollstart", function() {
        $('.promote-directory').each(function() {
            var dirOffset = $(this).offset();
            if((dirOffset.left) <= $('.directory-container-impression').width()) {
                if(!$(this).hasClass('impression')) {
                    $.ajax({ 
                        url: '{{ route("directory-impression") }}/' + $(this).data('slug'),
                        type: 'POST',
                        data: "_token={{ csrf_token() }}"
                    });

                    $(this).addClass('impression');
                }
            }
        });
    });
    
    $('.product-container-impression').on('scrollstart', function() {
        $('.promote-product').each(function() {
            var dirOffset = $(this).offset();
            if((dirOffset.left) <= $('.product-container-impression').width()) {
                if(!$(this).hasClass('impression')) {
                    $.ajax({ 
                        url: '{{ route("product-detail-impression") }}/' + $(this).data('slug'),
                        type: 'POST',
                        data: "_token={{ csrf_token() }}"
                    });

                    $(this).addClass('impression');
                }
            }
        });
    });
    
    $('.promote-directory').each(function() {
        var dirOffset = $(this).offset();
        if(dirOffset.left < $('.directory-container-impression').width()) {
            if(!$(this).hasClass('impression')) {
                $.ajax({ 
                    url: '{{ route("directory-impression") }}/' + $(this).data('slug'),
                    type: 'POST',
                    data: "_token={{ csrf_token() }}"
                });

                $(this).addClass('impression');
            }
        }
    });
    
    $('.promote-product').each(function() {
        var dirOffset = $(this).offset();
        if(dirOffset.left < $('.product-container-impression').width()) {
            if(!$(this).hasClass('impression')) {
                $.ajax({ 
                    url: '{{ route("product-detail-impression") }}/' + $(this).data('slug'),
                    type: 'POST',
                    data: "_token={{ csrf_token() }}"
                });

                $(this).addClass('impression');
            }
        }
    });
    </script>
@stop
