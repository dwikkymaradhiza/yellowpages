@extends('mobile.layouts.general')

@section('style-content')
{!! Html::style('/mobile_assets/css/registration.css') !!}
@stop

@section('header')
@include('mobile.layouts.nav_general')
@stop

@section('content')
    <div class="full-block-white">
	    <div class="signup-mobile signup-business">
	    	<h1 class="text-uppercase title-signup">TAMBAHKAN BISNIS ANDA</h1>
            <p class="signed-already">Untuk memulai, tambahkan informasi salah satu bisnis anda. Jika Anda memiliki lebih dari 1 bisnis, Anda bisa menambahkannya nanti.</p>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))

                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div> <!-- end .flash-message -->
	    	<div class="flex-box">
	    		<div class="flex-100">
                    <form class="form-horizontal" id="main-form" action="{{ route('signup_directory_post') }}" method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-input-group">
    	    				<input type="text" class="input-text-signup input-text-space" name="name" value="{{ old('name') }}" placeholder="Nama bisnis">
                            <select data-placeholder="Kategori Bisnis" class="input-text-signup input-text-space auto-category" multiple="multiple" name="categories[]"></select>
                            <input type="number" class="input-text-signup input-text-space" name="phone_1" value="{{ old('phone_1') }}" placeholder="Nomor Telepon / Handphone">
                            <select class="input-text-signup input-text-space auto-province" name="province_id">
                                <option value="">Provinsi</option>
                                @foreach($provinces as $province)<option value="{{$province['id']}}" {{ (old('province_id') == $province['id']) ? 'selected' : '' }}>{{$province['name']}}</option>@endforeach
                            </select>
                            <select class="input-text-signup input-text-space auto-city" name="city_id">
                                <option value="">Kota</option>
                            </select>
                            <div class="map-signup">
                                <input id="pac-input" class="input-text-signup input-text-space" type="text" placeholder="Alamat" style="margin-top: 0px;position:absolute;" name="address_1" value={{ old('address_1') }}>
                                <div id="map" style="height:295px;"></div>
                            </div>
                            <textarea class="input-text-signup input-text-space textare-signup" placeholder="Tulis deskripsi bisnis" name="description">{{ old('description') }}</textarea>
                            <a href="#" class="btn-upload" id="main-image-button"><i class="mdi mdi-camera" aria-hidden="true"></i> <span>Unggah Foto Utama Bisnis</span></a>
                            <small>Maks. ukuran: 2MB</small>
                            <input type="hidden" name="latlng" id="latlng" value="{{ old('latlng') }}" />
                            <input type="file" class="hidden" name="image" id="image" />
	    				</div>

                        <div class="btn-bottom-signup">
                            <button type="submit" class="btn-primary-signup text-uppercase">Lanjut</button>
                        </div>
	    			</form>
	    		</div>
	    	</div>
	    	
	    </div>
    </div>
@stop

@section('script-content')
    <script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
    <link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <script src="/js/messages_id.js"></script>
    <script>
        $('.btn-upload').click(function(e) {
            e.preventDefault();
            $('#image').trigger('click');
        })
        $('#image').change(function() {
            $('#main-image-button span').html($('#image').val());
        })

        map = {}
        input = {}
        searchBox = {}

        function initAutocomplete() {
            function placeMarker(location) {
                console.log(location);
                $('#latlng').val(location.lat() + ';' + location.lng());
                markers.push(new google.maps.Marker({
                    position: location,
                    map: map
                }));
            }

            @if(old('latlng'))
            @php list($lat, $lng) = explode(';', old('latlng')); @endphp
            map = new google.maps.Map(document.getElementById('map'), {
                center: new google.maps.LatLng({{ $lat }}, {{ $lng }}),
                zoom: 13,
                mapTypeId: 'roadmap',
                mapTypeControl: false
            });
            @endif

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -6.1753924, lng: 106.8249587},
                zoom: 13,
                mapTypeId: 'roadmap',
                mapTypeControl: false
            });

            // Create the search box and link it to the UI element.
            input = document.getElementById('pac-input');
            searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    $('#latlng').val(place.geometry.location.lat() + ';' + place.geometry.location.lng())
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });

            google.maps.event.addListener(map, 'click', function(event){
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];
                placeMarker(event.latLng);
            })
        }

        $(".auto-province, .auto-city").select2();

        var urlApiCategory = "{{route('directory-categories')}}";
        $(".auto-category").select2({
            ajax: {
                url: function (params) {
                    return urlApiCategory + '/' + params.term;
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
        });
        $('.auto-province').on('change', function(e) {
            $.ajax({
                url: '/ajax/city/' + $('.auto-province').val() + '/option',
                dataType: 'json',
                success: function(d) {
                    $('.auto-city').select2('destroy').html(d.data).select2()
                }
            })
        })

        @if(old('province_id'))
        $('.auto-province').trigger('change');
        @endif

        $("#main-form").validate({
                    rules: {
                        name: {
                            required: true,
                            minlength: 10
                        },
                        'categories[]': {
                            required: true
                        },
                        phone_1: {
                            required: true,
                            minlength: 5,
                            number: true
                        },
                        description: {
                            required: true,
                            minlength: 100
                        },
                        province_id: "required",
                        city_id: "required",
                        address_1: {
                            required: true,
                            minlength: 20
                        }
                    },
                    messages: {}
                });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GMAPS_KEY')}}&libraries=places&callback=initAutocomplete&language=id" async defer></script>
@endsection