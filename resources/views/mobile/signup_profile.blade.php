@extends('mobile.layouts.general')

@section('style-content')
{!! Html::style('/mobile_assets/css/registration.css') !!}
@stop

@section('header')
@include('mobile.layouts.nav_general')
@stop

@section('content')
    <div class="full-block-white">
	    <div class="signup-mobile signup-profile">
	    	<h1 class="text-uppercase title-signup">LENGKAPI DATA DIRI ANDA</h1>
	    	<div class="flex-box">
	    		<div class="flex-100">
	    			<form id="main-form" role="form" method="POST" action="{{ route('signup_profile_post') }}">
	    				{!! csrf_field() !!}
                        @if(!$user_info->email)
                            <input type="email" required class="input-text-signup input-text-space" name="email" placeholder="Alamat Email" value="" />
                        @endif
	    				<input type="text" required class="input-text-signup input-text-space" name="first_name" placeholder="Nama depan" value="{{ $user_info->first_name or '' }}" />
                        <input type="text" required class="input-text-signup input-text-space" name="last_name" placeholder="Nama belakang" value="{{ $user_info->last_name or '' }}" />
                        @if(!session('social_user', FALSE))
                        <input type="password" class="input-text-signup input-text-space" placeholder="Password" name="password" id="password" />
                        <input type="password" class="input-text-signup input-text-space" placeholder="Password" id="password2" name="confirm_password" />
                        @endif
                        <input type="number" class="input-text-signup input-text-space" name="number" required placeholder="Nomor Handphone" value="{{ $user_info->handphone or '' }}" />
                        <select class="form-control auto-province input-text-space" name="province_id" required>
                            <option value="">Provinsi</option>
                            @foreach($provinces as $province)
                            <option value="{{$province['id']}}">{{$province['name']}}</option>
                            @endforeach
                        </select>
                        <select class="form-control auto-city input-text-space" name="city_id" required>
                            <option value="">Kota</option>
                        </select>
                        <textarea class="input-text-signup input-text-space textare-signup" name="address" placeholder="Alamat lengkap" required minlength="20"></textarea>
	    				
                        <div class="btn-bottom-signup">
                            <button type="submit" class="btn-primary-signup text-uppercase">Lanjut</button>
                        </div>
	    			</form>
	    		</div>
	    	</div>
	    	
	    </div>
    </div>
@stop

@section('script-content')
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="/js/messages_id.js"></script>
<script>
$(".auto-province, .auto-city").select2();
$('.auto-province').on('change', function(e) {
    $.ajax({
            url: '/ajax/city/' + $('.auto-province').val() + '/option',
            dataType: 'json',
            success: function(d) {
                $('.auto-city').select2('destroy').html(d.data).select2()
            }
        })
})

$("#main-form").validate({
    rules: {
        password: {
            required: true,
            minlength: 5
        },
        confirm_password: {
            required: true,
            minlength: 5,
            equalTo: "#password"
        },
        first_name: "required",
        last_name: "required",
        handphone: {
            required: true,
            minlength: 5,
            number: true
        },
        province_id: "required",
        city_id: "required",
        address: {
            required: true,
            minlength: 20
        }
    }
});
</script>
@stop