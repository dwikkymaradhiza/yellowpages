@extends('mobile.layouts.dashboard')


@section('header')
    @include('mobile.layouts.dashboard_header')
@stop

@section('content')
    <div class="m-maincontent" id="m-nomargin-top2">

        <div class='m-bungkus'>
            <h2 class="text-uppercase m-judulmain">Bisnis Anda</h2>
        </div>

        <div class="m-div-produkBtn">
            <a class="btn btn-info m-btn-query" type="button"
               href="{{ route('dashboard-directory-create-product-mobile') }}"><img
                        src='/mobile_assets/img/playlist_add_white_18x18.png' height='15px'> TAMBAH BISNIS</a>
            <button class="btn btn-info m-btn-query-sms" type="button" id='delete-bulk-btn'><img
                        src='/mobile_assets/img/delete.png' height='15px'> HAPUS BISNIS
            </button>
        </div>

    </div>

    <div class='m-maincontent' style='padding-bottom:40px'>
        @foreach($directories as $k => $v)

            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-9'>
                        <div class='col-xs-1'>
                            <div class="checkboxFive">
                                <input type="checkbox" value="{{ $v->id }}" data-id="{{ $v->id }}" id="{{ $v->id }}" name="directory-cb"/>
                                <label for="{{ $v->id }}"></label>
                            </div>
                        </div>
                        <div class='col-xs-8'>
                            &emsp;<a href="{{ route('dashboard-directory-edit', ['id' => $v->id]) }}">
                                <span class='m-tulisan-utama-check'>{{ $v->name }}</span></a>
                            <br>
                            &emsp;<span
                                    class='m-tulisan-sub-check'>{{ str_limit($v->categories->pluck('name_lid')->implode(', '), 20) }}</span>
                        </div>
                    </div>
                    <div class="col-xs-1">
                        <a href="{{ route('directory', $v->slug) }}" target="_self"> <i class="fa fa-eye"
                                                                                        aria-hidden="true"
                                                                                        data-toggle="tooltip"
                                                                                        data-placement="right"></i></a>
                    </div>
                    <div class="col-xs-1">
                        <a href="{{ route('verify-form', $v->slug) }}" target="_self">
                            <i class="mdi mdi-briefcase-check" aria-hidden="true"></i></a>
                    </div>
                    {{--<div class="col-xs-3">--}}
                    {{--<a href="{{ route('directory', $v->slug) }}" target="_blank"> <i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i></a>--}}
                    {{--<a href="{{ route('verify-form', $v->slug) }}">--}}
                    {{--<i class="mdi mdi-briefcase-check" aria-hidden="true"></i></a>--}}
                    {{--</div>--}}
                </div>
            </div>
        @endforeach
    </div>
@stop

@section('script-content')
    {!! Html::script('/js/jquery.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $("#delete-bulk-btn").on('click', function () {
                var reply = confirm('Apakah anda akan menghapus semua data ini?')
                if (reply === false) {
                    return false;
                }
                var id = $("input[name='directory-cb']:checked").map(function () {
                    return $(this).data("id");
                }).get().join(',');
                console.log(id)

                if (id === '') {
                    alert('Please check the product');
                    return false;

                }

                var requestData = {
                    'id': id,
                };
                var urlAPI = "{{route('dashboard-directory-delete-bulk')}}";
                $.ajax({
                    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                    data: requestData,
                    url: urlAPI,
                    type: 'post',
                    beforeSend: function () {
                        //beginTransition();
                    },
                    complete: function () {
                        //endLoadingMapTransition();
                    },
                    success: function (data) {
                        if (data === 'success') {
                            location.reload();
                        } else {
                            alert('failed');
                        }

                    }
                });
            });
        });
    </script>
@stop