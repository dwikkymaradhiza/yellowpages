<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {!! Html::style('/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('/bootstrap/css/responsive.css') !!}
    {!! Html::style('/css/styles.min.css') !!}
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    {!! Html::style('/fonts/font-awesome.min.css') !!}
    {!! Html::style('/fonts/material-icons.css') !!}
    {!! Html::style('/mobile_assets/css/flowpage.css') !!}
    <link rel="stylesheet" href="http://cdn.materialdesignicons.com/1.8.36/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    {!! Html::style('/mobile_assets/css/produk.css') !!}
    {!! Html::style('/mobile_assets/css/styles.css') !!}
    {!! Html::style('/mobile_assets/css/global.css') !!}
    {!! Html::style('/css/select2.css') !!}
    {!! Html::style('/css/main.min.css') !!}

</head>

<body>
<header class="nav_header2">
    <div class='row m-bungkus'>
        <div class='col-xs-1 col-sm-1'>
            <a href="/dashboard/directories">
                <img src='/mobile_assets/img/arrow-kiri.png' width='12px' style='margin-top:10px;'>
            </a>
        </div>
        <div class='col-xs-10 col-sm-10'>
            <h2 class="text-uppercase text-center m-judulmain">Ubah Direktori</h2>
        </div>
    </div>
</header>
<div class="m-bungkus-bisnis-baru">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->
</div>
<form method='post' id="directoryForm" action="{{ route('dashboard-directory-update', ['id' => $directory->id]) }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    {{method_field('POST')}}
    <input type="hidden" name="latlng" id="latlng" value="{{ $directory->latitude }};{{ $directory->longitude }}"/>
    <input type="hidden" id="id" name="id" value="{{ $directory->id }}"/>

    <div class='col-xs-12 m-bungkus-bisnis-baru'>
        <div class="col-xs-12">
            <label class="m-paket-setting-main">Nama Bisnis*</label>
            <input type='text' id="name" name="name" value="{{ $directory->name }}"
                   class='form-control m-setting-inputan'
                   placeholder='Nama Bisnis'/>
        </div>

        <div class="col-xs-12 col-sm-12	col-md-12">
            <label class="m-paket-setting-main">Kategori Bisnis*</label>
            <input name="categories" id="categories" value="{{ $directory->categories->implode('id', ",") }}"
                   type="hidden"/>
            <select required class="form-control category-directory" id="categories-id" multiple>
                @foreach($categories as $ck => $cv)
                    <option value="{{ $cv->id }}">{{ $cv->name_lid }}</option>@endforeach
            </select>
        </div>

        <div class="col-xs-12">
            <label class="m-paket-setting-main">Nomor Telepon*</label>
            <input type='text' id="phone" name="phone" value="{{ $directory->phone_1 }}"
                   class='form-control m-setting-inputan'
                   placeholder='Nomor Telepon'/>
        </div>

        <div class="col-xs-6 col-sm-6">
            <label class="m-paket-setting-main">Provinsi*</label>
            <select required class="form-control auto-province" name="province_id">
                <option value="">Provinsi</option>
                @foreach($provinces as $province)
                    <option value="{{$province['id']}}">{{$province['name']}}</option>@endforeach
            </select>
        </div>

        <div class="col-xs-6 col-sm-6">
            <label class="m-paket-setting-main">Kota*</label>
            <select required class="form-control auto-city" id="city_id" name="city_id">
                <option value="">Kota</option>
            </select>
        </div>

        <div class="bungkus_map col-xs-12 col-sm-12">
            <input id="pac-input" class="pac-controls m-setting-inputan form-control" type="text" name="address"
                   placeholder="Alamat" value="{{ $directory->address_1 }}">
            <div id="map"></div>
        </div>

        <div class='col-xs-12'>
            <input type="file" name="logo" id="file" class="inputfile primaryFile"/>
            <label for="file"><i class="material-icons m-ikonbutton-gambar">image</i>
                <span id="logoText"> Gambar Utama *(Ukuran Maksimum 2 MB)</span>
            </label>
        </div>

        <div class="col-xs-12">
            <div class="col-md-6">
                <img src="{{ URL::to('/') . "/uploads/directories/thumb_" . $directory->logo }}"/>
            </div>
        </div>
        <div class='col-xs-12'>
            <input type="file" name="image[]" multiple id="fileExtra" class="inputfile extraFile"/>
            <label for="fileExtra"><i class="material-icons m-ikonbutton-gambar">image</i>
                <span id="multipleImageLabel">Gambar *(Ukuran Maksimum 2MB)</span>
            </label>
        </div>

        <div class="col-xs-12">
            <label class="m-paket-setting-main">Deskripsi Bisnis</label>
            <textarea id="description" id="description" name="description" rows=6 class="form-control m-setting-inputan"
                      placeholder="Masukan Deskripsi bisnis anda">{{ $directory->description_lid }}</textarea>
        </div>
    </div>

    <button type='submit' class='btn btn-info btn-block m-btn-create'>
        <i class="material-icons m-ikonbutton">save</i> SIMPAN
    </button>
</form>

<div class="m-bungkus-bisnis-baru">
    <div class="table-responsive">
        <table class="table-fill">
            <thead>
            <tr>
                <th class="text-center">Gambar</th>
                <th class="text-center">Opsi</th>
            </tr>
            </thead>
            <tbody class="table-hover">
            @if(count($pictures) > 0)
                @foreach($pictures as $k => $v)
                    <tr>
                        <td class="text-center" width="80%"><img width="30%"
                                                                 src="{{ URL('/') . '/uploads/directories/' . $v->picture_file }}"/>
                        </td>
                        <td class="text-center">
                            <div class="col-md-4" style="padding: 0px">
                                <form method="POST"
                                      action="{{ route('dashboard-directory-picture-delete', ['id' => $v->id]) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button onclick="return window.confirm('Apakah anda ingin menghapus gambar ini?')"
                                            class="btn-yellow btn-hapus" style="border:none">
                                        <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip"
                                           title="Hapus" data-placement="right"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" style="text-align:center">Belum ada gambar</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>



{!! Html::script('/js/jquery.min.js') !!}
{!! Html::script('/bootstrap/js/bootstrap.min.js') !!}
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{!! Html::script('/js/script.min.js') !!}
{!! Html::script('/mobile_assets/js/jquery.mobile.js') !!}
{!! Html::script('/mobile_assets/js/kostum.js') !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $("#directoryForm").validate();
        $('#fileExtra').change(function () {
            var fileName = $(this).val().replace(/C:\\fakepath\\/i, '');
            $('#multipleImageLabel').text(fileName)
        })

        $('.primaryFile').change(function () {
            var fileName = $(this).val().replace(/C:\\fakepath\\/i, '');
            $('#logoText').text(fileName);
        });
        var urlApiCategory = "{{route('directory-categories')}}";
        $(".category-directory").select2({
            ajax: {
                url: function (params) {
                    return urlApiCategory + '/' + params.term;
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
        });
        $(".auto-category, .auto-province, .auto-city").select2();

        $('.auto-city').on('select2:select', function (e) {
            $("#city_id").val($('.auto-city').val());
        });

        $('.auto-province').on('select2:select', function (e) {
            $.ajax({
                url: '/ajax/city/' + $('.auto-province').val() + '/option',
                dataType: 'json',
                success: function (d) {
                    $('.auto-city').select2('destroy').html(d.data).select2();
                }
            });
        });

        //Set default city
        $.ajax({
            url: '/ajax/city/' + '{{ $directory->city->province_id }}' + '/option',
            dataType: 'json',
            success: function (d) {
                $('.auto-city').select2('destroy').html(d.data).select2();
                $('.auto-city').val('{{ $directory->city_id }}').trigger("change");
            }
        });

        //Set default province
        $('.auto-province').val('{{ $directory->city->province_id }}').trigger("change");

        //Set default categories
        var categoriesValue = '{{ $directory->categories->implode("id",",") }}';
        try {
            var selectedValues = categoriesValue.split(',');
        } catch (e) {
            var selectedValues = "" + categoriesValue + "";
        }

        $("#categories-id").val(selectedValues).trigger("change");

        $(".category-directory").on("select2:select select2:unselect", function (e) {
            //this returns all the selected item
            var items = $(this).val();
            $("#categories").val(items.join(','));
        });
    });

    function initAutocomplete() {
        function placeMarker(location) {
            $('#latlng').val(location.lat() + ';' + location.lng());
            markers.push(new google.maps.Marker({
                position: location,
                map: map
            }));
        }

        //Set default map point
        var latlngVal = $("#latlng").val().split(';');
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(latlngVal[0], latlngVal[1]),
            zoom: 13,
            mapTypeId: 'roadmap',
            mapTypeControl: false
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(latlngVal[0], latlngVal[1]),
            map: map,
            title: '{{ $directory->address_1 }}'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };
                // Create a marker for each place.
                $('#latlng').val(place.geometry.location.lat() + ';' + place.geometry.location.lng());
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);

            google.maps.event.addListener(map, 'click', function(event){
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];
                placeMarker(event.latLng);
            });
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcE2BDUnr6CkeHkVnUZcayxGqdACDTgWw&libraries=places&callback=initAutocomplete"
        async defer></script>
</body>
</html>