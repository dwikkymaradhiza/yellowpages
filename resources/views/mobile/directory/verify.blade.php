<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {!! Html::style('/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('/bootstrap/css/responsive.css') !!}
    {!! Html::style('/css/styles.min.css') !!}
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    {!! Html::style('/fonts/font-awesome.min.css') !!}
    {!! Html::style('/fonts/material-icons.css') !!}
    {!! Html::style('/mobile_assets/css/flowpage.css') !!}
    <link rel="stylesheet" href="http://cdn.materialdesignicons.com/1.8.36/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    {!! Html::style('/mobile_assets/css/produk.css') !!}
    {!! Html::style('/mobile_assets/css/styles.css') !!}
    {!! Html::style('/mobile_assets/css/global.css') !!}
    {!! Html::style('/css/select2.css') !!}
</head>

<body>
<header class="nav_header2">
    <div class='row m-bungkus'>
        <div class='col-xs-1 col-sm-1'>
            <a href="/dashboard/directories">
                <img src='/mobile_assets/img/arrow-kiri.png' width='12px' style='margin-top:10px;'>
            </a>
        </div>
        <div class='col-xs-10 col-sm-10'>
            <h2 class="text-uppercase text-center m-judulmain">Ubah Produk</h2>
        </div>
    </div>
</header>
<div class="m-bungkus-bisnis-baru">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->
</div>
<form method='post' action="" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input id="directory_id" name="directory_id" type="hidden" value="{{$directory->id}}">

    <div class='col-xs-12 m-bungkus-bisnis-baru'>
        <p style="color: #b3b3b3; margin:10px">Silahkan isi form di bawah dengan melampirkan foto SIUP / TDP &amp; KTP.</p>
        <div class="col-xs-12">
            <label class="m-paket-setting-main">Nama Bisnis*</label>
            <input type="text" value="{{$directory->name }}" class="form-control" readonly/><br/>
        </div>
    <div class='col-xs-12'>
        <input type="file" name="siup" id="file" class="inputfile primaryFile"/>
        @if(!isset($verify->file_name_siup))
            <label for="file"><i class="material-icons m-ikonbutton-gambar">image</i>
                <span id="logoText"> Foto SIUP / TDP (Ukuran Maksimum 2 MB)</span>
            </label>
        @else
            <img src="{{asset('uploads/verification/'.$verify->file_name_siup)}}" class="img-responsive" />
        @endif
    </div>
    <div class='col-xs-12'>
        @if(!isset($verify->file_name_ktp))
            <input type="file" name="ktp" multiple id="fileExtra" class="inputfile extraFile"/>
        @else
            <img src="{{asset('uploads/verification/'.$verify->file_name_ktp)}}" class="img-responsive" />
        @endif
        <label for="fileExtra"><i class="material-icons m-ikonbutton-gambar">image</i>
            <span id="multipleImageLabel">Foto KTP Pemilik (Ukuran Maksimum 2MB)</span>
        </label>
    </div>
    </div>
    @if($verify)
        <div class="form-group row">
            <div class="col-md-12">
                <label for="firstName">Status Terakhir</label>
                <ol class="history-logs">
                    @foreach($logs as $k => $v)
                        <li>{{$v['msg']}}<span class="date">{{\Carbon\Carbon::parse($v['time'])->diffForHumans()}}</span></li>
                    @endforeach
                </ol>
            </div>
        </div>
    @endif
    @if(!$verify)
        <button type='submit' class='btn btn-info btn-block m-btn-create'>
            <i class="material-icons m-ikonbutton">save</i> SIMPAN
        </button>
    @endif
</form>

{!! Html::script('/js/jquery.min.js') !!}
{!! Html::script('/bootstrap/js/bootstrap.min.js') !!}
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{!! Html::script('/js/script.min.js') !!}
{!! Html::script('/mobile_assets/js/jquery.mobile.js') !!}
{!! Html::script('/mobile_assets/js/kostum.js') !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#file').change(function () {
            var fileName = $(this).val().replace(/C:\\fakepath\\/i, '');
            $('#logoText').text(fileName);
        });

        $('#fileExtra').change(function () {
            var fileName = $(this).val().replace(/C:\\fakepath\\/i, '');
            $('#multipleImageLabel').text(fileName)
        })

        $("#directoryForm").validate();
    });

</script>

</body>
</html>