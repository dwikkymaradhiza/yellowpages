@extends('mobile.layouts.general')

@section('style-content')
{!! Html::style('/mobile_assets/css/registration.css') !!}
@stop

@section('header')
@include('mobile.layouts.nav_general')
@stop

@section('content')
<div class="full-block-white">
    <div class="signup-mobile signup-method">
        <h1 class="text-uppercase title-signup">Lupa Sandi Akun YellowPages Anda?</h1>
        <div class="flex-box">
            <div class="flex-100">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form role="form" action="{{ url('/password/email') }}" method="POST" id="main-form">
                    {!! csrf_field() !!}
                    <input type="email" required class="input-text-signup" name="email" value="{{ old('email') }}" placeholder="Masukkan email anda">
                    @if ($errors->has('email'))
                        <label id="email-error" class="error" for="email">{{ $errors->first('email') }}</label>
                    @endif
                    <button type="submit" class="btn-social btn-imel">
                        <i class="mdi mdi-email-outline" aria-hidden="true"></i>Reset Password
                    </button>
                </form>

                <p class="signed-already">Sudah punya akun YellowPages? <a href="{{route('signin')}}">Masuk di sini.</a></p>
            </div>
        </div>
    </div>
</div>
@stop

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="/js/messages_id.js"></script>
<script>
$("#main-form").validate({
    rules: {
        email: {
            required: true,
            email: true,
            remote: {
                url: '/ajax/check_email',
                type: 'post',
                data: {
                    _token: window.Laravel.csrfToken,
                    must_exists: true
                }
            }
        }
    },
    messages: {
        email: {
            remote: "Email tidak terdaftar"
        }
    }
});
</script>
@stop