@extends('mobile.layouts.general')

@section('style-content')
{!! Html::style('/mobile_assets/css/registration.css') !!}
@stop

@section('header')
@include('mobile.layouts.nav_general')
@stop

@section('content')
<div class="full-block-white">
    <div class="signup-mobile signin-method">
        <h1 class="text-uppercase title-signup">MASUK AKUN YELLOWPAGES</h1>
        <p class="signed-already">Belum punya akun YellowPages? <a href="{{route('signup')}}">Daftar di sini.</a></p>
        <div class="flex-box">
            <div class="flex-100">
                <a href="{{ route('social_redirect', ['social' => 'facebook']) }}" class="btn-social btn-fesbuk"><i class="mdi mdi-facebook" aria-hidden="true"></i>Masuk menggunakan Facebook</a>
                <a href="{{ route('social_redirect', ['social' => 'google']) }}" class="btn-social btn-gugel"><i class="mdi mdi-google" aria-hidden="true"></i>Masuk menggunakan Google</a>

                <div class="strike"><span>ATAU</span></div>

                <form role="form" action="{{ route('signin_post') }}" method="POST" id="main-form">
                    {!! csrf_field() !!}
                    <input type="email" required class="input-text-signup input-text-space" name="email" placeholder="Masukkan email anda" value="{{old('email')}}">
                    @if ($errors->has('email'))
                    <label id="email-error" class="error" for="email">{{$errors->first('email')}}</label>
                    @endif
                    <input type="password" required class="input-text-signup" name="password" placeholder="Password anda">
                    @if ($errors->has('password'))
                    <label id="password-error" class="error" for="password">{{$errors->first('password')}}</label>
                    @endif
                    <span class="clearfix"></span>
                    <label class="checkbox-signin">
                        <input class="ingat-saya" type="checkbox" value="remember-me" id="rememberMe" name="remember">Ingat saya
                    </label>
                    <button type="submit" class="btn-social btn-imel">
                        <i class="mdi mdi-email-outline" aria-hidden="true"></i>Masuk menggunakan email
                    </button>
                </form>

                <p class="forgot-pass"><a href="{{route('forgot')}}">Lupa password?</a></p>
            </div>
        </div>
        
    </div>
</div>
@stop

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="/js/messages_id.js"></script>
<script>
    $(document).ready(function() {
        $("#main-form").validate();
    });
</script>
@stop
