@extends('mobile.layouts.general')

@section('style-content')
{!! Html::style('/mobile_assets/css/registration.css') !!}
@stop

@section('header')
@include('mobile.layouts.nav_general')
@stop

@section('content')
<div class="full-block-white">
    <div class="signup-mobile signup-method">
    	<h1 class="text-uppercase title-signup">Bergabung dengan YellowPages</h1>
    	<div class="flex-box">
    		<div class="flex-100">
    			<a href="{{ route('social_redirect', ['social' => 'facebook']) }}" class="btn-social btn-fesbuk"><i class="mdi mdi-facebook" aria-hidden="true"></i>Lanjutkan menggunakan Facebook</a>
    			<a href="{{ route('social_redirect', ['social' => 'google']) }}" class="btn-social btn-gugel"><i class="mdi mdi-google" aria-hidden="true"></i>Lanjutkan menggunakan Google</a>

    			<div class="strike"><span>ATAU</span></div>

    			<form role="form" action="{{ route('signup_email') }}" method="POST" id="main-form">
    				{!! csrf_field() !!}
    				<input type="email" required class="input-text-signup" name="email" placeholder="Masukkan email anda" value="{{old('email')}}">
    				@if ($errors->has('email'))
                    <label id="email-error" class="error" for="email">{{$errors->first('email')}}</label>
                    @endif
    				<button type="submit" class="btn-social btn-imel">
    					<i class="mdi mdi-email-outline" aria-hidden="true"></i>Daftar menggunakan email
    				</button>
    			</form>

    			<p class="signed-already">Sudah punya akun YellowPages? <a href="{{route('signin')}}">Masuk di sini.</a></p>

    			<p class="signed-already">
    				Dengan mendaftar saya menyatakan menyetujui, <a href="{{ route('static-page', 'syarat-ketentuan') }}" target="_blank">Ketentuan Layanan</a> dan <a href="{{ route('static-page', 'kebijakan-privasi') }}" target="_blank">Kebijakan Privasi</a></p>
    		</div>
    	</div>
    </div>
</div>
@stop

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="/js/messages_id.js"></script>
<script>
    $(document).ready(function() {
        $("#main-form").validate({
        	rules: {
        		email: {
		            required: true,
		            email: true,
		            remote: {
		                url: '/ajax/check_email',
		                type: 'post',
		                data: {
		                    _token: window.Laravel.csrfToken,
		                }
		            }
		        }
        	},
		    messages: {
		        email: {
		            remote: "Email sudah pernah terdaftar"
		        }
		    } 
        });
    });
</script>
@stop