@extends('mobile.layouts.dashboard')

@section('style-content')
    {!! Html::style('/css/select2.css') !!}
    {!! Html::style('/bootstrap/css/bootstrap.min.css') !!}

@stop

@section('header')
    @include('mobile.layouts.dashboard_header')
@stop

@section('content')
    <div class="m-bungkus-bisnis-baru">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                             data-dismiss="alert"
                                                                                             aria-label="close">&times;</a>
                    </p>
                @endif
            @endforeach
        </div> <!-- end .flash-message -->
    </div>
    <form method='post' id="settings-form" action="{{route('dashboard-setting-edit')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}">

        <div class='col-xs-12 m-bungkus-bisnis-baru'>
            <div class="col-xs-12">
                <label class="m-paket-setting-main">Nama Depan</label>
                <input type="text" required class="form-control" name="first_name" value="{{ $user->first_name }}">
            </div>

            <div class="col-xs-12">
                <label class="m-paket-setting-main">Nama Belakang</label>
                <input type="text" required class="form-control" name="last_name" value="{{ $user->last_name }}">
            </div>

            <div class="col-xs-12">
                <label class="m-paket-setting-main">Nomor Telepon/Mobile</label>
                <input type="text" required class="form-control" name="handphone" value="{{ $user->handphone }}">
            </div>

            <div class="col-xs-12">
                <label class="m-paket-setting-main">Email</label>
                <input required type="text" class="form-control" name="email" value="{{ $user->email }}">
            </div>

            <div class="col-xs-12 col-sm-6">
                <label class="m-paket-setting-main">Provinsi*</label>
                <select required class="form-control auto-province" name="province" id="province">
                    <option value="">Provinsi</option>
                    @foreach($province_all as $province_all_detail)
                        <option value="{{$province_all_detail->id}}"
                        @if($province_all_detail->id === $user->province_id)
                            {{'selected'}}
                                @endif
                        >{{$province_all_detail->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-xs-12 col-sm-6">
                <label class="m-paket-setting-main">Kota*</label>
                <select required class="form-control auto-city" id="city" name='city'>
                    @foreach ($city_all as $city_all_detail)
                        <option value="{{$city_all_detail->id}}"
                        @if($city_all_detail->id === $user->city_id)
                            {{'selected'}}
                                @endif
                        >{{$city_all_detail->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-xs-12">
                <label for="firstName">Alamat </label>
                <textarea required class="form-control" style="height: 100px" name="address">{{ $user->address }}</textarea>
            </div>
        </div>
        <button type='submit' class='btn btn-info btn-block m-btn-create'>
            <i class="material-icons m-ikonbutton">save</i> SIMPAN
        </button>
    </form>
@stop

@section('script-content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://select2.github.io/dist/js/select2.full.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#province, #city").select2();

            $("#province").on('change', function () {
                var requestData = {
                    'province': $("#province").val(),
                };
                var urlAPI = "{{route('getAllCityByProvince')}}";
                $.ajax({
                    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                    data: requestData,
                    url: urlAPI,
                    type: 'post',
                    beforeSend: function () {
                        //beginTransition();
                    },
                    complete: function () {
                        //endLoadingMapTransition();
                    },
                    success: function (data) {
                        var template_html = '';

                        for (index in data) {
                            template_html += '<option value="' + data[index]['id'] + '">' + data[index]['name'] + '</option>';
                        }

                        $("#city").html(template_html);

                    }
                });
            });
            $("#settings-form").validate();
        });
    </script>
@stop