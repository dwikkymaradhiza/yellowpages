@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_form_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container" style="margin-top: 35px;">

    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="alert alert-success" style="border-radius: 0">
        <ul>
            <li>Anda akan mengirim <b class="count_phone_number"></b> SMS atau pilih jumlah SMS yang dikirimkan.</li>
        </ul>
    </div>

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content-3">
        <form >
            {{ csrf_field() }}
            <ul class="prd-ls">
                @foreach($packages as $package)
                <li>
                    <div class="col-xs-9">
                        <div class="row">{{ $package->package_name }}</div>
                    </div>
                    <div class="col-xs-3">
                        <div class="row"><input id="amount-{{ $package->id }}" data-amount="100000" type="number" placeholder="x 0" class="topup-input" value="0"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                @endforeach
            </ul>

            <div class="col-xs-12" style="margin-top: -15px;">
                <p>Anda akan melakukan blast sebanyak <b id="total"></b><b> SMS</b>, jumlah yang harus dibayarkan <b>Rp </b><b id="total-price"></b>.</p>
            </div>

            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a id="simpan"><div class="col-xs-6 btn-back-step">SIMPAN</div></a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    function format_number(n) 
    {
        return n.toFixed(0).replace(/./g, function(c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
        });
    }
    $(document).ready(function() {

        var custom_package = true;
        var total_custom_package = 0;
        var count = 0;

        // init amount
        @foreach($packages as $package)
        var amount{{ $package->id }} = 0;
        @endforeach

        // init quantity
        @foreach($packages as $key => $package)
            if(localStorage.getItem('amount{{ $key }}') != 'null')
                amount{{ $package->id }} = localStorage.getItem('amount{{ $key }}');
            else
                amount{{ $package->id }} = 0;
            $( "#amount-{{ $package->id }}" ).val(localStorage.getItem('amount{{ $key }}'));
            //console.log($("#amount-{{ $package->id }}").val());
            total_custom_package += parseInt(amount{{ $package->id }});
            console.log(amount{{ $package->id }});
            console.log(parseInt(amount{{ $package->id }}));
            console.log(total_custom_package);
        @endforeach

        console.log(total_custom_package);

        var price = {{ $sms_price }};

        if(total_custom_package > 0)
        {
            count = localStorage.getItem('count_sms');
            $(".count_phone_number").text(parseInt(localStorage.getItem('count_sms')));
            $( "#total" ).text(parseInt(localStorage.getItem('count_sms')));
            $( "#total-price" ).text(parseInt(localStorage.getItem('total_price')));
        }
        else
        {
            count = localStorage.getItem('count_phone_number');

            //console.log(count);
            $(".count_phone_number").text(count);
            $("#total").text(format_number(parseInt(count)));
            $("#total-price").text(format_number(parseInt(count * price)));
        }

        
        @foreach($packages as $package)
        $( "#amount-{{ $package->id }}" ).change(function() {
            amount{{ $package->id }} = $("#amount-{{ $package->id }}").val();

            count = 0;
            var total_price = 0
            @foreach($packages as $package)
                count += amount{{ $package->id }}*{{ $package->description }};
                total_price += amount{{ $package->id }}*{{ $package->price }};

                //console.log(count);
                //console.log(total_price);
            @endforeach

            $("#total").text(format_number(parseInt(count)));
            $("#total-price").text(format_number(total_price));
        });
        @endforeach

        $( "#simpan" ).click(function() {
            localStorage.setItem('count_sms', count);
            localStorage.setItem('total_price', count * price);

            localStorage.setItem('count_package', {{ count($packages) }});
            //console.log(localStorage.getItem('count_package'));

            @foreach($packages as $key => $package)
            localStorage.setItem('amount{{ $key }}', amount{{ $package->id }});
            localStorage.setItem('package{{ $key }}', {{ $package->id }});
            //console.log('amount{{ $key }}');
            //console.log(localStorage.getItem('amount{{ $key }}'));
            @endforeach

            history.go(-1);
        });
        
    });
</script>
@endsection
