@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Jenis Paket SMS</h1>
        <p>Silakan pilih jenis paket Targeted SMS yang sesuai dengan promo Anda.</p>
    </div>
    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content">
        <form method="POST" action="{{ route('topup-payment-method') }}">
            {{ csrf_field() }}
            <ul class="prd-ls">
            	<a href="{{route('dashboard-sms-blast')}}">
	                <li>
	                    <div class="col-xs-12">
	                        <div class="row"><h4>SMS Blast</h4></div>
	                    </div>
	                    <div class="clearfix"></div>
	                </li>
                </a>
                <a href="{{route('dashboard-sms-targeted')}}">
	                <li class="sms-li-2">
	                    <div class="col-xs-12">
	                        <div class="row"><h4>SMS Targeted</h4></div>
	                    </div>
	                    <div class="clearfix"></div>
	                </li>
                </a>
                <a href="{{route('dashboard-sms-call-history')}}">
	                <li class="sms-li-2">
	                    <div class="col-xs-12">
	                        <div class="row"><h4>SMS Call History</h4></div>
	                    </div>
	                    <div class="clearfix"></div>
	                </li>
                </a>
            </ul>

         
            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-12 btn-back-step">KEMBALI</div></a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    $(document).ready(function() {
        $(".order_id").val(localStorage.getItem('order_id'));
        
        var calculate = function() {
            var totalOrderAmount = 0;
            $('.topup-input').each(function() {
                var value = parseInt($(this).val());
                var amount = parseInt($(this).data('amount'));
                totalOrderAmount += parseInt(amount * value);
            });
            
            $('.order_balance_input').val(totalOrderAmount);
            $('.order-balance').html(totalOrderAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
        };
        
        calculate();
        $('.topup-input').on('keyup', function(e) {
            if(parseInt($(this).val()) < 0){
                $(this).val(0);
            }
            
            calculate();
        });
    });
</script>
@endsection
