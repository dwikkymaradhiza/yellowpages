@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<div class="m-maincontent"id="m-nomargin-top2">
	
    <div class='m-bungkus'>
        <h2 class="text-uppercase m-judulmain">Update Status Anda</h2>
    </div>

    <div class="m-div-produkBtn">
        <button onclick="window.location='{{ route('dashboard-status-add') }}'" class="btn btn-info m-btn-query-sms" type="button" id='delete'><img src='{{ URL::asset('/mobile_assets/img/playlist_add_white_18x18.png') }}' height='15px'> TULIS STATUS</button>
        <button id="delete-bulk-btn" class="btn btn-info m-btn-query-sms" type="button" id='delete'><img src='{{ asset('img/delete.png') }}' height='15px'> HAPUS STATUS</button>
    </div>

</div>

<div class='m-maincontent'style='padding-bottom:40px'>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    @if(count($infos) > 0)
        @foreach($infos as $k => $v)
            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-10'>
                        <div class='col-xs-1'>
                            <div class="">
                                <input name="inquiry-cb" data-id="{{ $v->id }}" type="checkbox" class="row-check" />
                                <label for="checkboxFiveInput"></label>
                            </div>
                        </div>
                        <div class='col-xs-9'>
                            &emsp;<span class='m-tulisan-utama-check'>{{ str_limit($v->message, 15) }}</span><br>&emsp;<span class='m-tulisan-sub-check'>Makanan</span>
                        </div>
                    </div>
                    <div class='col-xs-2' style="padding: 0">
                        <form method="POST" action="{{ route('dashboard-status-delete', ['id' => $v->id]) }}">
                            <a href="{{ route('dashboard-status-detail', ['id' => $v->id]) }}"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" title="Edit" data-placement="right"></i></a>
                            {{ csrf_field() }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <button onclick="return window.confirm('Apakah anda ingin menghapus data ini?')" style="border:none; background: transparent">
                                <i class="material-icons m-ikonprod-delete" data-toggle="tooltip" title="Hapus" data-placement="right">delete</i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    @else
    <div class="col-md-12 text-center">
        Tidak ada data.
    </div>
    @endif
    
    <div class="col-md-12">
        {{ $infos->appends(['size' => $size])->links() }}
    </div>
</div> 
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script>
    $("#delete-bulk-btn").on('click', function () {
        var reply = confirm('Apakah anda akan menghapus semua data ini?');
        if (reply === false) {
            return false;
        }
        var id = $("input[name='inquiry-cb']:checked").map(function () {
            return $(this).data("id");
        }).get().join(',');

        if (id === '') {
            alert('Please check the status');
            return false;

        }

        var requestData = {
            'id': id,
        };
        var urlAPI = "{{route('dashboard-status-delete-bulk')}}";
        $.ajax({
            headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
            data: requestData,
            url: urlAPI,
            type: 'post',
            beforeSend: function () {
                //beginTransition();
            },
            complete: function () {
                //endLoadingMapTransition();
            },
            success: function (data) {
                if (data === 'success') {
                    location.reload();
                } else {
                    alert('failed');
                }

            }
        });
    });
</script>
@endsection
