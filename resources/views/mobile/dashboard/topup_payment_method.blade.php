@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Metode Pembayaran</h1>
        <p>Pilih metode pembayaran di bawah untuk membeli saldo iklan Anda.</p>
    </div>

    <div class="promote-main-content">
        <ul class="prd-ls payment-method">
            <li>
                <i class="mdi mdi-bank"></i> Transfer Bank
                <ul class="sublist hide">
                    <li>
                        <form method="POST" action="{{ route('topup-payment-method-action', ['channel' => 'tf-bca', 'method' => 1]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_balance" value="{{ $order_balance }}" />
                            <input type="hidden" name="order_id" value="{{ $order_id }}" />
                            <button style="background: none; width: 100%; text-align: left; padding-left: 0;border:none"><img src="/img/icon-bca.png"/> Bank BCA</button>
                        </form>

                    </li>
                    <li>
                        <form method="POST" action="{{ route('topup-payment-method-action', ['channel' => 'tf-bni', 'method' => 1]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_balance" value="{{ $order_balance }}" />
                            <input type="hidden" name="order_id" value="{{ $order_id }}" />
                            <button style="background: none; width: 100%; text-align: left; padding-left: 0;border:none"><img src="/img/icon-bni.png"/> Bank BNI</button>
                        </form>
                    </li>
                </ul>
            </li>
            <li>
                <i class="mdi mdi-cursor-pointer"></i> Internet Banking
                <ul class="sublist hide">
                    <li>
                        <form method="POST" action="{{ route('topup-payment-method-action', ['channel' => 'klik-bca', 'method' => 1]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_balance" value="{{ $order_balance }}" />
                            <input type="hidden" name="order_id" value="{{ $order_id }}" />
                            <button style="background: none; width: 100%; text-align: left; padding-left: 0;border:none"><img src="/img/icon-klikbca.png"/> Klik BCA</button>
                        </form>
                    </li>
                    <li>
                        <form method="POST" action="{{ route('topup-payment-method-action', ['channel' => 'internet-mandiri', 'method' => 1]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_balance" value="{{ $order_balance }}" />
                            <input type="hidden" name="order_id" value="{{ $order_id }}" />
                            <button style="background: none; width: 100%; text-align: left; padding-left: 0;border:none"><img src="/img/icon-mandiri-internet.gif"/> Mandiri Internet</button>
                        </form>
                    </li>
                </ul>
            </li>
            <li>
                <i class="mdi mdi-store"></i> Gerai Retail / Tunai
                <ul class="sublist hide">
                    <li class="disabled">
                        <img src="/img/icon-indomaret.png"/> Indomaret
                        <!--form method="POST" action="{{ route('topup-payment-method-action', ['channel' => 'indomaret', 'method' => 1]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_balance" value="{{ $order_balance }}" />
                            <input type="hidden" name="order_id" value="{{ $order_id }}" />
                            <button style="background: none; width: 100%; text-align: left; padding-left: 0;border:none"><img src="/img/icon-indomaret.png"/> Indomaret</button>
                        </form-->
                    </li>
                    <li>
                        <form method="POST" action="{{ route('topup-payment-method-action', ['channel' => 'alfamart', 'method' => 1]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_balance" value="{{ $order_balance }}" />
                            <input type="hidden" name="order_id" value="{{ $order_id }}" />
                            <button style="background: none; width: 100%; text-align: left; padding-left: 0;border:none"><img src="/img/icon-alfamart.png"/> Alfamart</button>
                        </form>
                    </li>
                    <li>
                        <form method="POST" action="{{ route('topup-payment-method-action', ['channel' => 'pospay', 'method' => 1]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_balance" value="{{ $order_balance }}" />
                            <input type="hidden" name="order_id" value="{{ $order_id }}" />
                            <button style="background: none; width: 100%; text-align: left; padding-left: 0;border:none"><img src="/img/icon-pospay.png"/> Kantor Pos</button>
                        </form>
                    </li>
                </ul>
            </li>
            <li class="disabled">
                <i class="mdi mdi-credit-card"></i> Kartu Kredit
                <!--form method="POST" action="{{ route('topup-payment-method-action', ['channel' => 'cc', 'method' => 2]) }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="order_balance" value="{{ $order_balance }}" />
                    <input type="hidden" name="order_id" value="{{ $order_id }}" />
                    <button style="background: none; width: 100%; text-align: left; padding-left: 0;border:none"><i class="mdi mdi-credit-card"></i> Kartu Kredit</button>
                </form-->
            </li>
            <li class="disabled"><i class="mdi mdi-cellphone-basic"></i> Pulsa Telkomsel</li>
        </ul>
        <a href="#" onclick="javascript: history.go(-1)"><div class="promote-step-btn text-center">KEMBALI</div></a>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    $('.payment-method > li').click(function(){
        $('.payment-method .sublist').addClass('hide');
        _child = $(this).find('.sublist')
       if(_child.length) {
           _child.toggleClass('hide');
       }
    });
</script>
@endsection
