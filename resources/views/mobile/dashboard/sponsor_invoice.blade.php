@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Konfirmasi Pemasangan Iklan</h1>
        <p>Berikut adalah ringkasan pembelian iklan Anda. Mohon periksa terlebih dahulu sebelum melanjutkan.</p>
    </div>

    <div class="promote-main-content">
            <ul class="prd-ls">
                <li>
                    <div class="col-xs-8">
                        <div class="row">Biaya Iklan</div>
                    </div>
                    <div class="col-xs-4">
                        <div class="row text-right promote-credit"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-8">
                        <div class="row">Saldo Iklan YP</div>
                    </div>
                    <div class="col-xs-4">
                        <div class="row text-right promote-saldo" data-saldo="{{$saldo}}">{{number_format($saldo,'0',',','.')}}</div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-8">
                        <div class="row">SISA SALDO</div>
                    </div>
                    <div class="col-xs-4">
                        <div class="row text-right insufficient"></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                
                <div class="insufficient-warning">
                    <p>Maaf, saldo Anda tidak mencukupi untuk melakukan transaksi ini.</p>
                    <p>Silakan ketuk tombol LANJUTKAN untuk melakukan topup saldo iklan Anda terlebih dahulu.</p>
                </div>
            </ul>
            <div class="promote-step-btn text-center">
                <a href="{{route('sponsor-package')}}"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a href="#" onclick='createPackagesOrder();'><div class="col-xs-6">LANJUTKAN</div></a>
            </div>
        </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    var insufficient;
    
    $(document).on('ready',function(){
        var package_price = localStorage.getItem('package_price');
        if(typeof package_price !== 'undefined'){
            $('.promote-credit').html('Rp '+ package_price);
        }        
            var saldo = $('.promote-saldo').data('saldo');
        
        if(saldo >= package_price){
            insufficient = false;
            $(".insufficient-warning").hide();
        }else{
            insufficient = true;
            $(".insufficient-warning").show();
        }
        
        var total = parseInt(saldo) - parseInt(package_price);
        $(".insufficient").html('Rp ' + total);
    });
    
    function createPackagesOrder(){
        if(insufficient){
            insertPackagesOrder(0);
        }else{
            insertPackagesOrder(1);
        }
    }
    
    function insertPackagesOrder(status){
        var requestData = {
            'price' : localStorage.getItem('package_price'),
            'package_id' : localStorage.getItem('package_id'),
            'status' : status,
            'directories_id' : localStorage.getItem('directory_sponsor'),
            'products_id' : localStorage.getItem('product_sponsor'),
        };
        var urlAPI = "{{route('sponsor-invoice-update')}}";
        if(status === 0){
            var nextUrl = "{{route('topup')}}";
        }else{
            var nextUrl = "{{route('sponsor-thanks')}}"; 
        }
        
        $.ajax({
            headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
            data: requestData,
            url: urlAPI,
            type: 'post',
            beforeSend: function () {
                //beginTransition();
            },
            complete: function () {
                //endLoadingMapTransition();
            },
            success: function (data) {
                var result = JSON.parse(data);
                localStorage.setItem('order_id', result.order_id);
                
                if(result.message ==='success'){
                    localStorage.setItem('order_id', result.order_id);
                    //clear all local storage
                    localStorage.clear();
                    window.location = nextUrl;
                }else{
                    localStorage.setItem('order_id', result.order_id);
                    window.location = nextUrl;
                }                    
            },
            error: function (data){
                error.log('Email not sent');
            }
        });
    }
    
</script>
@endsection
