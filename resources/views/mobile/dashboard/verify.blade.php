@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <h2>Ubah Produk</h2>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

    <div class="row rowKontenProduk">
        <div class="table-responsive">
            <form id="main-form" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input id="directory_id" name="directory_id" type="hidden" value="{{$directory->id}}">
                <p style="color: #b3b3b3">Silahkan isi form di bawah dengan melampirkan foto SIUP / TDP & KTP.</p>
                <fieldset>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Nama Bisnis</label>
                            <input type="text" value="{{$directory->name }}" class="form-control" readonly /><br />
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="firstName">Foto SIUP / TDP</label>
                            @if(!isset($verify->file_name_siup))
                            <input type="file" name='siup' class="form-control" required="true">
                            <br>
                            <small>*Ukuran gambar maksimal 2 MB</small>
                            @else
                            <img src="{{asset('uploads/verification/'.$verify->file_name_siup)}}" class="img-responsive" />
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="firstName">Foto KTP Pemilik</label>
                            @if(!isset($verify->file_name_ktp))
                            <input type="file" name='ktp' class="form-control" required="true">
                            @else
                            <img src="{{asset('uploads/verification/'.$verify->file_name_ktp)}}" class="img-responsive" />
                            @endif
                        </div>
                    </div>
                    @if($verify)
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="firstName">Status Terakhir</label>
                            <ol class="history-logs">
                                @foreach($logs as $k => $v)
                                <li>{{$v['msg']}}<span class="date">{{\Carbon\Carbon::parse($v['time'])->diffForHumans()}}</span></li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                    @endif
                    <hr/>
                    @if(!$verify)
                    <p class="text-center">
                        <button type="submit" class="btn btn-yellow btn-simpan">SIMPAN</button>
                    </p>
                    @endif
                </fieldset>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script-content')
<script src="{{ asset('bootstrap/js/bootstrap.min') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/script.min.js') }}"></script>
<script src="{{ asset('js/messages_id.js') }}"></script>
<script>

$(document).ready(function () {
    $("#main-form").validate();
});

</script>
<style>
    .history-logs{
        margin-left: 15px;
        margin-top: 15px;
    }
    .history-logs li{
        margin-bottom: 10px;
    }
    .history-logs .date{
        float:right; color:#219fd1;
    }
</style>
@endsection
