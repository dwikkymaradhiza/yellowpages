@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_form_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">

    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content-2">
        <form>
            {{ csrf_field() }}
            <ul class="prd-ls">
                <li>
                    <div class="col-xs-6">
                        <div class="row">MULAI</div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row">
                            <input id="date_start" class="datepicker sms-date" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li id="li-date">
                    <div class="col-xs-6">
                        <div class="row">AKHIR</div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row">
                            <input id="date_end" class="datepicker2 sms-date" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

            <div class="col-xs-12 error-box">
                
            </div> 
            
            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a id="save"><div class="col-xs-6 btn-back-step">SIMPAN</div></a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script> -->
<script src="{{ URL::to('js/pickdate/legacy.js') }}"></script>
<script src="{{ URL::to('js/pickdate/picker.js') }}"></script>
<script src="{{ URL::to('js/pickdate/picker.date.js') }}"></script>
<script src="{{ URL::to('js/pickdate/picker.time.js') }}"></script>
<link rel="stylesheet" href="{{ URL::to('js/pickdate/default.css') }}">
<link rel="stylesheet" href="{{ URL::to('js/pickdate/default.date.css') }}">
<link rel="stylesheet" href="{{ URL::to('js/pickdate/default.time.css') }}">
<style type="text/css">
    th {
        background: none;
        text-shadow: none;
    }

    td, th {
        text-align: center; 
        border-right: none; 
    }

    tr {
        border-top: none; 
        text-shadow: none; 
    }

    .picker {
        line-height: 1.8;
    }

</style>
<script>
    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
            ].join('-');
    };

    Date.prototype.ddmmyyyy = function() {
        var dd = this.getMonth() + 1; // getMonth() is zero-based
        var mm = this.getDate();

        return [(dd>9 ? '' : '0') + dd,
            (mm>9 ? '' : '0') + mm,
            this.getFullYear()
            ].join('-');
    };

    $(document).ready(function() 
    {
        var date_start = localStorage.getItem('date_start');
        var date_start_not_formated = localStorage.getItem('date_start_not_formated');
        var date_end = localStorage.getItem('date_end');
        var date_end_not_formated = localStorage.getItem('date_end_not_formated');

        if(date_start_not_formated != null)
            $("#date_start").val(date_start_not_formated);
        // else
        //     $("#date_start").val(new Date());
        if(date_end_not_formated != null)
            $("#date_end").val(date_end_not_formated);
        // else
        //     $("#date_end").datepicker("setDate", new Date());

        $('#date_start').pickadate({
            format: 'dd/mm/yyyy',
            formatSubmit: 'yyyy-mm-dd'
        });
        $('#date_end').pickadate({
            format: 'dd/mm/yyyy',
            formatSubmit: 'yyyy-mm-dd'
        });

        $("#date_start").change(function() {
            var date_start = new Date($('#date_start').pickadate('picker').get('select').obj).yyyymmdd();
            var date_start_not_formated = $('#date_start').val();
            localStorage.setItem('date_start', date_start);
            localStorage.setItem('date_start_not_formated', date_start_not_formated);
        });

        $("#date_end").change(function() {
            var date_end = new Date($('#date_end').pickadate('picker').get('select').obj).yyyymmdd();
            var date_end_not_formated = $('#date_end').val();
            localStorage.setItem('date_end', date_end);
            localStorage.setItem('date_end_not_formated', date_end_not_formated);
        });

        $("#save").click(function(){
            var start = new Date($('#date_start').pickadate('picker').get('select').obj);
            var end = new Date($('#date_end').pickadate('picker').get('select').obj);

            $(".error-box").html("");
            if(start > end)
            {
                $("#li-date").addClass("li-error");
                $("#date_end").css("background", "#fdd9d6");
                $(".error-box").append("<p>Tanggal selesai tidak lebih dari tanggal mulai.</p>");
            }
            else
            {
                history.go(-1);
            }
        });
    });
</script>
@endsection
