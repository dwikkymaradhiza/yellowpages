@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Target Penerima SMS</h1>
        <p>Masukkan nomor penerima secara manual di bawah atau unggah file yang berisi daftar penerima.</p>
    </div>
    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content" style="min-height: 535px;">
        <form id="sms-blast">
            {{ csrf_field() }}
            <input type="hidden" name="uuid" value="{{ $uuid }}">
            <div class="col-xs-12">
                <textarea id="numbers" name="numbers" type="number" class="form-control sms-number-textarea" placeholder="Ketik nomor penerima, satu nomor per baris."></textarea>
            </div>
            <div class="col-xs-12">
                <label for="file-upload" class="custom-file-upload">
                    UNGGAH FILE
                </label>
                <input id="file-upload" name="file_upload" type="file"/>
            </div>

            <div class="col-xs-12" style="padding-top: 15px;">
                <p id="phone_required" class="text-error">Nomor telepon harus diisi.</p>
                <p id="must_numeric" class="text-error">Nomor telepon harus angka.</p>
            </div>


            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a id="sms-button-post" class="sms-button-post" >LANJUT</a>
            </div>
        </form>
    </div>
</div>


<div class="loader"></div>
<div class="blur"></div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<style>
    .blur
    {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(1,1,1,0.5);
    }
    .loader {
        position: fixed;
        top: 43%;
        left: 40%;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 80px;
        height: 80px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        z-index: 20;
    }

    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
</style>
<script>
    $(document).ready(function() 
    {
        $("#phone_required").hide();
        $("#must_numeric").hide();
        $(".loader").hide();
        $(".blur").hide();

        var count_phone_number = 0;
        
        localStorage.setItem('uuid', "{{ $uuid }}");
        localStorage.setItem('sms_type', "BLAST");

        $( "#sms-button-post" ).click(function() {
            //$( "#sms-blast" ).submit();
            var is_required = true;

            var length = $("#numbers").val().length;
            if(length < 1)
            {
                $("#numbers").css("border-color", "red");
                $("#phone_required").show();
                is_required = false;
            }
            else
            {
                $("#numbers").css("border-color", "rgb(169, 169, 169)");
                $("#phone_required").hide();
                is_required = true;
            }

            var content = $("#numbers").val();

            var is_number = true;

            if(content.length > 0)
            {
                for(i = 0; i < content.length; i++)
                {
                    if(content[i] != "\n" && content[i] != " ")
                    {
                        if(!$.isNumeric(content[i]))
                        {
                            $("#numbers").css("border-color", "red");
                            $("#must_numeric").show();

                            is_number = is_number && false;
                        }
                        else
                        {
                            $("#numbers").css("border-color", "rgb(169, 169, 169)");
                            $("#must_numeric").hide();
                            is_number = is_number && true;
                        }
                    } 
                }
            }

            if(is_required && is_number)
            {
                $(".loader").show();
                $(".blur").show();
                
                var requestData = {
                    "uuid" : "{{ $uuid }}",
                    "numbers" : $("#numbers").val()
                };
                
                var urlAPI = "{{route('phone-number-store')}}";
                
                $.ajax({
                    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                    data: requestData,
                    url: urlAPI,
                    type: 'post',
                    beforeSend: function () {
                        //beginTransition();
                    },
                    complete: function () {
                        //endLoadingMapTransition();
                    },
                    success: function (data) {
                        console.log(data);
                        localStorage.setItem('count_phone_number', data);
                        window.location = "{{ route('dashboard-sms-targeted-setting') }}";
                    },
                    error: function (data){
                        console.log(data);
                    }
                });
            }
        });

        $('#file-upload').on("change", function(){ 
            var requestData = {
                "uuid" : "{{ $uuid }}",
                "file-upload" : $("#file-upload").val()
            };
            
            var urlAPI = "{{route('phone-number-store')}}";
            
            $.ajax({
                headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                data: requestData,
                url: urlAPI,
                type: 'post',
                beforeSend: function () {
                    //beginTransition();
                },
                complete: function () {
                    //endLoadingMapTransition();
                },
                success: function (data) {
                    console.log(data);
                    localStorage.setItem('count_phone_number', data);
                    window.location = "{{ route('dashboard-sms-targeted-setting') }}";
                },
                error: function (data){
                    console.log(data);
                }
            });
        });
    });
</script>
@endsection
