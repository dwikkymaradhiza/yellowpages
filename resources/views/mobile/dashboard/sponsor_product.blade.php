@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Pilih Bisnis Atau Produk</h1>
        <p>Pilih bisnis atau produk yang Anda akan iklankan di Yellowpages Sponsored Business</p>
    </div>

    <div class="promote-main-content">
        <ul class="prd-ls">
            <?php if(count($directories)>0) { ?>
            @foreach($directories as $directory_detail)
            <li>
                <div class="col-xs-2 row">
                    <div class="checkbox">
                        <label class="label-checkbox">
                            <input type="checkbox" data-type="{{$directory_detail->type}}" id="{{$directory_detail->type.'_'.$directory_detail->id}}" value="{{$directory_detail->id}}" name="productcb">
                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                            
                        </label>
                    </div>
                </div>
                <div class="col-xs-10 row">
                    <label class="item-list" for="{{$directory_detail->type.'_'.$directory_detail->id}}">{{$directory_detail->product_name}}<span>{{$directory_detail->product_caption}}</span></label>
                </div>
                <div class="clearfix"></div>
            </li>
            @endforeach
            <?php } else { ?>
                <div class="col-xs-12 text-center" style="margin-top: calc(100vh - 495px);">Anda belum mempunyai bisnis atau produk. Silahkan buat bisnis anda terlebih dahulu.
                </div>
            <?php } ?> 
        </ul>
        <?php if(count($directories)>0) { ?>
        <a href="#" onclick="saveSponsorItem();return false;"><div class="promote-step-btn text-center">LANJUTKAN</div></a>
        <?php } else { ?>
            <a href="{{route('dashboard-directory-create-product-mobile')}}"><div class="promote-step-btn text-center">BUAT BISNIS</div></a>
        <?php } ?> 
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    $(document).on('ready',function(){
        var product_id = localStorage.getItem('product_sponsor');
        var directory_id = localStorage.getItem('directory_sponsor');
        

        var product_id_array = product_id.split(',');
        var directory_id_array = directory_id.split(',');

        if(product_id_array.length > 0){
            for (var index in product_id_array){
                $("#product_"+product_id_array[index]).prop('checked',true);
            }            
        }
        
        if(directory_id_array.length > 0){
            for (var index in directory_id_array){
                $("#directory_"+directory_id_array[index]).prop('checked',true);
            }            
        }
        
    });
function saveSponsorItem() {
    var product_id_array = $("input[name=productcb]:checked").map(function () {
        if ($(this).data('type') === 'product') {
            return $(this).val();
        }
    }).get().join(',');


    var directory_id_array = $("input[name=productcb]:checked").map(function () {
        if ($(this).data('type') === 'directory') {
            return $(this).val();
        }
    }).get().join(',');
    
    localStorage.setItem('product_sponsor', product_id_array);
    localStorage.setItem('directory_sponsor', directory_id_array);
    
    window.location = "{{route('sponsor-package')}}";
    
    return false;
}
</script>
@endsection
