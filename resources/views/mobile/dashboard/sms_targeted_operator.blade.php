@extends('mobile.layouts.dashboard')


@section('header')
    @include('mobile.layouts.dashboard_form_header')
@stop

@section('content')

    <div class='m-maincontent' style='padding-bottom:40px; margin-top: 35px; min-height: 650px;'>
        @foreach($operators as $operator)
            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="row">
                            <div class="checkbox">
                                <label>
                                    <input id="operator{{ $operator->id }}" name="operator[]" type="checkbox" value="{{ $operator->id }}">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    {{ $operator->name }}
                                </label>
                            </div>
                        </div>
                    </div>
                   
                 
                    
                </div>
            </div>
        @endforeach
    </div>

    <div id="error-interest" class="promote-step-btn text-center" style="position: fixed; bottom: 52px; background: rgba(244,67,54,0.9);">
        Operator harus dipilih.
    </div>

    <div class="promote-step-btn text-center" style="position: fixed; bottom: 0;">
        <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
        <a id="simpan"><div class="col-xs-6 btn-back-step">SIMPAN</div></a>
    </div>
@stop

@section('script-content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <style type="text/css">
        .checkbox label:after, 
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            float: left;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"] + .cr > .cr-icon,
        .radio label input[type="radio"] + .cr > .cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
        .radio label input[type="radio"]:checked + .cr > .cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled + .cr,
        .radio label input[type="radio"]:disabled + .cr {
            opacity: .5;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#error-interest").hide();

            var provider = [];
            
            if(JSON.parse(localStorage.getItem('provider') != null))
                provider = JSON.parse(localStorage.getItem('provider'));

            console.log(provider);

            // set checked
            for (var i = 0; i < provider.length; i++) 
            {
                $('#operator' + provider[i]).prop('checked', true);
            }

            @foreach($operators as $operator)
            $("#operator{{ $operator->id }}").change(function(e){
                if ($(this).is(":checked"))
                {
                    provider.push({{ $operator->id }});
                }
                else
                {

                    for (var i = 0; i < provider.length; i++) 
                    {
                        if(provider[i] == {{ $operator->id }})
                            provider.splice(i,1);
                    }
                }
            });
            @endforeach

             $( "#simpan" ).click(function() {
                    localStorage.setItem('provider', JSON.stringify(provider));
                    provider = JSON.parse(localStorage.getItem('provider'));

                    if(provider.length < 1)
                        $("#error-interest").show();
                    else
                        history.go(-1);
                });
            });

    </script>
@stop