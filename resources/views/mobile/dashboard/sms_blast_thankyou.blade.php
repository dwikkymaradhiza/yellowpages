@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    
    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-container">
        <div class="promote-header-full">
            <h1>Terima kasih</h1>

            <div class="top-success"><i class="mdi mdi-check"></i></div>

            <p>Terima kasih telah pembelian paket <font name="sms_type_label"></font> SMS, <font name="sms_type_label"></font> SMS akan dilakukan setelah melewati proses moderasi.</p>

            <p>Ketuk tombol di bawah untuk mengakhiri transaksi dan kembali ke halaman dashboard Anda.</p>

            <a href="{{ route('dashboard') }}"><div class="promote-step-btn text-center thankyou-btn">DASHBOARD</div></a>
        </div>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<script>
    $(document).ready(function() {
        var sms_type = localStorage.getItem('sms_type');
        console.log(sms_type);

        if(sms_type == "BLAST")
            $("[name=sms_type_label]").text("Blast");
        else if(sms_type == "TARGETED")
            $("[name=sms_type_label]").text("Targeted");

        localStorage.clear();
    });

</script>
@endsection
