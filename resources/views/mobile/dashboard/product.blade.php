@extends('mobile.layouts.dashboard')

@section('header')
{!! Html::style('/css/produk.css') !!}
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <div class="m-maincontent"id="m-nomargin-top2">

        <div class='m-bungkus'>
            <h2 class="text-uppercase m-judulmain">Produk Anda</h2>
        </div>

        <div class="m-div-produkBtn">
            <button class="btn btn-info m-btn-query" type="button" id="addnewbtn" ><img src="{{ URL::asset('/mobile_assets/img/playlist_add_white_18x18.png') }}" height='15px'> TAMBAH PRODUK</button>
            <button class="btn btn-info m-btn-query-sms" type="button" disabled="disabled" id='delete'><img src="{{ URL::asset('/mobile_assets/img/delete.png') }}" height='15px'> HAPUS PRODUK</button>
        </div>

    </div>

    <div class="row rowKontenProduk2 ">
        <div class="table-responsive">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class='m-maincontent'style='padding-bottom:40px'>
                @if(count($products) > 0)
                @foreach($products as $k => $v)
                <div class='m-baris-info'>
                    <div class='row m-forminput2'>
                        <div class='col-xs-9'>
                            <div class='col-xs-1'>
                                <div class="checkboxFive">
                                    <input type="checkbox" id="productcb_{{$k}}}}" value="{{$v->id}}" name="productcb" class='checkbox'/>
                                </div>
                            </div>
                            <div class='col-xs-9'>
                                &emsp;<span class='m-tulisan-utama-check'>{{ $v->name }}</span>
                                <br>&emsp;<span class='m-tulisan-sub-check'>{{ str_limit($v->directory->categories->pluck('name_lid')->implode(', '), 20) }}</span>
                            </div>
                        </div>
                        <div class='col-xs-1'>
                            <a href="{{ route('edit_product', ['id' => $v->id]) }}">
                                <i class="material-icons m-ikonprod-pencil">edit</i>
                            </a>
                        </div>
                        <div class='col-xs-1'>    
                            <form class="form-horizontal" action="{{route('dashboard-product-delete')}}" method="post">
                                <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="id" value="{{$v->id}}">
                                <button onclick="return window.confirm('Apakah anda ingin menghapus data ini?')" style="background:none;border:none">
                                    <i class="material-icons m-ikonprod-delete">delete</i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-md-12 text-center">
                    Tidak ada data.
                </div>
                @endif
            </div>

        </div>

    </div>
</div>
<!-- modal -->
<style>
    .modal-dialog{
        width: 900px;
    }
    @media (max-width:991px){
        .modal-dialog{
            width: 100%;
            padding: 10px;
        }
    }
</style>
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
        $(function () {
            $(".checkbox").click(function () {
                $('#delete').prop('disabled', $('input.checkbox:checked').length == 0);
            });
        });
        $("#delete").on('click', function () {
            var confirmation = confirm("Anda Yakin Akan di Hapus!");
            
            if(confirmation === true){
                var id_array = $("input[name=productcb]:checked").map(function () {
                    return $(this).val()
                }).get().join(',');

                var requestData = {
                    'id_array': id_array
                };

                var urlAPI = "{{route('delete-bulk-produk')}}"
                $.ajax({
                    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                    data: requestData,
                    url: urlAPI,
                    type: 'post',
                    beforeSend: function () {
                        //beginTransition();
                    },
                    complete: function () {
                        //endLoadingMapTransition();
                    },
                    success: function (data) {
                        console.log(data);
                        if (data === 'success') {
                            location.reload();
                        } else {
                            alert('Silakan pilih produk yang mau dihapus');
                        }

                    }
                });
            }else{
                return false;
            }
            
        })
        
        $("#addnewbtn").on('click',function(){
             window.location = "{{ route('edit_product') }}";
        })
</script>
@endsection
