@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Pengaturan SMS Blast</h1>
        <p>Silakan pilih jenis paket Targeted SMS yang sesuai dengan promo Anda.</p>
    </div>
    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content-5">
        <form>
            {{ csrf_field() }}
            <ul class="prd-ls">
            	<a class="block" href="{{route('dashboard-sms-blast-content')}}">
	                <li id="li-sms" class="sms-li-2">
	                    <div class="col-xs-10">
	                        <div class="row">
                                <h4>Isi SMS</h4>
                                <p id="sms_preview" style="color: darkgrey;"></p>
                            </div>
	                    </div>
                        <div class="col-xs-2">
                            <div class="pull-right"><h4><i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></h4></div>
                        </div>
                        
	                    <div class="clearfix"></div>
	                </li>
                </a>
                <a href="{{route('dashboard-sms-blast-quantity')}}">
                    <li id="li-quantity" class="sms-li-2">
                        <div class="col-xs-10">
                            <div class="row"><h4>Kuantitas</h4></div>
                        </div>
                        <div class="col-xs-2">
                            <div class="pull-right"><h4><i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></h4></div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </li>
                </a>
                <a href="{{route('dashboard-sms-blast-schedule')}}">
                    <li id="li-schedule" class="sms-li-2">
                        <div class="col-xs-10">
                            <div class="row"><h4>Jadwal Blast</h4></div>
                        </div>
                        <div class="col-xs-2">
                            <div class="pull-right"><h4><i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></h4></div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </li>
                </a>
                <a id="sender_url" href="">
                    <li id="li-sender" class="sms-li-2">
                        <div class="col-xs-10">
                            <div class="row"><h4>ID Pengirim</h4></div>
                        </div>
                        <div class="col-xs-2">
                            <div class="pull-right"><h4><i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></h4></div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </li>
                </a>
            </ul>

            <div class="col-xs-12 error-box">
                
            </div> 

         
            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a id="save"><div class="col-xs-6 btn-back-step">LANJUT</div></a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
            ].join('-');
    };

    $(document).ready(function() {

        var customer_id = {{ $user->id }};
        var uuid = localStorage.getItem('uuid');
        var content = localStorage.getItem('content');
        var count_sms = localStorage.getItem('count_sms');
        var total_price = localStorage.getItem('total_price');
        var date_start = localStorage.getItem('date_start');
        var date_end = localStorage.getItem('date_end');
        var sender_id = localStorage.getItem('sender_id');
        console.log(uuid);
        console.log(content);
        console.log(count_sms);
        console.log(total_price);
        console.log(sender_id);
        console.log(date_start);
        console.log(date_end);

        var sms_type = localStorage.getItem('sms_type');
        console.log(sms_type);

        var sender_url = "{{route('dashboard-sms-blast-sender')}}";
        var sender_targeted_url = "{{route('dashboard-sms-targeted-sender')}}";

        if(sms_type == "BLAST")
            $("#sender_url").attr("href", sender_url);
        else
            $("#sender_url").attr("href", sender_targeted_url);

        $("#save").click(function(){
            $(".error-box").html("");

            if(date_start == null && date_end == null)
            {
                date_start = new Date().yyyymmdd();
                date_end = new Date().yyyymmdd();
                localStorage.setItem('date_start', date_start);
                localStorage.setItem('date_end', date_end);
                console.log(date_start);
                console.log(date_end);
            }

            if(content == null)
            {
                $("#li-sms").addClass("li-error");
                $(".error-box").append("<p>Isi SMS harus diisi.</p>");
            }
            else if(count_sms == null)
            {
                $("#li-quantity").addClass("li-error");
                $(".error-box").append("<p>Jumlah SMS harus diisi.</p>");
            }
            else if(sender_id == null)
            {
                $("#li-sender").addClass("li-error");
                $(".error-box").append("<p>Sender ID harus dipilih.</p>");
            }
            else
            {
                window.location = "{{ route('dashboard-sms-blast-confirmation') }}";
            }
        });

        if(content != null)
            $("#sms_preview").text(content);
    });
</script>
@endsection
