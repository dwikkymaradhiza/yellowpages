@extends('mobile.layouts.dashboard')

@section('header')
{!! Html::style('/css/produk.css') !!}
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    <header class="nav_header2">
		<div class='row m-bungkus'>
			<div class='col-xs-1 col-sm-1'>
                            <a href="{{route('dashboard-product-index')}}"><img src="{{ URL::asset('/mobile_assets/img/arrow-kiri.png') }}" width='12px'style='margin-top:10px;'></a>
			</div>
			<div class='col-xs-10 col-sm-10'>
                            @if(count($product)===0)
				<h2 class="text-uppercase text-center m-judulmain">TAMBAH PRODUK</h2>
                            @else
                                <h2 class="text-uppercase text-center m-judulmain">EDIT PRODUK</h2>
                            @endif
			</div>
		</div>
    </header>

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

    @if(count($product)===0)
        <form id="main-form" class="form-horizontal" action="{{ route('create_product') }}" method="POST" enctype="multipart/form-data">
    @else
        <form id="main-form" class="form-horizontal" action="{{ route('update_product', ['id' => $product->id]) }}" method="POST" enctype="multipart/form-data">
    @endif
        {!! csrf_field() !!}
        <input id="product_id" name="product_id" type="hidden" value="{{isset($product->id)?$product->id:''}}">
        
        <div class='col-xs-12 m-bungkus-bisnis-baru'>
            <div class="col-xs-12">
                <label class="m-paket-setting-main">Nama Produk*</label>
                <input type="text" required id="name" class="form-control m-setting-inputan" name="name" value="{{ isset($product->name)?$product->name:'' }}" placeholder='Nama Bisnis'>
            </div>
    
            <div class="col-xs-12">
                <label class="m-paket-setting-main">Stock</label>
                <input type="text" id="stock" class="form-control m-setting-inputan" name="stock" value="{{ isset($product->stock)?$product->stock:'' }}" placeholder='Stock'>
            </div>
            
            <div class="col-xs-12">
                <label class="m-paket-setting-main">Tambah Gambar</label>
                <input type="hidden" id="picture" name="picture">
                <input type="file" class="form-control m-setting-inputan" multiple name="image[]" />
                <span>Max size: 2MB</span>
                <br>
                <div id="pic"></div>
            </div>
            
            <div class="col-xs-12">
                <label class="m-paket-setting-main">Bisnis</label>
                <select required name="directory_id" id="directory_id" class="form-control m-setting-inputan">
                    @if(count($directoriesList)>0)
                    @foreach($directoriesList as $directoriesListDetail)
                        @if(isset($product->directory_id) && $product->directory_id == $directoriesListDetail->id)
                            <option selected="selected" value="{{$directoriesListDetail->id}}">{{$directoriesListDetail->name}}</option>
                        @else
                            <option value="{{$directoriesListDetail->id}}">{{$directoriesListDetail->name}}</option>
                        @endif
                    @endforeach
                    @endif
                </select>
            </div>
            
            <div class="col-xs-12">
                <label class="m-paket-setting-main">Deskripsi Produk</label>
                <textarea required class="form-control m-setting-inputan" id="description"  name="description" placeholder="deskripsi produk">{{ isset($product->description)?$product->description:'' }}</textarea>
            </div>
            
            
            <button type='submit' class='btn btn-info btn-block m-btn-create'>
		<i class="material-icons m-ikonbutton">save</i> SIMPAN PERUBAHAN
            </button>
        </div>
        </form>
        
    @if(count($product) > 0)
        <div class="table-responsive">
            <table class="table-fill">
                <thead>
                    <tr>
                        <th class="text-center">Gambar</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody class="table-hover">
                    @if(count($pictures) > 0)
                        @foreach($pictures as $k => $v)
                        <tr>
                            <td class="text-center" width="80%"><img width="30%" src="{{ URL('/') . '/uploads/products/' . $v->picture }}" /></td>
                            <td class="text-center">
                                <div class="col-md-4" style="padding: 0px">
                                    <form method="POST" action="{{ route('dashboard-product-picture-delete', ['id' => $v->id]) }}">
                                        {{ csrf_field() }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        <button onclick="return window.confirm('Apakah anda ingin menghapus gambar ini?')" class="btn-yellow btn-hapus" style="border:none">
                                            <i class="fa fa-trash-o" aria-hidden="true" data-toggle="tooltip" title="Hapus" data-placement="right"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="2" style="text-align:center">Belum ada gambar</td>
                        </tr>
                    @endif
                </tbody>
            </table>   
        </div>
    @endif
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>

$(document).ready(function () {
    $("#main-form").validate();
});

</script>
@endsection
