@extends('mobile.layouts.dashboard')


@section('header')
    @include('mobile.layouts.dashboard_form_header')
@stop

@section('content')

    <div class='m-maincontent' style='padding-bottom:40px; margin-top: 35px; min-height: 650px;'>

            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="row">
                            <div class="checkbox">
                                <label>
                                    <input id="Senin" name="days[]" type="checkbox" value="Senin">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Senin
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="row">
                            <div class="checkbox">
                                <label>
                                    <input id="Selasa" name="days[]" type="checkbox" value="Selasa">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Selasa
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="row">
                            <div class="checkbox">
                                <label>
                                    <input id="Rabu" name="days[]" type="checkbox" value="Rabu">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Rabu
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="row">
                            <div class="checkbox">
                                <label>
                                    <input id="Kamis" name="days[]" type="checkbox" value="Kamis">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Kamis
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="row">
                            <div class="checkbox">
                                <label>
                                    <input id="Jumat" name="days[]" type="checkbox" value="Jumat">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Jumat
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="row">
                            <div class="checkbox">
                                <label>
                                    <input id="Sabtu" name="days[]" type="checkbox" value="Sabtu">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Sabtu
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="row">
                            <div class="checkbox">
                                <label>
                                    <input id="Minggu" name="days[]" type="checkbox" value="Minggu">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Minggu
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>

    <div class="promote-step-btn text-center" style="position: fixed; bottom: 0;">
        <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
        <a id="simpan"><div class="col-xs-6 btn-back-step">SIMPAN</div></a>
    </div>
@stop

@section('script-content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <style type="text/css">
        .checkbox label:after, 
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            float: left;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"] + .cr > .cr-icon,
        .radio label input[type="radio"] + .cr > .cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
        .radio label input[type="radio"]:checked + .cr > .cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled + .cr,
        .radio label input[type="radio"]:disabled + .cr {
            opacity: .5;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            
            var days = [];
            
            if(JSON.parse(localStorage.getItem('days') != null))
                days = JSON.parse(localStorage.getItem('days'));

            console.log(days);

            // set checked
            for (var i = 0; i < days.length; i++) 
            {
                $('#' + days[i]).prop('checked', true);
            }

            $("#Senin").change(function(e){
                if ($(this).is(":checked"))
                {
                    days.push("Senin");
                }
                else
                {
                    for (var i = 0; i < days.length; i++) 
                    {
                        if(days[i] == "Senin")
                            days.splice(i,1);
                    }
                }
            });
            $("#Selasa").change(function(e){
                if ($(this).is(":checked"))
                {
                    days.push("Selasa");
                }
                else
                {
                    for (var i = 0; i < days.length; i++) 
                    {
                        if(days[i] == "Selasa")
                            days.splice(i,1);
                    }
                }
            });
            $("#Rabu").change(function(e){
                if ($(this).is(":checked"))
                {
                    days.push("Rabu");
                }
                else
                {
                    for (var i = 0; i < days.length; i++) 
                    {
                        if(days[i] == "Rabu")
                            days.splice(i,1);
                    }
                }
            });
            $("#Kamis").change(function(e){
                if ($(this).is(":checked"))
                {
                    days.push("Kamis");
                }
                else
                {
                    for (var i = 0; i < days.length; i++) 
                    {
                        if(days[i] == "Kamis")
                            days.splice(i,1);
                    }
                }
            });
            $("#Jumat").change(function(e){
                if ($(this).is(":checked"))
                {
                    days.push("Jumat");
                }
                else
                {
                    for (var i = 0; i < days.length; i++) 
                    {
                        if(days[i] == "Jumat")
                            days.splice(i,1);
                    }
                }
            });
            $("#Sabtu").change(function(e){
                if ($(this).is(":checked"))
                {
                    days.push("Sabtu");
                }
                else
                {
                    for (var i = 0; i < days.length; i++) 
                    {
                        if(days[i] == "Sabtu")
                            days.splice(i,1);
                    }
                }
            });
            $("#Minggu").change(function(e){
                if ($(this).is(":checked"))
                {
                    days.push("Minggu");
                }
                else
                {
                    for (var i = 0; i < days.length; i++) 
                    {
                        if(days[i] == "Minggu")
                            days.splice(i,1);
                    }
                }
            });

           
             $( "#simpan" ).click(function() {
                    localStorage.setItem('days', JSON.stringify(days));
                    days = JSON.parse(localStorage.getItem('days'));

                    console.log(days);
                    history.go(-1);
                });
            });

    </script>
@stop