@extends('mobile.layouts.dashboard')


@section('header')
    @include('mobile.layouts.dashboard_form_header')
@stop

@section('content')

    <div class='m-maincontent' style='padding-bottom:40px; margin-top: 55px; min-height: 650px;'>
        @foreach($senders as $sender)
            <div class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="radio radio-primary">
                            <input type="radio" name="sender" id="{{ $sender->id }}" value="{{ $sender->id }}">
                            <label for="{{ $sender->id }}" class="block">
                                {{ $sender->name }}
                            </label>
                        </div> 
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="promote-step-btn text-center">
        <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
        <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">SIMPAN</div></a>
    </div>
@stop

@section('script-content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <style type="text/css">  
        
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var sender_id = localStorage.getItem('sender_id');
            $("#" + sender_id).prop("checked", true);

            $('[name="sender"]').change(function() {
                var sender_id = $('[name="sender"]').val();
                localStorage.setItem('sender_id', sender_id);
            });
        });
    </script>
@stop