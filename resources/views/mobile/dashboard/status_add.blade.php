@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_form_header')
@stop

@section('content')
@if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<form method='post' id="statusForm" action='{{ route('dashboard-status-create') }}' >
    {{ csrf_field() }}
    <input type="hidden" name="mobile" value="1" />
    <div class='col-xs-12 m-bungkus-bisnis-baru'>

        <div class='col-xs-12'>
            <label class="m-paket-setting-main">Bisnis*</label>
        </div>
        <div class="col-xs-12 col-sm-12	col-md-12">
            <select  name="directory_id" id='directory_id' class='form-control m-setting-inputan'>
                @foreach($directories as $dk => $dv)
                    <option value="{{ $dv->id }}">{{ $dv->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-xs-12">
            <label class="m-paket-setting-main">Status*</label>
            <textarea minlength="30" id="message" required name="message" rows=6 class="form-control m-setting-inputan" placeholder="Tulis status"></textarea>
        </div>
    </div>
    <button type='submit' class='btn btn-info btn-block m-btn-create'>
        <i class="material-icons m-ikonbutton">save</i> SIMPAN
    </button>
</form>
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
{!! Html::script('/mobile_assets/js/kostum.js') !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="{{ asset('js/messages_id.js') }}"></script>
<script>
    $(document).ready(function(){
        $("#statusForm").validate();
        
    });
</script>
@endsection