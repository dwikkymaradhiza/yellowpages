@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_form_header')
@stop

@section('content')
<div class='col-xs-12 m-bungkus-bisnis-baru'>
    <div class='col-xs-12'>
        <label class="m-paket-setting-main">Bisnis</label>
    </div>
    <div class="col-xs-12 col-sm-12	col-md-12">
        <div>{{ $status->directory->name }}</div>
    </div>
    <div class="col-xs-12">
        <label class="m-paket-setting-main">Status</label>
        <div>{{ $status->message }}</div>
    </div>
</div>
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
{!! Html::script('/mobile_assets/js/kostum.js') !!}
@endsection