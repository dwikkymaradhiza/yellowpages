@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_form_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">

    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content-2">
        <form method="POST" action="{{ route('dashboard-sms-blast-content-store') }}">
            {{ csrf_field() }}

            <div class="col-xs-12">
                <textarea id="content" onkeyup="countChar(this)" name="content" class="form-control sms-number-textarea" placeholder="Ketik isi SMS, maksimal 160 huruf termasuk spasi dan tanda baca."></textarea>
                <div class="col-xs-12 text-right" style="padding: 5px 0;">
                <p><font id="counter">160</font>/160</p>
                </div>
            </div>

            <div class="col-xs-12 error-box" style="margin-top: 20px;">
                
            </div> 
           
            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a id="sms-button-post"><div class="col-xs-6 btn-back-step">SIMPAN</div></a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    function countChar(val){
        var len = val.value.length;
        var counter = 160 - len;
        $("#counter").text(counter);
        if (len > 160) 
        {
            var val = $("#content").val();
            var val = val.substring(0, 160);
            $("#content").val(val);
            $("#counter").text(0);
            // document.getElementById("content").style.cssText = 'border-color:red;';
            // $("#li-sender").addClass("li-error");
            // $(".error-box").html("");
            // $(".error-box").append("<p>SMS lebih dari 160 karakter.</p>");
        }
        else if(len < 1)
        {
            document.getElementById("content").style.cssText = 'border-color:red;';
            $("#li-sender").addClass("li-error");
            $(".error-box").html("");
            $(".error-box").append("<p>Isi SMS harus diisi.</p>");
        }
        else
        {
            document.getElementById("content").style.cssText = 'border-color: rgb(169, 169, 169)';
            $(".error-box").html("");
        }
    };
    $(document).ready(function() {
        
        $( "#sms-button-post" ).click(function() {
            len = $("#content").val().length;

            var urlAPI = "{{route('dashboard-sms-bad-word')}}";
            var bad_word = false;
                    
            $.ajax({
                headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                data: { 'content' : $("#content").val() },
                url: urlAPI,
                type: 'post',
                beforeSend: function () {
                    //beginTransition();
                    console.log('ambil');
                },
                complete: function () {
                    //endLoadingMapTransition();
                },
                success: function (data) {
                   var content = JSON.parse(data);
                   console.log(data);
                   document.getElementById("content").style.cssText = 'border-color:red;';
                    $("#li-sender").addClass("li-error");
                    $(".error-box").html("");
                   for(var i = 0; i < content.length; i++)
                   {
                        console.log(content[i]['words']);
                        
                        $(".error-box").append("<p>Kata " + content[i]['words'] + " tidak boleh dipakai.</p>");
                   }

                    console.log(content.length);
                    if(content.length == 0)
                        bad_word = false;
                    else
                        bad_word = true;

                    console.log(bad_word);
                    if(len <= 160 && len > 0 && bad_word == false)
                    {
                        localStorage.setItem('content', $("#content").val());
                        history.go(-1);
                    }
                    else if(len <= 0)
                    {
                        document.getElementById("content").style.cssText = 'border-color:red;';
                        $("#li-sender").addClass("li-error");
                        $(".error-box").html("");
                        $(".error-box").append("<p>Isi SMS harus diisi.</p>");
                    }
                },
                error: function (data){
                    console.log(data);
                }
            });
            
            
        });

        var content = localStorage.getItem('content');

        if(content != null)
            $( "#content" ).text(content);

        $("#content").change(function(){
            //console.log("ganti");
        });
    });
</script>
@endsection
