@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_form_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">

    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content-2">
        <form>
            {{ csrf_field() }}
            <ul class="prd-ls" style="margin-bottom: 20px;">
                <div name="form_input">

                    <li style="padding-bottom: 0px; padding-top: 15px;">
                        <div class="col-xs-7">
                            <div class="row"><h4>Hari Percakapan</h4></div>
                        </div>
                        <div class="col-xs-5">
                            <div class="pull-right">
                                <div class="form-group">
                                    <select class="form-control" name="days">
                                        <option value="Senin">Senin</option>
                                        <option value="Selasa">Selasa</option>
                                        <option value="Rabu">Rabu</option>
                                        <option value="Kamis">Kamis</option>
                                        <option value="Jumat">Jumat</option>
                                        <option value="Sabtu">Sabtu</option>
                                        <option value="Minggu">Minggu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </li>
                    <li>
                        <div class="col-xs-9">
                            <div class="row">Waktu Percakapan (Mulai)</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input id="timepicker1" name="time_start" type="text" class="form-control input-small input-time" placeholder="HH:MM">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <li>
                        <div class="col-xs-9">
                            <div class="row">Waktu Percakapan (Akhir)</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input id="timepicker2" name="time_end" type="text" class="form-control input-small input-time">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <li style="text-align: right;">
                        <a id="remove_history" class="btn"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                    </li>
                </div>
            </ul>
            
            <div class="col-xs-12">
                <p id="add_time" style="text-align: center; color: #31b0d5;">Tambah Waktu</p>
            </div>
            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a id="save"><div class="col-xs-6 btn-back-step">SIMPAN</div></a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="{{ URL::to('js/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
<link rel="stylesheet" href="{{ URL::to('js/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
<style type="text/css">
    input {
        border: 1px solid black;
        box-shadow: none;
      }
</style>
<script>
    $(document).ready(function() {
        // var time_start = "00:00";
        // var time_end = "00:00";
        // if(localStorage.getItem('time_start') != null)
        //     var time_start = localStorage.getItem('time_start');
        // if(localStorage.getItem('time_end') != null)
        //     var time_end = localStorage.getItem('time_end');
        
        // $("#time_start").val(time_start);
        // $("#time_end").val(time_end);

        // $("#time_start").change(function() {
        //    var time_start = $("#time_start").val();
        //    localStorage.setItem('time_start', time_start);
        // });
        // $("#time_end").change(function() {
        //    var time_end = $("#time_end").val();
        //    localStorage.setItem('time_end', time_end);
        // });

        $("[name='time_start'").timepicker({
            minuteStep: 1,
            showMeridian: false,
        });

        $("[name='time_end'").timepicker({
            minuteStep: 1,
            showMeridian: false,
        });

        var days = [];
        var time_start = [];
        var time_end = [];

        if(JSON.parse(localStorage.getItem('days') != null))
        {
            days = JSON.parse(localStorage.getItem('days'));
            console.log(days);
        }

        if(JSON.parse(localStorage.getItem('time_start') != null))
        {
            time_start = JSON.parse(localStorage.getItem('time_start'));
            console.log(time_start);
        }

        if(JSON.parse(localStorage.getItem('time_end') != null))
        {
            time_end = JSON.parse(localStorage.getItem('time_end'));
            console.log(time_end);
        }

        var length = time_start.length;

        for (var i = 0; i < length-1; i++) 
        {
            $("[name='form_input']").first().clone().appendTo(".prd-ls");
        }

        for (var i = 0; i < length; i++) 
        {
            document.getElementsByName("days")[i].value = days[i];
            document.getElementsByName("time_start")[i].value = time_start[i];
            document.getElementsByName("time_end")[i].value = time_end[i];
        }


        $("#add_time").click(function()
        {
            $("[name='form_input']").first().clone().appendTo(".prd-ls");
            $("[name='time_start'").timepicker({
                minuteStep: 1,
                showMeridian: false,
            });

            $("[name='time_end'").timepicker({
                minuteStep: 1,
                showMeridian: false,
            });

            //document.getElementsByName("time_start")[0].value = "14:00";
        });

        $("#save").click(function()
        {
            var length = document.getElementsByName("time_start").length;
            
            days = [];
            time_start = [];
            time_end = [];

            for (var i = 0; i < length; i++) 
            {
                days.push(document.getElementsByName("days")[i].value);
                time_start.push(document.getElementsByName("time_start")[i].value);
                time_end.push(document.getElementsByName("time_end")[i].value);
            }

            localStorage.setItem('days', JSON.stringify(days));
            localStorage.setItem('time_start', JSON.stringify(time_start));
            localStorage.setItem('time_end', JSON.stringify(time_end));

            console.log(days);
            console.log(time_start);
            console.log(time_end);

            history.go(-1);
        });

        $('body').on('click', '#remove_history',function () {
            $(this).parent().parent().remove();   
        });

        
    });
</script>
@endsection
