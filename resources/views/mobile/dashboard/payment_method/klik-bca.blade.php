@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Transfer Bank</h1>
        <p>Silakan ikuti cara pembayaran di bawah.<br />Saldo akan otomatis bertambah setelah pembayaran diterima.</p>
    </div>

    <div class="promote-main-content">
        <form method="POST" action="{{ route('topup-payment-process', ['method' => 1]) }}">
            {{ csrf_field() }}
            <input type="hidden" name="order_balance" value="{{ $order_balance }}" />
            <input type="hidden" name="order_id" value="{{ $order_id }}" />
            <ul class="prd-ls payment-method">
                <li>
                    <div class="col-xs-4 row"><img src="{{ asset('img/icon-klikbca.png') }}" width="90" /></div>
                    <div class="col-xs-9 row">
                        Gunakan kode pembayaran untuk membayar transaksi ini. <br>Rp {{ number_format($order_balance, 0, "", ".") }}
                        <br /><br />
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-xs-12">
                        <strong>Cara Pembayaran</strong><br />
                        1. Login ke KlikBCA : <a href="http://www.klikbca.com/" target="_blank" style="color: #337ab7 !important;">klikbca.com</a><br />
                        2. Pilih menu PAYMENT.<br />
                        3. Pilih menu TELEPHONE.<br/>
                        4. Pilih operator Telkom.<br/>
                            5. Masukkan <b>“{{$paycode}}”</b> (12 digit) kode pembayaran.<br/>
                            6. Klik Next.<br/>
                            8. Klik Submit.<br/>
                        </div>
                    </div>
                </li>
            </ul>

            <div class="how-to-permata hide">
                <h2>Mangalami kendala?</h2>
                <p><a href="#">Ketuk di sini</a> untuk informasi tata cara transfer ke rekening virtual Bank Permata</p>    
            </div>

            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a href="#">
                    <div class="col-xs-6">
                        <input style="background: none;width: 100%;border:none" type="submit" value="LANJUTKAN" />
                    </div>
                </a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
</script>
@endsection
