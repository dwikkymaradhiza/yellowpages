@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Target Penerima SMS</h1>
        <p>Tambahkan penerima SMS dengan melengkapi informasi minat (interest) dan lokasi di bawah.</p>
    </div>
    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content-5">
        <form>
            {{ csrf_field() }}
            <ul class="prd-ls">
            	<a id="sms-targeted-route">
	                <li id="li-interest" class="sms-li-2">
	                    <div class="col-xs-10">
	                        <div class="row"><h4>Tambahkan Minat Penerima</h4></div>
	                    </div>
                        <div class="col-xs-2">
                            <div class="pull-right"><h4><i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></h4></div>
                        </div>
                        
	                    <div class="clearfix"></div>
	                </li>
                </a>
                <!-- <a id="sms-targeted-not-interest-route">
                    <li id="li-not-interest" class="sms-li-2">
                        <div class="col-xs-10">
                            <div class="row"><h4>Pengecualian Minat Penerima</h4></div>
                        </div>
                        <div class="col-xs-2">
                            <div class="pull-right"><h4><i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></h4></div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </li>
                </a> -->
                <a id="location-route">
                    <li id="li-location" class="sms-li-2">
                        <div class="col-xs-10">
                            <div class="row"><h4>Lokasi Penerima</h4></div>
                        </div>
                        <div class="col-xs-2">
                            <div class="pull-right"><h4><i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></h4></div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </li>
                </a>
                <a href="{{route('dashboard-sms-targeted-operator')}}">
                    <li id="li-operator" class="sms-li-2">
                        <div class="col-xs-10">
                            <div class="row"><h4>Operator Telekomunikasi</h4></div>
                        </div>
                        <div class="col-xs-2">
                            <div class="pull-right"><h4><i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></h4></div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </li>
                </a>
                
            </ul>

            <div class="col-xs-12 error-box">
                
            </div> 

            <!-- <div class="col-xs-12">
                <p>Berdasar informasi di atas, SMS akan dikirim ke <b id="receiver_count">23.168</b> <b>orang</b> penerima.</p>
            </div> -->
         
            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a id="save"><div class="col-xs-6 btn-back-step">LANJUT</div></a>
            </div>
        </form>
    </div>
</div>

<div class="loader"></div>
<div class="blur"></div>

@endsection

@section('script-content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<style>
    .blur
    {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(1,1,1,0.5);
    }
    .loader {
        position: absolute;
        top: 43%;
        left: 40%;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 80px;
        height: 80px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        z-index: 20;
    }

    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
</style>
<script>
    $(document).ready(function() {
        $(".loader").hide();
        $(".blur").hide();

        var count_phone_number = 0;
        localStorage.setItem('count_phone_number', count_phone_number);
        localStorage.setItem('uuid', "{{ $uuid }}");

        localStorage.setItem('sms_type', "TARGETED");

        var segment = [];
        if(JSON.parse(localStorage.getItem('segment')) != null)
            segment = JSON.parse(localStorage.getItem('segment'));
        console.log(segment);

        var segment_exclude = [];
        if(JSON.parse(localStorage.getItem('segment_exclude')) != null)
            segment_exclude = JSON.parse(localStorage.getItem('segment_exclude'));
        console.log(segment_exclude);

        // profile
        // intereset - not interest
        for (var i = 0; i < segment.length; i++) 
        {
            for (var j = 0; j < segment_exclude.length; j++) 
            {
                if(segment[i] == segment_exclude[j])
                segment.splice(i,1);
            } 
        }

        console.log(segment);
        localStorage.setItem('profiles', JSON.stringify(segment));

        var location = [];
        if(JSON.parse(localStorage.getItem('location')) != null)
            location = JSON.parse(localStorage.getItem('location'));
        console.log(location);

        var provider = [];
        if(JSON.parse(localStorage.getItem('provider')) != null)
            provider = JSON.parse(localStorage.getItem('provider'));
        console.log(provider);

        $("#save").click(function(){
            $(".error-box").html("");
            if(segment.length < 1)
            {
                $("#li-interest").addClass("li-error");
                $(".error-box").append("<p>Minat Penerima harus diisi.</p>");
            }
            else if(location.length < 1)
            {
                $("#li-location").addClass("li-error");
                $(".error-box").append("<p>Lokasi Penerima harus diisi.</p>");
            }
            else if(provider.length < 1)
            {
                $("#li-operator").addClass("li-error");
                $(".error-box").append("<p>Operator Telekomunikasi harus dipilih.</p>");
            }
            else
            {
                window.location = "{{ route('dashboard-sms-targeted-setting') }}";
            }
        });

        $("#sms-targeted-route").click(function(){
            $(".loader").show();
            $(".blur").show();

            window.location = "{{route('dashboard-sms-targeted-interest')}}";
        });

        $("#sms-targeted-not-interest-route").click(function(){
            $(".loader").show();
            $(".blur").show();

            window.location = "{{route('dashboard-sms-targeted-not-interest')}}";
        });

        $("#location-route").click(function(){
            $(".loader").show();
            $(".blur").show();

            window.location = "{{route('dashboard-sms-targeted-location')}}";
        });

    });
</script>
@endsection
