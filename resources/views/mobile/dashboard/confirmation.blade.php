@extends('frontend.layouts.homepage')

@section('header')
@include('frontend.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
@if (isset($_SESSION['package_order_id']))
    @php
        $package_order_id = $_SESSION['package_order_id'];
    @endphp
@else
    @php
        $package_order_id = '';
    @endphp
@endif

<div class="divBodiKonten">
        <h2>Konfirmasi Pembayaran</h2>
        <div class="row rowKontenProduk">
            <form class="form-horizontal" method="post" action="{{route('payment-confirmation')}}" enctype="multipart/form-data" id="main-form">
                <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}" />
                <p style="color: #b3b3b3">Silahkan melakukan konfirmasi transfer bank dengan melengkapi form berikut.</p>
                <fieldset>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Nomor Order</label>
                            <input readonly="true" type="text" id='nomor_order' name='nomor_order' class="form-control" value='{{$package_order_id}}' required="true">
                        </div>
                        <div class="col-md-6">
                            <label>Bank Tujuan</label>
                            <input type="text" id='bank_recipient' name='bank_recipient' class="form-control" required="true">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                        <label for="firstName">Nama Pengirim </label>
                            <input type="text" name='name' id='name' class="form-control" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="firstName">Nominal </label>
                            <input type="text" name='nominal' id='nominal' class="form-control" required="true" onkeypress="return isNumberKey(event)">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="firstName">Bukti Transfer </label>
                            <input type="file" name='image' class="form-control" required="true">
                            <br>
                            <small>*Ukuran gambar maksimal 1 MB</small>
                        </div>
                        
                        
                        <div class="col-md-6">
                        <label for="firstName">Bank Asal </label>
                            <input type="text" name='bank_sender' id='bank_sender' class="form-control" required="true">
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <p class="text-center">
                            <button type="submit" class="btn btn-yellow btn-simpan">KIRIM</button>
                        </p>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="clear"></div>
@endsection

@section('script-content')
<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>

<script src="{{ asset('js/script.min') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    var membership_id = localStorage.getItem('membership_id');
    var membership_name = localStorage.getItem('membership_name');
    var membership_price = localStorage.getItem('membership_price');
    var order_id = localStorage.getItem('order_id');
    
    $("#nomor_order").val(order_id);
    $("#nominal").val(membership_price);
    $("#membership").text(membership_name.toUpperCase());
    
    function isNumberKey(evt)
    {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}
    // upload foto
    jQuery(document).ready(function($) {
        $("#main-form").validate();
        var outputHandlerFunc = function(imgObj) {
            var sizeInKB = function(bytes) {return (typeof bytes == 'number') ? (bytes/1024).toFixed(2) + 'Kb' : bytes;};
            var getThumbnail = function(original, maxWidth, maxHeight, upscale) {
                var canvas = document.createElement("canvas"), width, height;
                if (original.width<maxWidth && original.height<maxHeight && upscale == undefined) {
                    width = original.width;
                    height = original.height;
                }
                else {
                    width = maxWidth;
                    height = parseInt(original.height*(maxWidth/original.width));
                    if (height>maxHeight) {
                        height = maxHeight;
                        width = parseInt(original.width*(maxHeight/original.height));
                    }
                }
                canvas.width = width;
                canvas.height = height;
                canvas.getContext("2d").drawImage(original, 0, 0, width, height);
                $(canvas).attr('title','Original size: ' + original.width + 'x' + original.height);
                return canvas;
            }

            $(new Image()).on('load', function(e) {
                console.log('imgobj',e)
                var $wrapper = $('<li class="new-item"><div class="list-content"><span class="preview"></span><span class="type">' + imgObj.type + '<br>' + (e.target.width + '&times;' + e.target.height) + '<br>' + sizeInKB(imgObj.size) + '</span>Berhasil upload  <span class="name">' + imgObj.name +'</span><span class="options"><span class="imagedelete" title="Remove image"><i class="fa fa-times" aria-hidden="true"></i></span></span></div></li>').appendTo('#output ul');
                $('.imagedelete',$wrapper).one('click',function(e) {
                    $wrapper.toggleClass('new-item').addClass('removed-item');
                    $wrapper.one('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(e) {
                        $wrapper.remove();
                    });
                });

                var thumb = getThumbnail(e.target,50,50);
                var $link = $('<a rel="fancybox">').attr({
                    target:"_blank",
                    href: imgObj.imgSrc
                }).append(thumb).appendTo($('.preview', $wrapper));

            }).attr('src',imgObj.imgSrc);

        }

        $("a[rel=fancybox]").fancybox();
        var fileReaderAvailable = (typeof FileReader !== "undefined");
        var clipBoardAvailable = (window.clipboardData !== false);
        var pasteAvailable = Boolean(clipBoardAvailable & fileReaderAvailable & !eval('/*@cc_on !@*/false'));
        if (fileReaderAvailable) {
            $('.dropzone').imageUpload({
                errorContainer: $('span','#errormessages'),
                trigger: 'click',
                enableCliboardCapture: pasteAvailable,
                onBeforeUpload: function() {$('body').css('background-color','green');console.log('start',Date.now());},
                onAfterUpload: function() {$('body').css('background-color','#eee');console.log('end',Date.now());},
                outputHandler:outputHandlerFunc
            })
            $('.dropzone').prev('#textbox-wrapper').find('#textbox').append('<i class="fa fa-camera fa-3x" aria-hidden="true"></i>');
        }
        else {
            $('body').addClass('nofilereader');
        }
        if (!pasteAvailable) {
            $('body').addClass('nopaste');
        }
    });
    $(".js-example-basic-multiple").select2();


</script>
@endsection