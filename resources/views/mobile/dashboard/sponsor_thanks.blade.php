@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header-full">
            <h1>Terima kasih</h1>

            <div class="top-success"><i class="mdi mdi-check"></i></div>

            <p>Terima kasih telah melakukan pengisian ulang saldo iklan YP. Saldo iklan Anda akan otomatis bertambah ketika pembayaran telah kami terima.</p>

            <p>Secara bersamaan, iklan bisnis Anda pun akan otomatis kami aktifkan setelah saldo mencukupi dan melewati proses moderasi.</p>

            <p>Ketuk tombol di bawah untuk mengakhiri transaksi dan kembali ke halaman dashboard Anda.</p>

            <a href="{{route('dashboard')}}"><div class="promote-step-btn text-center thankyou-btn">DASHBOARD</div></a>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    $(document).on('ready',function(){
        localStorage.clear();
    });    
</script>
@endsection
