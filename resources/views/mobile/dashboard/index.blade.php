@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="divBodiKonten">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->

    <center><div class='m-tulisan-welcome' style="padding-top: 10px;font-family: inherit;font-size: 18px;">Selamat Datang, {{ Auth::user()->first_name }}</div></center>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->


        <!-- deklarasi carousel -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <center><div class='m-angka-welcome' id='view-count'></div></center>
                <center><div class='m-tulisan-welcome-sub'>Tayangan Pekan Ini</div></center>
                <div class='m-chart'>
                    <canvas id='chart-view' height='125px'></canvas>
                </div>
            </div>
            <div class="item">
                <center><div class='m-angka-welcome' id='click-count'></div></center>
                <center><div class='m-tulisan-welcome-sub'>Jumlah Klik Pekan Ini</div></center>
                <div class='m-chart'>
                    <canvas id='chart-click' height='125px'></canvas>
                </div>
            </div>
            <div class="item">
                <center><div class='m-angka-welcome' id='inquiry-count'></div></center>
                <center><div class='m-tulisan-welcome-sub'>Informasi Inquiry</div></center>
                <div class='m-chart'>
                    <canvas id='chart-inquiry' height='125px'></canvas>
                </div>
            </div>
            <div class="item">
                <center><div class='m-angka-welcome' id='sms-count'></div></center>
                <center><div class='m-tulisan-welcome-sub'>Informasi SMS</div></center>
                <div class='m-chart'>
                    <canvas id='chart-sms' height='125px'></canvas>
                </div>
            </div>
        </div>

        <center>
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>
        </center>
    </div>

    <div class="m-maincontent">
        <h3 class="text-uppercase text-center m-judulmain">BERIKLAN SEKARANG</h3>
        <div class="col-xs-12">
            <div class="block-promote-btn">
                <a href="{{route('sponsor-business')}}">
                    <div class="col-xs-4">
                        <div class="promote-btn"><i class="mdi mdi-briefcase-check fa-3x text-center"></i></div>
                        <span class="promote-btn-text">SPONSORED BUSINESS</span>
                    </div>
                </a>
                <a href="#" class="disabled">
                    <div class="col-xs-4">
                        <div class="promote-btn"><i class="mdi mdi-google fa-3x text-center"></i></div>
                        <span class="promote-btn-text">GOOGLE ADWORDS</span>
                    </div>
                </a>
                <a href="{{route('dashboard-sms-index')}}">
                    <div class="col-xs-4">
                        <div class="promote-btn"><i class="mdi mdi-message-reply-text fa-3x text-center"></i></div>
                        <span class="promote-btn-text">SMS TARGETED & BLAST</span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="m-maincontent">
        <h2 class="text-uppercase text-center m-judulmain">TULIS UPDATE BISNIS</h2>
        <form class="form-horizontal" action="{{ route('dashboard-status-create') }}" method="POST">
            {{ csrf_field() }}
        <div class="m-forminput">
            <select class="form-control" name="directory_id"  style="border-radius: 0px;">
                @foreach($directories as $dk => $dv)
                <option value="{{ $dv->id }}">{{ $dv->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="m-forminput" style="margin-top:12px;">
            <textarea class="form-control" name="message" placeholder='Tulis Update terkait bisnis anda' style="border-radius: 0px;"></textarea>
        </div>
        <button class='submit_dash' type="submit">
            <i class="material-icons m-ikonbutton">send</i> KIRIM UPDATE BISNIS
        </button>
    </div>
    
    @if(count($last_activity) > 0 || count($last_activity) > 0)
    <div class="m-maincontent">
        @if(count($last_activity) > 0)
            <h2 class="text-uppercase text-center m-judulmain">INFORMASI TERBARU</h2>
        @endif
        <div class='m-informasi'>
            @foreach($last_activity as $k => $last_activity_detail)
            <div class='row m-baris-info'>
                <div class='col-xs-8 col-sm-6 m-ket-info'>
                    <a href="#">{{$last_activity_detail['activity']}}</a>
                </div>
                <div class='col-xs-4 col-sm-6 m-waktu-info'>
                    {{\Carbon\Carbon::parse($last_activity_detail['time'])->diffForHumans()}}
                </div>
            </div>           
            @endforeach

        </div>
    </div>
    @endif

    

    <div class="clear"></div>
    @endsection

    @section('script-content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>


    <script>
        var date_array = '{!! json_encode($date_array) !!}';
        var total_view = '{!! json_encode($total_view) !!}';
        var total_click = '{!! json_encode($total_click) !!}';
        
        var total_view_parse = JSON.parse(total_view);
        
        var grand_total_view = 0;
        for(x in total_view_parse){
            grand_total_view += total_view_parse[x];
        }
        $("#view-count").html(grand_total_view);
        
        var total_click_parse = JSON.parse(total_click);
        var grand_total_count = 0;
        for(x in total_click_parse){
            grand_total_count += total_click_parse[x];
        }
        $("#click-count").html(grand_total_count);

        $(document).ready(function(){
	setTimeout(function(){
        var ctx = document.getElementById('chart-view').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: JSON.parse(date_array),
                datasets: [{
                        label: 'Total',
                        data: total_view_parse,
			backgroundColor: "rgba(202,145,54,0.3)",
                    	borderColor: "rgba(256,256,256,.4)",
                    	borderWidth: 2,
                    	pointRadius: 4,
                    	pointBackgroundColor: "rgba(256,256,256,.6)",
                        //backgroundColor: "rgba(255,255,255,0)",
                        //pointRadius: 6,
                        //pointBorderColor: "rgba(255,255,255,1)",
                        //pointBackgroundColor: "#fff",
                        //borderColor: "rgb(255,255,255)",
			//borderRadius: 2,
                        //lineTension: 0.1,
                        //hitRadius: 2,
                        //hoverRadius: 2,
                        maintainAspectRatio: false,
                        responsive: true,
                    }]
            },
            options: {
                fullWidth: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                            display: true,
                            gridLines: {
                                display: false,
                                color: "#f5d120"
                            },
                            ticks: {
                                fontColor: "rgba(0,0,0,0)", // this here
                            }
                        }],
                    yAxes: [{
                            display: false,
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                display: false
                            }
                        }],
                }

            },
        });

        var ctx2 = document.getElementById('chart-click').getContext('2d');
        var myChart = new Chart(ctx2, {
            type: 'line',
            data: {
                labels: JSON.parse(date_array),
                datasets: [{
                        label: 'Total',
                        data: total_click_parse,
                        backgroundColor: "rgba(202,145,54,0.3)",
                        borderColor: "rgba(256,256,256,.4)",
                        borderWidth: 2,
                        pointRadius: 4,
                        pointBackgroundColor: "rgba(256,256,256,.6)",
                        maintainAspectRatio: false,
                        responsive: true,
                    }]
            },
            options: {
                fullWidth: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                            display: true,
                            gridLines: {
                                display: false,
                                color: "#f5d120"
                            },
                            ticks: {
                                fontColor: "rgba(0,0,0,0)", // this here
                            }
                        }],
                    yAxes: [{
                            display: false,
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                display: false
                            }
                        }],
                }

            },
        });
	});
	}, 15000);

requestData();

function requestData() {
    $.ajax({
        url: '/ajax/info_counter',
        method: 'GET',
        dataType: 'JSON',
        success: function (e) {
            var total_sms = e.data.sms;
            var grand_total_count = 0;
            for(x in total_sms){
                grand_total_count += total_sms[x];
            }
            $("#sms-count").html(grand_total_count);
            
            var total_inquiry = e.data.inquiry;
            var grand_total_count = 0;
            for(x in total_inquiry){
                grand_total_count += total_inquiry[x];
            }
            $("#inquiry-count").html(grand_total_count);
            
            generateCartSMSandInquiry(total_sms, total_inquiry );           
            
        },
        cache: false
    });
}
    
    function generateCartSMSandInquiry(totalSMS, totalInquiry){
        var ctx3 = document.getElementById('chart-inquiry').getContext('2d');
        var myChart = new Chart(ctx3, {
            type: 'line',
            data: {
                labels: ['Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Ming'],
                datasets: [{
                        label: 'Total',
                        data: totalInquiry,
                        backgroundColor: "rgba(202,145,54,0.3)",
                        borderColor: "rgba(256,256,256,.4)",
                        borderWidth: 2,
                        pointRadius: 4,
                        pointBackgroundColor: "rgba(256,256,256,.6)",
                        maintainAspectRatio: false,
                        responsive: true,
                    }]
            },
            options: {
                fullWidth: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                            display: true,
                            gridLines: {
                                display: false,
                                color: "#f5d120"
                            },
                            ticks: {
                                fontColor: "rgba(0,0,0,0)", // this here
                            }
                        }],
                    yAxes: [{
                            display: false,
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                display: false
                            }
                        }],
                }

            },
        });
        
        var ctx4 = document.getElementById('chart-sms').getContext('2d');
        var myChart = new Chart(ctx4, {
            type: 'line',
            data: {
                labels: ['Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Ming'],
                datasets: [{
                        label: 'Total',
                        data: totalSMS,
                        backgroundColor: "rgba(202,145,54,0.3)",
                        borderColor: "rgba(256,256,256,.4)",
                        borderWidth: 2,
                        pointRadius: 4,
                        pointBackgroundColor: "rgba(256,256,256,.6)",
                        maintainAspectRatio: false,
                        responsive: true,
                    }]
            },
            options: {
                fullWidth: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                            display: true,
                            gridLines: {
                                display: false,
                                color: "#f5d120"
                            },
                            ticks: {
                                fontColor: "rgba(0,0,0,0)", // this here
                            }
                        }],
                    yAxes: [{
                            display: false,
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                display: false
                            }
                        }],
                }

            },
        });
    }
        $('.carousel').carousel({
            interval: false
        });

        $(document).ready(function () {
            $(".carousel").swiperight(function () {
                $(this).carousel('prev');
            });
            $(".carousel").swipeleft(function () {
                $(this).carousel('next');
            });
        });
    </script>
    @endsection
