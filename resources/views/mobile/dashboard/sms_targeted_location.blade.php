@extends('mobile.layouts.dashboard')


@section('header')
    @include('mobile.layouts.dashboard_form_header')
@stop

@section('content')

    <div class='m-maincontent' style='padding-bottom:40px; margin-top: 42px; min-height: 700px;'>
        <div class="modal-search">
            <input id="search-input" type="text" placeholder="Search Location">
            <i id="search" class="mdi mdi-magnify"></i>
        </div>
        @foreach($locations as $location)
            <div id="location-id-{{ $location->id }}" class='m-baris-info'>
                <div class='row m-forminput2'>
                    <div class='col-xs-12'>
                        <div class="row">
                            <div class="checkbox">
                                <label>
                                    <input id="location{{ $location->id }}" name="location[]" type="checkbox" value="{{ $location->id }}">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    {{ $location->name }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div id="error-interest" class="promote-step-btn text-center" style="position: fixed; bottom: 52px; background: rgba(244,67,54,0.9);">
        Lokasi Penerima harus dipilih.
    </div>

    <div class="promote-step-btn text-center" style="position: fixed; bottom: 0;">
        <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
        <a id="simpan"><div class="col-xs-6 btn-back-step">SIMPAN</div></a>
    </div>

    <div class="loader"></div>
    <div class="blur"></div>
@stop

@section('script-content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <style type="text/css">
        
        input:focus {
            outline: none;
            text-decoration: none;
        }
    </style>
    <style>
        .blur
        {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(1,1,1,0.5);
        }
        .loader {
            position: fixed;
            top: 43%;
            left: 40%;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 80px;
            height: 80px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
            z-index: 20;
        }

        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#error-interest").hide();
            $(".loader").hide();
            $(".blur").hide();

            var lists = [
            @foreach($locations as $location)
            {!! "{ 'id' : ".$location->id.", 'name' : \"".$location->name."\"}," !!}
            @endforeach
            ];
            var location = [];
            
            if(JSON.parse(localStorage.getItem('location') != null))
                location = JSON.parse(localStorage.getItem('location'));

            console.log(location);

            // set checked
            for (var i = 0; i < location.length; i++) 
            {
                $('#location' + location[i]).prop('checked', true);
            }

            @foreach($locations as $location)
            $("#location{{ $location->id }}").change(function(e){
                $("#error-interest").hide();

                if ($(this).is(":checked"))
                {
                    location.push({{ $location->id }});
                }
                else
                {

                    for (var i = 0; i < location.length; i++) 
                    {
                        if(location[i] == {{ $location->id }})
                            location.splice(i,1);
                    }
                }
            });
            @endforeach

            $( "#simpan" ).click(function() {
                localStorage.setItem('location', JSON.stringify(location));
                location = JSON.parse(localStorage.getItem('location'));

                if(location.length < 1)
                    $("#error-interest").show();
                else
                    history.go(-1);
            });

            function is_substring(substring, string)
            {
                return (string.indexOf(substring) !== -1);
            }

            function search_interest(input, lists)
            {
                var ids = [];

                for (var i = 0; i < lists.length; i++) 
                {
                    if(is_substring(input.toLowerCase(), lists[i]['name'].toLowerCase()))
                        ids.push(lists[i]['id']);
                }

                return ids;
            }

            $("#search").click(function() {
                $(".loader").show();
                $(".blur").show();

                var input = $("#search-input").val();
                var ids = search_interest(input, lists);

                for (var i = 0; i < lists.length; i++) 
                {
                    if(ids.indexOf(lists[i]['id']) >= 0)
                        $("#location-id-" + lists[i]['id']).fadeIn("slow");
                    else
                        $("#location-id-" + lists[i]['id']).fadeOut("slow");
                }

                $(".loader").hide();
                $(".blur").hide();
            });
        });

    </script>
@stop