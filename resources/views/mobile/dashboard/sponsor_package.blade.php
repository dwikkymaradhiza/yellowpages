@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Pilih Paket Sponsored Business</h1>
        <p>Silakan pilih paket sponsored business yang Anda kehendaki.</p>
    </div>

    <div class="promote-main-content">
        <ul class="prd-ls">
            @if(count($packages)>0)
            @foreach($packages as $packages_detail)
            <li>
                <div class="col-xs-2 row">
                     <div class="radio radio-primary">
                        <input type="radio" data-price="{{$packages_detail->price}}" name="productcb" id="{{$packages_detail->id}}" value="{{$packages_detail->id}}">
                        <label for="{{$packages_detail->id}}">
                            
                        </label>
                    </div> 
                </div>
                <div class="col-xs-10 row">
                    <label class="item-list" for="{{$packages_detail->id}}">{{$packages_detail->package_name}}<span>Rp {{$packages_detail->price}}</span></label>
                </div>
                <div class="clearfix"></div>                
            </li>
            @endforeach
            @endif
        </ul>
        <div class="promote-step-btn text-center">
            <a href="{{route('sponsor-business')}}"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
            <a href="#" onclick="saveSponsorPackages();return false;">
                <div class="col-xs-6">LANJUTKAN</div>                
            </a>
        </div>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    $(document).on('ready',function(){
        var package_id = localStorage.getItem('package_id');
        if(typeof package_id !== 'undefined' && package_id !== null){
            $("#"+package_id).prop('checked',true);
        }else{
            $("input[name=productcb]:first").prop("checked",true)
        }
    });
function saveSponsorPackages() {
    var package_id = $("input[name=productcb]:checked").val();
    var package_price = $("input[name=productcb]:checked").data('price');

    localStorage.setItem('package_id', package_id);
    localStorage.setItem('package_price', package_price);
    
    window.location = "{{route('sponsor-invoice')}}";
    
    return false;
}
</script>
@endsection
