@extends('mobile.layouts.dashboard')

@section('header')
@include('mobile.layouts.dashboard_header')
@stop

@section('content')
<!-- content -->
<div class="promote-container">
    <div class="promote-header">
        <h1>Konfirmasi SMS <font name="sms_type_label"></font></h1>
        <p>Berikut rincian SMS <font name="sms_type_label"></font> dan biaya yang harus Anda bayarkan.</p>
    </div>
    @if (count($errors) > 0)
    <div class="alert alert-danger" style="border-radius: 0">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="flash-message" style="border-radius: 0">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="promote-main-content">
        <form>
            {{ csrf_field() }}
            <ul class="prd-ls">
                <li>
                    <div class="col-xs-8">
                        <div class="row"><h4>Saldo Iklan YP</h4></div>
                    </div>
                    <div class="col-xs-4">
                        <div class="row pull-right"><h4 id="user_balance"></h4></div>
                    </div>
                    
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-8">
                        <div class="row"><h4>Biaya SMS <font name="sms_type_label"></font></h4></div>
                    </div>
                    <div class="col-xs-4">
                        <div class="row pull-right"><h4 id="total_price"></h4></div>
                    </div>
                    
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="col-xs-8">
                        <div class="row"><h4>SISA SALDO</h4></div>
                    </div>
                    <div class="col-xs-4">
                        <div class="row pull-right"><h4 id="saldo_akhir"></h4></div>
                    </div>
                    
                    <div class="clearfix"></div>
                </li>
            </ul>

            <div id="saldo_min" class="col-xs-12">
                <p>Maaf, saldo Anda tidak mencukupi untuk melakukan transaksi ini.</p>
                <br>
                <p>Silakan ketuk tombol LANJUT untuk melakukan topup saldo iklan Anda terlebih dahulu.</p>
            </div>
         
            <div class="promote-step-btn text-center">
                <a href="#" onclick="javascript: history.go(-1)"><div class="col-xs-6 btn-back-step">KEMBALI</div></a>
                <a id="create_campaign"><div class="col-xs-6 btn-back-step">LANJUT</div></a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>

<script>
    Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
            ].join('-');
    };

    function format_number(n) 
    {
        return n.toFixed(0).replace(/./g, function(c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
        });
    }

    $(document).ready(function() {
        var uuid = localStorage.getItem('uuid');
        var content = localStorage.getItem('content');
        var count_sms = localStorage.getItem('count_sms');
        var total_price = localStorage.getItem('total_price');
        var date_start = localStorage.getItem('date_start');
        var date_end = localStorage.getItem('date_end');
        var sender_id = localStorage.getItem('sender_id');
        console.log(uuid);
        console.log(content);
        console.log(count_sms);
        console.log(total_price);
        console.log(sender_id);
        var date_start = new Date(date_start).yyyymmdd();
        var date_end = new Date(date_end).yyyymmdd();
        console.log(date_start);
        console.log(date_end);

        var sms_type = localStorage.getItem('sms_type');
        console.log(sms_type);

        // targeted SMS
        var segment = [];
        if(JSON.parse(localStorage.getItem('segment')) != null)
            segment = JSON.parse(localStorage.getItem('segment'));
        console.log(segment);

        var segment_exclude = [];
        if(JSON.parse(localStorage.getItem('segment_exclude')) != null)
            segment_exclude = JSON.parse(localStorage.getItem('segment_exclude'));
        console.log(segment_exclude);

        var profiles = [];
        if(JSON.parse(localStorage.getItem('profiles')) != null)
            profiles = JSON.parse(localStorage.getItem('profiles'));
        console.log(profiles);

        var location = [];
        if(JSON.parse(localStorage.getItem('location')) != null)
            location = JSON.parse(localStorage.getItem('location'));
        console.log(location);

        var provider = [];
        if(JSON.parse(localStorage.getItem('provider')) != null)
            provider = JSON.parse(localStorage.getItem('provider'));
        console.log(provider);

        var pattern = localStorage.getItem('call_pattern');
        console.log(pattern);

        // if(JSON.parse(localStorage.getItem('days') != null))
        // {
        //     days = JSON.parse(localStorage.getItem('days'));
        //     console.log(days);
        // }

        // if(JSON.parse(localStorage.getItem('time_start') != null))
        // {
        //     time_start = JSON.parse(localStorage.getItem('time_start'));
        //     console.log(time_start);
        // }

        // if(JSON.parse(localStorage.getItem('time_end') != null))
        // {
        //     time_end = JSON.parse(localStorage.getItem('time_end'));
        //     console.log(time_end);
        // }

        if(sms_type == "BLAST")
            $("[name=sms_type_label]").text("Blast");
        else if(sms_type == "TARGETED")
            $("[name=sms_type_label]").text("Targeted");
        else if(sms_type == "CALL_HISTORY")
            $("[name=sms_type_label]").text("Call History");

        var saldo = {{ $user->user_balance }};
        var saldo_akhir = saldo - total_price;

        $("#user_balance").text("Rp. " + format_number(saldo));
        $("#total_price").text("Rp. " + format_number(parseInt(total_price)));

        if(saldo_akhir < 0)
        {
            $("#saldo_akhir").text("-Rp. " + format_number(Math.abs(parseInt(saldo_akhir))));
            $("#saldo_akhir").css("color", "red");
        }
        else
        {
            $("#saldo_akhir").text("Rp. " + format_number(Math.abs(parseInt(saldo_akhir))));
            $("#saldo_min").hide();
        }

        if(sms_type == "BLAST")
        {
            $( "#create_campaign" ).click(function() {
                if(saldo_akhir >= 0)
                {
                    var requestData = {
                        data : {
                            "uuid" : uuid,
                            "content" : content,
                            "quantity": count_sms,
                            "period": {
                                "start": date_start,
                                "end": date_end
                            },
                            "sender_id": {
                                "id" : sender_id,
                                "is_new" : 0
                            }
                        },
                        price : total_price
                    };
                    
                    var urlAPI = "{{route('dashboard-sms-create-campaign')}}";
                    
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: requestData,
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });

                    console.log(localStorage.getItem('count_package'));

                    var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 1, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/sms/blast/thankyou";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                }
                else
                {
                    console.log(localStorage.getItem('count_package'));

                     var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 0, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/topup";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                    
                }
            });
        }
        else if(sms_type == "TARGETED")
        {
            $( "#create_campaign" ).click(function() {
                if(saldo_akhir >= 0)
                {
                    var requestData = {
                        data : {
                            "customer_id" : {{ $user->id }},
                            "customer_name" : "{{ $user->first_name." ".$user->last_name}}",
                            "uuid" : uuid,
                            "content" : content,
                            "quantity": count_sms,
                            "period": {
                                "start": date_start,
                                "end": date_end
                            },
                            "sender_id": {
                                "id" : sender_id,
                                "is_new" : 0
                            },
                            "client_id" : 83,
                            "type" : "segmented",
                            "profiles" : profiles,
                            "locations" : location,
                            "providers" : provider
                        },
                        price : total_price
                    };
                    
                    var urlAPI = "{{route('dashboard-sms-targeted-create-campaign')}}";
                    
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: requestData,
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                            
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });

                    console.log(localStorage.getItem('count_package'));

                    var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 1, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/sms/blast/thankyou";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                }
                else
                {
                    console.log(localStorage.getItem('count_package'));

                     var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 0, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/topup";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                    
                }
            });
        }
        if(sms_type == "CALL_HISTORY")
        {
            $( "#create_campaign" ).click(function() {
                if(saldo_akhir >= 0)
                {
                    var requestData = {
                        data : {
                            "customer_id" : {{ $user->id }},
                            "customer_name" : "{{ $user->first_name." ".$user->last_name}}",
                            "uuid" : uuid,
                            "content" : content,
                            "quantity": count_sms,
                            "period": {
                                "start": date_start,
                                "end": date_end
                            },
                            "sender_id": {
                                "id" : sender_id,
                                "is_new" : 0
                            },
                            "client_id" : 83,
                            "type" : "call_pattern",
                            "profiles" : profiles,
                            "locations" : location,
                            "providers" : provider,
                            "patterns" : pattern,
                            "call_history" : {}
                        },
                        price : total_price
                    };
                    
                    var urlAPI = "{{route('dashboard-sms-call-history-create-campaign')}}";
                    
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: requestData,
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });

                    //console.log(localStorage.getItem('count_package'));

                    var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    //console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 1, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/sms/blast/thankyou";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                }
                else
                {
                    console.log(localStorage.getItem('count_package'));

                     var requestData = [];

                    for (i = 0; i < localStorage.getItem('count_package'); i++) {
                        console.log(i);
                        if(localStorage.getItem('amount' + i) > 0)
                        {
                            var newData = { "id" : localStorage.getItem('package' + i), "quantity" : localStorage.getItem('amount' + i) };
                            requestData.push(newData);
                        }
                    }

                    //console.log(requestData);
                    
                    var urlAPI = "{{route('dashboard-sms-blast-save-order')}}";

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                        data: { "data" : requestData, "status" : 0, "count_sms" : count_sms },
                        url: urlAPI,
                        type: 'post',
                        beforeSend: function () {
                            //beginTransition();
                        },
                        complete: function () {
                            //endLoadingMapTransition();
                        },
                        success: function (data) {
                           console.log(data);
                           localStorage.setItem('order_id', data);
                           window.location = "/dashboard/topup";
                        },
                        error: function (data){
                            console.log(data);
                        }
                    });
                    
                }
            });
        }
        
    });
</script>
@endsection
