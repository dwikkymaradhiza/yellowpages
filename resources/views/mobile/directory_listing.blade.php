@extends('mobile.layouts.general')

@section('seo-meta')
    @include('seo.directory_listing')
@stop

@section('header')
    @include('mobile.layouts.nav_general')
@stop

@section('content')
<div class="m-subheader">
    <div class="m-div-subh">
        @if($category_name !== '')
            <h3 class="m-sub-head">Daftar Bisnis {{$category_name}}
                @if($address !=='')
                di {{$address}}
                @endif
            </h3></div>
        @else
            <h3 class="m-sub-head">Hasil {{$search_by}}: '{{$keyword}}' 
                @if($address !=='')
                di {{$address}}
                @endif
            </h3></div>    
        @endif
    <div class="m-d-share">
        <a href="#" id="pop-share" class="m-ic-share" rel="popover" data-placement="bottom" data-toggle="pop-share"> <i class="material-icons">share</i></a>
        <div id="share-konten" class="hide">
            <a target="_blank" href="{{ Share::load(Request::url())->facebook() }}" class="li-share-facebook">
                <i class="fa fa-facebook-official ic-share-facebook"></i>
            </a>
            <a target="_blank" href="{{ Share::load(Request::url())->twitter() }}" class="li-share-facebook">
                <i class="fa fa-twitter ic-share-twitter"></i>
            </a>
            <a target="_blank" href="{{ Share::load(Request::url())->linkedin() }}" class="li-share-facebook">
                <i class="fa fa-linkedin ic-share-inked"></i>
            </a>
        </div>
    </div>
</div>
<div class="m-block-div">
    <div class="m-div-produkBtn"><a class="btn btn-info m-btn-urutkan" role="button" href="#accordion-1 .item-1" data-toggle="collapse" aria-expanded="false" aria-controls="colaps-sort" data-parent="#accordion-1"><i class="fa fa-sort-amount-desc m-ic-sort"></i>URUTKAN</a><a class="btn btn-info m-btn-filter"
                                                                                                                                                                                                        role="button" href="#accordion-1 .item-2" data-toggle="collapse" aria-expanded="false" aria-controls="colaps-sort" data-parent="#accordion-1"><i class="fa fa-filter m-ic-filter"></i>FILTER</a></div>
    <div class="panel-group" role="tablist" aria-multiselectable="true"
         id="accordion-1">
        <div class="panel panel-default m-item-kolapse">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" aria-expanded="false" href="#accordion-1 .item-1">Accordion Item</a></h4></div>
            <div class="panel-collapse collapse item-1" role="tabpanel">
                <div class="panel-body m-item-kolapse">
                    @if(!$search_page)
                    <form action="{{route('directory-category-ord')}}" method="post">
                        @else
                        <form class="src-form-order" method="get">
                            @endif
                            <div class="form-group">                       

                                @if($urutkan === '' || $urutkan === 'rating')
                                @php
                                $rating = 'checked="true"';
                                $id = '';
                                $name_asc = '';
                                $name_desc = '';
                                @endphp
                                @elseif($urutkan === 'id')
                                @php
                                $id = 'checked="true"';
                                $rating = '';
                                $name_desc = '';
                                $name_asc = '';
                                @endphp    
                                @elseif($urutkan === 'name_asc')
                                @php
                                $name_asc = 'checked="true"';
                                $name_desc = '';
                                $rating = '';
                                $id = '';
                                @endphp   
                                @elseif($urutkan === 'name_desc')
                                @php
                                $name_desc = 'checked="true"';
                                $name_asc = '';
                                $rating = '';
                                $id = '';
                                @endphp 
                                @endif
                                <input type="hidden" name="address" id="address_hid" value='{{$address}}' />
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="category" id="category" value="{{$category}}">
                                <label class="radio-inline">
                                    <input type="radio" class="labelRadio" id="option-1" value="name_asc" name="urutkan" {{$name_asc}}>Nama (A-Z)
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" id="option-2" value="name_desc" name="urutkan" {{$name_desc}}>Nama (Z-A)
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" id="option-3" value="id" name="urutkan" {{$id}}>Terbaru
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" id="option-4" value="rating"  name="urutkan" {{$rating}}>Rating
                                </label>

                            </div>
                            <div class="m-div-btn-filter">
                                <button class="btn btn-info m-btn-go" type="submit">URUTKAN </button>
                            </div>

                        </form>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab">
                <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" aria-expanded="false" href="#accordion-1 .item-2">Accordion Item</a></h4></div>
            <div class="panel-collapse collapse item-2" role="tabpanel">
                <div class="panel-body">
                    <div class="form-group m-form-lokasi">
                        @if($category!=='')
                            <form action="{{route('directory-category-location')}}" id="form-filter-area" method="post">
                        @else
                            <form onsubmit='return false;' id="form-filter-search">
                        @endif        
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="text" placeholder="Lokasi" id="address" name="address" class="m-input-lokasi" value='{{$address}}'>
				@if($address)<button type="button" class="clear-btn"><i class="fa fa-times"></i></button>@endif
                            <input type="hidden" name="category" value="{{$category}}">
                            <input type="hidden" name="urutkan" id="urutkan" value='{{$urutkan}}' />
                            <div class="m-div-btn-filter">
                                <button class="btn btn-info m-btn-go" id="filter-btn" type="submit">FILTER </button>
                            </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="directory_content">

</div>
@if($page === '' || $page === 0)
@php
$page = 1; 
@endphp
@endif
<input type="hidden" name="page" id="page" value="{{$page}}">
<div>
    <div class="m-div-more"><a id="more_btn" class="text-uppercase m-lebih-banyak" href="#tampilkan-banyak" data-toggle="collapse" aria-expanded="false" aria-controls="tampilkan-banyak" role="button">tampilkan lebih banyak</a></div>
    <div id='error-message' align='center' class='no-result alert-danger'></div>
</div>
<div class="m-block-div">
    @if(count($affiliate_article)>0)
    <h3 class="text-uppercase m-title-tumb">artikel terkait</h3>
    <div class="m-list-artikel">        

        @foreach($affiliate_article as $affiliate_article_detail)
        <div class="m-recent-artikel">
            <a href="{{$affiliate_article_detail->full_url}}">
                <div class="m-tumb-img"><img src="{{ asset('uploads/affiliate/'.$affiliate_article_detail->image_url) }}" class="m-tumb-artikel"></div>
                <div class="m-desk-tum">
                    <h4 class="m-title-artikel">{{$affiliate_article_detail->title}}</h4>
                    <p class="m-par-artikel">{{$affiliate_article_detail->head_line}}</p>
                </div>
            </a>
        </div>
        @endforeach               
    </div>
    @endif
</div>
<div class="modal js-loading-bar">
    <div class="modal-dialog">
        <div class="mobile-modal-content">
            <div class="modal-body">
                <div class="progress progress-popup">
                    <div class="progress-bar"></div>
                </div>
                <div align='center'>
                    <h2>Loading</h2>
                    <img src="{{URL::asset('img').'/yp-logo-2.png'}}" class="logoWebsiteFloat"/>
                </div>
            </div>
        </div>
    </div>
</div>

<p id="back-top">
    <a href="top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</p>
@stop

@section('script-content')

<script>
    var limit = {!! $limit !!}
    getLoad();

    // hide #back-top first
    $("#back-top").hide();

    // fade in #back-top
    $(function () {
            $(window).scroll(function () {
                    if ($("#page").val() > 3 && $(this).scrollTop() > 100) {
                        $('#back-top').fadeIn();
                    } else {
                        $('#back-top').fadeOut();
                    }
            });

            // scroll body to 0px on click
            $('#back-top').click(function () {
                    $('body,html').animate({
                            scrollTop: 0
                    }, 1000);
                    return false;
            });
    });
        
    $("#more_btn").on('click', function () {
        getLoad();
    })
    
    $("#filter-btn").on('click',function(){
        var address = $("#address").val();
        var urutkan = $("#urutkan").val();
        var base_url ='';
        
        base_url = "{{route('search',['search_by'=> 'bisnis','keyword'=> $keyword])}}";
        if(address !== ''){
            base_url += '/' + address;
        }
        
        if(urutkan !== ''){
            base_url += '/sort/' + urutkan;
        }

        window.location.href = base_url;
    });
    
    function getLoad() {
        var requestData = {
            'address': $("#address").val(),
            'category': $("#category").val(),
            'page': $("#page").val(),
            'urutkan': $("#urutkan").val(),
            'search_by': $('.src-by').val(),
            'keyword': $('.src-keyword').val(),
        };
        var urlAPI = "{{route('get-directory-listing')}}";
        var urlProduct = "{{route('product-detail')}}";
        var urlBusiness = "{{route('directory')}}";
        $.ajax({
            headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
            data: requestData,
            url: urlAPI,
            type: 'post',
            beforeSend: function () {
                beginTransition();
            },
            complete: function () {
                endLoadingMapTransition();
            },
            success: function (data) {
                $("#error-message").hide();
                var result = JSON.parse(data);
                var arr_result = result.data;

                if (arr_result.length <= 0) {
                    $(".m-div-more").hide();
                    $("#error-message").show();
                    $("#error-message").html('Data tidak ditemukan');
                    return false;
                }

                $("#page").val(parseInt($("#page").val()) + 1);
                var html_template = '';
                var urlAsset = "{{URL::asset('uploads/products')}}";
                var urlImg = "{{URL::asset('img')}}";
                for (var n in arr_result) {

                    html_template += '<div class="m-block-div">' +
                            '<div class="m-produk-desk">' +
                            '<a href="' + urlBusiness + '/' + arr_result[n]['directory_slug'] + '"><div>' +
                            '<div>' +
                            '<h4 class="m-judul-toko">' + arr_result[n]['directory_name'];
                    if(arr_result[n]['is_verified'])
                        html_template += '<i class="mdi mdi-check-circle verified" aria-hidden="true"></i>';

                    html_template += '</h4>' +
                            '<p class="text-uppercase m-label-tolo">' + arr_result[n]['city_name'] + '</p>' +
                            '</div>' +
                            '</div>' +
                            '<div>';


                    for (var rate = 1; rate <= 5; rate++) {
                        if (rate > parseInt(arr_result[n]['rating'])) {
                            html_template += '<i class="material-icons m-ic-star-inactive">star</i>';
                        } else {
                            html_template += '<i class="material-icons m-ic-star">star</i>';
                        }
                    }


                    html_template += '</div>' +
                            '<div class="m-alamat-produk">';
                    if (arr_result[n]['directory_address'] !== null) {
                        html_template += '<p class="m-par-addres">' + arr_result[n]['directory_address'] + '</p>';
                    }
                    html_template += '</div></a>' +
                            '<div class="m-media-sponsor">';

                    if (arr_result[n]['product'].length > 0) {
                        for (var picture_index in arr_result[n]['product']) {

                            html_template += '<div class="m-div-thumb">';
                            if(arr_result[n]['product'][picture_index]['picture'] !== ''){
                                html_template += '<a href="' + urlProduct + '/' + arr_result[n]['product'][picture_index]['slug'] + '">' +
                                    '<img src="' + urlAsset + '/' + arr_result[n]['product'][picture_index]['picture'] + '"  \n\
                                    onerror="this.src=\''+ urlImg +'/yp-logo-2.png\'; this.onerror=null;" \n\
                                    class="m-media-thum" alt="' + arr_result[n]['product'][picture_index]['name'] + '" title="' + arr_result[n]['product'][picture_index]['name'] + '">' +
                                    '</a>';
                            }
                                    html_template += '</div>';
                        }
                    }


                    html_template += '</div>' +
                            '</div>' +
                            '</div>';

                }

                $("#directory_content").append(html_template);

                if (arr_result.length < limit) {
                    $(".m-div-more").hide();
                    return false;
                }
            }
        });

    }


    // Setup
    this.$('.js-loading-bar').modal({
        backdrop: 'static',
        show: false
    });

    function beginTransition() {
        var $modal = $('.js-loading-bar'),
                $bar = $modal.find('.progress-bar');
        $bar.width(0);
        $modal.modal('show');
        $bar.addClass('animate');
        setInterval(function () {
            $bar.width($bar.width() + 50);
        }, 1000);
    }

    function endLoadingMapTransition() {
        var $modal = $('.js-loading-bar'),
                $bar = $modal.find('.progress-bar');
        $bar.removeClass('animate');
        $modal.modal('hide');
        $bar.width(100);
    }


    $("#address").on('input', function () {
        var requestData = {
            'city': $("#address").val(),
        };
        var urlAPI = "{{route('getAllCity')}}";
        $.ajax({
            headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
            data: requestData,
            url: urlAPI,
            type: 'post',
            beforeSend: function () {
                //beginTransition();
            },
            complete: function () {
                //endLoadingMapTransition();
            },
            success: function (data) {
                $("#address").autocomplete({
                    source: data
                });
            }
        });
    })

$('#form-filter-search .clear-btn').click(function() {
$('#address').val('');
$(this).hide()
return false;
})

    @if ($search_page)
            $('.src-form-order').submit(function (e) {
        e.preventDefault()
        base_url = '/search/bisnis/{{$keyword}}';
        val = $(this).closest('form').find('input[type=radio]:checked').val()
        window.location.href = "?sort=" + val;
    })
            @endif
</script>
@stop
