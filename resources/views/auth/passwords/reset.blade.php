@extends('frontend.layouts.homepage')

@section('header')
{!! Html::style('/css/main.min.css') !!}
@include('frontend.layouts.main_header')
@stop

@section('content')
<!-- content -->
<div class="daftar-data">
    <h2>KEMBANGKAN BISNIS ANDA BERSAMA KAMI</h2>
    <p align="center">Belum punya akun Yellow Pages? <a href="{{ route('signup') }}">Daftar disini</a></p>
    <div class="container">
        <div class="row  pad-top">
            <div class="col-md-6 col-md-offset-3" >
                <div class="panel panel-default login">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form role="form" action="{{ url('/password/reset') }}" method="POST" id="main-form">
                        {!! csrf_field() !!}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Alamat Email" name="email" value="{{ $email or old('email') }}" required autofocus id="username">
                            <span class="fa fa-envelope-o fa-2x form-control-feedback"></span>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" placeholder="Password" name="password" required id="password">
                            <span class="fa fa-lock fa-2x form-control-feedback"></span>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" placeholder="Konfirmasi Password" name="password_confirmation" required>
                            <span class="fa fa-lock fa-2x form-control-feedback"></span>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </form>
                    <div class="col-md-6">
                    </div>
                    <button type="submit" class="btn btn-yellow btn-login" form="main-form">Reset Password</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="/js/messages_id.js"></script>
<script>
$("#main-form").validate({
    rules: {
        email: {
            required: true,
            email: true,
            remote: {
                url: '/ajax/check_email',
                type: 'post',
                data: {
                    _token: window.Laravel.csrfToken,
                    must_exists: true
                }
            }
        },
        password: {
            required: true,
            minlength: 5
        },
        password_confirmation: {
            required: true,
            minlength: 5,
            equalTo: "#password"
        },
    },
    messages: {
        email: {
            remote: "Email tidak terdaftar"
        }
    }
});
</script>
@endsection
