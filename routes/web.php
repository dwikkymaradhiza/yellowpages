<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'Front\HomepageController@index']);

Route::get('/keluar', ['as' => 'signout', 'uses' => 'Auth\LoginController@logout']);
Route::get('/signin', ['as' => 'signin', 'uses' => 'Front\UserController@showLoginForm'])->middleware('guest');;
Route::get('/forgot', ['as' => 'forgot', 'uses' => 'Front\UserController@showForgotForm']);
Route::post('/signin', ['as' => 'signin_post', 'uses' => 'Front\UserController@login']);
Route::get('/konfirmasi_email/{token}', ['as' => 'email_confirmation', 'uses' => 'Front\UserController@activateUser']);

Route::group(['middleware' => ['web'], 'prefix' => 'daftar'], function() {
	Route::get('/', ['as' => 'signup', 'uses' => 'Front\UserController@signup']);
    Route::post('/', ['as' => 'signup_email', 'uses' => 'Front\UserController@signup_email']);
	Route::get('/profil', ['as' => 'signup_profile', 'uses' => 'Front\UserController@signup_profile']);
	Route::post('/profil', ['as' => 'signup_profile_post', 'uses' => 'Front\UserController@signup_profile_post']);
	Route::get('/direktori', ['as' => 'signup_directory', 'uses' => 'Front\UserController@signup_directory']);
	Route::post('/direktori', ['as' => 'signup_directory_post', 'uses' => 'Front\UserController@signup_directory_post']);
	Route::get('/produk', ['as' => 'signup_product', 'uses' => 'Front\UserController@signup_product']);
	Route::post('/produk', ['as' => 'signup_product_post', 'uses' => 'Front\UserController@signup_product_post']);
	Route::get('/terimakasih', ['as' => 'signup_success', 'uses' => 'Front\UserController@signup_success']);
    Route::get('/selesai', ['as' => 'signup_done', 'uses' => 'Front\UserController@signup_done']);
});

Route::get('/claim/{slug}', ['as' => 'claim', 'uses' => 'Front\UserController@claim']);

Route::get('/redirect/{social}', ['as' => 'social_redirect', 'uses' => 'SocialAuthController@redirect']);
Route::get('/callback/{social}', ['as' => 'social_callback', 'uses' => 'SocialAuthController@callback']);

Route::post('/inquiry', ['as' => 'inquiry', 'uses' => 'Front\InquiryController@store']);
Route::get('/produk/{slug?}', ['as' => 'product-detail', 'uses' => 'Front\ProductController@show']);
Route::get('/produk-click/{slug?}', ['as' => 'product-detail-click', 'uses' => 'Front\ProductController@click']);
Route::post('/produk-impression/{slug?}', ['as' => 'product-detail-impression', 'uses' => 'Front\ProductController@impress']);
Route::post('/produk/rating', ['as' => 'product-rating-create', 'uses' => 'Front\RatingController@store_product']);
Route::post('/produk/favorit', ['as' => 'product-favorite', 'uses' => 'Front\ProductController@favorite']);

Route::get('/cari/{search_by}/{keyword}/', ['as' => 'search', 'uses' => 'Front\DirectoryController@showDirectorySearch']);
Route::get('/cari/{search_by}/{keyword}/sort/{sort}', ['as' => 'search-sort', 'uses' => 'Front\DirectoryController@showDirectorySearchSort']);

Route::get('/cari/{search_by}/{keyword}/{location}', ['as' => 'search-full', 'uses' => 'Front\DirectoryController@showDirectorySearch']);
Route::get('/cari/{search_by}/{keyword}/{location}/sort/{sort}', ['as' => 'search-full-sort', 'uses' => 'Front\DirectoryController@showDirectorySearchSort']);

Route::post('/bisnis/getListing', ['as' => 'get-directory-listing', 'uses' => 'Front\DirectoryController@getDirectoryListing']);
Route::post('/bisnis/kategori/order/', ['as' => 'directory-category-ord', 'uses' => 'Front\DirectoryController@showDirectoryListingWithOrd']);
Route::post('/bisnis/kategori/location/', ['as' => 'directory-category-location', 'uses' => 'Front\DirectoryController@showDirectoryListingWithLocation']);
Route::get('/bisnis/kategori/{category?}', ['as' => 'directory-category', 'uses' => 'Front\DirectoryController@showDirectoryListing']);
Route::get('/bisnis/kategori/{category?}/page/{page?}', ['as' => 'directory-category-with-page', 'uses' => 'Front\DirectoryController@showDirectoryListing']);

Route::get('/bisnis/kategori/{category?}/location/{location?}/page/{page?}', ['as' => 'directory-category-with-location', 'uses' => 'Front\DirectoryController@showDirectoryListingLocation']);
Route::get('/bisnis/kategori/{category?}/ord/{ord?}/page/{page?}', ['as' => 'directory-category-with-ord', 'uses' => 'Front\DirectoryController@showDirectoryListingOrd']);
Route::get('/bisnis/kategori/{category?}/location/{location?}/ord/{ord?}/page/{page?}', ['as' => 'directory-category-with-location-ord', 'uses' => 'Front\DirectoryController@showDirectoryLocationOrd']);

Route::get('/bisnis/{slug?}', ['as' => 'directory', 'uses' => 'Front\DirectoryController@show']);
Route::get('/bisnis-click/{slug?}', ['as' => 'directory-click', 'uses' => 'Front\DirectoryController@click']);
Route::post('/bisnis-impression/{slug?}', ['as' => 'directory-impression', 'uses' => 'Front\DirectoryController@impress']);
Route::post('/bisnis/rating', ['as' => 'directory-rating-create', 'uses' => 'Front\RatingController@store_directory']);
Route::post('/bisnis/favorit', ['as' => 'directory-favorite', 'uses' => 'Front\DirectoryController@favorite']);

Route::post('/city/', ['as' => 'getAllCity', 'uses' => 'Front\CityController@getAllCity']);
Route::post('/city-by-province/', ['as' => 'getAllCityByProvince', 'uses' => 'Front\CityController@getAllCityByProvince']);

Route::get('/ajax/city/{province_id}/{format}', ['as' => 'findCity', 'uses' => 'Front\CityController@ajaxFindCity']);
Route::post('/ajax/check_email', ['uses' => 'Front\UserController@ajaxCheckEmail']);
Route::get('/ajax/info_counter', ['uses' => 'Front\DashboardController@ajaxInfoCounter']);

Auth::routes();

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'dashboard'], function() {
    Route::get('/', ['as' => 'dashboard', 'uses' => 'Front\DashboardController@index']);
    Route::get('/first', ['as' => 'dashboard-first', 'uses' => 'Front\DashboardController@first']);
    
    Route::get('/inquiries', ['as' => 'dashboard-inquiry-index', 'uses' => 'Front\InquiryController@index']);
    Route::get('/inquiries/{id}/detail', ['as' => 'dashboard-inquiry-detail', 'uses' => 'Front\InquiryController@show']);
    Route::post('/inquiries/delete-bulk' ,['as' => 'dashboard-inquiry-delete-bulk', 'uses' => 'Front\InquiryController@destroyBulk']);
    Route::delete('/inquiries/{id}', ['as' => 'dashboard-inquiry-delete', 'uses' => 'Front\InquiryController@destroy']);
    
    Route::get('/products', ['as' => 'dashboard-product-index', 'uses' => 'Front\ProductController@index']);
    
    Route::get('/directories', ['as' => 'dashboard-directory-index', 'uses' => 'Front\DirectoryController@index']);
    Route::get('/directory/mobile/create', ['as' => 'dashboard-directory-create-product-mobile', 'uses' => 'Front\DirectoryController@mobileCreate']);
    Route::get('/directory/{id}', ['as' => 'dashboard-directory-edit', 'uses' => 'Front\DirectoryController@update']);
    Route::post('/directory/{id}', ['as' => 'dashboard-directory-update', 'uses' => 'Front\DirectoryController@edit']);
    Route::post('/directories', ['as' => 'dashboard-directory-create', 'uses' => 'Front\DirectoryController@store']);
    Route::delete('/directories/{id}', ['as' => 'dashboard-directory-delete', 'uses' => 'Front\DirectoryController@destroy']);
    Route::delete('/directory/picture/{id}', ['as' => 'dashboard-directory-picture-delete', 'uses' => 'Front\DirectoryController@destroyPicture']);
    Route::post('/directories/delete-bulk' ,['as' => 'dashboard-directory-delete-bulk', 'uses' => 'Front\DirectoryController@destroyBulk']);
    Route::get('/ajax/directory-categories/{param?}', ['as' => 'directory-categories', 'uses' => 'Front\DirectoryController@getDirectoryCategories']);

    Route::post('/ajax/delete-bulk-produk', ['as' => 'delete-bulk-produk', 'uses' => 'Front\productController@deleteBulk']);

    Route::get('/status', ['as' => 'dashboard-status-index', 'uses' => 'Front\DirectoryInfoController@index']);
    Route::post('/status', ['as' => 'dashboard-status-create', 'uses' => 'Front\DirectoryInfoController@store']);
    Route::get('/status/add', ['as' => 'dashboard-status-add', 'uses' => 'Front\DirectoryInfoController@create']);
    Route::post('/status/delete-bulk' ,['as' => 'dashboard-status-delete-bulk', 'uses' => 'Front\DirectoryInfoController@destroyBulk']);
    Route::get('/status/{id}/detail', ['as' => 'dashboard-status-detail', 'uses' => 'Front\DirectoryInfoController@show']);
    Route::delete('/status/{id}', ['as' => 'dashboard-status-delete', 'uses' => 'Front\DirectoryInfoController@destroy']);
    
    Route::get('/settings', ['as' => 'dashboard-setting-index', 'uses' => 'Front\UserController@profile']);
    Route::post('/settings/edit', ['as' => 'dashboard-setting-edit', 'uses' => 'Front\UserController@editprofile']);
    
    Route::post('/settings/update-membership', ['as' => 'dashboard-membership-update', 'uses' => 'Front\UserController@membershipUpdate']);
    Route::get('/settings/thanks', ['as' => 'thanks-page', 'uses' => 'Front\UserController@thanks']);
    Route::get('/settings/confirmation', ['as' => 'confirmation-page', 'uses' => 'Front\UserController@confirmation']);
    Route::post('/settings/payment-confirmation', ['as' => 'payment-confirmation', 'uses' => 'Front\UserController@paymentConfirmation']);    
    
    Route::post('/produk/create' ,['as' => 'create_product', 'uses' => 'Front\ProductController@create']);
    Route::post('/produk/delete' ,['as' => 'dashboard-product-delete', 'uses' => 'Front\ProductController@destroy']);
    Route::post('/produk/delete-bulk' ,['as' => 'dashboard-product-delete-bulk', 'uses' => 'Front\ProductController@destroyBulk']);
    Route::get('/produk/{id?}' ,['as' => 'edit_product', 'uses' => 'Front\ProductController@edit']);
    Route::post('/produk/{id}' ,['as' => 'update_product', 'uses' => 'Front\ProductController@update']);
    Route::delete('/produck/picture/{id}', ['as' => 'dashboard-product-picture-delete', 'uses' => 'Front\ProductController@destroyPicture']);
    Route::get('/verify/{slug}', ['as' => 'verify-form', 'uses' => 'Front\DirectoryController@verifyForm']);
    Route::post('/verify/{slug}', ['as' => 'verify-submit', 'uses' => 'Front\DirectoryController@verifySubmit']);
    
    Route::get('/sponsor/', ['as' => 'sponsor-business', 'uses' => 'Front\SponsorController@index']);
    Route::get('/sponsor/package/', ['as' => 'sponsor-package', 'uses' => 'Front\SponsorController@package']);
    Route::get('/sponsor/invoice/', ['as' => 'sponsor-invoice', 'uses' => 'Front\SponsorController@invoice']);
    Route::post('/sponsor/invoice-update', ['as' => 'sponsor-invoice-update', 'uses' => 'Front\SponsorController@sponsorUpdate']);
    Route::get('/sponsor/thanks', ['as' => 'sponsor-thanks', 'uses' => 'Front\SponsorController@thanks']);
    
    Route::get('/topup', ['as' => 'topup', 'uses' => 'PaymentController@topup']);
    Route::get('/payment/thank_you', ['as' => 'topup-thank-you', 'uses' => 'PaymentController@payment_finish']);
    Route::get('/payment/error', ['as' => 'topup-error', 'uses' => 'PaymentController@payment_error']);
    Route::get('/payment/success', ['as' => 'topup-success', 'uses' => 'PaymentController@payment_success']);
    Route::post('/payment/{method}', ['as' => 'topup-payment-process', 'uses' => 'PaymentController@payment_process']);
    Route::post('/payment_method', ['as' => 'topup-payment-method', 'uses' => 'PaymentController@payment_method']);
    Route::get('/payment_method', ['as' => 'topup-payment-method-get', 'uses' => 'PaymentController@payment_method_view']);
    Route::post('/payment_method/{channel}/{method}', ['as' => 'topup-payment-method-action', 'uses' => 'PaymentController@payment_method_action']);
    Route::get('/payment_callback/success', ['as' => 'payment-callback-success', 'uses' => 'PaymentController@callback_payment_success']);
    Route::get('/payment_callback/failed', ['as' => 'payment-callback-failed', 'uses' => 'PaymentController@callback_payment_failed']);

    // dashboard sms routes
    Route::get('/sms', ['as' => 'dashboard-sms-index', 'uses' => 'Front\SmsController@index']);

    // SMS Blast
    Route::get('/sms/blast', ['as' => 'dashboard-sms-blast', 'uses' => 'Front\SmsController@blast']);
    Route::post('/sms/phone-number-store', ['as' => 'phone-number-store', 'uses' => 'Front\SmsController@phoneNumberStore']);
    Route::get('/sms/blast/setting', ['as' => 'dashboard-sms-blast-setting', 'uses' => 'Front\SmsController@blastSetting']);
    Route::get('/sms/blast/content', ['as' => 'dashboard-sms-blast-content', 'uses' => 'Front\SmsController@content']);
    Route::post('/sms/blast/content/store', ['as' => 'dashboard-sms-blast-content-store', 'uses' => 'Front\SmsController@contentStore']);

    Route::get('/sms/blast/quantity', ['as' => 'dashboard-sms-blast-quantity', 'uses' => 'Front\SmsController@quantity']);
    Route::get('/sms/blast/schedule', ['as' => 'dashboard-sms-blast-schedule', 'uses' => 'Front\SmsController@schedule']);
    Route::get('/sms/blast/sender', ['as' => 'dashboard-sms-blast-sender', 'uses' => 'Front\SmsController@sender']);
    Route::post('/sms/blast/create-campaign', ['as' => 'dashboard-sms-create-campaign', 'uses' => 'Front\SmsController@createCampaign']);
    Route::get('/sms/blast/confirmation', ['as' => 'dashboard-sms-blast-confirmation', 'uses' => 'Front\SmsController@confirmation']);
    Route::post('/sms/blast/save-order', ['as' => 'dashboard-sms-blast-save-order', 'uses' => 'Front\SmsController@saveOrder']);
    Route::get('/sms/blast/thankyou', ['as' => 'dashboard-sms-blast-thankyou', 'uses' => 'Front\SmsController@thankyou']);

    // SMS Targeted
    Route::get('/sms/targeted', ['as' => 'dashboard-sms-targeted', 'uses' => 'Front\SmsController@targeted']);
    Route::get('/sms/targeted/interest', ['as' => 'dashboard-sms-targeted-interest', 'uses' => 'Front\SmsController@interest']);
    Route::get('/sms/targeted/interest', ['as' => 'dashboard-sms-targeted-interest', 'uses' => 'Front\SmsController@interest']);
    Route::get('/sms/targeted/not-interest', ['as' => 'dashboard-sms-targeted-not-interest', 'uses' => 'Front\SmsController@notInterest']);
    Route::get('/sms/targeted/location', ['as' => 'dashboard-sms-targeted-location', 'uses' => 'Front\SmsController@location']);
    Route::get('/sms/targeted/operator', ['as' => 'dashboard-sms-targeted-operator', 'uses' => 'Front\SmsController@operator']);
    Route::get('/sms/targeted/setting', ['as' => 'dashboard-sms-targeted-setting', 'uses' => 'Front\SmsController@blastSetting']);
    Route::get('/sms/targeted/sender', ['as' => 'dashboard-sms-targeted-sender', 'uses' => 'Front\SmsController@senderTargeted']);
    Route::post('/sms/targeted/create-campaign', ['as' => 'dashboard-sms-targeted-create-campaign', 'uses' => 'Front\SmsController@targetedCreateCampaign']);

    // SMS Call History
    Route::get('/sms/call-history', ['as' => 'dashboard-sms-call-history', 'uses' => 'Front\SmsController@callHistory']);
    Route::get('/sms/call-history/interest', ['as' => 'dashboard-sms-call-history-interest', 'uses' => 'Front\SmsController@callHistoryInterest']);
    Route::get('/sms/call-history/receiver', ['as' => 'dashboard-sms-call-history-receiver', 'uses' => 'Front\SmsController@callHistoryReceiver']);
    Route::get('/sms/call-history/receiver-days', ['as' => 'dashboard-sms-call-history-receiver-days', 'uses' => 'Front\SmsController@callHistoryReceiverDays']);
    Route::post('/sms/call-history/create-campaign-call-history', ['as' => 'dashboard-sms-call-history-create-campaign', 'uses' => 'Front\SmsController@callHistoryCreateCampaign']);
    Route::get('/sms/call-history/pattern', ['as' => 'dashboard-sms-call-history-pattern', 'uses' => 'Front\SmsController@callHistoryPattern']);

    Route::post('/sms/bad-word', ['as' => 'dashboard-sms-bad-word', 'uses' => 'Front\SmsController@badWordValidation']);

    Route::get('/piwik', ['as' => 'dashboard-piwik', 'uses' => 'Front\SmsController@piwik']);
});

Route::post('/payment_callback', ['as' => 'topup-payment-code-callback', 'uses' => 'PaymentController@callback_payment']);
Route::get('/smsupdatemo', ['as' => 'smsupdate', 'uses' => 'Front\SmsupdateController@smsupdatemo']);
Route::get('/smsupdatedr', ['as' => 'smsupdate-dr', 'uses' => 'Front\SmsupdateController@smsupdatedr']);
Route::get('/p/{slug}', ['as' => 'static-page', 'uses' => 'Front\StaticController@show']);

