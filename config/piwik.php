<?php

return array(

    /*
     *  Server URL
     */

    'piwik_url'     => 'http://192.168.33.10/piwik',

    /*
     *  Piwik Username and Password
     */

    'username'      => 'piwik',
    'password'      => 'piwikpiwik',

    /*
     *  Optional API Key (will be used instead of Username and Password) 
     *  The bundle works much faster with the API Key, rather than username and password.
     */

    'api_key'       =>  '697907bdaecebd4da2cf3b3ce2a4abed',

    /*
     *  Format for API calls to be returned in
     *  
     *  Can be [php, xml, json, html, rss, original]
     *  
     *  The default is 'json'
     */

    'format'        => 'json',

    /*
     *  Period/Date range for results
     *  
     *  Can be [today, yesterday, previous7, previous30, last7, last30, currentweek, currentmonth, currentyear] as well as a date range in the format of "yyyy-MM-dd,yyyy-MM-dd"
     *
     *  The default is 'yesterday'
     */

    'period'        => 'today',

    /*
     *  The Site ID you want to use
     */

    'site_id'       => '1',
);