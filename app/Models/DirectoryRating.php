<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class DirectoryRating extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'directory_rating';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Get the product that owns the rating.
     */
    public function directory()
    {
        return $this->belongsTo('App\Models\Directory', 'directory_id', 'id');
    }
    
    /**
     * Get the user that owns the rating.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    
    /**
     * Get the rating date attribute.
     *
     * @return string
     */
    public function getRatingDateAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['create_time']))->diffForHumans();
    }
    
    public function getAverageRating($id)
    {
        $avg_result = DB::table($this->table)
                ->where('directory_id', $id)
                ->avg('rate');
        
        return $avg_result;
    }
    
    public function updateDirectoryRating($directory_id, $rating){
        DB::table('directory')->where('id', '=', $directory_id)
                ->update(['rating' => $rating]);
        
        return true;
    }

    public static function getDirectoryRating($directory) {
        return \Cache::remember('dir:'.$directory->slug.':ratings', config('cache.expires.dir:ratings', config('cache.expires.general')), function() use($directory) {
            return DirectoryRating::from('directory_rating as a')
                ->leftJoin('users AS u','user_id','=','u.id')
                ->where('directory_id', '=', $directory->id)
                ->select('a.*', 'u.first_name as uf_name', 'u.last_name as ul_name')
                ->orderby('a.id','desc')
                ->get();
        });
    }
}
