<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Package extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'packages';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Get the product that owns the rating.
     */
    
    public static function getSMSPriceID()
    {
        $data = DB::select( DB::raw("SELECT id FROM `packages` WHERE type = 'SMS' AND description = '1'"));

        if(!empty($data))
            return $data[0]->id;
        else
            return $data;
    }

    public static function getPrice($id)
    {
        $data = DB::select( DB::raw("SELECT price FROM `packages` WHERE id = :id"),
                    array(
                        "id"    => $id
                    ));

        if(!empty($data))
            return $data[0]->price;
        else
            return $data;
    }
    
    public static function getSMSPrice()
    {
        $data = DB::select( DB::raw("SELECT price FROM `packages` WHERE type = 'SMS' AND description = 1"));

        if(!empty($data))
            return $data[0]->price;
        else
            return $data;
    }

    public static function getSMSPackage()
    {
        $data = DB::select( DB::raw("SELECT * FROM `packages` WHERE type = 'SMS' AND description != 1"));

        return $data;
    }
    
}
