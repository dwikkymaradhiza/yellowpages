<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliateWebsite extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'affiliate_website';
}
