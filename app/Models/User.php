<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use App\Models\Directory;
use App\Models\Package_order;
use Auth;

use Mail;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Get the rating record associated with the product.
     */
    public function ratings()
    {
        return $this->hasMany('App\Models\Rating');
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = $value;
    }
    
    public function updateUser($data_user) {
        
        DB::table('users')->where('id', '=', $data_user['user_id'])
                ->update(
        [
            'first_name' => $data_user['first_name'],
            'last_name' => $data_user['last_name'],
            'handphone' => $data_user['handphone'],
            'email' => $data_user['email'],
            'province_id' => $data_user['province'],
            'city_id' => $data_user['city'],
            'address' => $data_user['address'],            
        ]
                        );
        
        return true;
    }

    public function sendPasswordResetNotification($token) {
        $reset_url = url('password/reset', [$token]);
        Mail::send('emails.reset_password', ['reset_url' => $reset_url], function ($m) {
            $m->from('noreply@mail.yellowpages.co.id', 'Yellow Pages');
            $m->to($this->email, $this->first_name)->subject('Reset Password');
        });
    }
    
    public static function log($activity, $user_id = null) {
        if(empty($user_id)) {
            return false;
        }
        
        $mongo = new \MongoDB\Client('mongodb://'.env('MONGO_HOST'));
        
        $collection = $mongo->log_activity->{$user_id};
        
        $collection->insertOne([
            'activity' => $activity,
            'time' => date('Y-m-d H:i:s')
        ]);
    }
    
    public static function getLogs($limit = 6) {
        if(!Auth::check()){
            return false;
        }
        
        $mongo = new \MongoDB\Client('mongodb://'.env('MONGO_HOST'));
        
        $user_id = Auth::id();
        $collection = $mongo->log_activity->{$user_id};
        $cursor = $collection->find([], [
            'limit' => $limit,
            'sort' => ['time' => -1]
        ]);
        
        return $cursor;
    }

    public static function checkPackage($id) {
        $package_order = Package_order::where('user_id',$id)
            ->join('packages as P','P.id','package_order.package_id')
            ->where('package_order.status','1')
            ->orderby('package_order.id','desc')->first();

        return $package_order;
    }
}
