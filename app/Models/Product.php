<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'directory_product';

    protected $fillable = [
        'name', 'description', 'slug', 'published', 'directory_id', 'stock', 'picture', 'brand', 'masterid', 'masterid_key'
    ];

    
    /**
     * Get the directory that owns the product.
     */
    public function directory()
    {
        return $this->belongsTo('App\Models\Directory');
    }
    
    /**
     * Get the rating record associated with the product.
     */
    public function ratings()
    {
        return $this->hasMany('App\Models\Rating')->orderBy('id', 'desc');
    }
    
    /**
     * Get similar product by categories.
     */
    public function scopeFindSimilarByCategories($query, $categories)
    {
        $product = new Product;
        $directory = new Directory;
        
        $ids = [];
        foreach($categories as $k => $v) {
            $ids[] = $v->id;
        }
        
        return $query->join($directory->getTable(), "{$directory->getTable()}.id", "{$product->getTable()}.directory_id")
                     ->join('directory_category', "{$directory->getTable()}.id", "directory_category.directory_id")
                     ->whereIn('directory_category.category_id', $ids)
                     ->select("{$product->getTable()}.name", "{$product->getTable()}.id", "{$product->getTable()}.slug", "{$directory->getTable()}.address_name", "{$directory->getTable()}.address_1")
                     ->distinct();
    }
    
    /**
     * 
     * @param type $segment
     * @param type $date
     * @param type $limit
     * @return type $result
     */
    public static function getFeaturedProduct($segment, $limit) {
        $sql = DB::table('directory_product AS DP')
                ->leftJoin('product_counter AS PC','PC.product_id','=','DP.id')
                ->leftJoin('directory AS D','DP.directory_id','=','D.id')
                ->where('PC.segment', '=', $segment)
                ->whereRaw('PC.counter < PC.kuota')
                ->select('PC.product_counter_id','PC.counter','DP.name','DP.description','D.name as directory_name' ,'DP.picture','DP.slug')
                ->limit($limit)
                ->orderby('PC.counter','asc');
                
        $result = $sql->get();
        
        //changed to view only
//        foreach($result as $resultDetail){
//            $total = $resultDetail->counter + 1;
//
//            DB::table('product_counter')->where('product_counter_id', '=', $resultDetail->product_counter_id)->update([
//                'counter' => $total
//            ]); 
//        }        
        return $result;
    }
    
    public function updateProductCounter($id,$kuota){
        $product_counter = DB::table('product_counter AS PC')
                ->where('PC.product_id', '=', $id)                
                ->where('PC.segment', '=', 'feature')
                ->whereRaw('PC.counter < PC.kuota')
                ->select('PC.product_counter_id','PC.counter', 'PC.kuota')
                ->first();

        if(count($product_counter)>0){
            DB::table('product_counter')->where('product_id', '=', $id)
                    ->whereRaw('counter < kuota')
                    ->where('segment', '=', 'feature')
                    ->update([
                        'kuota' => ($product_counter->kuota + $kuota),
                        'updated_at' => date('Y-m-d')
                    ]);
        }else{
            
            $sql = 'insert into product_counter (product_id, segment, counter, kuota, created_at, updated_at) values ( ?, ?, ?, ?, ?, ?)';
            DB::insert($sql, 
                    [
                        $id,
                        'feature',
                        0,
                        $kuota,
                        date('Y-m-d'),
                        date('Y-m-d')                        
                    ]);
        }
        
        return $product_counter;
    }
    
    public function addingProductCounter ($segment, $product_id){
        $sql = DB::table('product_counter AS PC')
                ->where('PC.product_id', '=', $product_id)
                ->where('PC.segment', '=', $segment)
                ->whereRaw('PC.counter < PC.kuota')
                ->select('PC.product_counter_id','PC.counter','PC.kuota')
                ->orderby('PC.counter','asc');
        $result = $sql->first();
        
        $total = $result->counter + 1;

        DB::table('product_counter')->where('product_counter_id', '=', $result->product_counter_id)->update([
            'counter' => $total
        ]); 
    }
    
    public function counter($action = '', $user_id = '', $product_id){
        if(empty($user_id) || empty($action)) {
            return false;
        }
        
        $mongo = new \MongoDB\Client('mongodb://'.env('MONGO_HOST'));
        
        if($action === 'click'){
            $col = $mongo->selectDatabase("click_counter")->{$user_id};
        }elseif($action === 'view'){
            $col = $mongo->selectDatabase("view_counter")->{$user_id};
            
            // give counter ++
            $this->addingProductCounter('feature', $product_id);
        }
        
       
        $col->findOneAndUpdate(
                array('product_id' => $product_id, 'date' => date('Y-m-d')),
                array('$inc' => array('counter' => 1)),
                array('new' => true, 'upsert' => true)
        );
       
    }
    
    public static function getCounter($action = '', $user_id = '', $date){
        $mongo = new \MongoDB\Client('mongodb://'.env('MONGO_HOST'));
        
        if($action === 'click'){
            $col = $mongo->selectDatabase("click_counter")->{$user_id};
        }elseif($action === 'view'){
            $col = $mongo->selectDatabase("view_counter")->{$user_id};
        }
        
        $counter = $col->find(array('date' => $date))->toArray();    
        return $counter;
    }

    public static function getLatest($limit=5) {
        $cache_key = 'pdp:latest';
        $data = \Cache::remember($cache_key, config('cache.expires.'.$cache_key, config('cache.expires.general')), function() {
            return Product::from('directory_product as dp')
                ->select('dp.*', 'd.name as dir_name', 'd.id as dir_id', 'd.slug as dir_slug')
                ->join('directory as d', 'dp.directory_id', '=', 'd.id')
                ->where('dp.published','=','1')
                ->orderby('dp.id','desc')
                ->limit(100)
                ->get();
        });

        return $data->splice(0, $limit);
    }

    public static function getDirectoryProduct($dirId) {
        $data = \Cache::remember('dir:id:'.$dirId.':products', config('cache.expires.'.$cache_key, config('cache.expires.general')), function() {
            return Product::where('published','=','1')
                ->orderby('id','desc')
                ->limit(100)
                ->get();
        });
    }

    public static function findBySlug($slug) {
        $cache_key = 'pdp';
        $data = \Cache::remember($cache_key.':slug:'.$slug, config('cache.expires.'.$cache_key, config('cache.expires.general')), function() use($slug) {
            return Product::where('slug', $slug)->where('published', 1)->first();
        });

        if($data) {
            \Cache::add($cache_key.':id:'.$data->id, $data, config('cache.expires.'.$cache_key, config('cache.expires.general')));
        }

        return $data;
    }
}
