<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Get the directories record associated with the category.
     */
    public function directories()
    {
        return $this->belongsToMany('App\Models\Directory', 'directory_category', 'category_id', 'directory_id');
    }
}
