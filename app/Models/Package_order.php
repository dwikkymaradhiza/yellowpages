<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Package_order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'package_order';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * Get the product that owns the rating.
     */
    
    public function updateMembership($array){
        $order_id = date("Ym").rand('1000','9999');
        DB::insert('insert into package_order (order_id, user_id,package_id,payment_id,price,status,directories_id,products_id,created_at, updated_at) values (?,?,?, ?, ?, ?, ?, ?, now(), now())', 
                    [
                        $order_id,
                        $array['user_id'],
                        $array['package_id'],
                        $array['payment_id'],
                        $array['price'], 
                        $array['status'], 
                        $array['directories_id'],
                        $array['products_id'],
                    ]);
        return $order_id;
    }
    
    public function getPendingSponsorPackage(){
        $sql = DB::table('package_order AS PO')
                ->join('packages AS P', function($join) {
                    $join->on('P.id','=','PO.package_id');
                    $join->on('P.type','=',DB::raw("'SPONSORED'"));
                })
                ->where('PO.status', '=', 1)
                ->select('PO.directories_id','PO.products_id','PO.id', 'P.description');
               
        $result = $sql->get();
        
        return $result;
    }
    
    public function updateSponsorPackage($id,$status){
        $sql = DB::table('package_order AS PO')
                ->where('PO.id','=',$id)
                ->update([
                        'status' => $status
                    ]);
    }
    
}
