<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SMSBlastNumber extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sms_blast_numbers';

}
