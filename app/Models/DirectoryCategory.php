<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirectoryCategory extends Model
{
    protected $table = 'directory_category';

    protected $fillable = [
        'directory_id', 'category_id'
    ];

    public $timestamps = false;
}
