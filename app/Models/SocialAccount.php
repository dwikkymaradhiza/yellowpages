<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialAccount extends Model
{
    protected $fillable = ['user_id', 'provider_user_id', 'provider'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function createOrGetUser(ProviderUser $providerUser, $provider)
    {
        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return [
                'data' => $account->user, 
                'is_new' => FALSE, 
                'profile_submit' => $account->user->profile_submit,
                'directory_submit' => $account->user->directory_submit
            ];
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => ($providerUser->getEmail()) ? $providerUser->getEmail() : '',
                    'first_name' => $providerUser->getName(),
                    'avatar' => $providerUser->getAvatar(),
                    'password' => '',
                    'confirm_submit' => 1
                ]);
            }

            $account->user()->associate($user);
            $account->save();

	    return ['data' => $user, 'is_new' => (TRUE && !$user->profile_submit && !$user->directory_submit), 'profile_submit' => $user->profile_submit, 'directory_submit' => $user->directory_submit];
        }

    }
}
