<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPicture extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_picture';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public static function findByOwner($product) {
        return \Cache::remember('pdp:id:'.$product->id.':gallery', config('cache.expires.pdp_gallery', config('cache.expires.general')), function() use($product) {
            return ProductPicture::where('product_id', $product->id)->get();
        });
    }
}
