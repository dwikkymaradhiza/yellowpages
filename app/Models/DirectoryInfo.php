<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class DirectoryInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'directory_info';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Indicates date fields.
     */
    protected $dates = ['created_at'];
    
    /**
     * Get the directory that owns the product.
     */
    public function directory()
    {
        #$cache_key = 'dir_detail';
        #return \Cache::remember($cache_key.'_{$this->id}' , config('cache.expires.'.$cache_key, config('cache.expires.general')), function()
        #{
            return $this->belongsTo('App\Models\Directory');
        #});
    }

    public static function getLatestInfo($limit) {
        $cache_key = 'home:info_latest';
        return \Cache::remember($cache_key, config('cache.expires.'.$cache_key, config('cache.expires.general')), function() use($limit) {
            return $res = DB::table('directory_info AS di')
                ->join('directory AS d','di.directory_id','=','d.id')
                ->orderBy('di.id', 'desc')
                ->limit($limit)
                ->select('di.*', 'd.slug')
                ->get();
        });
    }
    
    /**
     * Get the info time attribute.
     *
     * @return string
     */
    public function getInfoTimeAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']))->diffForHumans();
    }

    public static function badWordValidation($message) {
        // remove all non space & non characters
        $sentences = preg_replace('/[^ \w]+/', '', $message);
        // split on 1+ whitespace & ignore empty (eg. trailing space)
        $sentences = preg_split('/\s+/', $sentences, -1, PREG_SPLIT_NO_EMPTY);

        return DB::table('directory_info_words')->where(function ($q) use ($sentences) {
            foreach ($sentences as $value) {
                $q->orWhere('words', 'like', "%{$value}%");
            }
        })->count();
    }

    public static function smsBadWordValidation($message) {
        // remove all non space & non characters
        $sentences = preg_replace('/[^ \w]+/', '', $message);
        // split on 1+ whitespace & ignore empty (eg. trailing space)
        $sentences = preg_split('/\s+/', $sentences, -1, PREG_SPLIT_NO_EMPTY);

        return DB::table('directory_info_words')->where(function ($q) use ($sentences) {
            foreach ($sentences as $value) {
                $q->orWhere('words', 'like', "%{$value}%");
            }
        })->get();
    }
}
