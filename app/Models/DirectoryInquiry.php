<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirectoryInquiry extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'directory_inquiry';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Indicates date fields.
     */
    protected $dates = ['created_at'];
    
    /**
     * Get the directory that owns the inquiry.
     */
    public function directory()
    {
        return $this->belongsTo('App\Models\Directory', 'directory_id', 'id');
    }
    
    /**
     * Get the product that owns the inquiry.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
}
