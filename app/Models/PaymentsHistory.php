<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentsHistory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payment_history';
    
    /**
     *
     * @var string 
     */
    protected $fillable = ['package_order_id', 'total_payment', 'description', 'user_id'];
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * The province that belong to the city.
     */
}
