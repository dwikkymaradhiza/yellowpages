<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use App\Models\City;

class Directory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string


     */

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $table = 'directory';

    protected $fillable = [
        'name', 'phone_1', 'website', 'description_lid', 'address_1', 'city_id', 'latitude', 'longitude', 'slug', 'logo', 'user_id', 'ecatalog', 'rating'
    ];
    

    
    /**
     * Get the product record associated with the directory.
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
    
    /**
     * The categories that belong to the directory.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'directory_category', 'directory_id', 'category_id');
    }
    
    /**
     * Get the rating record associated with the directory.
     */
    public function ratings()
    {
        return $this->hasMany('App\Models\DirectoryRating')->orderBy('id', 'desc');
    }
    
    /**
     * The user that belong to the directory.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    
    /**
     * The user that belong to the directory.
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id', 'id');
    }

    public static function findBySlug($slug) {
        $cache_key = 'dir';
        $data = \Cache::remember($cache_key.':slug:'.$slug, config('cache.expires.'.$cache_key, config('cache.expires.general')), function() use($slug) {
            return Directory::where('slug', $slug)->where('published', 1)->first();
        });

        if($data) {
            \Cache::add($cache_key.':id:'.$data->id, $data, config('cache.expires.'.$cache_key, config('cache.expires.general')));
        }

        return $data;

    }

    public static function getLatest($limit = 5) {
        $cache_key = 'dir:latest';
        $data = \Cache::remember($cache_key, config('cache.expires.'.$cache_key, config('cache.expires.general')), function() {
            return Directory::where('published','=','1')
                ->orderby('id','desc')
                ->limit(100)
                ->get();
        });

        return $data->splice(0, $limit);
    }
    
    /**
     * Get similar directory by categories.
     */
    public function scopeFindSimilarByCategories($query, $categories)
    {
        $directory = new Directory;
        
        $ids = [];
        foreach($categories as $k => $v) {
            $ids[] = $v->id;
        }
        
        return $query->join('directory_category', "{$directory->getTable()}.id", "directory_category.directory_id")
                     ->whereIn('directory_category.category_id', $ids)
                     ->select("{$directory->getTable()}.id", "{$directory->getTable()}.name", "{$directory->getTable()}.slug", "{$directory->getTable()}.address_name", "{$directory->getTable()}.address_1")
                     ->distinct();
    }
    
    /**
     * 
     * @param type $segment
     * @param type $date
     * @param type $limit
     * @return type $result
     */
    public static function getFeaturedDirectory($segment, $limit) {
        $sql = DB::table('directory AS D')
                ->leftJoin('directory_counter AS DC','DC.directory_id','=','D.id')
                ->where('DC.segment', '=', $segment)
                ->whereRaw('DC.counter < DC.kuota')
                ->select('DC.directory_counter_id','DC.counter','D.name','D.address_1 as address_name','D.logo','D.slug')
                ->limit($limit)
                ->orderby('DC.counter','asc');
                
        $result = $sql->get();
        
        //changed to impress only
//        foreach($result as $resultDetail){
//            $total = $resultDetail->counter + 1;
//
//            DB::table('directory_counter')->where('directory_counter_id', '=', $resultDetail->directory_counter_id)->update([
//                'counter' => $total
//            ]); 
//        }
        
        return $result;
    }
    
    public static function getSponsorDirectory($segment, $limit) {
        $sql = DB::table('directory AS D')
                ->leftJoin('directory_counter AS DC','DC.directory_id','=','D.id')
                ->where('DC.segment', '=', $segment)
                ->whereRaw('DC.counter < DC.kuota')
                ->select('DC.directory_counter_id','DC.counter','D.name','D.address_name','D.logo','D.slug')
                ->limit($limit)
                ->orderby('DC.counter','asc');
                
        $result = $sql->get();
        
        foreach($result as $resultDetail){
            $total = $resultDetail->counter + 1;

            DB::table('directory_counter')->where('directory_counter_id', '=', $resultDetail->directory_counter_id)->update([
                'counter' => $total
            ]); 
        }
        
        return $result;
    }

    public function updateDirectoryCounter($id,$kuota){
        $directory_counter = DB::table('directory_counter AS DC')
                ->where('DC.directory_id', '=', $id)                
                ->where('DC.segment', '=', 'feature')
                ->whereRaw('DC.counter < DC.kuota')
                ->select('DC.directory_counter_id','DC.counter', 'DC.kuota')
                ->first();

        if(count($directory_counter)>0){
            DB::table('directory_counter')->where('directory_id', '=', $id)
                    ->whereRaw('counter < kuota')
                    ->where('segment', '=', 'feature')
                    ->update([
                        'kuota' => ($directory_counter->kuota + $kuota),
                        'updated_at' => date('Y-m-d')
                    ]);
        }else{
            
            $sql = 'insert into directory_counter (directory_id, segment, counter, kuota, created_at, updated_at) values ( ?, ?, ?, ?, ?, ?)';
            DB::insert($sql, 
                    [
                        $id,
                        'feature',
                        0,
                        $kuota,
                        date('Y-m-d'),
                        date('Y-m-d')                        
                    ]);
        }
        
        return $directory_counter;
    }
    
    public static function solr_search($offset, $limit, $sort, $location, $search) {
        $solr_config = config('database.solr');
        $endpoint = "http://{$solr_config['host']}:{$solr_config['port']}{$solr_config['path']}{$solr_config['core']}".'/select?';

        $params = [];
        $params[] = 'wt=json';
        $params[] = 'indent=on';
        $params[] = 'start='.$offset;
        $params[] = 'rows='.$limit;

        if($sort === 'rating'){
            $params[] = 'sort='.rawurlencode('directory_rating_tf desc');
        }elseif($sort === 'id'){
            $params[] = 'sort='.rawurlencode('id desc');
        }elseif($sort === 'name_desc'){
            $params[] = 'sort='.rawurlencode('directory_title_t desc');
        }elseif($sort === 'name_asc'){
            $params[] = 'sort='.rawurlencode('directory_title_t asc');
        }elseif(!empty($search)){
            // let use most relevance order
        }

        if($location) {
            $params[] = 'fq=city_name_t:'.rawurlencode($location);
        }

        if(!empty($search)) {
            $kw = urlencode($search['keyword']);
            $params[] = ('q=(directory_title_t:'.urlencode($kw).')^=5.0%20directory_desc_t:'.$kw);
        }

        $params = implode('&', $params);
        $endpoint .= $params;

        $c = curl_init();
        curl_setopt_array($c, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $endpoint,
        ]);

        $resp = curl_exec($c);
        curl_close($c);

        $response = json_decode($resp, true);
        $rows = $response['response']['docs'];
        $result = [];
        $result['total'] = $response['response']['numFound'];
        $result['data'] = [];
dd($rows);
        foreach($rows as $row) {
            $pdp_slug = explode(',', $row['pdp_slug_t']);
            $pdp_pics = explode(',', $row['pdp_picture_t']);
            $pdp_name = explode(',', $row['pdp_name_t']);
            $pdp = [];

            foreach($pdp_name as $i => $v) {
                $object = new \stdClass();
                $object->slug = isset($pdp_slug[$i]) ? $pdp_slug[$i] : '';
                $object->name = $v;
                $object->picture = isset($pdp_pics[$i]) ? $pdp_pics[$i] : '';

                $pdp[] = $object;
            }

            $result['data'][] = [
                'directory_id' => $row['id'],
                'directory_name' => $row['directory_title_t'],
                'directory_logo' => $row['directory_logo_t'],
                'directory_slug' => $row['directory_slug_t'],
                'directory_address' => $row['directory_address_t'],
                'city_name' => $row['city_name_t'],
                'city_slug' => $row['city_slug_t'],
                'province_name' => $row['prov_name_t'],
                'province_slug' => $row['prov_slug_t'],
                'product' => collect($pdp),
                'rating' => $row['directory_rating_tf'],
                'is_verified' => $row['directory_verify_i']
            ];
        }

        return $result;
    }
    
    public static function getDirectoryListing($offset = '',$limit = '', $category = '',$ord = 'rating' , $address = '', $search=[]) {
        $sql = DB::table('directory AS D')                
                ->leftJoin('city as C','C.id','=','D.city_id')
                ->leftJoin('province as P','P.id','=','C.province_id')
                ->leftJoin('directory_category as DC','DC.directory_id','=','D.id')
                ->leftJoin('category as CA','DC.category_id','=','CA.id')
                ->where('published','=','1');

        $search_mode = FALSE;
        if( array_key_exists('keyword', $search) && !empty($search['keyword']) ) {
            $search_mode = TRUE;
        }

        if($search_mode) {
            $kw = trim($search['keyword']);
            $kw = preg_replace('/\s+/', ' ',$kw);
            $kw = preg_replace('/[^a-z0-9\ ]/i', '', $kw);

            $kws = '+'.implode(' +', explode(' ', $kw));
            $sql->whereRaw('MATCH (D.name, D.description_lid, D.address_1) AGAINST (? IN BOOLEAN MODE)' , array($kws));
            $sql->groupBy('D.id');
        }
               
        if($category !== ''){
           $sql->where('CA.slug','=',$category);
        }
        
        if($address !== ''){
            $city_list = DB::table('city as c')
                ->whereRaw('MATCH (c.name) AGAINST (? IN BOOLEAN MODE)', array($address))
                ->get()->first();

            if(!$city_list) {
                $city_id = 99999;
            } else {
                $city_id = $city_list->id;
            }

            $sql->where('C.id', $city_id);
        }

        if($ord === 'rating'){
            $sql->orderby('D.rating', 'desc');
        }elseif($ord === 'id'){
            $sql->orderby('D.id', 'desc');
        }elseif($ord === 'name_desc'){
            $sql->orderby('D.name', 'desc');
        }elseif($ord === 'name_asc'){
            $sql->orderby('D.name', 'asc');
        }elseif($search_mode){
            // let use most relevance order
        }
        
        if($offset !== '' && $limit !== ''){
           $sql->offset($offset); 
           $sql->limit($limit);
        } 
                
        
        $sql->select('D.id AS directory_id', 'D.is_verified', 'D.slug AS directory_slug', 'D.name AS directory_name', 'D.name AS directory_name', 'D.logo as directory_logo',
                'D.address_1 as directory_address', 'C.name as city_name', 'C.slug as city_slug','P.name as province_name', 'P.slug as province_slug',
                'CA.slug as category_slug','CA.name_lid as category_name', 'D.rating as rating'
                ) ;
                
        return $sql->get();
    }
    
    public static function getDirectoryProduct($directory_id, $ord ='desc', $limit = '5') {
        $sql = DB::table('directory_product AS DP')                
                ->join('directory as D','DP.directory_id','=','D.id')
                ->where('DP.published','=','1')
                ->where('DP.directory_id','=',$directory_id)
                ->select('DP.name','DP.id','DP.brand','DP.slug','DP.picture');
        
        $sql->orderby('DP.id', $ord)
            ->limit($limit);

   
        $result = $sql->get();        
        
        return $result;
    }
    
    public function addingDirectoryCounter($segment, $directory_id) {
        $sql = DB::table('directory_counter AS DC')
                ->where('DC.directory_id','=',$directory_id)
                ->where('DC.segment', '=', $segment)
                ->whereRaw('DC.counter < DC.kuota')
                ->select('DC.directory_counter_id','DC.counter')
                ->orderby('DC.counter','asc');
                
        $result = $sql->first();
        
        $total = $result->counter + 1;

        DB::table('directory_counter')->where('directory_counter_id', '=', $result->directory_counter_id)->update([
            'counter' => $total
        ]); 
      
        return $result;
    }
    
    public function counter($action = '', $user_id = '', $directory_id){
        if(empty($user_id) || empty($action)) {
            return false;
        }
        
        $mongo = new \MongoDB\Client('mongodb://'.env('MONGO_HOST'));
        
        if($action === 'click'){
            $col = $mongo->selectDatabase("click_counter")->{$user_id};
        }elseif($action === 'view'){
            $col = $mongo->selectDatabase("view_counter")->{$user_id};
            
            // give counter ++
            $this->addingDirectoryCounter('feature', $directory_id);
        }
        
       
        $col->findOneAndUpdate(
                array('directory_id' => $directory_id, 'date' => date('Y-m-d')),
                array('$inc' => array('counter' => 1)),
                array('new' => true, 'upsert' => true)
        );
       
    }
    
    public static function updateDirectory($request, $id){
        try {
            list($latitude, $longitude) = explode(';', $request->get('latlng'));

            $updateValue = [
                'name' => $request->get('name'),
                
            ];
            
            $directory = Directory::where('id', $id)->firstOrFail();
            $directory->name = $request->get('name');
            $directory->description_lid = $request->get('description');
            $directory->address_1 = $request->get('address');
            $directory->phone_1 = $request->get('phone');
            $directory->city_id = $request->get('city_id');
            $directory->longitude = $longitude;
            $directory->latitude = $latitude;
            $stat = $directory->save(); 
            
            if($stat) {
                return true;
            }
            
            return false;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function getProducts($directory) {
        return \Cache::remember('dir:'.$directory->slug.':products', config('cache.expires.dir:products', config('cache.expires.general')), function() use($directory) {
            return $directory->products->where('published', 1);
        });
    }
    
    public static function getProductAndDirectoryByUserId($userId) {
        $sql_product = DB::table('directory_product AS DP')                
                ->join('directory as D','DP.directory_id','=','D.id')
                ->where('DP.published','=','1')
                ->where('D.user_id','=',$userId)
                ->selectRaw("DP.name as product_name, D.name as product_caption, DP.id, 'product' as type");
        
        $sql = DB::table('directory AS D') 
                ->where('D.user_id','=',$userId)
                ->selectRaw("D.name as product_name, D.address_1 as product_caption , D.id, 'directory' as type")
                ->union($sql_product)
                ->orderBy('product_name', 'asc');
        
        $result = $sql->get();        
       
        return $result;
    }
}
