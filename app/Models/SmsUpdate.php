<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsUpdate extends Model
{

    protected $fillable = [
        'msisdn', 'trx_id', 'directory_id', 'user_id', 'msg', 'status'
    ];
}
