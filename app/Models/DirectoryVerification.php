<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirectoryVerification extends Model
{
    protected $table = 'directory_verification';

    protected $fillable = [
        'verify_number', 'file_name_siup', 'file_name_ktp', 'user_id', 'directory_id', 'status', 'created_at', 'updated_at'
    ];

    public static function log($verify, $msg) {
        $mongo = new \MongoDB\Client('mongodb://'.env('MONGO_HOST'));

        $collection = $mongo->verification->logs;

        $collection->insertOne([
            'id' => $verify->id,
            'msg' => $msg,
            'time' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getLogs($id) {
        $mongo = new \MongoDB\Client('mongodb://'.env('MONGO_HOST'));

        $collection = $mongo->verification->logs;
        $cursor = $collection->find(['id' => $id], [
            'sort' => ['time' => -1]
        ]);

        return $cursor;
    }

}
