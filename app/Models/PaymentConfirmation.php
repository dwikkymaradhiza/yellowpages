<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PaymentConfirmation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bt_confirmation';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    
    public static function insert($array){
        DB::insert('insert into bt_confirmation ('
                . 'package_order_id, '
                . 'bank_sender, '
                . 'bank_owner_name,'
                . 'bank_transfer_amount,'
                . 'bank_recipient,'
                . 'receipt_photo,'
                . 'approved_status,'
                . 'approved_by)'
                . ' values (?, ?, ?, ?, ?, ?, ?, ?)', 
                    [
                        $array['nomor_order'],
                        $array['bank_sender'],
                        $array['name'],
                        $array['nominal'],
                        $array['bank_recipient'],
                        $array['file_name'],
                        0,
                        0,
                    ]);
    }
}
