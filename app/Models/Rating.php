<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Rating extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_rating';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Get the product that owns the rating.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
    
    /**
     * Get the user that owns the rating.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    
    /**
     * Get the rating date attribute.
     *
     * @return string
     */
    public function getRatingDateAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->attributes['create_time']))->diffForHumans();
    }

    public static function getProductRating($product) {
        return \Cache::remember('pdp:'.$product->slug.':ratings', config('cache.expires.pdp:ratings', config('cache.expires.general')), function() use($product) {
            return Rating::from('product_rating as a')
                ->leftJoin('users AS u','user_id','=','u.id')
                ->where('product_id', '=', $product->id)
                ->select('a.*', 'u.first_name as uf_name', 'u.last_name as ul_name')
                ->orderby('a.id','desc')
                ->get();
        });
    }
}
