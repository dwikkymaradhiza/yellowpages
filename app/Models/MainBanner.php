<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class MainBanner extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'main_banner';

    public static function getBanner() {
        $cache_key = 'home:banner';
        $data = Cache::remember($cache_key, config('cache.expires.'.$cache_key, config('cache.expires.general')), function() {
            return MainBanner::where('status','=','1')->first()->toArray();
        });

        return $data;
    }
    
}
