<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Affiliate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'affiliate_article';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public static function getLatestArticle($limit=8) {
        $redis_key = 'home:affiliate_article';
        $data = Cache::remember($redis_key, config('cache.expires.'.$redis_key, config('cache.expires.general')), function() use($limit) {
            $data = Affiliate::limit($limit)->get()->sortByDesc('id');
            return $data;
        });

        return $data;
    }

    public static function search($keywords='') {
        $articles = Affiliate::whereRaw('MATCH (tag, title, `desc`) AGAINST (? IN BOOLEAN MODE)' , array($keywords))
            ->limit(5)
            ->orderBy('id', 'desc')
            ->get();

        return $articles;
    }
}
