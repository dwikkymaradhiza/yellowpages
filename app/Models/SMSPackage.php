<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class SMSPackage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sms_packages';

    public static function getPackage()
    {
    	$user = Auth::user();

        $data = DB::select( DB::raw("SELECT * FROM `sms_packages` WHERE user_id = :user_id AND status = 'DRAFT'"),
                    array(
                        "user_id"    => $user->id
                    ));

        if(!empty($data))
            return $data[0];
        else
            return $data;
    }

}
