<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $table = 'article';
	public $timestamps = false;

	public static function getBySlug($slug) {
		$cache_key = 'static:'.$slug;
		return $data = \Cache::remember($cache_key, config('cache.expires.'.$cache_key, config('cache.expires.general')), function() use($slug) {
			return Article::where('slug', $slug)->first();
		});
	}
}
