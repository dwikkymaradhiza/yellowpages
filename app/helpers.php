<?php
/**
 * Created by PhpStorm.
 * User: fardhana
 * Date: 5/2/17
 * Time: 2:26 PM
 */

function amount_format($amount) {
    return "Rp " . number_format($amount, 0, ',', '.');
}