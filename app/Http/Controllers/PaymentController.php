<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Front\Controller;
use App\Models\Package_order;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use Auth;
use Mail;
use Redirect;

class PaymentController extends Controller
{
    const BANK_TRANSFER = 1;
    const CREDIT_CARD = 2;
    const INDOMART = 3;
    const TSEL_PULSA = 4;
    const KLIK_BCA = 5;
    
    const ERROR_MERCHANT_NOT_FOUND = "99 Merchant Not Found";
    const ERROR_SOURCE_INCOMPLETE = "99 Source Data Incomplete";
    const ERROR_INVALID_SIGNATURE = "01";
    const ERROR_BAD_GATEWAY = "500";
    const SUCCESS_RESPONSE = "00";
    const SUCCESS_PAYMENT = "00";
    const FAILED_PAYMENT = "05";
    const PAYMENT_CODE = 'Payment';
    const CANCEL_CODE = 'Cancel';
    
    private $signature;
    protected $parameters;

    private function set_signature($data) {
        $parameters = [
            'merchant_id' => env('FINPAY_USER'),
            'invoice' => $data['order_id'],
            'amount' => $data['order_balance'],
            'add_info1' => Auth::user()->first_name . " " . Auth::user()->last_name,
            'add_info2' => '',
            'add_info3' => '',
            'add_info4' => '',
            'add_info5' => '',
            'timeout' => 30,
            'return_url' => route('topup-payment-code-callback')
        ];
        
        $joinedString = '';
        foreach($parameters as $key => $val){
            if(!empty($val)){
                $joinedString .= $val.'%';
            }
	}
        
        $joinedString .= env('FINPAY_PASS');
        $joinedString = strtoupper($joinedString);
        $hashedString = strtoupper(hash('sha256', $joinedString));
        
        $signature = ['mer_signature' => $hashedString];
        $signature = array_merge($signature, $parameters);
        
        $this->parameters = $signature;
        $this->signature = $hashedString;
    }
    
    private function get_signature() {
        return $this->signature;
    }
     
    public function topup(Request $request) {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.topup');
        }
        
        return view('frontend.dashboard.topup');
    }
    
    public function payment_method(Request $request) {
        $validator = Validator::make($request->all(), [
            'order_balance' => 'required',
            'order_id' => 'required'
        ]);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.topup_payment_method', $request->all());
        }
        
        return view('frontend.dashboard.topup_payment_method', $request->all());
    }
    
    public function payment_method_view(Request $request) {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.topup_payment_method', $request->all());
        }
        
        return view('frontend.dashboard.topup_payment_method', $request->all());
    }
    
    public function payment_method_action(Request $request, $channel, $method) {
        $validator = Validator::make($request->all(), [
            'order_balance' => 'required',
            'order_id' => 'required'
        ]);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        if($request->attributes->get('mobileActive')) {
            switch($method) {
                default:
                    
                    return redirect()->back()->withInput()->withErrors(['Metode pembayaran salah']);
                case self::BANK_TRANSFER:

                    $this->set_signature($request->all());
                    $response = $this->finpay_request(env('FINPAY_REQUEST_PAYMENT_CODE_02111'), $this->parameters);

                    if($response !== self::ERROR_SOURCE_INCOMPLETE &&
                        $response !== self::ERROR_INVALID_SIGNATURE &&
                        $response !== self::ERROR_MERCHANT_NOT_FOUND &&
                        $response !== self::ERROR_BAD_GATEWAY) {

                        $response = parse_str($response, $respArr);

                        $packageOrder = Package_order::where('order_id', $request->get('order_id'))->first();
                        $packageOrder->payment_code = $respArr['payment_code'];
                        $packageOrder->topup_value = $request->get('order_balance');
                        $packageOrder->save();

                        Mail::send("emails.topup_request", ['invoice' => $packageOrder->order_id, 'payment_code' => $respArr['payment_code']], function ($m) use($packageOrder) {
                            $m->from(config('app.email_sender'), 'Yellow Pages');
                            $m->to(Auth::user()->email, Auth::user()->first_name)->subject("Pembelian Voucher #" . $packageOrder->order_id);
                        });
                    }
                    
                    return view('mobile.dashboard.payment_method.'.$channel, array_merge($request->all(), ['paycode' => $respArr['payment_code']]));
                case self::CREDIT_CARD:
                    if(empty(Auth::user()->handphone)) {
                        return Redirect::route('topup-payment-method-get',[
                                            'order_balance' => $request->get('order_balance'),
                                            'order_id' => $request->get('order_id')
                                        ])->withErrors([trans('validation.msisdn_required')]);
                    }
                    
                    $trxDate = Carbon::now()->format('YmdHis');
                    $parameters = [
                        'merchant_id' => env('FINPAY_CC_USER'),
                        'self_form' => 'N',
                        'trx_type' => 'Pay',
                        'trx_prod' => 'ecom',
                        'trx_amount' => $request->get('order_balance'),
                        'trx_invoice' => $request->get('order_id'),
                        'trx_date' => $trxDate,
                        'trx_returnurl' => route('index'),
                        'trx_successurl' => route('payment-callback-success'),
                        'trx_failedurl' => route('payment-callback-failed'),
                        'cust_id' => Auth::user()->id,
                        'cust_msisdn' => Auth::user()->handphone,
                        'cust_email' => Auth::user()->email,
                        'cust_name' => Auth::user()->first_name . " " . Auth::user()->last_name
                    ];                    
                    
                    $signatureNya = $this->mer_signature($parameters).env('FINPAY_CC_PASS');
                    $signature = hash('SHA256',$signatureNya);
                    $parameters['fin_securehash'] = $signature;
                    
                    $response = json_decode($this->finpay_request(env('FINPAY_CC_REQUEST_GETLINK'), $parameters));
                    if($response->fin_rc === self::SUCCESS_RESPONSE) {
                        $packageOrder = Package_order::where('order_id', $request->get('order_id'))->first();
                        $packageOrder->topup_value = $request->get('order_balance');
                        $packageOrder->save();
                        
                        return redirect($response->redirecturl);
                    } else {
                        return Redirect::route('topup-payment-method-get',[
                                    'order_balance' => $request->get('order_balance'),
                                    'order_id' => $request->get('order_id')
                                ])->withErrors([$response->fin_rc_desc]);
                    }
                    
                    break;
                case self::INDOMART:
                    return view('mobile.dashboard.payment_method.' . $channel);
                case self::TSEL_PULSA:
                    return view('mobile.dashboard.payment_method.' . $channel);
                case self::KLIK_BCA:
                    return view('mobile.dashboard.payment_method.' . $channel);
            }
        } else {
            switch($method) {
                default:
                    
                    return redirect()->back()->withInput()->withErrors(['Metode pembayaran salah']);
                case self::BANK_TRANSFER:

                    $this->set_signature($request->all());
                    $response = $this->finpay_request(env('FINPAY_REQUEST_PAYMENT_CODE_02111'), $this->parameters);

                    if($response !== self::ERROR_SOURCE_INCOMPLETE &&
                        $response !== self::ERROR_INVALID_SIGNATURE &&
                        $response !== self::ERROR_MERCHANT_NOT_FOUND &&
                        $response !== self::ERROR_BAD_GATEWAY) {

                        $response = parse_str($response, $respArr);

                        $packageOrder = Package_order::where('order_id', $request->get('order_id'))->first();
                        $packageOrder->payment_code = $respArr['payment_code'];
                        $packageOrder->topup_value = $request->get('order_balance');
                        $packageOrder->save();

                        Mail::send("emails.topup_request", ['payment_code' => $respArr['payment_code']], function ($m) {
                            $m->from(config('app.email_sender'), 'Yellow Pages');
                            $m->to(Auth::user()->email, Auth::user()->first_name)->subject("Topup Request");
                        });
                    }
                    
                    return view('frontend.dashboard.payment_method.'.$channel, array_merge($request->all(), ['paycode' => $respArr['payment_code']]));
                case self::CREDIT_CARD:
                    if(empty(Auth::user()->handphone)) {
                        return Redirect::route('topup-payment-method-get',[
                                            'order_balance' => $request->get('order_balance'),
                                            'order_id' => $request->get('order_id')
                                        ])->withErrors([trans('validation.msisdn_required')]);
                    }
                    
                    $trxDate = Carbon::now()->format('YmdHis');
                    $parameters = [
                        'merchant_id' => env('FINPAY_CC_USER'),
                        'self_form' => 'N',
                        'trx_type' => 'Pay',
                        'trx_prod' => 'ecom',
                        'trx_amount' => $request->get('order_balance'),
                        'trx_invoice' => $request->get('order_id'),
                        'trx_date' => $trxDate,
                        'trx_returnurl' => route('index'),
                        'trx_successurl' => route('payment-callback-success'),
                        'trx_failedurl' => route('payment-callback-failed'),
                        'cust_id' => Auth::user()->id,
                        'cust_msisdn' => Auth::user()->handphone,
                        'cust_email' => Auth::user()->email,
                        'cust_name' => Auth::user()->first_name . " " . Auth::user()->last_name
                    ];                    
                    
                    $signatureNya = $this->mer_signature($parameters).env('FINPAY_CC_PASS');
                    $signature = hash('SHA256',$signatureNya);
                    $parameters['fin_securehash'] = $signature;
                    
                    $response = json_decode($this->finpay_request(env('FINPAY_CC_REQUEST_GETLINK'), $parameters));
                    if($response->fin_rc === self::SUCCESS_RESPONSE) {
                        $packageOrder = Package_order::where('order_id', $request->get('order_id'))->first();
                        $packageOrder->topup_value = $request->get('order_balance');
                        $packageOrder->save();
                        
                        return redirect($response->redirecturl);
                    } else {
                        return Redirect::route('topup-payment-method-get',[
                                    'order_balance' => $request->get('order_balance'),
                                    'order_id' => $request->get('order_id')
                                ])->withErrors([$response->fin_rc_desc]);
                    }
                    
                    break;
                case self::INDOMART:
                    return view('frontend.dashboard.payment_method.' . $channel);
                case self::TSEL_PULSA:
                    return view('frontend.dashboard.payment_method.' . $channel);
                case self::KLIK_BCA:
                    return view('frontend.dashboard.payment_method.' . $channel);
            }
        }
    }
    
    public function callback_payment(Request $request) {
        $post = $_POST;
        Log::info('[FINNET_UPDATE] Data received : '.json_encode($post));
        //if transaction success
        if (
            isset($post['trx_type']) && $post['trx_type'] === self::PAYMENT_CODE &&
            isset($post['result_code']) && $post['result_code'] === self::SUCCESS_PAYMENT
        ) {
            $packageOrder = Package_order::where('payment_code', $post['payment_code'])->first();
            $user = User::find($packageOrder->user_id);
            $user->user_balance = (int) $user->user_balance + (int) $packageOrder->topup_value;
            $user->save();
            
            //if user balance is sufficient to pay package order price
            if( ((int) $user->user_balance - (int) $packageOrder->price) >= 0) {
                $user->user_balance = (int) $user->user_balance - (int) $packageOrder->price;
                $user->save();
                
                $packageOrder->status = 1;
                $packageOrder->save();
            }

            Mail::send("emails.payment_success", ['invoice' => $packageOrder->order_id, 'amount' => $packageOrder->topup_value], function ($m) use($user, $packageOrder) {
                $m->from(config('app.email_sender'), 'Yellow Pages');
                $m->to($user->email, $user->first_name)->subject("Pembayaran Diterima Untuk Order #" . $packageOrder->order_id);
            });
        }

        if (
            isset($post['trx_type']) && $post['trx_type'] === self::PAYMENT_CODE &&
            isset($post['result_code']) && $post['result_code'] === self::FAILED_PAYMENT
        ) {
            $packageOrder = Package_order::where('payment_code', $post['payment_code'])->first();
            $packageOrder->status = 5;
            $packageOrder->save();

            $user = User::find($packageOrder->user_id);

            Mail::send("emails.payment_expired", ['invoice' => $packageOrder->order_id], function ($m) use($user, $packageOrder) {
                $m->from(config('app.email_sender'), 'Yellow Pages');
                $m->to($user->email, $user->first_name)->subject("Pembayaran Gagal Untuk Order #" . $packageOrder->order_id);
            });
        }

        if (
            isset($post['trx_type']) && $post['trx_type'] === self::CANCEL_CODE
        ) {
            $packageOrder = Package_order::where('payment_code', $post['payment_code'])->first();
            $packageOrder->status = 5;
            $packageOrder->save();

            $user = User::find($packageOrder->user_id);

            Mail::send("emails.payment_cancel", ['invoice' => $packageOrder->order_id], function ($m) use($user, $packageOrder) {
                $m->from(config('app.email_sender'), 'Yellow Pages');
                $m->to($user->email, $user->first_name)->subject("Pembatalan Order #" . $packageOrder->order_id);
            });

        }

        echo '00';
    }
    
    public function callback_payment_success(Request $request) {
        //if transaction success
        if ($request->has('trx_invoice') && $request->has('amount')) {
            $packageOrder = Package_order::where('order_id', $request->input('trx_invoice'))->first();
            if((int) $packageOrder->topup_value !== (int) $request->get('amount')) {
                return redirect()->route('topup-error');
            }
            
            $user = User::find($packageOrder->user_id);
            $user->user_balance = (int) $user->user_balance + (int) $packageOrder->topup_value;
            $user->save();
            
            //if user balance is sufficient to pay package order price
            if( ((int) $user->user_balance - (int) $packageOrder->price) >= 0) {
                $user->user_balance = (int) $user->user_balance - (int) $packageOrder->price;
                $user->save();
                
                $packageOrder->status = 1;
                $packageOrder->save();
            }
            
            return redirect()->route('topup-success');
        }
    }
    
    public function callback_payment_failed(Request $request) {
        //if transaction success
        return redirect()->route('topup-error');
    }
    
    public function payment_process(Request $request, $method) {
        $validator = Validator::make($request->all(), [
            'order_balance' => 'required',
            'order_id' => 'required'
        ]);

        $packageOrder = Package_order::where('order_id', $request->get('order_id'))->first();

        return redirect()->route('topup-thank-you')->with('payment_code', $packageOrder->payment_code);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        $this->set_signature($request->all());
        
        if($request->attributes->get('mobileActive')) {
            switch($method) {
                default:
                    
                    return redirect()->back()->withInput()->withErrors(['Metode pembayaran salah']);
                case self::BANK_TRANSFER:
                    $response = $this->finpay_request(env('FINPAY_REQUEST_PAYMENT_CODE_02111'), $this->parameters);

                    if($response !== self::ERROR_SOURCE_INCOMPLETE && 
                        $response !== self::ERROR_INVALID_SIGNATURE &&
                        $response !== self::ERROR_MERCHANT_NOT_FOUND &&
                        $response !== self::ERROR_BAD_GATEWAY) {
                        
                        $response = parse_str($response, $respArr);
                        
                        $packageOrder = Package_order::where('order_id', $request->get('order_id'))->first();
                        $packageOrder->payment_code = $respArr['payment_code'];
                        $packageOrder->topup_value = $request->get('order_balance');
                        $packageOrder->save();
                        
                        Mail::send("emails.topup_request", ['payment_code' => $respArr['payment_code']], function ($m) {
                            $m->from(config('app.email_sender'), 'Yellow Pages');
                            $m->to(Auth::user()->email, Auth::user()->first_name)->subject("Topup Request");
                        });
                    }
                    
                    return redirect()->route('topup-thank-you')->with('payment_code', $respArr['payment_code']);
                case self::CREDIT_CARD:
                    
                    break;
                case self::INDOMART:
                    if($request->attributes->get('mobileActive')) {
                        return view('mobile.dashboard.payment_method.' . $channel);
                    }
                    
                    return view('frontend.dashboard.payment_method.' . $channel);
                case self::TSEL_PULSA:
                    if($request->attributes->get('mobileActive')) {
                        return view('mobile.dashboard.payment_method.' . $channel);
                    }
                    
                    return view('frontend.dashboard.payment_method.' . $channel);
                case self::KLIK_BCA:
                    if($request->attributes->get('mobileActive')) {
                        return view('mobile.dashboard.payment_method.' . $channel);
                    }
                    
                    return view('frontend.dashboard.payment_method.' . $channel);
            }
        }
    }
    
    public function payment_finish(Request $request) {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.topup_finish');
        }
        
        return view('frontend.dashboard.topup_finish');
    }
    
    public function payment_success(Request $request) {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.topup_success');
        }
        
        return view('frontend.dashboard.topup_success');
    }
    
    public function payment_error(Request $request) {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.topup_error');
        }
                    
        return view('frontend.dashboard.topup_error');
    }
    
    protected function finpay_request($url, $parameters) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);
        curl_close ($ch);
        
        return $server_output;
    }
    
    public function mer_signature($array){
        ksort ($array);
        $output = '';
        foreach($array as $key=>$val){
                if(!empty($val) && $key!='fin_securehash'){
                        $output .= $val.'%';
                }
        }

        return strtoupper($output);
    }
}
