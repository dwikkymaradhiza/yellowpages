<?php

namespace App\Http\Controllers;

use App\Models\Directory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Front\Controller;
use Socialite;
use App\Models\SocialAccount;

class SocialAuthController extends Controller
{
    
    public function redirect($social) {
    	switch($social) {
    		case "facebook":
    			return Socialite::driver('facebook')->redirect();
    		case "google":
    			return Socialite::driver('google')->redirect();
    	}
    }

    public function callback($social) {
        $user = SocialAccount::createOrGetUser(Socialite::driver($social)->user(), $social);
        auth()->login($user['data']);
        session(['social_user' => $user['data']['id']]);

        if ($user['is_new']) {
            return redirect()->route('signup_profile');
        } else {
            if(!$user['profile_submit']) {
                return redirect()->route('signup_profile');
            } elseif(!$user['directory_submit']) {
                if(session('claim_id', 0)) {
                    $directory = Directory::find(session('claim_id'));
                    return redirect()->route('verify-form', $directory->slug);
                } else {
                    return redirect()->route('signup_directory');
                }
            }

            if(session('claim_id', 0)) {
                $directory = Directory::find(session('claim_id'));
                return redirect()->route('verify-form', $directory->slug);
            } else {
                return redirect('/dashboard');
            }
        }
    }

}

?>
