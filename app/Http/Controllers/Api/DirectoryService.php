<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Directory;

class DirectoryService
{
    //
    public function getData(Request $request) {
        $directory = new Directory;
        
        $tags = urldecode($request->get('tags'));
        $tags = explode(",", $tags);
        
        $directories = $directory::where('published', 1)->where(function($query) use($tags) {
            foreach($tags as $k => $tag) {
                $query->orWhere('name','like',"%{$tag}%")
                        ->orWhere('popular_name','like',"%{$tag}%")
                        ->orWhere('keywords','like',"%{$tag}%")
                        ->orWhere('slug','like',"%{$tag}%")
                        ->orWhere('description_len','like',"%{$tag}%")
                        ->orWhere('description_lid','like',"%{$tag}%");
            }
        });
        
        $directories = $directories->limit(2)->get()->toArray();
        $data = [];
        
        foreach ($directories as $idx => $dir) {
            foreach ($dir as $k => $v) {
                if($k === "logo") {
                    $data[$idx][$k] = asset('uploads/directories/' . $v);
                } else {
                    $data[$idx][$k] = $v;
                }
            }
            
            $data[$idx]['detail_uri'] = route('directory', ['slug' => $dir['slug']]);
        }
        
        return response()->json($data);
    }
}
