<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\DirectoryInquiry;
use App\Models\Directory;
use App\Models\Product;
use App\Models\Counter;
use App\Models\Package_order;
use App\Models\User;
use Carbon\Carbon;
use Auth;
use Mail;

class InquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // define pagesize
        $queryString = $request->query();
        $pageSize = (!empty($queryString['size'])) ? (int) $queryString['size'] : 10;
        $size = ($pageSize > 25) ? 25 : $pageSize;
        
        // get data inquiries
        $directories = Directory::where('user_id', Auth::user()->id)->pluck('id')->toArray();
        $inquiries = DirectoryInquiry::whereIn('directory_id', $directories)->orderBy('id', 'desc')->paginate($size);
        
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.inquiry', compact('inquiries', 'size'));
        }
        
        return view('frontend.dashboard.inquiry', compact('inquiries', 'size'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'message' => 'required|min:20',
            'email' => 'required|email',
            'phone' => 'required',
            'directory_id' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        try {
            if($request->get('for') == 'inq') {
                $success_message = 'messages.success.insert_inquiry';
                $mail_subject = "Inquiry Baru";
                $mail_template = "emails.new_inquiry";
                
                $inquiry = new DirectoryInquiry;
                $inquiry->product_id = empty($request->get('product_id')) ? null : $request->get('product_id');
                $inquiry->directory_id = $request->get('directory_id');
                $inquiry->name = $request->get('name');
                $inquiry->phone = $request->get('phone');
                $inquiry->inquiry = $request->get('message');
                $inquiry->email = $request->get('email');
                $inquiry->saveOrFail();
                
                $dir = Directory::where('id', $inquiry->directory_id)->get()->first();
                $logContent = "bisnis {$dir->name}";
                if(!empty($request->get('product_id'))) {
                    $logContent = "produk {$inquiry->product->name}";
                }
                
                if(!is_null($dir->user)) {
                    $counter = Counter::where('user_id', $dir->user->id)->where('created_at', Carbon::now()->format('Y-m-d'))->get()->first();
                    if(empty($counter)) {
                        $counter = new Counter;
                        $counter->user_id = $dir->user->id;
                        $counter->created_at = Carbon::now()->format('Y-m-d');
                        $counter->saveOrFail();
                    }

                    Counter::where('user_id', $dir->user->id)->increment('inquiry_count');
                }
                
                $pdp = Product::where('id', $inquiry->product_id)->get()->first();
                $text = 'inquiri baru :' . $inquiry->name . ', ' . $inquiry->phone . ', silahkan cek dashboard anda';
                
                // $package_order = Package_order::where('user_id',$dir->user_id)
                //                 ->join('packages as P','P.id','package_order.package_id')
                //                 ->where('package_order.status','1')
                //                 ->orderby('package_order.id','desc')->first();
                
                //if(!empty($package_order)) {
                    $api_url = 'https://smsblast.id/api/sendsingle?user=ypmdm&password=ypmdm&senderid=YELLOWPAGES&message='.rawurlencode($text).'&msisdn='.$dir->phone_1;
                    
                    file_get_contents($api_url);
                //}
                
                User::log("Inquiry {$logContent} oleh {$request->get('name')} ({$request->get('phone')})", $dir->user_id);
            } else {
                $success_message = 'messages.success.insert_sms';
                $mail_subject = "SMS Baru";
                $mail_template = "emails.new_sms";
                
                $inquiry = new DirectoryInquiry;
                $inquiry->product_id = empty($request->get('product_id')) ? null : $request->get('product_id');
                $inquiry->directory_id = $request->get('directory_id');
                $inquiry->name = $request->get('name');
                $inquiry->phone = $request->get('phone');
                $inquiry->inquiry = $request->get('message');
                $inquiry->email = $request->get('email');
                $inquiry->saveOrFail();

                $dir = Directory::where('id', $inquiry->directory_id)->get()->first();
                
                if(!is_null($dir->user)) {
                    $counter = Counter::where('user_id', $dir->user->id)->where('created_at', Carbon::now()->format('Y-m-d'))->get()->first();
                    if(empty($counter)) {
                        $counter = new Counter;
                        $counter->user_id = $dir->user->id;
                        $counter->created_at = Carbon::now()->format('Y-m-d');
                        $counter->saveOrFail();
                    }
                    
                    Counter::where('user_id', $dir->user->id)->increment('sms_count');
                }
                
                $pdp = Product::where('id', $inquiry->product_id)->get()->first();
                $text = 'pesan baru ' . $inquiry->phone . ': ' . $inquiry->inquiry;
                
                // $package_order = Package_order::where('user_id',$dir->user_id)
                //                 ->join('packages as P','P.id','package_order.package_id')
                //                 ->where('package_order.status','1')
                //                 ->orderby('package_order.id','desc')->first();
                
                // if(!empty($package_order)) {
                    $api_url = 'https://smsblast.id/api/sendsingle?user=ypmdm&password=ypmdm&senderid=YELLOWPAGES&message='.rawurlencode($text).'&msisdn='.$dir->phone_1;

                    file_get_contents($api_url);
                // }
                
                User::log("SMS baru dari {$request->get('name')} ({$request->get('phone')})", $dir->user_id);
            }
            
            if(!is_null($dir->user_id)) {
                Mail::send($mail_template, ['name' => $inquiry->name, "phone" => $inquiry->phone, "email" => $inquiry->email, "msg" => $inquiry->inquiry], function ($m) use($dir, $mail_subject) {
                    $m->from(config('app.email_sender'), 'Yellow Pages');

                    $m->to($dir->user->email, $dir->user->first_name)->subject($mail_subject);
                });
            }
            
            return redirect()->back()->with('alert-success', trans($success_message));
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->withInput()->with('alert-danger', trans('messages.errors.insert_fail'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inquiry = DirectoryInquiry::where('id', $id)->firstOrFail();
        $form_title = "INQUIRY DETAIL";
        
        return view('mobile.dashboard.inquiry_detail', compact('inquiry', 'form_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = DirectoryInquiry::where('id', $id)->firstOrFail();
        if($item->directory->user_id !== Auth::user()->id) {
            return redirect()->back()->with('alert-danger', trans('messages.errors.delete_fail'));
        }
        
        $item->delete();
        
        return redirect()->back()->with('alert-success', trans('messages.success.delete_inquiry'));
    }
    
    public function destroyBulk(Request $request)
    {
        $id_array = explode(',',$request->id);
        DirectoryInquiry::destroy($id_array);
        return 'success';
    }
}
