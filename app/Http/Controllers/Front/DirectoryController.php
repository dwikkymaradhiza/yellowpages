<?php

namespace App\Http\Controllers\Front;

use App\Models\DirectoryRating;
use App\Models\DirectoryVerification;
use App\Models\Product;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Directory;
use App\Models\Affiliate;
use App\Models\DirectoryInfo;
use App\Models\Category;
use App\Models\Province;
use App\Models\DirectoryCategory;
use App\Models\DirectoryPicture;
use App\Models\DirectoryFavorite;
use Carbon\Carbon;
use Auth;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Cache;
use App\Models\SeoText;

class DirectoryController extends Controller
{
    const THUMB_WIDTH = 260;
    const THUMB_HEIGHT = 200;
    private $seo_content;
    
    public function __construct() {
        $slug = collect(request()->segments())->last();
        if($slug === null){
            $slug = '';
        }
        
        $seo_text = new SeoText;
        
        $seo_content = $seo_text::where('slug','=',$slug)->first();
        
        if(count($seo_content)>0){
            $this->seo_content = $seo_content->seo_text;
        }else{
            $this->seo_content = '';
        }
    }
    
    public function index(Request $request)
    {
        // define pagesize
        $queryString = $request->query();
        $pageSize = (!empty($queryString['size'])) ? (int) $queryString['size'] : 10;
        $size = ($pageSize > 25) ? 25 : $pageSize;
        
        // get data directories
        $directories = Directory::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate($size);
        $total_dir = Directory::where('user_id', Auth::user()->id)->count();
        $categories = Category::all(['name_lid', 'id']);
        $provinces = Province::all(['name', 'id'])->sortBy('name');

        if($request->attributes->get('mobileActive')) {
            return view('mobile.directory.index', compact('directories','last_activity', 'logs','date_array','total_click','total_view'));
        }

        return view('frontend.dashboard.directory', compact('directories', 'size', 'categories', 'provinces', 'total_dir'));
    }

    public function mobileCreate()
    {
        $provinces = Province::all(['name', 'id'])->sortBy('name');

        return view('mobile.directory.insert', compact('provinces'));
    }

    public function create()
    {
        return view('home');
    }
    
    public function delete()
    {
        return view('home');
    }
    
    public function listing(Request $request)
    {
        return view('home');
    }
    
    public function newCompanyList(Request $request)
    {
        return view('home');
    }
    
    public function getFeaturedDirectory()
    {
        #$redis_key = 'home:feat_directory';
        #$data = Cache::remember($redis_key, config('cache.expires.'.$redis_key, config('cache.expires.general')), function() {
            $todayDate = date('Ym');
            return Directory::getFeaturedDirectory('feature', 8);
        #});

        #return $data;
    }
    
    public function getNewestDirectory()
    {
        $result = Directory::where('published','=','1')
                ->orderby('id','desc')
                ->limit('5')
                ->get();
        
        return $result;
    }
    
    public function showDirectoryListingWithOrd(Request $request){
        $urutkan = $request->urutkan;
        $category = $request->category;
        $address = $request->address;

        if($urutkan !=='' && $address !== ''){
            return \Redirect::to(route('directory-category-with-location-ord',['category'=>$category,'location'=>$address,'ord'=>$urutkan]));
        }elseif($urutkan !==''){
            return \Redirect::to(route('directory-category-with-ord',['category'=>$category,'ord'=>$urutkan]));
        }elseif($address !==''){
            return \Redirect::to(route('directory-category-with-location',['category'=>$category,'location'=>$address]));
        }else{
            return \Redirect::to(route('directory-category',$category));
        }
        
    }
    
    public function showDirectoryListingWithLocation(Request $request){
        $address = $request->address;
        $category = $request->category;
        $urutkan = $request->urutkan;

        if($urutkan !=='' && $address !== ''){
            return \Redirect::to(route('directory-category-with-location-ord',['category'=>$category,'location'=>$address,'ord'=>$urutkan]));
        }elseif($urutkan !==''){
            return \Redirect::to(route('directory-category-with-ord',['category'=>$category,'ord'=>$urutkan]));
        }elseif($address !==''){
            return \Redirect::to(route('directory-category-with-location',['category'=>$category,'location'=>$address]));
        }else{
            return \Redirect::to(route('directory-category',$category));
        }
    }
    
    public function showDirectoryListingLocation($category,$location,$page=''){
        return $this->showDirectoryListing($category, $page, '',$location);
    }
    
    public function showDirectoryListingOrd($category,$ord,$page=''){
        return $this->showDirectoryListing($category, $page, $ord);
    }
    
    public function showDirectoryLocationOrd($category,$location ,$ord,$page=''){
        return $this->showDirectoryListing($category,$page, $ord, $location);
    }

    public function showDirectorySearch(Request $request, $search_by='', $keyword='', $location='') {
        $search_by = strtolower($search_by);

        if($search_by != 'bisnis' and $search_by != 'produk') {
            abort(404);
        }

        $sort = $request->input('sort', '');
        $page = $request->input('page', 1);

        return $this->showDirectoryListing('',$page, $sort, str_replace('-', ' ', $location), ['by' => $search_by, 'keyword' => str_replace('-', ' ', trim($keyword))]);
    }
    
    public function getDirectoryListing(Request $request){
        $data = [];
        $product = [];
        $products = [];
        $product_array = [];
        $category_name = '';
        $directory_listing = [];
        $total_category_listing = 0;
        $result = [];
        
        $category = isset($request->category)?$request->category:'';
        $urutkan = isset($request->urutkan)?$request->urutkan:'';
        $address = isset($request->address)?$request->address:'';        
        $page = isset($request->page)?$request->page:'';
        $search_by = isset($request->search_by)?$request->search_by:'';
        $keyword = isset($request->keyword)?$request->keyword:'';
        
        if($search_by !== '' && $keyword !== ''){
            $search = ['by'=>$search_by,'keyword'=>$keyword];
        }else{
            $search = ['by'=>'','keyword'=>''];
        }
        
        $limit = config('view.pagesize');
        $offset = ($page-1)*$limit;

        if(!empty($search['keyword'])) {
            $result = Directory::solr_search($offset,$limit, $urutkan, $address, $search);
            $data = $result['data'];
            $total_category_listing = $result['total'];
        } else {
            $directory_listing = Directory::getDirectoryListing($offset,$limit,$category, $urutkan, $address, $search);

            if(count($directory_listing)>0){
                foreach($directory_listing as $directory_listing_detail){
                    $product = Directory::getDirectoryProduct($directory_listing_detail->directory_id);

                    $directory = [
                        'directory_id' => $directory_listing_detail->directory_id,
                        'directory_name' => $directory_listing_detail->directory_name,
                        'directory_logo' => $directory_listing_detail->directory_logo,
                        'directory_slug' => $directory_listing_detail->directory_slug,
                        'directory_address' => $directory_listing_detail->directory_address,
                        'city_name' => $directory_listing_detail->city_name,
                        'city_slug' => $directory_listing_detail->city_slug,
                        'province_name' => $directory_listing_detail->province_name,
                        'province_slug' => $directory_listing_detail->province_slug,
                        'product' => $product,
                        'rating' => floor($directory_listing_detail->rating),
                        'is_verified' => $directory_listing_detail->is_verified
                    ];

                    $category_name = $directory_listing_detail->category_name;
                    array_push($data,$directory);
                }
            }
            $total_category_listing = count(Directory::getDirectoryListing('','', $category, $urutkan, $address, $search));
        }

        $result = [
            'data' => $data,
            'total' => $total_category_listing
        ];
        
        return json_encode($result);
    }
    
    public function showDirectoryListing($category='',$page='', $urutkan ='', $address ='', $search=[])
    {
        $seo_content = $this->seo_content;$seo_content = $this->seo_content;
        
        $data = [];
        $product = [];
        $products = [];
        $product_array = [];
        $category_name = '';
        $directory_listing = [];
        $total_category_listing = 0;
        
        $limit = config('view.pagesize');

        if($page === ''){
            $page = 1;
        }

        $agent = new Agent();
        if(!$agent->isPhone()){
            $offset = ($page-1)*$limit;

            if(!empty($search['keyword'])) {
                $result = Directory::solr_search($offset,$limit, $urutkan, $address, $search);
                $data = $result['data'];
                $total_category_listing = $result['total'];
                $category_name = $search['keyword'];

            } else {
                $directory_listing = Directory::getDirectoryListing($offset, $limit, $category, $urutkan, $address, $search);

                foreach ($directory_listing as $directory_listing_detail) {
                    $product = Directory::getDirectoryProduct($directory_listing_detail->directory_id);

                    $directory = [
                        'directory_id' => $directory_listing_detail->directory_id,
                        'directory_name' => $directory_listing_detail->directory_name,
                        'directory_logo' => $directory_listing_detail->directory_logo,
                        'directory_slug' => $directory_listing_detail->directory_slug,
                        'directory_address' => $directory_listing_detail->directory_address,
                        'city_name' => $directory_listing_detail->city_name,
                        'city_slug' => $directory_listing_detail->city_slug,
                        'province_name' => $directory_listing_detail->province_name,
                        'province_slug' => $directory_listing_detail->province_slug,
                        'product' => $product,
                        'rating' => $directory_listing_detail->rating,
                        'is_verified' => $directory_listing_detail->is_verified
                    ];

                    $category_name = $directory_listing_detail->category_name;
                    array_push($data, $directory);
                }

                $total_category_listing = count(Directory::getDirectoryListing('', '', $category, $urutkan, $address, $search));
            }
        } else {
            if($category) {
                $category_detail = Category::where('slug', $category)->first();
                $category_name = $category_detail->name_lid;
            }
        }
        $search_page = FALSE;
        $keyword = '';
        $search_by = '';
        if( array_key_exists('keyword', $search) && !empty($search['keyword']) ) {
            $search_page = TRUE;
            $keyword = $search['keyword'];
            $search_by = $search['by'];
        }

        $productDirectory = '';
        $todayDate = date('Ym');
        $directory_sponsor_listing = Directory::getSponsorDirectory('sponsor', $todayDate, '2', $category);
        $affiliate_article = Affiliate::where('tag','like','%'.$category_name.'%')->orderby('id','desc')->get();

        if(!count($affiliate_article)) {
            $affiliate_article = Affiliate::limit(8)->orderby('id','desc')->get();
        }

        $newest_bis = Directory::where('published','=','1')->orderby('id','desc')->limit('5')->get();

        if($agent->isPhone()) {
            return view('mobile.directory_listing', compact('directory_listing','data','total_category_listing','page','category','category_name','urutkan','address','affiliate_article', 'search_page', 'keyword', 'search_by', 'limit','seo_content'));
        } else {
            return view('frontend.directory_listing', compact('newest_bis', 'directory_listing','data','total_category_listing','page','category','category_name','urutkan','address','affiliate_article', 'search_page', 'keyword', 'search_by', 'limit', 'seo_content'));
        }
        
    }
    
    public function getDirectoryCategories($param){
        $category = Category::where('name_lid', 'LIKE' ,'%'. $param. '%')->get();
        $category_array = [];
        $result = [];
        
        foreach($category as $category_detail){
            $category_array[] = [
                'id' => $category_detail->id,
                'name' => $category_detail->name_lid,
            ];
        }
        
        $result = [
            'items' => $category_array,
            'total' => count($category_array),
        ];
        
        return $result;
    }
    public function click($slug)
    {
        $directory_array = [];
        
        $directory_array = Directory::where('slug',$slug)->first();
        $user_id = $directory_array->user_id;
        $directory_id = $directory_array->id;
        
        $directory = new Directory;
        $directory->counter('click',$user_id,$directory_id);

        return \Redirect::to(route('directory',$slug));
    }
    
    public function impress($slug)
    {
        $directory_array = [];
        
        $directory_array = Directory::where('slug',$slug)->firstOrFail();
        $user_id = $directory_array->user_id;
        $directory_id = $directory_array->id;
        
        $directoryObj = new Directory;
        $directoryObj->counter('view',$user_id,$directory_id);
    }
    
    public function show(Request $request, $slug){
        $seo_content = $this->seo_content;
        
        $directory = Directory::findBySlug($slug);
        if(!$directory) {
            abort(404);
        }

        $products = Directory::getProducts($directory);

        $ratings = DirectoryRating::getDirectoryRating($directory);

        $info = \Cache::remember('dir:'.$slug.':info', config('cache.expires.dir:info', config('cache.expires.general')), function() use($directory) {
            return DirectoryInfo::where('directory_id', $directory->id)->orderBy('created_at', 'desc')->limit(5)->get();
        });
       
        // get similar directories
        $similars = Directory::findSimilarByCategories($directory->categories)
                    ->where('directory.id', '!=', $directory->id)
                    ->limit(5)
                    ->inRandomOrder()
                    ->get();
        $use_newest_dir = false;        
        if($similars->count() === 0) {
            $use_newest_dir = true;
            $similars = Directory::getLatest(5);
        }
        
        // get average rating
        $sumRating = 0;
        foreach($ratings as $k => $v) {
            $sumRating += $v->rate;
        }
        
        $average_rate = ($sumRating === 0) ? 0 : (int) ceil($sumRating / count($ratings));
        
        // get affiliate article
        $keywords = [];
        foreach($directory->categories as $kc => $vc) {
            $keywords[] = $vc->name_lid;
        }
        $keywords[] = $directory->name;
        $articles = Affiliate::search(implode(' ', $keywords));

        $pictures = \Cache::remember('dir:'.$slug.':pics', config('cache.expires.dir:picts', config('cache.expires.general')), function() use($directory) {
            return DirectoryPicture::where('directory_id', $directory->id)->get();
        });
        
        // get favorite
        $isFavorited = 0;
        if(Auth::check()) {
            $favorite = DirectoryFavorite::where('directory_id', $directory->id)->where('user_id' , Auth::id())->first();
            if(!empty($favorite)) {
                $isFavorited = 1;
            }
        }

        if($request->attributes->get('mobileActive')) {
            return view('mobile.directory_detail', compact('directory', 'products', 'ratings', 'average_rate', 'similars', 'info', 'articles', 'use_newest_dir', 'pictures', 'isFavorited'));
        }
        
        return view('frontend.directory_detail', compact('directory', 'products', 'ratings', 'average_rate', 'similars', 'info', 'articles', 'use_newest_dir', 'pictures', 'isFavorited', 'seo_content'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Directory::where('id', $id)->where('user_id', Auth::user()->id)->firstOrFail();
        //unlink logo
        File::delete(public_path() . "/uploads/directories/{$item->logo}");
        File::delete(public_path() . "/uploads/directories/thumb_{$item->logo}");
        
        $item->delete();
        
        //unlink directory pictures
        $pictures = DirectoryPicture::where('directory_id', $id)->get();
        foreach($pictures as $k => $v) {
            File::delete(public_path() . "/uploads/directories/{$v->picture_file}");
            File::delete(public_path() . "/uploads/directories/thumb_{$v->picture_file}");
        }
        
        DirectoryCategory::where('directory_id', $id)->delete();
        DirectoryPicture::where('directory_id', $id)->delete();
        
        return redirect()->back()->with('alert-success', trans('messages.success.delete_directory'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPicture($id)
    {
        //unlink directory picture
        $pictures = DirectoryPicture::where('id', $id)->firstOrFail();
        File::delete(public_path() . "/uploads/directories/{$pictures->picture_file}");
        File::delete(public_path() . "/uploads/directories/thumb_{$pictures->picture_file}");
        
        DirectoryPicture::where('id', $id)->delete();
        
        return redirect()->back()->with('alert-success', trans('messages.success.delete_directory_picture'));
    }
    
    public function destroyBulk(Request $request)
    {
        $id_array = explode(',',$request->id);
        Directory::destroy($id_array);
        return 'success';
    }
    
    public function edit(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/\s+[^.!?]*/',
        ], ['name.regex' => 'Format nama bisnis tidak valid.']);

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        // Check if the updated directory is user's directory
        $directory = Directory::where('id', $id)->where('user_id', Auth::id())->firstOrFail();
        
        if(Directory::updateDirectory($request, $id) ) {
            // Set Categories
            $directoryCategory = new DirectoryCategory;
            $directoryCategory::where('directory_id',$id)->delete();

            $categories = explode(',', $request->get('categories'));
            foreach ($categories as $val) {                
                $directoryCategory = new DirectoryCategory;
                $directoryCategory->directory_id = $id;
                $directoryCategory->category_id = $val;
                $directoryCategory->save();
            }           

            // Upload images
            $fileName = "";
            $directoryImages = [];
            if ($request->hasFile('image')) {
                $files = $request->file('image');
                foreach ($files as $file) {
                    $image = array('image' => $file);
                    $imageValidator = Validator::make($image, ['image' => 'mimes:jpeg,jpg,png|max:2000']);
                    if ($imageValidator->fails()) {
                        continue;
                    }

                    $extension = $file->getClientOriginalExtension();
                    $destinationPath = 'uploads/directories/';
                    $fileName = microtime(true) . '.' . $extension;
                    
                    //Create thumb
                    $thumbName = "thumb_" . $fileName;
                    Image::make($file->getRealPath())->fit(self::THUMB_WIDTH, self::THUMB_HEIGHT)->save($destinationPath . $thumbName);

                    $file->move($destinationPath, $fileName);

                    $directoryImages[] = $fileName;
                }
            }

            foreach ($directoryImages as $val) {
                $directoryPicture = new DirectoryPicture;
                $directoryPicture->picture_file = $val;
                $directoryPicture->directory_id = $id;
                $directoryPicture->create_time = new \DateTime;
                $directoryPicture->save();
            }
            
            // Logo
            $logoFile = $request->file('logo');
            $logoFileName = '';

            if(!empty($logoFile)) {
                $destinationPath = 'uploads/directories/';
                $extension = $logoFile->getClientOriginalExtension();
                $logoFileName = microtime(true) . '.' . $extension;
                
                //Create thumb
                $thumbName = "thumb_" . $logoFileName;
                Image::make($logoFile->getRealPath())->fit(self::THUMB_WIDTH, self::THUMB_HEIGHT)->save($destinationPath . $thumbName);
                
                
                $logoFile->move($destinationPath, $logoFileName);
                
                $directory = Directory::where('id', $id)->first();
                
                File::delete(public_path() . "/uploads/directories/" . $directory->logo);
                File::delete(public_path() . "/uploads/directories/thumb_" . $directory->logo);
                
                $directory->logo = $logoFileName;
                $directory->save();
                
            }

            return redirect()->back()->with('alert-success', trans('messages.success.update_directory'));
        }
        
        return redirect()->back()->withInput()->with('alert-danger', trans('messages.errors.update_fail'));
    }
    
    public function update(Request $request, $id) {
        $directory = Directory::where('id', $id)->where('user_id', Auth::id())->firstOrFail();
        $categories = Category::all(['name_lid', 'id']);
        $provinces = Province::all(['name', 'id'])->sortBy('name');
        $pictures = DirectoryPicture::where('directory_id', $id)->get();

        if($request->attributes->get('mobileActive')) {
            return view('mobile.directory.update', compact('categories', 'provinces', 'directory', 'pictures'));
        }

        return view('frontend.dashboard.directory_edit', compact('categories', 'provinces', 'directory', 'pictures'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/\s+[^.!?]*/',
            'address' => 'required',
            'categories' => 'required',
            'phone' => 'required',
            'description' => 'required',
            'city_id' => 'required',
            'latlng' => 'required',
            'image' => 'array',
            'logo' => 'required|mimes:jpeg,jpg,png|max:2000',
        ], ['name.regex' => 'Format nama bisnis tidak valid.']);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        try {
            list($latitude, $longitude) = explode(';', $request->get('latlng'));
            
            $fileName = "";
            $directoryImages = [];
            if ($request->hasFile('image')) {
                $files = $request->file('image');
                foreach ($files as $file) {
                    $image = array('image' => $file);
                    $imageValidator = Validator::make($image, ['image' => 'mimes:jpeg,jpg,png|max:2000']);
                    if ($imageValidator->fails()) {
                        continue;
                    }
                    
                    $extension = $file->getClientOriginalExtension();
                    $destinationPath = 'uploads/directories/';
                    $fileName = microtime(true) . '.' . $extension;
                    
                    //Create thumb
                    $thumbName = "thumb_" . $fileName;
                    Image::make($file->getRealPath())->fit(self::THUMB_WIDTH, self::THUMB_HEIGHT)->save($destinationPath . $thumbName);
                
                    $file->move($destinationPath, $fileName);
                    
                    $directoryImages[] = $fileName;
                }
            }
            
            // Logo
            $logoFile = $request->file('logo');
            $logoFileName = '';

            if(!empty($logoFile)) {
                $destinationPath = 'uploads/directories/';
                $extension = $logoFile->getClientOriginalExtension();
                $logoFileName = microtime(true) . '.' . $extension;
                
                //Create thumb
                $thumbName = "thumb_" . $logoFileName;
                Image::make($logoFile->getRealPath())->fit(self::THUMB_WIDTH, self::THUMB_HEIGHT)->save($destinationPath . $thumbName);

                $logoFile->move($destinationPath, $logoFileName);
            }
            
            $directory = new Directory;
            $directory->user_id = Auth::id();
            $directory->name = $request->get('name');
            $directory->address_1 = $request->get('address');
            $directory->description_lid = $request->get('description');
            $directory->phone_1 = $request->get('phone');
            $directory->city_id = $request->get('city_id');
            $directory->latitude = $latitude;
            $directory->longitude = $longitude;
            $directory->logo = $logoFileName;
            $directory->ecatalog = '';
            $directory->create_time = Carbon::now();
            $directory->update_time = Carbon::now();
            $directory->rating = 5;
            $directory->saveOrFail();
            
            $categories = explode(',', $request->get('categories'));
            foreach ($categories as $val) {
                $directoryCategory = new DirectoryCategory;
                $directoryCategory->directory_id = $directory->id;
                $directoryCategory->category_id = $val;
                $directoryCategory->save();
            }
            
            foreach ($directoryImages as $val) {
                $directoryPicture = new DirectoryPicture;
                $directoryPicture->picture_file = $val;
                $directoryPicture->directory_id = $directory->id;
                $directoryPicture->create_time = new \DateTime;
                $directoryPicture->save();
            }
            
            return redirect()->back()->with('alert-success', trans('messages.success.insert_directory'));
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->withInput()->with('alert-danger', trans('messages.errors.insert_fail'));
        }
    }
            
    public function favorite(Request $request) {
        if(!Auth::check()){
            return response()->json(['error' => 'unauthenticated']);
        }
        
        $stat = true;
        
        if(empty($request->get('directory_id'))) {
            $stat = false;
        }
        
        if($stat === true) {
            $directoryFavorite = DirectoryFavorite::where('user_id', Auth::id())
                                    ->where('directory_id', $request->get('directory_id'))->first();
            
            if(empty($directoryFavorite)) {
                //unfavorite
                $favorite = new DirectoryFavorite;
                $favorite->directory_id = $request->get('directory_id');
                $favorite->user_id = Auth::id();
                $stat = $favorite->save();
            } else {
                //favorite
                $stat = DirectoryFavorite::where('user_id', Auth::id())
                        ->where('directory_id', $request->get('directory_id'))->delete();
            }
        }
        
        return response()->json(['stat' => $stat]);
    }

    public function verifyForm(Request $request, $slug) {
        $directory_id = $request->session()->remove('claim_id');
        $directory = Directory::where('slug', $slug)->firstOrFail();
        $verify = DirectoryVerification::where('user_id', Auth::user()->id)->where('directory_id', $directory->id)->first();
        $logs = [];
        if($verify) {
            $logs = DirectoryVerification::getLogs($verify->id)->toArray();
        }
        if($request->attributes->get('mobileActive')) {
            return view('mobile.directory.verify', compact('directory', 'verify', 'logs'));
        }
        return view('frontend.dashboard.verify', compact('directory', 'verify', 'logs'));
    }

    public function verifySubmit(Request $request) {
        $validator = Validator::make($request->all(), [
            'siup' => 'required|mimes:jpeg,jpg,png|max:2000',
            'ktp' => 'required|mimes:jpeg,jpg,png|max:2000',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $baseFilename = microtime(true);
        $destinationPath = 'uploads/verification/';

        // siup
        $siupFile = $request->file('siup');
        $siupFileName = $baseFilename . '_siup' . '.' . $siupFile->getClientOriginalExtension();;
        $siupFile->move($destinationPath, $siupFileName);

        // ktp
        $ktpFile = $request->file('ktp');
        $ktpFileName = $baseFilename . '_ktp' . '.' . $ktpFile->getClientOriginalExtension();;
        $ktpFile->move($destinationPath, $ktpFileName);



        $verify = New DirectoryVerification;
        $verify->verify_number = (date('YdmH')*100+rand(10,99));
        $verify->file_name_siup = $siupFileName;
        $verify->file_name_ktp = $ktpFileName;
        $verify->user_id = Auth::user()->id;
        $verify->directory_id = $request->input('directory_id');

        $verify->save();

        DirectoryVerification::log($verify, 'Dokumen verifikasi bisnis dikirim');

        return redirect()->back()->with('alert-success', trans('messages.success.verify_submit'));
    }
}
