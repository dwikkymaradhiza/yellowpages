<?php

namespace App\Http\Controllers\Front;

use App\Models\Directory;
use App\Models\DirectoryInfo;
use App\Models\SmsUpdate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class SmsupdateController extends Controller
{
    public function smsupdatemo(Request $request) {
    	$input = $request->all();
    	Log::info('[SMS_UPDATE_MO] update request logging', ['input' => $input]);
        $check = explode(' ', $input['sms']);
        if(sizeof($check) < 3 or !is_numeric($check[1])) {
            $response = ['trx_id' => $input['trx_id'], 'message' => 'Format SMS Salah (cth: YELLOWPAGES<spasi>[ID Bisnis]<spasi>[Pesan])', 'status' => 0];
            $this->_log($input, $response);
            return response()->json($response);
        }
        list($shortcode, $id, $msg) = explode(' ', $input['sms'], 3);

        $directory = Directory::find($id);
        if(!$directory) {
            $response = ['trx_id' => $input['trx_id'], 'message' => 'ID Bisnis yang Anda Gunakan Tidak Valid. Cek Dashboard untuk melihat ID Bisnis', 'status' => 0];
            $this->_log($input, $response);
            return response()->json($response);
        }

        if(!$directory->user_id) {
            $response = ['trx_id' => $input['trx_id'], 'message' => 'Bisnis Belum di Klaim Pemilik. Klaim bisnis di yellowpages.co.id.', 'status' => 0];
            $this->_log($input, $response);
            return response()->json($response);
        }

        $package = User::checkPackage($directory->user_id);
        if(!$package) {
            $response = ['trx_id' => $input['trx_id'], 'message' => 'Pemilik Belum Upgrade Paket', 'status' => 0];
            $this->_log($input, $response);
            return response()->json($response);
        }

        $bad_words = DirectoryInfo::badWordValidation($msg);
        if($bad_words) {
            $response = ['trx_id' => $input['trx_id'], 'message' => 'Status Mengandung Kata Terlarang', 'status' => 0];
            $this->_log($input, $response);
            return response()->json($response);
        }

        // check unique
        $check = SmsUpdate::where('trx_id', $input['trx_id'])->get()->first();
        if($check) {
            $response = ['trx_id' => $input['trx_id'], 'message' => 'Provider trx_id Sudah Terdaftar', 'status' => 0];
            $this->_log($input, $response);
            return response()->json($response);
        }

        $smsUpdate = new SmsUpdate;
        $smsUpdate->msisdn = $input['msisdn'];
        $smsUpdate->trx_id = $input['trx_id'];
        $smsUpdate->user_id = $directory->user_id;
        $smsUpdate->directory_id = $directory->id;
        $smsUpdate->msg = $msg;

        $smsUpdate->saveOrFail();

        $response = ['trx_id' => $input['trx_id'], 'message' => 'SMS Update Info Berhasil. Terima Kasih', 'status' => 1];
        $this->_log($input, $response);

        return response()->json($response);
    }

    public function smsupdatedr(Request $request) {
        $input = $request->all();
        $this->_log($input, [], 'DR');
        $smsUpdate = SmsUpdate::where('trx_id', $input['trx_id'])->where('status', 0)->first();

        if(!$smsUpdate or $input['status'] == 'failed') {
            die;
        }

        $info = new DirectoryInfo;
        $info->user_id = $smsUpdate->user_id;
        $info->directory_id = $smsUpdate->directory_id;
        $info->message = $smsUpdate->msg;
        $info->created_at = Carbon::now();
        $info->via = 'sms';
        $info->saveOrFail();

        $smsUpdate->status = 1;
        $smsUpdate->save();
    }

    protected function _log($input, $response, $flag = 'MO') {
        Log::info('[SMS_UPDATE_'.$flag.'] update request logging', ['input' => $input, 'response' => $response]);
    }
}
