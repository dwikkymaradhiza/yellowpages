<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Models\Article;

class StaticController extends Controller
{
	public function show($slug) {
		$article = Article::getBySlug($slug);
		if(!$article) {
			abort(404);
		}

		return view('frontend.static', compact('article'));
	}
}
