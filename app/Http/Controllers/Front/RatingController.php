<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Rating;
use App\Models\DirectoryRating;
use App\Models\Directory;
use App\Models\User;
use Carbon\Carbon;
use Auth;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_product(Request $request)
    {
        $rules = [
            'review' => 'required',
            'rate' => 'required|min:1|max:5',
            'product_id' => 'required'
        ];
        
        if(!Auth::check()) {
            $rules['g-recaptcha-response'] = 'required|recaptcha';
        }
        
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        try {
            $rating = new Rating;
            $rating->comment = $request->get('review');
            $rating->rate = $request->get('rate');
            $rating->product_id = $request->get('product_id');
            $rating->name = $request->get('name');
            $rating->email = $request->get('email');
            $rating->phone = $request->get('phone');
            $rating->create_time = Carbon::now();
            $rating->update_time = Carbon::now();
            $rating->user_id = (!is_null(Auth::id())) ? Auth::id() : 0;
            $rating->saveOrFail();
            
            $directory = Directory::where('id', $rating->product->directory_id)->first();
            User::log("Review baru untuk produk " . $rating->product->name, (!empty($directory)) ? $directory->user_id : null);
            
            return redirect()->back()->with('alert-success', trans('messages.success.insert_product_rating'));
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->withInput()->with('alert-danger', trans('messages.errors.insert_fail'));
        }
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_directory(Request $request)
    {
        $rules = [
            'review' => 'required',
            'rate' => 'required|min:1|max:5',
            'directory_id' => 'required'
        ];
        
        if(!Auth::check()) {
            $rules['g-recaptcha-response'] = 'required|recaptcha';
        }
        
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        try {
            $rating = new DirectoryRating;
            $rating->comment = $request->get('review');
            $rating->rate = $request->get('rate');
            $rating->name = $request->get('name');
            $rating->email = $request->get('email');
            $rating->phone = $request->get('phone');
            $rating->directory_id = $request->get('directory_id');
            $rating->create_time = Carbon::now();
            $rating->update_time = Carbon::now();
            $rating->user_id = Auth::id();
            $rating->saveOrFail();
            
            $averageRating = $rating->getAverageRating($rating->directory_id);            
            $rating->updateDirectoryRating($rating->directory_id,$averageRating);
            
            $directory = Directory::where('id', $rating->directory_id)->first();
            User::log("Review baru untuk bisnis " . $rating->directory->name, (!empty($directory)) ? $directory->user_id : null);
            
            return redirect()->back()->with('alert-success', trans('messages.success.insert_directory_rating'));
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->withInput()->with('alert-danger', trans('messages.errors.insert_fail'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
