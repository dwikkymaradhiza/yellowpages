<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\DirectoryInfo;
use App\Models\Directory;
use Carbon\Carbon;
use Auth;
use DB;

class DirectoryInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // define pagesize
        $queryString = $request->query();
        $pageSize = (!empty($queryString['size'])) ? (int) $queryString['size'] : 10;
        $size = ($pageSize > 25) ? 25 : $pageSize;
        
        // get data directories
        $infos = DirectoryInfo::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate($size);
        $directories = Directory::where('user_id', Auth::user()->id)->get();
        
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.status', compact('infos', 'size', 'directories'));
        }
        
        return view('frontend.dashboard.status', compact('infos', 'size', 'directories'));
    }
    
    public function create()
    {
        $directories = Directory::where('user_id', Auth::user()->id)->get();
        $form_title = 'TULIS STATUS BARU';
        
        return view('mobile.dashboard.status_add', compact('directories', 'form_title'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = DirectoryInfo::where('id', $id)->where('user_id', Auth::user()->id)->firstOrFail();
        $item->delete();
        
        return redirect()->back()->with('alert-success', trans('messages.success.delete_directory_info'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|min:30|regex:/\s+[^.!?]*/',
            'directory_id' => 'required',
        ], ['message.regex' => 'Format content tidak valid.']);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $bad_words = DirectoryInfo::badWordValidation($request->get('message'));
        
        if($bad_words > 0) {
            return redirect()->back()->with('alert-danger', trans('validation.badwords'));
        }
        
        if(!empty($request->status_id)){
            $this->update($request->all());
            return redirect()->back()->with('alert-success', trans('messages.success.update_directory_info'));
        }
        
        try {
            $info = new DirectoryInfo;
            $info->user_id = Auth::user()->id;
            $info->directory_id = $request->get('directory_id');
            $info->message = $request->get('message');
            $info->created_at = Carbon::now();
            $info->via = 'admin';
            $info->saveOrFail();
            
            if(!empty($request->get('mobile'))) {
                return redirect()->route('dashboard-status-index')->with('alert-success', trans('messages.success.insert_directory_info'));
            }
            
            return redirect()->back()->with('alert-success', trans('messages.success.insert_directory_info'));
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->withInput()->with('alert-danger', trans('messages.errors.insert_fail'));
        }
    }
    
    public function update($array)
    {
        $info = new DirectoryInfo;
        $info::where('id', '=', $array['status_id'])->update(['directory_id' => $array['directory_id'],'message' => $array['message']]);
        
    }
    
    public function destroyBulk(Request $request)
    {
        $id_array = explode(',',$request->id);
        DirectoryInfo::destroy($id_array);
        return 'success';
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = DirectoryInfo::where('id', $id)->firstOrFail();
        $form_title = "STATUS DETAIL";
        
        return view('mobile.dashboard.status_detail', compact('status', 'form_title'));
    }
}
