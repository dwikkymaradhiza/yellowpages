<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Models\Article;

class RedirectionController extends Controller
{
    public function check_path($path, $_query=array()) {
        $paths = explode('/', $path);
        $result = false;

	if(count($paths) < 2){
                return $result;
        }

        $query = [];

        switch($paths[0]) {
            case "directory":
                $url_path = ['bisnis'];
                $url_path[] = $paths[1];
                break;
            case "find":
                $url_path = ['cari', 'bisnis'];
                $url_path[] = $paths[1];
                if(isset($paths[2])) {
                    if(is_numeric($paths[2])) {
                        $query[] = 'page='.$paths[2];
                    } else {
                        $url_path[] = $paths[2];
                    }
                }

                if(isset($paths[3])) {
                    $query[] = 'page='.$paths[3];
                }

                break;
            case "category":
                $url_path = ['bisnis', 'kategori'];
                $url_path[] = $paths[1];

                if(isset($_query['page'])) {
                    $url_path[] = 'page';
                    $url_path[] = $_query['page'];
                }
                break;
            case "page":
                $article = Article::where('old_slug', $paths[1])->get()->first();
                if($article) {
                    $url_path = [$article->slug];
                }
                break;
            default:
                break;
        }

        if(!empty($url_path)) {
            $result = implode('/', $url_path);
            if (!empty($query)) {
                $result .= '?' . implode('&', $query);
            }
        }

        return $result;
    }
}
