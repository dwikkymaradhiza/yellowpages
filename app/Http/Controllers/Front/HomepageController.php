<?php

namespace App\Http\Controllers\Front;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Front\DirectoryController;
use App\Http\Controllers\Front\ProductController;
use App\Models\MainBanner;
use App\Models\Affiliate;
use App\Models\Directory;
use App\Models\DirectoryInfo;
use App\Models\SeoText;

class HomepageController extends Controller {

    private $seo_content;
    
    public function __construct() {
        $slug = collect(request()->segments())->last();
        if($slug === null){
            $slug = '';
        }
        
        $seo_text = new SeoText;
        
        $seo_content = $seo_text::where('slug','=',$slug)->first();
        
        if(count($seo_content)>0){
            $this->seo_content = $seo_content->seo_text;
        }else{
            $this->seo_content = '';
        }
    }
    
    public function index(Request $request) {
        $seo_content = $this->seo_content;
        $featured_directory = new DirectoryController;
        $featured_directory_array = $featured_directory->getFeaturedDirectory();

        $featured_product = new ProductController;
        $featured_product_array = $featured_product->getFeaturedProduct();

        $affiliate_article = Affiliate::getLatestArticle();
        $directory_info = DirectoryInfo::getLatestInfo(5);
        $all_banner = MainBanner::getBanner();
        
        $newest_directory = Directory::getLatest(5);
        $newest_product = Product::getLatest(5);

        $is_homepage = TRUE;
        if($request->attributes->get('mobileActive')) {
            return view('mobile.home', compact('featured_directory_array','all_banner','featured_product_array','newest_product','newest_directory','affiliate_article','directory_info', 'is_homepage', 'seo_content'));
        } else {
            return view('frontend.home', compact('featured_directory_array','all_banner','featured_product_array','newest_product','newest_directory','affiliate_article','directory_info', 'is_homepage', 'seo_content'));
        }

    }
}
