<?php

namespace App\Http\Controllers\Front;

use App\Models\ProductFavorite;
use App\Models\Rating;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Directory;
use App\Models\Affiliate;
use App\Models\ProductPicture;
use Auth;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Cache;
use App\Models\SeoText;

class ProductController extends Controller
{
    const THUMB_WIDTH = 260;
    const THUMB_HEIGHT = 200;
    private $seo_content;
    
    public function __construct() {
        $slug = collect(request()->segments())->last();
        if($slug === null){
            $slug = '';
        }
        
        $seo_text = new SeoText;
        
        $seo_content = $seo_text::where('slug','=',$slug)->first();
        
        if(count($seo_content)>0){
            $this->seo_content = $seo_content->seo_text;
        }else{
            $this->seo_content = '';
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // define pagesize
        $queryString = $request->query();
        $pageSize = (!empty($queryString['size'])) ? (int) $queryString['size'] : 10;
        $size = ($pageSize > 25) ? 25 : $pageSize;
        
        // get data inquiries
        $directories = Directory::where('user_id', Auth::user()->id)->pluck('id')->toArray();
        $products = Product::whereIn('directory_id', $directories)->orderBy('id', 'desc')->paginate($size);
        
        $directoriesList = Directory::where('user_id', Auth::user()->id)->get();
        
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.product', compact('products', 'size','directoriesList'));
        }
        
        return view('frontend.dashboard.product', compact('products', 'size','directoriesList'));
    }

    public function deleteBulk(Request $request){
        $id = $request->id_array;

        if($id !== ''){
            $id_array =explode(',',$id);
            
            foreach($id_array as $id_array_detail){
                Product::where('id', '=', $id_array_detail)->delete();                
            }            
            return 'success';
        }        
        return 'failed';
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/\s+[^.!?]*/',
            'description' => 'required',
            'directory_id' => 'required',
            'image' => 'array'
        ], ['name.regex' => 'Format nama produk tidak valid']);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        $fileName = '';
        $productImages = [];
        if ($request->hasFile('image')) {
            $files = $request->file('image');
            
            foreach ($files as $file) {
                $image = array('image' => $file);
                $imageValidator = Validator::make($image, ['image' => 'mimes:jpeg,jpg,png|max:2000']);
                if ($imageValidator->fails()) {
                    continue;
                }
                
                $destinationPath = 'uploads/products/';
                $extension = $file->getClientOriginalExtension();
                $fileName = microtime(true) . '.' . $extension;
                
                //Create thumb
                $thumbName = "thumb_" . $fileName;
                Image::make($file->getRealPath())->fit(self::THUMB_WIDTH, self::THUMB_HEIGHT)->save($destinationPath . $thumbName);
                
                $file->move($destinationPath, $fileName);
                
                $productImages[] = $fileName;
            }
        }
        
        $insert = Product::create([
            'name' => $request->input('name'),            
            'description' => $request->input('description'),
            'stock' => empty( (int) $request->input('stock')) ? 0 : (int) $request->input('stock'),
            'brand' => '',
            'masterid' => '',
            'masterid_key' => '',
            'picture' => $fileName,
            'directory_id' => $request->input('directory_id'),
            'published' => 1,
            'slug' => str_slug($request->input('name'), '-'),
        ]);
        
        foreach ($productImages as $val) {
            $productPicture = new ProductPicture;
            $productPicture->picture = $val;
            $productPicture->product_id = $insert->id;
            $productPicture->save();
        }

        return redirect()->route('dashboard-product-index')->with('alert-success', trans('messages.success.insert_product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function click($slug){
        $product = Product::where('slug', $slug)->first();
        
        $directory_array = Directory::where('id',$product->directory_id)->first();
        $user_id = $directory_array->user_id;
        $product_id = $product->id;
        
        Product::counter('click',$user_id,$product_id);

        return \Redirect::to(route('product-detail',$slug));        
    }

    public function impress($slug){
        $product = Product::where('slug', $slug)->firstOrFail();
        
        $directory_array = Directory::where('id',$product->directory_id)->firstOrFail();
        $user_id = $directory_array->user_id;
        $product_id = $product->id;
        
        $productObj = new Product;  
        $productObj->counter('view',$user_id,$product_id);
    }
            
    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        $seo_content = $this->seo_content;$seo_content = $this->seo_content;
        $product = Product::findBySlug($slug);
        if(!$product) {
            abort(404);
        }

        $directory = \Cache::remember('dir:id:'.$product->directory_id, config('cache.expires.dir', config('cache.expires.general')), function() use($product) {
            return $product->directory;
        });

        $ratings = Rating::getProductRating($product);

        // get other products
        $others = Directory::getProducts($directory);
        $others = $others->filter(function($key) use($product) {
            return $key->id != $product->id;
        });

        // get similar products
        $similars = Product::findSimilarByCategories($directory->categories)
                    ->where('directory_product.id', '!=', $product->id)
                    ->where('directory_product.published', 1)
                    ->limit(5)
                    ->inRandomOrder()
                    ->get();

        $use_newest_pdp = false;
        if($similars->count() === 0) {
            $use_newest_pdp = true;
            $similars = Product::getLatest(5);
        }
        
        // get average rating
        $sumRating = 0;
        foreach($ratings as $k => $v) {
            $sumRating += $v->rate;
        }
        
        $average_rate = ($sumRating === 0) ? 0 : (int) ceil($sumRating / count($ratings));

        // get affiliate article
        $keywords = [];
        foreach($directory->categories as $kc => $vc) {
            $keywords[] = $vc->name_lid;
        }
        $keywords[] = $directory->name;
        $articles = Affiliate::search(implode(' ', $keywords));

        // get pictures
        $pictures = ProductPicture::findByOwner($product);
        // get favorite
        $isFavorited = 0;
        if(Auth::check()) {
            $favorite = ProductFavorite::where('product_id', $product->id)->where('user_id' , Auth::id())->first();
            if(!empty($favorite)) {
                $isFavorited = 1;
            }
        }

        if($request->attributes->get('mobileActive')) {
            return view('mobile.product_detail', compact('isFavorited', 'pictures', 'product', 'directory', 'ratings', 'similars', 'others', 'average_rate', 'articles', 'use_newest_pdp', 'seo_content'));
        }
        return view('frontend.product_detail', compact('pictures', 'product', 'directory', 'ratings', 'similars', 'others', 'average_rate', 'articles', 'use_newest_pdp', 'seo_content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id = '')
    {
        $product = [];
        $directoriesList = [];
        $pictures = [];
        
        if($id !== ''){
            $product = Product::where('id', $id)->firstOrFail();            
            $pictures = ProductPicture::where('product_id', $id)->get();
        }
        
        $directoriesList = Directory::where('user_id', Auth::user()->id)->get();
        
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.product_edit', compact('directoriesList', 'pictures', 'product'));
        }
        
        return view('frontend.dashboard.product_edit', compact('directoriesList', 'pictures', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/\s+[^.!?]*/',
            'description' => 'required',
            'directory_id' => 'required',
            'image' => 'array'
        ], ['name.regex' => 'Format nama produk tidak valid']);
        
        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        $fileName = '';
        $productImages = [];
        if ($request->hasFile('image')) {
            $files = $request->file('image');
            
            foreach ($files as $file) {
                $image = array('image' => $file);
                $imageValidator = Validator::make($image, ['image' => 'mimes:jpeg,jpg,png|max:2000']);
                if ($imageValidator->fails()) {
                    continue;
                }

                $extension = $file->getClientOriginalExtension();
                $destinationPath = 'uploads/products/';
                $fileName = microtime(true) . '.' . $extension;
                
                //Create thumb
                $thumbName = "thumb_" . $fileName;
                Image::make($file->getRealPath())->fit(self::THUMB_WIDTH, self::THUMB_HEIGHT)->save($destinationPath . $thumbName);
                
                $file->move($destinationPath, $fileName);

                $productImages[] = $fileName;
            }
        }
        
        foreach ($productImages as $val) {
            $productPicture = new ProductPicture;
            $productPicture->picture = $val;
            $productPicture->product_id = $id;
            $productPicture->save();
        }
        
        Product::where('id', $id)
          ->update([ 
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'stock' => $request->get('stock'),
                'directory_id' => $request->get('directory_id'),
            ]);
        
        return redirect()->back()->with('alert-success', trans('messages.success.update_product'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPicture($id)
    {
        //unlink directory picture
        $pictures = ProductPicture::where('id', $id)->firstOrFail();
        File::delete(public_path() . "/uploads/products/{$pictures->picture}");
        File::delete(public_path() . "/uploads/products/thumb_{$pictures->picture}");
        
        ProductPicture::where('id', $id)->delete();
        
        return redirect()->back()->with('alert-success', trans('messages.success.delete_product_picture'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Product::where('id', '=', $request->id)->delete();
        
        //unlink product pictures
        $pictures = ProductPicture::where('product_id', $request->id)->get();
        foreach($pictures as $k => $v) {
            File::delete(public_path() . "/uploads/products/{$v->picture}");
            File::delete(public_path() . "/uploads/products/thumb_{$v->picture}");
        }
        
        ProductPicture::where('product_id', $request->id)->delete();
        
        return redirect()->route('dashboard-product-index')->with('alert-success', trans('messages.success.delete_product'));
    }
    
    public function destroyBulk(Request $request)
    {
        $id_array = explode(',',$request->id);
        Product::destroy($id_array);
        return 'success';
    }
    
    public function getFeaturedProduct(){
        #$redis_key = 'home:feat_product';
        #$data = Cache::remember($redis_key, config('cache.expires.'.$redis_key, config('cache.expires.general')), function() {
            $todayDate = date('Ym');
            $result = Product::getFeaturedProduct('feature', 8);
            return $result;
        #});

        #return $data;
    }
    
    public function getNewestProduct()
    {
        $result = Product::where('published','=','1')
                ->orderby('id','desc')
                ->limit('5')
                ->get();
        
        return $result;
    }

    public function favorite(Request $request) {
        if(!Auth::check()){
            return response()->json(['error' => 'unauthenticated']);
        }

        $stat = true;

        if(empty($request->get('product_id'))) {
            $stat = false;
        }

        if($stat === true) {
            $productFav = ProductFavorite::where('user_id', Auth::id())
                ->where('product_id', $request->get('product_id'))->first();

            if(empty($productFav)) {
                //unfavorite
                $favorite = new ProductFavorite;
                $favorite->product_id = $request->get('product_id');
                $favorite->user_id = Auth::id();
                $stat = $favorite->save();
            } else {
                //favorite
                $stat = ProductFavorite::where('user_id', Auth::id())
                    ->where('product_id', $request->get('product_id'))->delete();
            }
        }

        return response()->json(['stat' => $stat]);
    }
}
