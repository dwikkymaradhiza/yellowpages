<?php

namespace App\Http\Controllers\Front;

use App\Models\Directory;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\Package_order;
use App\Models\Payments;
use App\Models\PaymentsHistory;
use DB;
use Auth;

use Mail;
use Illuminate\Http\Request;

class SponsorController extends Controller
{
    public function index(Request $request)
    {
        $directories = Directory::getProductAndDirectoryByUserId(Auth::user()->id);
        
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sponsor_product', compact('directories'));
        }else{
            return view('frontend.dashboard.sponsor_product', compact('directories'));
        }
    }
    
    public function package(Request $request)
    {
        $packages = Package::where('type','=','SPONSORED')->get();

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sponsor_package', compact('packages'));
        }else{
            return view('frontend.dashboard.sponsor_package', compact('packages'));
        }
    }
    
    public function invoice(Request $request)
    {
        $saldo = User::where('id','=',Auth::user()->id)->pluck('user_balance')->first();

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sponsor_invoice', compact('saldo'));
        }else{
            return view('frontend.dashboard.sponsor_invoice', compact('saldo'));
        }
    }
    
    public function sponsorUpdate(Request $request) {
        $package_order = new Package_order;
        $payments = new Payments; 
        $payment_arr = Payments::where('code','=','saldo')->first();
        
        $invoice_arr = [
            'price' => $request->price,
            'package_id' => $request->package_id,
            'status' => $request->status,
            'directories_id' => isset($request->directories_id)?$request->directories_id:'',
            'products_id' => isset($request->products_id)?$request->products_id:'',
            'payment_id' => isset($payment_arr->id)?$payment_arr->id:1,
            'user_id' => Auth::user()->id,
            'description' => isset($payment_arr->name)?$payment_arr->name:'',
        ];
        
        
        $saldo = User::where('id','=',Auth::user()->id)->pluck('user_balance')->first();
        $last_saldo = $saldo - $invoice_arr['price'];
        
        //double check saldo
        if($last_saldo < 0){
            $invoice_arr['status'] == 0;
            $order_id = $package_order->updateMembership($invoice_arr);
            $response = [
                'message' => 'failed',
                'order_id' => $order_id,
                'message' => 'Saldo tidak mencukupi'
            ];
            return json_encode($response);
        }
        
        $order_id = $package_order->updateMembership($invoice_arr);
        
        if($invoice_arr['status'] == 1){
            $this->insertPayment($invoice_arr,$order_id);
            
            //update balance
            User::where('id', Auth::user()->id)
            ->update([ 
                'user_balance' => $last_saldo,
            ]);
        }
        //$this->sendMemberNotification($invoice_arr, $order_id);
        $response = [
            'message' => 'success',
            'order_id' => $order_id
        ];
        
        return json_encode($response);
    }
    
    public function insertPayment($invoice_arr,$order_id){
        PaymentsHistory::create([
            'package_order_id' => $order_id,            
            'total_payment' => $invoice_arr['price'],
            'description' => $invoice_arr['description'],
            'user_id' => Auth::user()->id,
        ]);
    }
    
    public function sendMemberNotification($array, $order_id) {
        Mail::send('emails.sponsor', ['package_order_id' => $order_id], function ($m) {
            $user = Auth::user();
            $m->from(config('app.sponsor'), 'Yellow Pages');
            $m->to($user->email,$user->name)->subject('Sponsor');
        });
        
    }
    
    public function thanks(Request $request)
    {
        
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sponsor_thanks');
        }else{
            return view('frontend.dashboard.sponsor_thanks');
        }
    }
}
