<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Models\City;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function getAllCity(Request $request){
        $city = isset($request->city)?$request->city:'';
        
        $result = [];
        
        $city_array = City::where('name','like',"%".$city."%")->limit('10')->get();
        
        foreach($city_array as $city_array_detail){
            array_push($result,$city_array_detail->name);
        }
        
        return $result;
    }
    
    public function getAllCityByProvince(Request $request){
        $province = isset($request->province)?$request->province:'';
        
        $result = [];
        
        $city_array = City::where('province_id','=', $province)->get();
        
        foreach($city_array as $city_array_detail){
            $city = [
                'id' => $city_array_detail->id,
                'name' => $city_array_detail->name,                
            ];
            array_push($result,$city);
        }
        
        return $result;
    }
    

    public function ajaxFindCity($province_id, $format) {
        $results = ['data' => []];

        $cities = City::where('province_id', $province_id)->orderBy('name')->get();

        foreach($cities as $city) {
            if($format == 'option') {
                $results['data'][] = '<option value="'.$city->id.'">'.$city->name.'</option>';
            } else {
                $results[] = ['id' => $city->id, 'name' => $city->name];
            }
        }

        if($format == 'option') {
            $results['data'] = implode('', $results['data']);
        }

        return $results;
    }

}
