<?php

namespace App\Http\Controllers\Front;

use App\Models\Directory;
use App\Models\Counter;
use App\Models\User;
use Carbon\Carbon;
use App\Models\DirectoryInfo;
use App\Models\DirectoryInquiry;
use DB;
use Auth;
use App\Models\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $directories = Directory::where('user_id', Auth::user()->id)->get();
        
        $directoryInfo = new DirectoryInfo;
        $directoryInquiry = new DirectoryInquiry;
        
        $info = $directoryInfo->orderby('id','desc')->select('id','message','created_at')->where('user_id', Auth::user()->id);
        
        $last_activity = User::getLogs()->toArray();

	$day_id = ['Ming', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'];        
        $product = new Product;
        $date_array = [];
        for($i= -6; $i <= 0; $i++){
            $date = date('Y-m-d', strtotime($i.' days'));
            $dateDMY = $day_id[date('w' , strtotime($i.' days'))];
            $date_array[] = $dateDMY;
            
            $click_array = $product->getCounter('click', Auth::user()->id, $date);
            $total = 0;
            foreach($click_array as $arr){
                $total += (int) $arr['counter'];
            }
            $total_click[] = $total; 
            
            $view_array = $product->getCounter('view', Auth::user()->id, $date);
            $total = 0;
            foreach($view_array as $arr){
                $total += (int) $arr['counter'];
            }
            $total_view[] = $total;
            
        }
        
        if($request->attributes->get('mobileActive')) {
            $beta_tester = explode(',', env('BETA_TESTER'));
            $tester = FALSE;
            if(!empty($beta_tester)) {
                $user_email = Auth::user()->email;
                if(in_array($user_email, $beta_tester)) {
                    $tester = TRUE;
                }
            }

            return view('mobile.dashboard.index', compact('directories','last_activity', 'logs','date_array','total_click','total_view', 'tester'));
        }
        return view('frontend.dashboard.index', compact('directories','last_activity', 'logs','date_array','total_click','total_view'));
    }
    
    public function ajaxInfoCounter() {
        if(!Auth::check()) {
            abort(403);
        }
        
        $data = [];
        $chartData = [];
        $fromDate = Carbon::now()->startOfWeek()->toDateString(); 
        $tillDate = Carbon::now()->endOfWeek()->toDateString();
        
        $startDate = Carbon::now()->startOfWeek();
        for($i = 0; $i <= 6; $i++) {
            $dateString = $startDate->toDateString();
            $data[$dateString]['inquiry_count'] = 0;
            $data[$dateString]['sms_count'] = 0;
            
            $startDate->addDay();
        }
        
        $counters = Counter::selectRaw('created_at, inquiry_count, sms_count')
                    ->whereBetween('created_at', [$fromDate, $tillDate] )
                    ->where('user_id', Auth::id())
                    ->groupBy('created_at', 'inquiry_count', 'sms_count')
                    ->orderBy('created_at', 'DESC')
                    ->get();
        
        foreach ($counters as $k => $v) {
            $data[$v->created_at]['inquiry_count'] = $v->inquiry_count;
            $data[$v->created_at]['sms_count'] = $v->sms_count;
        }
        
        foreach ($data as $kd => $vd) {
            $chartData['inquiry'][] = $vd['inquiry_count'];
            $chartData['sms'][] = $vd['sms_count'];
        }
        
        return response()->json(['data' => $chartData]);
    }

    public function first() {
    	return view('frontend.dashboard.first');
    }
}
