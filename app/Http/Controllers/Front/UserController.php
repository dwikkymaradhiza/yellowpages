<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Directory;
use App\Models\DirectoryCategory;
use App\Models\Package;
use App\Models\Package_order;
use App\Models\PaymentConfirmation;
use Auth;
use App\Models\City;
use App\Models\Province;
use Mail;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    const THUMB_WIDTH = 260;
    const THUMB_HEIGHT = 200;

    protected $redirectTo = '/dashboard';

    use AuthenticatesUsers;

    protected function redirectTo()
    {
        if(session('claim_id', 0)) {
            $directory = Directory::find(session('claim_id'));
            return '/claim/' . $directory->slug;
        } else {
            return $this->redirectTo;
        }
    }

    public function showLoginForm(Request $request) {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.signin');
        }

    	return view('frontend.signin');
    }

    public function showForgotForm(Request $request) {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.forgot');
        }
        return view('frontend.forgot');
    }

    public function signup(Request $request) {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.signup');
        }

    	return view('frontend.signup');
    }

    public function signup_profile(Request $request) {
        $provinces = Province::all()->sortBy('name');

        if($request->attributes->get('mobileActive')) {
            return view('mobile.signup_profile', ['provinces' => $provinces]);
        }

    	return view('frontend.signup_profile', ['provinces' => $provinces]);
    }

    public function signup_email(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = new User;
        $user->email = $request->input('email');
        $user->password = bcrypt(microtime(true));
        $user->first_name = '';
        $user->confirm_token = sha1(time());

        $user->save();

        $link = url('konfirmasi_email', [$user->confirm_token]);
        Mail::send('emails.confirmation_email', ['link' => $link], function ($m) use($user) {
            $m->from(config('app.email_sender'), 'Yellow Pages');

            $m->to($user->email, $user->first_name)->subject('Konfirmasi Email');
        });

        return redirect()->route('signup_success');
    }

    public function signup_profile_post(Request $request) {
        if(Auth::check()) {
            $user = Auth::user();
        } else {
            $user = new User;
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
        }

        $user->password = bcrypt($request->input('password'));
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->handphone = $request->input('handphone');
        $user->province_id = $request->input('province_id');
        $user->city_id = $request->input('city_id');
        $user->address = $request->input('address');
        $user->profile_submit = 1;
        $user->confirm_token = sha1(time());

        $user->save();

        if(!Auth::check()) {
            auth()->login($user);
        }

        $request->session()->remove('social_user');

        if($request->session()->get('claim_id', 0)) {
            $directory = Directory::find(session('claim_id'));
            return redirect()->route('verify-form', $directory->slug);
        }

        if(!$user->directory_submit) {
            return redirect()->route('signup_directory');
        } else {
            return redirect('/');
        }
    }

    public function signup_directory(Request $request) {
        $categories = Category::all();
        $provinces = Province::all()->sortBy('name');

        if($request->attributes->get('mobileActive')) {
            return view('mobile.signup_directory', compact('categories', 'provinces'));
        }

    	return view('frontend.signup_directory', compact('categories', 'provinces'));
    }

    public function signup_directory_post(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/\s+[^.!?]*/',
        ], ['name.regex' => 'Format nama bisnis tidak valid.']);

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $post = $request->all();
        list($post['lat'],$post['lng']) = explode(';', $post['latlng']);
        $post['slug'] = str_slug($post['name'], '-');

        $file = $request->file('image');
        $fileName = '';

        if(!empty($file)) {
            $validator = Validator::make($request->all(), [
                'image' => 'mimes:jpeg,jpg,png|max:2000'
            ]);

            if ($validator->fails()) {
                return redirect()->route('signup_directory')
                    ->withErrors($validator)
                    ->withInput();
            }

            $destinationPath = 'uploads/directories/';
            $fileName = time() . '.' . $request->image->extension();

            //Create thumb
            $thumbName = "thumb_" . $fileName;
            Image::make($file->getRealPath())->fit(self::THUMB_WIDTH, self::THUMB_HEIGHT)->save($destinationPath . $thumbName);

            $file->move($destinationPath, $fileName);
        }

        $dir = Directory::create([
            'name' => $post['name'],
            'phone_1' => $post['phone_1'],
            'logo' => $fileName,
            'description_lid' => $post['description'],
            'address_1' => $post['address_1'],
            'latitude' => $post['lat'],
            'longitude' => $post['lng'],
            'slug' => $post['slug'],
            'user_id' => Auth::user()->id,
            'city_id' => $post['city_id']
        ]);

        $dir_cats = [];
        foreach($post['categories'] as $cat) {
            $dir_cats[] = ['directory_id' => $dir->id, 'category_id' => (int) $cat];
        }

        DirectoryCategory::insert($dir_cats);

        Auth::user()->directory_submit = 1;
        Auth::user()->save();

        session(['wizard_dir_slug' => $post['slug'], 'wizard_dir_id' => $dir->id]);
        return redirect()->route('signup_product');
    }

    public function signup_product(Request $request) {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.signup_product');
        }

    	return view('frontend.signup_product');
    }

    public function signup_product_post(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/\s+[^.!?]*/',
        ], ['name.regex' => 'Format nama produk tidak valid.']);

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $file = $request->file('image');
        $fileName = '';

        if(!empty($file)) {
            $validator = Validator::make($request->all(), [
                'image' => 'mimes:jpeg,jpg,png|max:2000'
            ]);

            if ($validator->fails()) {
                return redirect()->route('signup_product')
                    ->withErrors($validator)
                    ->withInput();
            }

            $destinationPath = 'uploads/products/';
            $fileName = time() . '.' . $request->image->extension();

            //Create thumb
            $thumbName = "thumb_" . $fileName;
            Image::make($file->getRealPath())->fit(self::THUMB_WIDTH, self::THUMB_HEIGHT)->save($destinationPath . $thumbName);

            $file->move($destinationPath, $fileName);
        }

        Product::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'stock' => ($request->input('stock') ? $request->input('stock') : 0),
            'picture' => $fileName,
            'directory_id' => $request->input('directory_id'),
            'published' => 1,
            'slug' => str_slug($request->input('name'), '-')
        ]);

        return redirect()->route('signup_done');
        
    }

    public function signup_done(Request $request) {
        $user = Auth::user();

        Mail::send('emails.wellcome', [], function ($m) use($user) {
            $m->from(config('app.email_sender'), 'Yellow Pages');
            $m->to($user->email, $user->first_name)->subject('Selamat datang di YellowPages');
        });

        if($request->attributes->get('mobileActive')){
            return view('mobile.signup_done');
        }
        return view('frontend.signup_done');
    }

    public function ajaxCheckEmail(Request $request) {
        $email = $request->input('email');
        $must_exists = $request->input('must_exists', false);

        if(Auth::check()) {
            if($email == Auth::user()->email) {
                return 'true';
            }
        }
        
        $user = User::where('email', $email)->get()->count();

        if($must_exists) {
            if($user) {
                return 'true';
            } else {
                return 'false';
            }
        } else {
            if(!$user) {
                return 'true';
            } else {
                return 'false';
            }
        }
        
    }

    public function signup_success(Request $request) {
        if($request->attributes->get('mobileActive')){
            return view('mobile.signup_success');
        }
    	return view('frontend.signup_success');
    }
    
    public function profile(Request $request) {
        $user = Auth::user();
        $province_all = Province::all();
        $city_all = City::where('province_id','=', $user->province_id)->get();
        $package_all = Package::all();
        $package_order = Package_order::where('user_id',$user->id)
                ->join('packages as P','P.id','package_order.package_id')
                ->where('package_order.status','1')
                ->orderby('package_order.id','desc')->first();
        if($request->attributes->get('mobileActive')) {
            return view('mobile.setting.index',compact('user','province_all','city_all','package_all','package_order'));
        }
        return view('frontend.dashboard.setting',compact('user','province_all','city_all','package_all','package_order'));
    }
    
    public function editprofile(Request $request) {
        $user_id = $request->user_id;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $handphone = $request->handphone;
        $email = $request->email;
        $province = $request->province;
        $city = $request->city;
        $address = $request->address;
        
        $data_user = [
            'user_id' => $user_id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'handphone' => $handphone,
            'email' => $email,
            'province' => $province,
            'city' => $city,
            'address' => $address,
        ];

        $user = new User;
        $user->updateUser($data_user);
        
        return redirect()->route('dashboard-setting-index');
    }

    public function thanks(){        
        return view('frontend.dashboard.thanks');
    }   
    
    public function confirmation(){        
        return view('frontend.dashboard.confirmation');
    }    
    
    public function paymentConfirmation(Request $request){
                    
        $file = $request->file('image');
        $fileName = '';
            
        if(!empty($file)) {
            $validator = Validator::make($request->all(), [
                'image' => 'mimes:jpeg,jpg,png|max:2000'
            ]);

            if ($validator->fails()) {
                return redirect()->route('payment-confirmation')
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $destinationPath = 'uploads/payments/';
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
            $file->move($destinationPath, $fileName);
            }
        $payment_confirmation = new PaymentConfirmation;
        
        $array = [
            'nomor_order' => $request->nomor_order,
            'bank_sender' => $request->bank_sender,
            'name' => $request->name,
            'nominal' => $request->nominal,
            'bank_recipient' => $request->bank_recipient,
            'file_name' => $fileName
        ];
        
        $payment_confirmation->insert($array);
        $this->sendPaymentConfirmation();
        return redirect()->route('dashboard-setting-index')->with('alert-success', trans('messages.success.payment'));      
        
    }
    
    public function activateUser($token) {
        $user = User::where('confirm_token', $token)->get();
        
        if(!$user->count()) {
            return redirect('/');
        }

        $user = $user->first();

        $user->confirm_submit = 1;
        $user->save();
        auth()->login($user);

        return redirect()->route('signup_profile');
    }
    
    public function membershipUpdate(Request $request) {
        $package_order = new Package_order;
        $order_id = $package_order->updateMembership($request->all());
        $this->sendMemberNotification($request->all(), $order_id);
        $response = [
            'message' => 'success',
            'order_id' => $order_id
        ];
        
        return json_encode($response);
    }
    
    public function sendMemberNotification($array, $order_id) {
        Mail::send('emails.membership', ['name'=>$array['name'], 'price' => $array['price'], 'package_order_id' => $order_id], function ($m) {
            $user = Auth::user();
            $m->from(config('app.email_sender'), 'Yellow Pages');
            $m->to($user->email,$user->name)->subject('Membership');
        });
        
    }
    
    public function sendPaymentConfirmation() {
        Mail::send('emails.confirmation_payment', [], function ($m) {
            $user = Auth::user();
            $m->from(config('app.email_sender'), 'Yellow Pages');
            $m->to($user->email,$user->name)->subject('Confirmation Payment');
        });
        
    }

    public function claim(Request $request, $slug) {
        $directory = Directory::where('slug', $slug)->firstOrFail();
        $request->session()->set('claim_id', $directory->id);

        return redirect()->route('verify-form', $slug);
    }
}
