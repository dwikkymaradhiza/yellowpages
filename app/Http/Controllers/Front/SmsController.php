<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Auth;
use App\Models\SMSBlastNumber;
use Redirect;
use App\Models\Package_order;
use App\Models\Package;
use App\Models\User;
use App\Models\DirectoryInfo;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use GuzzleHttp\Client;
use Piwik;

class SMSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms');
        }else{
            return view('frontend.dashboard.sms');
        }
    }

    public function blast(Request $request)
    {
        $user = Auth::user();
        $uuid = strtoupper(Uuid::uuid1()->toString());

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_blast')
                ->with('user', $user)
                ->with('uuid', $uuid);
        }else{
            return view('frontend.dashboard.sms_blast')
                ->with('user', $user)
                ->with('uuid', $uuid);
        }
    }

    public function phoneNumberStore(Request $request)
    {
        $user = Auth::user();

        // set post fields
        if(!empty($request->file_upload))
        {
            $post = [
                'phonebook' => $request->file_upload,
                'uuid' => $request->uuid,
                'customer_id' => $user->id,
                'customer_name' => $user->first_name." ".$user->last_name
            ];
        }
        else
        {
            $post = [
                'phonetext' => $request->numbers,
                'uuid' => $request->uuid,
                'customer_id' => $user->id,
                'customer_name' => $user->first_name." ".$user->last_name
            ];
        }

        $ch = curl_init('http://104.199.226.125/api/smsblast/phone_book');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        // execute!
        $response = curl_exec($ch);

        // close the connection, release resources used
        curl_close($ch);

        $total_phone = json_decode($response)->data->total_phone;

        return $total_phone;
    }

    public function blastSetting(Request $request)
    {
        $user = Auth::user();

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_blast_setting')
                ->with('user', $user);
        }else{
            $form_title = "Sender ID";

            $ch = curl_init();  

            curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smsblast/sender_id");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            //  curl_setopt($ch,CURLOPT_HEADER, false); 

            $output=curl_exec($ch);

            curl_close($ch);
            $senders = json_decode($output);
            
            $packages = Package::where('type','=','SMS')->get();
       
            return view('frontend.dashboard.sms_blast_setting')
                ->with('user', $user)
                ->with('senders', $senders)
                ->with('packages', $packages);
            
        }
    }

    public function createNewSMSPackage()
    {
        $package = new SMSPackage;
        $package->user_id = Auth::user()->id;
        $package->type = 'BLAST';
        $package->save();

        return $package;
    }

    public function content(Request $request)
    {
        $form_title = "Isi SMS";

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_blast_content')
                ->with('form_title', $form_title);
        }
    }

    public function contentStore(Request $request)
    {
        $package_id = SMSPackage::getPackage()->id;

        $package = SMSPackage::find($package_id);
        $package->content = $request->input('content');
        $package->save();

        if($request->attributes->get('mobileActive')) {
            return redirect()->route('dashboard-sms-blast-setting');
        }
    }

    public function quantity(Request $request)
    {
        $form_title = "Kuantitas SMS Blast";

        $sms_price = Package::getSMSPrice();
        $packages = Package::getSMSPackage();

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_blast_quantity')
                ->with('form_title', $form_title)
                ->with('sms_price', $sms_price)
                ->with('packages', $packages);
        }
    }

    public function schedule(Request $request)
    {
        $form_title = "Jadwal Blast";

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_blast_schedule')
                ->with('form_title', $form_title);
        }
    }

    public function sender(Request $request)
    {
        $form_title = "Sender ID";

        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smsblast/sender_id");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //  curl_setopt($ch,CURLOPT_HEADER, false); 
     
        $output=curl_exec($ch);
     
        curl_close($ch);
        $senders = json_decode($output);

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_blast_sender')
                ->with('form_title', $form_title)
                ->with('senders', $senders);
        }else{
            return $senders;
        }
    }

    public function senderTargeted(Request $request)
    {
        $form_title = "Sender ID";

        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/sender_id?client_id=2");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //  curl_setopt($ch,CURLOPT_HEADER, false); 
     
        $output=curl_exec($ch);
     
        curl_close($ch);
        $senders = json_decode($output)->data;

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_targeted_sender')
                ->with('form_title', $form_title)
                ->with('senders', $senders);
        }
    }

    public function confirmation(Request $request)
    {
        $user = Auth::user();

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_blast_confirmation')
                ->with('user', $user);
        }else{
            $saldo = User::where('id','=',Auth::user()->id)->pluck('user_balance')->first();
            return view('frontend.dashboard.sms_blast_confirmation')
                ->with('user', $user)
                ->with('saldo', $saldo);    
        }
    }

    public function createCampaign(Request $request)
    {        
        // set post fields
        $post = [
            'uuid' => $request->data['uuid'],
            'content' => $request->data['content'],
            'quantity' => $request->data['quantity'],
            'period' => [
                'start' => $request->data['period']['start'],
                'end' => $request->data['period']['end'],
            ],
            'sender_id' => [
                'id' => $request->data['sender_id']['id'],
                'is_new' => 0
            ]
        ];

        $post_json = json_encode($post);

        $ch = curl_init('http://104.199.226.125/api/smsblast/create_campaign');


        curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_json );


        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);

        if(json_decode($result)->result == "success")
        {
            $user = User::find(Auth::user()->id);
            $user->user_balance -= $request->price;
            $user->save();

            return $result;
        }
    }

    public function targetedCreateCampaign(Request $request)
    {        
        // set post fields
        $post = [
            'customer_id' => $request->data['customer_id'],
            'customer_name' => $request->data['customer_name'],
            'uuid' => $request->data['uuid'],
            'content' => $request->data['content'],
            'quantity' => $request->data['quantity'],
            'period' => [
                'start' => $request->data['period']['start'],
                'end' => $request->data['period']['end'],
            ],
            'sender_id' => [
                'id' => $request->data['sender_id']['id'],
                'is_new' => 0
            ],
            'client_id' => $request->data['client_id'],
            'type' => $request->data['type'],
            'profiles' => $request->data['profiles'],
            'locations' => $request->data['locations'],
            'providers' => $request->data['providers'],
            'call_history' => [
                'days' => [],
                'start_time' => [],
                'end_time' => []
            ]
        ];

        $post_json = json_encode($post);

        $ch = curl_init('http://104.199.226.125/api/smssegmented/create_campaign');


        curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_json );


        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);

        if(json_decode($result)->result == "success")
        {
            $user = User::find(Auth::user()->id);
            $user->user_balance -= $request->price;
            $user->save();

            return $result;
        }
    }

    public function callHistoryCreateCampaign(Request $request)
    {        
        // set post fields
        $post = [
            'customer_id' => $request->data['customer_id'],
            'customer_name' => $request->data['customer_name'],
            'uuid' => $request->data['uuid'],
            'content' => $request->data['content'],
            'quantity' => $request->data['quantity'],
            'period' => [
                'start' => $request->data['period']['start'],
                'end' => $request->data['period']['end'],
            ],
            'sender_id' => [
                'id' => $request->data['sender_id']['id'],
                'is_new' => 0
            ],
            'client_id' => $request->data['client_id'],
            'type' => $request->data['type'],
            'profiles' => $request->data['profiles'],
            'locations' => $request->data['locations'],
            'providers' => $request->data['providers'],
            'patterns' => $request->data['patterns'],
            'call_history' => []
        ];

        $post_json = json_encode($post);

        $ch = curl_init('http://104.199.226.125/api/smssegmented/create_campaign');


        curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_json );


        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);

        if(json_decode($result)->result == "success")
        {
            $user = User::find(Auth::user()->id);
            $user->user_balance -= $request->price;
            $user->save();

            return $result;
        }
    }

    public function saveOrder(Request $request)
    {
        $packages = $request->data;
        $user = Auth::user();

        $order_id = date("Ym").rand('1000','9999');

        if(!empty($packages))
        {
            foreach ($packages as $package) 
            {
                $order = new Package_order;
                $order->order_id = $order_id;
                $order->user_id = $user->id;
                $order->package_id = $package['id'];
                $order->quantity = $package['quantity'];
                $order->payment_id = 1; // saldo
                $order->price = Package::getPrice($package['id']);
                $order->status = $request->status;
                $order->save();
            }
        }
        else
        {
            $order = new Package_order;
            $order->order_id = $order_id;
            $order->user_id = $user->id;
            $order->package_id = Package::getSMSPriceID();
            $order->quantity = $request->count_sms;
            $order->payment_id = 1; // saldo
            $order->price = Package::getPrice(Package::getSMSPriceID()) * $request->count_sms;
            $order->status = $request->status;
            $order->save();
        }
        
        return $order_id;
    }

    public function thankyou(Request $request)
    {
        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_blast_thankyou');
        }else{
            return view('frontend.dashboard.sponsor_thanks');
        }
    }

    public function targeted(Request $request)
    {
        $user = Auth::user();
        $uuid = strtoupper(Uuid::uuid1()->toString());

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_targeted')
                ->with('user', $user)
                ->with('uuid', $uuid);
        } else {
            // Interest
            $ch = curl_init();  
 
            curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/segment");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

            $output=curl_exec($ch);

            curl_close($ch);
            $interests = json_decode($output)->data;
            
            // Location
            $ch = curl_init();  

            curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/location");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

            $output=curl_exec($ch);

            curl_close($ch);
            $locations = json_decode($output)->data;
            
            // Provider
            $ch = curl_init();  
 
            curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/provider");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

            $output=curl_exec($ch);

            curl_close($ch);
            $operators = json_decode($output)->data;
            
            return view('frontend.dashboard.sms_targeted')
                ->with('user', $user)
                ->with('uuid', $uuid)
                ->with('interests', json_encode($interests))
                ->with('operators', $operators)
                ->with('locations', $locations);
        }
    }
    
    public function interest(Request $request)
    {
        $form_title = "Tambahkan Minat Pembaca";

        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/segment");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //  curl_setopt($ch,CURLOPT_HEADER, false); 
     
        $output=curl_exec($ch);
     
        curl_close($ch);
        $interests = json_decode($output)->data;

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_targeted_interest')
                ->with('form_title', $form_title)
                ->with('interests', $interests);
        }
    }

    public function callHistoryInterest(Request $request)
    {
        $form_title = "Tambahkan Minat Pembaca";

        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/interest");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //  curl_setopt($ch,CURLOPT_HEADER, false); 
     
        $output=curl_exec($ch);
     
        curl_close($ch);
        $interests = json_decode($output)->data;

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_targeted_interest')
                ->with('form_title', $form_title)
                ->with('interests', $interests);
        }
    }

    public function notInterest(Request $request)
    {
        $form_title = "Pengecualian Minat Pembaca";
        
        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/segment?exclude=1,2,3");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //  curl_setopt($ch,CURLOPT_HEADER, false); 
     
        $output=curl_exec($ch);
     
        curl_close($ch);
        $interests = json_decode($output)->data;

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_targeted_not_interest')
                ->with('form_title', $form_title)
                ->with('interests', $interests);
        }
    }

    public function location(Request $request)
    {
        $form_title = "Lokasi";
        
        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/location");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //  curl_setopt($ch,CURLOPT_HEADER, false); 
     
        $output=curl_exec($ch);
     
        curl_close($ch);
        $locations = json_decode($output)->data;

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_targeted_location')
                ->with('form_title', $form_title)
                ->with('locations', $locations);
        }
    }

    public function operator(Request $request)
    {
        $form_title = "Lokasi";
        
        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/provider");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //  curl_setopt($ch,CURLOPT_HEADER, false); 
     
        $output=curl_exec($ch);
     
        curl_close($ch);
        $operators = json_decode($output)->data;

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_targeted_operator')
                ->with('form_title', $form_title)
                ->with('operators', $operators);
        }
    }

    public function callHistory(Request $request)
    {
        $user = Auth::user();
        $uuid = strtoupper(Uuid::uuid1()->toString());

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_call_history')
                ->with('user', $user)
                ->with('uuid', $uuid);
        }
    }

    public function callHistoryReceiver(Request $request)
    {
        $form_title = "Call History Penerima";

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_call_history_receiver')
                ->with('form_title', $form_title);
        }
    }

    public function callHistoryReceiverDays(Request $request)
    {
        $form_title = "Call History Hari";

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_call_history_receiver_days')
                ->with('form_title', $form_title);
        }
    }

    public function callHistoryPattern(Request $request)
    {
        $form_title = "Call History Pattern";
        
        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL, "http://104.199.226.125/api/smssegmented/pattern");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //  curl_setopt($ch,CURLOPT_HEADER, false); 
     
        $output=curl_exec($ch);
     
        curl_close($ch);
        $patterns = json_decode($output);

        if($request->attributes->get('mobileActive')) {
            return view('mobile.dashboard.sms_call_history_pattern')
                ->with('form_title', $form_title)
                ->with('patterns', $patterns);
        }
    }

    public function piwik()
    {
        $type = 'bisnis';                                       // bisnis or produk
        $ads = 'impression';                                    // impression or click
        $url = 'http://127.0.0.1:8001/'.$type.'/'.$ads.'/';
        $slug = $url.'pecel-lele';                              // slug
        return Piwik::custom('Actions.getPageUrl', [ 'pageUrl' => $slug], true, true, null);
    }

    public function badWordValidation(Request $request)
    {
        $bad_words = DirectoryInfo::smsBadWordValidation($request->get('content'));

        return json_encode($bad_words);
    }
    

}
