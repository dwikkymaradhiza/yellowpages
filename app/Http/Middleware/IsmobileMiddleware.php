<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;

class IsmobileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $agent = new Agent;
        $request->attributes->add(['mobileActive' => $agent->isMobile() || $agent->isTablet()]);

        return $next($request);
    }
}
