<?php

namespace App\Console\Commands;

use App\Models\Package_order;
use App\Models\Product;
use App\Models\Directory;
use Illuminate\Console\Command;

class SponsorWorker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'worker:sponsored';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get sponsor package order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->comment("Get package order");
            $getPackageOrder = $this->getPackageOrder();

            $this->info("{$getPackageOrder['total_product']} was updated to product counter table and {$getPackageOrder['total_directory']} was updated to directory counter table.");
        } catch (\Illuminate\Database\QueryException $e ) {
            $this->error($e->getMessage());
        }
    }
    
    public function getPackageOrder(){
        $package_order = new Package_order;
        $pending_order = $package_order->getPendingSponsorPackage();
        
        $result = [
            'total_product' => 0,
            'product_id' => [],
            'total_directory' => 0,
            'directory_id' => [],
        ];
        
        if(count($pending_order)>0){
            foreach ($pending_order as $pending_order_detail){
                $arr_directory = explode(',',$pending_order_detail->directories_id);
                $total_directory = count($arr_directory);
                $result['total_directory'] += $total_directory;
                array_push($result['directory_id'], $arr_directory);
                                
                $arr_product = explode(',',$pending_order_detail->products_id);
                $total_product = count($arr_product);
                $result['total_product'] += $total_product;
                array_push($result['product_id'], $arr_product);
                
                $kuota = ceil($pending_order_detail->description/($total_directory + $total_product));
                if($total_directory>0){
                    $this->updateDirectoryCounter($arr_directory,$kuota);
                }
    
                if($total_product>0){
                    $this->updateProductCounter($arr_product,$kuota);
                }
                
                $package_order->updateSponsorPackage($pending_order_detail->id,'2');
            }

            
            return $result;
        }else{
            return $result;
        }        
    }
    
    public function updateDirectoryCounter($dir_array, $kuota){
        foreach($dir_array as $dir){
            $directory = new Directory;
            $directory_counter_detail = $directory->updateDirectoryCounter($dir, $kuota);
        }
        
    }
    
    public function updateProductCounter($dir_array, $kuota){
        foreach($dir_array as $dir){
            $product = new Product;
            $product_counter_detail = $product->updateProductCounter($dir, $kuota);
        }
        
    }
}
