<?php

namespace App\Console\Commands;

use App\Models\Affiliate;
use App\Models\AffiliateWebsite;
use Illuminate\Console\Command;

class ArticleFeeder extends Command {
    
    protected $signature = 'article:update-feed';
    
    protected $description = 'Update affiliate article from RSS feed';
            
    public function handle()
    {
        try {
            $this->comment("Inserting RSS feed articles...");
            $update = $this->updateArticle();

            $this->info("{$update['success']} article(s) was inserted to affiliate_article table and {$update['failed']} article(s) was failed.");
        } catch (\Illuminate\Database\QueryException $e ) {
            $this->error($e->getMessage());
        }
    }
    
    public function updateArticle() {
        $feeds = AffiliateWebsite::pluck('rss_url')->toArray();
        $count = 0;
        $failed = 0;
        
        $items = \Feeds::make($feeds)->get_items();
        
        foreach ($items as $item) {
            $exist = Affiliate::where('title', $item->get_title())->first();
            
            if($exist !== null) {
                $failed++;
                $this->comment("{$failed} article failed to insert, article is exist");
                
                continue;
            }
            
            $affiliate = new Affiliate;
            $affiliate->title = $item->get_title();
            $affiliate->headline = str_limit($item->get_description(), 140);
            $affiliate->desc = $item->get_content();
            $affiliate->full_url = $item->get_link();
            $affiliate->image_url = '';
            $affiliate->tag = '';
            $affiliate->created_at = $item->get_date('Y-m-d H:i:s');
            $stat = $affiliate->save();

            if(!$stat) {
                $failed++;
                $this->comment("{$failed} article failed to insert, query error");
                
                continue;
            } 
            
            $count++;
            $this->comment("{$count} article is inserted");
        }
        
        return ['success' => $count, 'failed' => $failed];
    }
}