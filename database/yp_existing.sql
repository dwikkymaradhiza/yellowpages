-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: inyp_yellowpages
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `article_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `career`
--

DROP TABLE IF EXISTS `career`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `career` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `body` text CHARACTER SET utf8,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `slug_2` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `career`
--

LOCK TABLES `career` WRITE;
/*!40000 ALTER TABLE `career` DISABLE KEYS */;
/*!40000 ALTER TABLE `career` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `career_viewer`
--

DROP TABLE IF EXISTS `career_viewer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `career_viewer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `create_time` datetime NOT NULL,
  `career_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `career_id` (`career_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `career_viewer`
--

LOCK TABLES `career_viewer` WRITE;
/*!40000 ALTER TABLE `career_viewer` DISABLE KEYS */;
/*!40000 ALTER TABLE `career_viewer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_len` varchar(255) NOT NULL,
  `name_lid` varchar(255) NOT NULL,
  `description_len` text,
  `description_lid` text NOT NULL,
  `classification` varchar(45) DEFAULT NULL,
  `popular` tinyint(1) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `slug` (`slug`),
  KEY `weight` (`weight`),
  KEY `classification` (`classification`),
  CONSTRAINT `category_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_old`
--

DROP TABLE IF EXISTS `category_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_old`
--

LOCK TABLES `category_old` WRITE;
/*!40000 ALTER TABLE `category_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dty` varchar(45) DEFAULT NULL,
  `area_code` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` text,
  `slug` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `province_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `fk_city_province` (`province_id`),
  KEY `area_code` (`area_code`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `province` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_old`
--

DROP TABLE IF EXISTS `city_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_old`
--

LOCK TABLES `city_old` WRITE;
/*!40000 ALTER TABLE `city_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `city_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claim_status`
--

DROP TABLE IF EXISTS `claim_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claim_status`
--

LOCK TABLES `claim_status` WRITE;
/*!40000 ALTER TABLE `claim_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `claim_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idnas` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description_len` text,
  `description_lid` text,
  `logo` varchar(45) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_bak`
--

DROP TABLE IF EXISTS `company_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nam` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `region1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `region2` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `areacode` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_bak`
--

LOCK TABLES `company_bak` WRITE;
/*!40000 ALTER TABLE `company_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dir`
--

DROP TABLE IF EXISTS `dir`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dir` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `popular_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `info` text CHARACTER SET utf8,
  `keywords` text CHARACTER SET utf8,
  `alcon` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid_key` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_1` text CHARACTER SET utf8,
  `address_2` text CHARACTER SET utf8,
  `address_3` text CHARACTER SET utf8,
  `kelurahan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kecamatan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kabupaten` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kawasan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `building` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `phone_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `webshot` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `payment` text CHARACTER SET utf8,
  `op_day_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_day_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `anniversary` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `facility` text CHARACTER SET utf8,
  `ecatalog` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pl` enum('P1','P2','P3') CHARACTER SET utf8 DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `dtm_index2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text CHARACTER SET utf8,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL,
  `noindex` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dir`
--

LOCK TABLES `dir` WRITE;
/*!40000 ALTER TABLE `dir` DISABLE KEYS */;
/*!40000 ALTER TABLE `dir` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dir_bak`
--

DROP TABLE IF EXISTS `dir_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dir_bak` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `popular_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `info` text CHARACTER SET utf8,
  `keywords` text CHARACTER SET utf8,
  `alcon` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid_key` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_1` text CHARACTER SET utf8,
  `address_2` text CHARACTER SET utf8,
  `address_3` text CHARACTER SET utf8,
  `kelurahan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kecamatan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kabupaten` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kawasan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `building` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `phone_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `webshot` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `payment` text CHARACTER SET utf8,
  `op_day_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_day_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `anniversary` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `facility` text CHARACTER SET utf8,
  `ecatalog` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pl` enum('P1','P2','P3') CHARACTER SET utf8 DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `dtm_index2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text CHARACTER SET utf8,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dir_bak`
--

LOCK TABLES `dir_bak` WRITE;
/*!40000 ALTER TABLE `dir_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `dir_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dir_bakectory`
--

DROP TABLE IF EXISTS `dir_bakectory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dir_bakectory` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `popular_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `info` text CHARACTER SET utf8,
  `keywords` text CHARACTER SET utf8,
  `alcon` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid_key` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_1` text CHARACTER SET utf8,
  `address_2` text CHARACTER SET utf8,
  `address_3` text CHARACTER SET utf8,
  `kelurahan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kecamatan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kabupaten` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kawasan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `building` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `phone_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `webshot` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `payment` text CHARACTER SET utf8,
  `op_day_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_day_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `anniversary` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `facility` text CHARACTER SET utf8,
  `ecatalog` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pl` enum('P1','P2','P3') CHARACTER SET utf8 DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `dtm_index2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text CHARACTER SET utf8,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dir_bakectory`
--

LOCK TABLES `dir_bakectory` WRITE;
/*!40000 ALTER TABLE `dir_bakectory` DISABLE KEYS */;
/*!40000 ALTER TABLE `dir_bakectory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dir_hits`
--

DROP TABLE IF EXISTS `dir_hits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dir_hits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) NOT NULL,
  `hits` int(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `directory_id` (`directory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dir_hits`
--

LOCK TABLES `dir_hits` WRITE;
/*!40000 ALTER TABLE `dir_hits` DISABLE KEYS */;
/*!40000 ALTER TABLE `dir_hits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory`
--

DROP TABLE IF EXISTS `directory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `popular_name` varchar(255) DEFAULT NULL,
  `description_len` text,
  `description_lid` text,
  `info` text,
  `keywords` text,
  `alcon` varchar(45) DEFAULT NULL,
  `masterid` varchar(45) DEFAULT NULL,
  `masterid_key` varchar(50) DEFAULT NULL,
  `barcode` varchar(45) DEFAULT NULL,
  `address_name` varchar(45) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `address_3` text,
  `kelurahan` varchar(45) DEFAULT NULL,
  `kecamatan` varchar(45) DEFAULT NULL,
  `kabupaten` varchar(45) DEFAULT NULL,
  `kawasan` varchar(45) DEFAULT NULL,
  `building` varchar(45) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) DEFAULT NULL,
  `phone_1` varchar(45) DEFAULT NULL,
  `phone_2` varchar(45) DEFAULT NULL,
  `phone_3` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `webshot` varchar(45) DEFAULT NULL,
  `payment` text,
  `op_day_1` varchar(45) DEFAULT NULL,
  `op_time_1` varchar(45) DEFAULT NULL,
  `op_day_2` varchar(45) DEFAULT NULL,
  `op_time_2` varchar(45) DEFAULT NULL,
  `anniversary` varchar(45) DEFAULT NULL,
  `facility` text,
  `ecatalog` varchar(255) NOT NULL,
  `pl` enum('P1','P2','P3') DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) DEFAULT NULL,
  `dtm_index2` varchar(100) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL,
  `hits` int(255) NOT NULL DEFAULT '0',
  `type` varchar(50) DEFAULT 'free',
  `service_code` varchar(3) DEFAULT '000',
  PRIMARY KEY (`id`),
  KEY `fk_directory_company1` (`company_id`),
  KEY `fk_directory_city1` (`city_id`),
  KEY `slug` (`slug`),
  KEY `masterid_key` (`masterid_key`),
  KEY `dtm_index` (`dtm_index`),
  KEY `update_time` (`update_time`),
  KEY `alcon` (`alcon`),
  KEY `dtm_index2` (`dtm_index2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory`
--

LOCK TABLES `directory` WRITE;
/*!40000 ALTER TABLE `directory` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_bak`
--

DROP TABLE IF EXISTS `directory_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `popular_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `info` text CHARACTER SET utf8,
  `keywords` text CHARACTER SET utf8,
  `alcon` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid_key` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_1` text CHARACTER SET utf8,
  `address_2` text CHARACTER SET utf8,
  `address_3` text CHARACTER SET utf8,
  `kelurahan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kecamatan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kabupaten` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kawasan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `building` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `phone_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `webshot` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `payment` text CHARACTER SET utf8,
  `op_day_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_day_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `anniversary` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `facility` text CHARACTER SET utf8,
  `ecatalog` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pl` enum('P1','P2','P3') CHARACTER SET utf8 DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `dtm_index2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text CHARACTER SET utf8,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dtm_index` (`dtm_index`),
  KEY `dtm_index2` (`dtm_index2`),
  KEY `update_time` (`update_time`),
  KEY `slug` (`slug`),
  KEY `slug_2` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=895670 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_bak`
--

LOCK TABLES `directory_bak` WRITE;
/*!40000 ALTER TABLE `directory_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_category`
--

DROP TABLE IF EXISTS `directory_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_directory_category_directory1` (`directory_id`),
  KEY `fk_directory_category_category1` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_category`
--

LOCK TABLES `directory_category` WRITE;
/*!40000 ALTER TABLE `directory_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_category_bak`
--

DROP TABLE IF EXISTS `directory_category_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_category_bak` (
  `id` int(11) NOT NULL DEFAULT '0',
  `directory_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_category_bak`
--

LOCK TABLES `directory_category_bak` WRITE;
/*!40000 ALTER TABLE `directory_category_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_category_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_claim`
--

DROP TABLE IF EXISTS `directory_claim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_claim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `claim_status_id` int(11) NOT NULL,
  `claim_name` varchar(45) DEFAULT NULL,
  `claim_position` varchar(45) DEFAULT NULL,
  `claim_email` varchar(45) DEFAULT NULL,
  `claim_phone` varchar(45) DEFAULT NULL,
  `claim_remark` text,
  `claim_document` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `verified_by` varchar(55) NOT NULL,
  `verify_desc` text NOT NULL,
  `noindex` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `directory_id` (`directory_id`),
  KEY `user_id` (`user_id`),
  KEY `claim_status_id` (`claim_status_id`),
  CONSTRAINT `directory_claim_ibfk_2` FOREIGN KEY (`claim_status_id`) REFERENCES `claim_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_claim`
--

LOCK TABLES `directory_claim` WRITE;
/*!40000 ALTER TABLE `directory_claim` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_claim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_claim_history`
--

DROP TABLE IF EXISTS `directory_claim_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_claim_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_claim_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `claim_status_id` int(11) NOT NULL,
  `remark` text CHARACTER SET utf8,
  `document` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `directory_claim_id` (`directory_claim_id`),
  KEY `user_id` (`user_id`),
  KEY `claim_status_id` (`claim_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_claim_history`
--

LOCK TABLES `directory_claim_history` WRITE;
/*!40000 ALTER TABLE `directory_claim_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_claim_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_extended`
--

DROP TABLE IF EXISTS `directory_extended`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_extended` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) NOT NULL,
  `masterid_key` varchar(50) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `ext_value` text NOT NULL,
  `sites` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `is_indexed` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `directory_id` (`directory_id`),
  KEY `name` (`name`),
  KEY `sites` (`sites`),
  KEY `slug` (`slug`),
  KEY `masterid_key` (`masterid_key`),
  KEY `is_indexed` (`is_indexed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_extended`
--

LOCK TABLES `directory_extended` WRITE;
/*!40000 ALTER TABLE `directory_extended` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_extended` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_inquiry`
--

DROP TABLE IF EXISTS `directory_inquiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_inquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `inquiry` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_inquiry`
--

LOCK TABLES `directory_inquiry` WRITE;
/*!40000 ALTER TABLE `directory_inquiry` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_inquiry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_picture`
--

DROP TABLE IF EXISTS `directory_picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title_len` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_lid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `picture_file` varchar(255) CHARACTER SET utf8 NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `directory_id` (`directory_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `directory_picture_ibfk_1` FOREIGN KEY (`directory_id`) REFERENCES `directory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_picture`
--

LOCK TABLES `directory_picture` WRITE;
/*!40000 ALTER TABLE `directory_picture` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_picture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_product`
--

DROP TABLE IF EXISTS `directory_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `masterid` varchar(45) NOT NULL,
  `masterid_key` varchar(50) NOT NULL,
  `remarks` text,
  PRIMARY KEY (`id`),
  KEY `masterid_key` (`masterid_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_product`
--

LOCK TABLES `directory_product` WRITE;
/*!40000 ALTER TABLE `directory_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_rating`
--

DROP TABLE IF EXISTS `directory_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate` float NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `directory_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directory_id` (`directory_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_rating`
--

LOCK TABLES `directory_rating` WRITE;
/*!40000 ALTER TABLE `directory_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_submitted`
--

DROP TABLE IF EXISTS `directory_submitted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_submitted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `popular_name` varchar(255) DEFAULT NULL,
  `description_len` text,
  `description_lid` text,
  `info` text,
  `keywords` text,
  `alcon` varchar(45) DEFAULT NULL,
  `masterid` varchar(45) DEFAULT NULL,
  `masterid_key` varchar(50) DEFAULT NULL,
  `barcode` varchar(45) DEFAULT NULL,
  `address_name` varchar(45) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `address_3` text,
  `kelurahan` varchar(45) DEFAULT NULL,
  `kecamatan` varchar(45) DEFAULT NULL,
  `kabupaten` varchar(45) DEFAULT NULL,
  `kawasan` varchar(45) DEFAULT NULL,
  `building` varchar(45) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) DEFAULT NULL,
  `phone_1` varchar(45) DEFAULT NULL,
  `phone_2` varchar(45) DEFAULT NULL,
  `phone_3` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `webshot` varchar(45) DEFAULT NULL,
  `payment` text,
  `op_day_1` varchar(45) DEFAULT NULL,
  `op_time_1` varchar(45) DEFAULT NULL,
  `op_day_2` varchar(45) DEFAULT NULL,
  `op_time_2` varchar(45) DEFAULT NULL,
  `anniversary` varchar(45) DEFAULT NULL,
  `facility` text,
  `pl` enum('P1','P2','P3') DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL,
  `noindex` tinyint(1) DEFAULT '0',
  `type` varchar(50) DEFAULT 'free',
  PRIMARY KEY (`id`),
  KEY `fk_directory_company1` (`company_id`),
  KEY `fk_directory_city1` (`city_id`),
  KEY `slug` (`slug`),
  KEY `masterid_key` (`masterid_key`),
  KEY `dtm_index` (`dtm_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_submitted`
--

LOCK TABLES `directory_submitted` WRITE;
/*!40000 ALTER TABLE `directory_submitted` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_submitted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_submitted_category`
--

DROP TABLE IF EXISTS `directory_submitted_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_submitted_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_directory_category_directory1` (`directory_id`),
  KEY `fk_directory_category_category1` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_submitted_category`
--

LOCK TABLES `directory_submitted_category` WRITE;
/*!40000 ALTER TABLE `directory_submitted_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_submitted_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_tag`
--

DROP TABLE IF EXISTS `directory_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_directory_tag_directory1` (`directory_id`),
  KEY `fk_directory_tag_tag1` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_tag`
--

LOCK TABLES `directory_tag` WRITE;
/*!40000 ALTER TABLE `directory_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_video`
--

DROP TABLE IF EXISTS `directory_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title_len` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_lid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `video_file` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `directory_id` (`directory_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_video`
--

LOCK TABLES `directory_video` WRITE;
/*!40000 ALTER TABLE `directory_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `directory_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `display_ad`
--

DROP TABLE IF EXISTS `display_ad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `display_ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` text,
  `display_file` varchar(45) DEFAULT NULL,
  `display_ad_type_id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_display_ad_display_ad_type` (`display_ad_type_id`),
  KEY `fk_display_ad_directory1` (`directory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `display_ad`
--

LOCK TABLES `display_ad` WRITE;
/*!40000 ALTER TABLE `display_ad` DISABLE KEYS */;
/*!40000 ALTER TABLE `display_ad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `display_ad_45716`
--

DROP TABLE IF EXISTS `display_ad_45716`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `display_ad_45716` (
  `id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(45) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  `display_file` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `display_ad_type_id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `display_ad_45716`
--

LOCK TABLES `display_ad_45716` WRITE;
/*!40000 ALTER TABLE `display_ad_45716` DISABLE KEYS */;
/*!40000 ALTER TABLE `display_ad_45716` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `display_ad_backup`
--

DROP TABLE IF EXISTS `display_ad_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `display_ad_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` text,
  `display_file` varchar(45) DEFAULT NULL,
  `display_ad_type_id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_display_ad_display_ad_type` (`display_ad_type_id`),
  KEY `fk_display_ad_directory1` (`directory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `display_ad_backup`
--

LOCK TABLES `display_ad_backup` WRITE;
/*!40000 ALTER TABLE `display_ad_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `display_ad_backup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `display_ad_backup_2`
--

DROP TABLE IF EXISTS `display_ad_backup_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `display_ad_backup_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` text,
  `display_file` varchar(45) DEFAULT NULL,
  `display_ad_type_id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_display_ad_display_ad_type` (`display_ad_type_id`),
  KEY `fk_display_ad_directory1` (`directory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `display_ad_backup_2`
--

LOCK TABLES `display_ad_backup_2` WRITE;
/*!40000 ALTER TABLE `display_ad_backup_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `display_ad_backup_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `display_ad_type`
--

DROP TABLE IF EXISTS `display_ad_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `display_ad_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `bpt` varchar(45) NOT NULL,
  `description` text,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `display_ad_type`
--

LOCK TABLES `display_ad_type` WRITE;
/*!40000 ALTER TABLE `display_ad_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `display_ad_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `microsite_tmp`
--

DROP TABLE IF EXISTS `microsite_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `microsite_tmp` (
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subdomain` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ket` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `microsite_tmp`
--

LOCK TABLES `microsite_tmp` WRITE;
/*!40000 ALTER TABLE `microsite_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `microsite_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `microsite_tmp1`
--

DROP TABLE IF EXISTS `microsite_tmp1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `microsite_tmp1` (
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subdomain` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ket` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `microsite_tmp1`
--

LOCK TABLES `microsite_tmp1` WRITE;
/*!40000 ALTER TABLE `microsite_tmp1` DISABLE KEYS */;
/*!40000 ALTER TABLE `microsite_tmp1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `micrositetmp`
--

DROP TABLE IF EXISTS `micrositetmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `micrositetmp` (
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subdomain` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ket` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `micrositetmp`
--

LOCK TABLES `micrositetmp` WRITE;
/*!40000 ALTER TABLE `micrositetmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `micrositetmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_rating`
--

DROP TABLE IF EXISTS `product_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate` float NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directory_id` (`product_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_rating`
--

LOCK TABLES `product_rating` WRITE;
/*!40000 ALTER TABLE `product_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `province`
--

DROP TABLE IF EXISTS `province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `province`
--

LOCK TABLES `province` WRITE;
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
/*!40000 ALTER TABLE `province` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refine`
--

DROP TABLE IF EXISTS `refine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buswith` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nam` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refine`
--

LOCK TABLES `refine` WRITE;
/*!40000 ALTER TABLE `refine` DISABLE KEYS */;
/*!40000 ALTER TABLE `refine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scategory`
--

DROP TABLE IF EXISTS `scategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scategory` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scategory`
--

LOCK TABLES `scategory` WRITE;
/*!40000 ALTER TABLE `scategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `scategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search`
--

DROP TABLE IF EXISTS `search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL DEFAULT '1',
  `what` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `where` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_search` (`sid`),
  CONSTRAINT `fk_search` FOREIGN KEY (`sid`) REFERENCES `scategory` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search`
--

LOCK TABLES `search` WRITE;
/*!40000 ALTER TABLE `search` DISABLE KEYS */;
/*!40000 ALTER TABLE `search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_code`
--

DROP TABLE IF EXISTS `service_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_code` (
  `service_code` varchar(3) NOT NULL DEFAULT '000' COMMENT 'service code subscription (000 - 999)',
  `price` varchar(20) NOT NULL COMMENT 'service code price information',
  `description` text COMMENT 'service code information description',
  `timestamp` varchar(100) NOT NULL COMMENT 'service code create timestamp',
  `created_by` varchar(100) NOT NULL DEFAULT 'System' COMMENT 'service code created by',
  `updated_by` varchar(100) NOT NULL DEFAULT 'System' COMMENT 'service code update by',
  PRIMARY KEY (`service_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table service code information detail';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_code`
--

LOCK TABLES `service_code` WRITE;
/*!40000 ALTER TABLE `service_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_subscription`
--

DROP TABLE IF EXISTS `service_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alcon` varchar(45) DEFAULT NULL,
  `dtm_index` varchar(45) DEFAULT NULL,
  `service_code` varchar(3) NOT NULL DEFAULT '000',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `service_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `dtm_index` (`dtm_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_subscription`
--

LOCK TABLES `service_subscription` WRITE;
/*!40000 ALTER TABLE `service_subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `id` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `expire` int(11) NOT NULL,
  `data` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subdomain`
--

DROP TABLE IF EXISTS `subdomain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subdomain` (
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subdomain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ket` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subdomain`
--

LOCK TABLES `subdomain` WRITE;
/*!40000 ALTER TABLE `subdomain` DISABLE KEYS */;
/*!40000 ALTER TABLE `subdomain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_len` varchar(255) NOT NULL,
  `name_lid` varchar(255) NOT NULL,
  `description_len` text,
  `description_lid` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `slug_lid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `slug_lid` (`slug_lid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_activation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webinstant`
--

DROP TABLE IF EXISTS `webinstant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webinstant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(35) NOT NULL,
  `lastname` varchar(35) NOT NULL,
  `companyname` varchar(150) NOT NULL,
  `address1` varchar(250) NOT NULL,
  `city` varchar(64) NOT NULL,
  `state` varchar(64) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country` varchar(35) NOT NULL,
  `phonenumber` varchar(35) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password2` varchar(100) NOT NULL,
  `currency` varchar(5) NOT NULL,
  `user_token` varchar(250) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `isactive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webinstant`
--

LOCK TABLES `webinstant` WRITE;
/*!40000 ALTER TABLE `webinstant` DISABLE KEYS */;
/*!40000 ALTER TABLE `webinstant` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-18 18:23:14
