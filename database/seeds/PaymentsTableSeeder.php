<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            'name' => 'Saldo Yellow Pages',
            'code' => 'saldo',
            'config' => '',
            'status' => 1
        ]);
        
        DB::table('payments')->insert([
            'name' => 'Bank Transfer',
            'code' => 'bank_transfer',
            'config' => '',
            'status' => 1
        ]);
    }
}
