<?php

use Illuminate\Database\Seeder;

class DirectorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directory')->insert([
            'name' => 'Ayam Bakar Mas Mono',
            'popular_name' => 'Ayam Bakar Mas Mono',
            'description_len' => 'Ayam Bakar Mas Mono mantap',
            'keywords' => 'Ayam Bakar, Ayam Bakar Mas Mono',
            'published' => 1,
            'logo' => 'dummy-1.png',
            'address_name' => 'Jakarta Selatan',
            'ecatalog' => '',
            'slug' => 'ayam-bakar-mas-mono',
            'city_id' => 1,
        ]);
        
        DB::table('directory')->insert([
            'name' => 'Auto 2000 Tebet Saharjo',
            'popular_name' => 'Auto 2000 Tebet Saharjo',
            'description_len' => 'Auto 2000 Tebet Saharjo',
            'keywords' => 'auto 2000',
            'published' => 1,
            'logo' => 'dummy-2.png',
            'address_name' => 'Jakarta Selatan',
            'ecatalog'=>'',
            'slug' => 'auto-2000-tebet-saharjo',
            'city_id' => 1,
        ]);
        
        DB::table('directory')->insert([
            'name' => 'Ayam Bakar Mas Mono',
            'popular_name' => 'Ayam Bakar Mas Mono',
            'description_len' => 'Ayam Bakar Mas Mono mantap',
            'keywords' => 'Ayam Bakar, Ayam Bakar Mas Mono',
            'published' => 1,
            'logo' => 'dummy-3.png',
            'address_name' => 'Jakarta Selatan',
            'ecatalog'=>'',
            'slug' => 'ayam-bakar-mas-mono',
            'city_id' => 1,
        ]);
        
        DB::table('directory')->insert([
            'name' => 'Seafood 68 Santa',
            'popular_name' => 'Seafood 68 Santa',
            'description_len' => 'Seafood 68 Santa',
            'keywords' => 'Seafood 68 Santa',
            'published' => 1,
            'logo' => 'dummy-4.png',
            'address_name' => 'Jakarta Selatan',
            'ecatalog'=>'',
            'slug' => 'seafood-68-santa',
            'city_id' => 1,
        ]);
        
        DB::table('directory')->insert([
            'name' => 'Apotek K2 Slipi',
            'popular_name' => 'Apotek K2 Slipi',
            'description_len' => 'Apotek K2 Slipi',
            'keywords' => 'Apotek K2 Slipi',
            'published' => 1,
            'logo' => 'dummy-5.png',
            'address_name' => 'Jakarta Selatan',
            'ecatalog'=>'',
            'slug' => 'apotek-k2-slipi',
            'city_id' => 1,
        ]);
        
        DB::table('directory')->insert([
            'name' => 'Lorem Ipsum',
            'popular_name' => 'Lorem Ipsum',
            'description_len' => 'Lorem Ipsum',
            'keywords' => 'Lorem Ipsum',
            'published' => 1,
            'logo' => 'dummy-5.png',
            'address_name' => 'Jakarta Selatan',
            'ecatalog'=>'',
            'slug' => 'lorem-ipsum',
            'city_id' => 1,
        ]);
        
        DB::table('directory')->insert([
            'name' => 'Lorem Ipsum ABC',
            'popular_name' => 'Lorem Ipsum ABC',
            'description_len' => 'Lorem Ipsum ABC',
            'keywords' => 'Lorem Ipsum ABC',
            'published' => 1,
            'logo' => 'dummy-6.png',
            'address_name' => 'Jakarta Selatan',
            'ecatalog'=>'',
            'slug' => 'lorem-ipsum-abc',
            'city_id' => 1,
        ]);
        
        DB::table('directory')->insert([
            'name' => 'Lorem Ipsum XXX',
            'popular_name' => 'Lorem Ipsum XXX',
            'description_len' => 'Lorem Ipsum XXX',
            'keywords' => 'Lorem Ipsum XXX',
            'published' => 1,
            'logo' => 'dummy-6.png',
            'address_name' => 'Jakarta Selatan',
            'ecatalog'=>'',
            'slug' => 'lorem-ipsum-xxx',
            'city_id' => 1,
        ]);
        
        DB::table('directory')->insert([
            'name' => 'Lorem Ipsum ZZZ',
            'popular_name' => 'Lorem Ipsum ZZZ',
            'description_len' => 'Lorem Ipsum ZZZ',
            'keywords' => 'Lorem Ipsum ZZZ',
            'published' => 1,
            'logo' => 'dummy-6.png',
            'address_name' => 'Jakarta Selatan',
            'ecatalog'=>'',
            'slug' => 'lorem-ipsum-zzz',
            'city_id' => 1,
        ]);
    }
}
