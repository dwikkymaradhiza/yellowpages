<?php

use Illuminate\Database\Seeder;

class ProductCounterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_counter')->insert([
            'product_id' => 1,
            'counter_date' => '201701',
            'segment' => 'feature',
            'counter' => 1,            
        ]);
        
        DB::table('product_counter')->insert([
            'product_id' => 2,
            'counter_date' => '201701',
            'segment' => 'feature',
            'counter' => 1,            
        ]);
        
        DB::table('product_counter')->insert([
            'product_id' => 3,
            'counter_date' => '201701',
            'segment' => 'feature',
            'counter' => 1,            
        ]);
        
        DB::table('product_counter')->insert([
            'product_id' => 4,
            'counter_date' => '201701',
            'segment' => 'feature',
            'counter' => 1,            
        ]);
        
        DB::table('product_counter')->insert([
            'product_id' => 5,
            'counter_date' => '201701',
            'segment' => 'feature',
            'counter' => 1,            
        ]);
        
        DB::table('product_counter')->insert([
            'product_id' => 6,
            'counter_date' => '201701',
            'segment' => 'feature',
            'counter' => 1,            
        ]);
        
        DB::table('product_counter')->insert([
            'product_id' => 7,
            'counter_date' => '201701',
            'segment' => 'feature',
            'counter' => 1,            
        ]);
        
        DB::table('product_counter')->insert([
            'product_id' => 8,
            'counter_date' => '201701',
            'segment' => 'feature',
            'counter' => 1,            
        ]);
    }
}
