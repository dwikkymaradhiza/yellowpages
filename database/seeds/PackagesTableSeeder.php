<?php

use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            'package_name' => '10.000 x Tayangan',
            'type' => 'SPONSORED',
            'price' => 10000,
            'description' => '10000'
        ]);
        
        DB::table('packages')->insert([
            'package_name' => '25.000 x Tayangan',
            'type' => 'SPONSORED',
            'price' => 25000,
            'description' => '25000'
        ]);
        
        DB::table('packages')->insert([
            'package_name' => '50.000 x Tayangan',
            'type' => 'SPONSORED',
            'price' => 50000,
            'description' => '50000'
        ]);
                
        DB::table('packages')->insert([
            'package_name' => '100.000 x Tayangan',
            'type' => 'SPONSORED',
            'price' => 100000,
            'description' => '100000'
        ]);

        DB::table('packages')->insert([
            'package_name' => '1 SMS',
            'type' => 'SMS',
            'price' => 1,
            'description' => '1'           
        ]);

        DB::table('packages')->insert([
            'package_name' => '10.000 SMS',
            'type' => 'SMS',
            'price' => 10000,
            'description' => '10000'           
        ]);
        
        DB::table('packages')->insert([
            'package_name' => '25.000 SMS',
            'type' => 'SMS',
            'price' => 25000,
            'description' => '25000'           
        ]);
        
        DB::table('packages')->insert([
            'package_name' => '50.000 SMS',
            'type' => 'SMS',
            'price' => 50000,
            'description' => '50000'           
        ]);
        
        DB::table('packages')->insert([
            'package_name' => '100.000 SMS',
            'type' => 'SMS',
            'price' => 100000,
            'description' => '100000'           
        ]);
    }
}
