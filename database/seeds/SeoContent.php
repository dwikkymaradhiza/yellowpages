<?php

use Illuminate\Database\Seeder;

class SeoContent extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seo_text')->insert([
            'seo_text' => '<p class="paragraf-intro"><a href="#" style="color: white">www.yellowpages.co.id</a> adalah direktori bisnis terlengkap yang telah dipercaya oleh ratusan ribu bisnis yang tersebar di seluruh Indonesia. Website ini memiliki 700 ribu database dengan 3.000 klasifikasi bisnis yang selalu diperbarui secara berkala, serta dilengkapi dengan peta lokasi untuk mempermudah dalam pencarian. </p>
                            <p class="paragraf-intro">PT Metra Digital Media (MD Media) adalah <i>member of</i> Telkom Group yang merupakan subsidiary PT Telkom Metra yang fokus pada bisnis <i>Information, Media, dan Edutainment</i> (IME). MD Media resmi didirikan pada 1 Mei 2013 untuk mengembangkan portofolio bisnis Media dan Advertising yang sebelumnya telah dibangun semasa bergabung dengan anak perusahaan Telkom yang lain yaitu PT Infomedia Nusantara. </p>
                            <p class="paragraf-intro">MD Media memiliki <i>legacy business</i> yang telah dibangun selama 30 tahun dalam bisnis direktori dan media. Melalui pengelolaan data direktori yang optimal selama bertahun-tahun telah menjadi kekuatan sekaligus modal awal untuk memasuki bisnis media digital disamping berbagai upaya pengembangan yang senantiasa dilakukan untuk menjaga kualitas layanan dan menjadi terdepan di kelasnya. </p>',
            'slug' => '',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),            
        ]);
    }
}
