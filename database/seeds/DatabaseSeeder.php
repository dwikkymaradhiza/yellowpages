<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AffiliateArticleSeeder::class);
        $this->call(DirectoryCounterSeeder::class);
        $this->call(DirectoryInfoSeeder::class);
        $this->call(DirectoryProductSeeder::class);
        $this->call(DirectorySeeder::class);
        $this->call(MainBannerSeeder::class);
        $this->call(ProductCounterSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PackagesTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
    }
}
