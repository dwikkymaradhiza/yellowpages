<?php

use Illuminate\Database\Seeder;

class MainBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('main_banner')->insert([
            'image' => 'building.jpg',
            'main_text' => '["Solusi","Untuk Berkembang Dan Menjangkau"]',
            'dynamic_text' => '[[ "UMKM", "TEST", "DETES" ],[ "Pasar Lokal", "Pasar Global", "Apapun" ]]',
            'status' => 1,
            'created_at' => date("Y-m-d"),
            'updated_at' => date("Y-m-d"),
            
        ]);

        DB::table('main_banner')->insert([
            'image' => 'home-hero.png',
            'main_text' => '["Solusi 2","Untuk Berkembang Dan Menjangkau 2"]',
            'dynamic_text' => '[[ "UMKM 2", "TEST 2", "DETES 2" ],[ "Pasar Lokal 2", "Pasar Global 2", "Apapun 2" ]]',
            'status' => 1,
            'created_at' => date("Y-m-d"),
            'updated_at' => date("Y-m-d"),
            
        ]);
    }
}
