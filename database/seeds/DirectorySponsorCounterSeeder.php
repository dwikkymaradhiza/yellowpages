<?php

use Illuminate\Database\Seeder;

class DirectorySponsorCounterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directory_counter')->insert([
            'directory_id' => 1,
            'counter_date' => '201701',
            'segment' => 'sponsor',
            'counter' => 1,            
        ]);
        
        DB::table('directory_counter')->insert([
            'directory_id' => 2,
            'counter_date' => '201701',
            'segment' => 'sponsor',
            'counter' => 1,            
        ]);
        
        DB::table('directory_counter')->insert([
            'directory_id' => 3,
            'counter_date' => '201701',
            'segment' => 'sponsor',
            'counter' => 1,            
        ]);
        
        DB::table('directory_counter')->insert([
            'directory_id' => 4,
            'counter_date' => '201701',
            'segment' => 'sponsor',
            'counter' => 1,            
        ]);
        
        DB::table('directory_counter')->insert([
            'directory_id' => 5,
            'counter_date' => '201701',
            'segment' => 'sponsor',
            'counter' => 1,            
        ]);
        
        DB::table('directory_counter')->insert([
            'directory_id' => 6,
            'counter_date' => '201701',
            'segment' => 'sponsor',
            'counter' => 1,            
        ]);
        
        DB::table('directory_counter')->insert([
            'directory_id' => 7,
            'counter_date' => '201701',
            'segment' => 'sponsor',
            'counter' => 1,            
        ]);
        
        DB::table('directory_counter')->insert([
            'directory_id' => 8,
            'counter_date' => '201701',
            'segment' => 'sponsor',
            'counter' => 1,            
        ]);
        
        DB::table('directory_counter')->insert([
            'directory_id' => 9,
            'counter_date' => '201701',
            'segment' => 'sponsor',
            'counter' => 1,            
        ]);
    }
}
