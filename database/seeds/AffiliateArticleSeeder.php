<?php

use Illuminate\Database\Seeder;

class AffiliateArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('affiliate_article')->insert([
            'title' => 'Kuliner Nusantara di Pasar Modern Bintaro',
            'headline' => 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah',
            'desc' => 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah',
            'image_url' => 'http://ytimg.googleusercontent.com/vi/j6fjizJef38/0.jpg',
            'tag' => 'Kuliner Nusantara',
            'full_url' => 'http://videomusiclink.com/view/j6fjizJef38',
            'created_at' => date("Y-m-d"),
        ]);
        
        DB::table('affiliate_article')->insert([
            'title' => 'Kuliner Nusantara di Pasar Modern Bintaro',
            'headline' => 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah',
            'desc' => 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah',
            'image_url' => 'http://ytimg.googleusercontent.com/vi/j6fjizJef38/0.jpg',
            'tag' => 'Kuliner Nusantara',
            'full_url' => 'http://videomusiclink.com/view/j6fjizJef38',
            'created_at' => date("Y-m-d"),
        ]);
    }
}
