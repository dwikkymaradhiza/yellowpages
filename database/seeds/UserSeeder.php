<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'dwikkymaradhiza@yahoo.com',
            'password' => bcrypt('admin'),
            'first_name' => 'Dwikky',
            'last_name' => 'Maradhiza',
        ]);
    }
}
