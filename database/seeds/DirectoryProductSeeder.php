<?php

use Illuminate\Database\Seeder;

class DirectoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directory_product')->insert([
            'name' => 'Paket Bebek Peking',
            'brand' => 'Bebek',
            'slug' => 'paket-bebek-deking',
            'directory_id' => 1,   
            'picture' =>'dummy-5.png',
            'description' => 'Bebek Kaleo Tebet',
            'masterid' => 1,
            'masterid_key' => '',
            'remarks' => '',
            'published' => 1
        ]);
        
        DB::table('directory_product')->insert([
            'name' => 'Shockbraker Honda',
            'brand' => 'Otomotif',
            'slug' => 'shockbraker-honda',
            'directory_id' => 2,   
            'picture' =>'dummy-6.png',
            'description' => 'Ledeng Motor Bandung',
            'masterid' => 2,
            'masterid_key' => '',
            'remarks' => '',
            'published' => 1
        ]);
        
        DB::table('directory_product')->insert([
            'name' => 'Topeng Kuda Latex',
            'brand' => 'topeng latex',
            'slug' => 'topeng-kuda-latex',
            'directory_id' => 3,   
            'picture' =>'dummy-7.png',
            'description' => 'Susan Shop',
            'masterid' => 3,
            'masterid_key' => '',
            'remarks' => '',
            'published' => 1
        ]);
        
        
        DB::table('directory_product')->insert([
            'name' => 'Buldozer Komatsu',
            'brand' => 'Komatsu',
            'slug' => 'buldozer-komatsu',
            'directory_id' => 4,   
            'picture' =>'dummy-8.png',
            'description' => 'Jago Rental',
            'masterid' => 4,
            'masterid_key' => '',
            'remarks' => '',
            'published' => 1
        ]);
        
        
        DB::table('directory_product')->insert([
            'name' => 'Regulator LPG Luzzini ',
            'brand' => 'LPG',
            'slug' => 'regulator-lpg-luzzini ',
            'directory_id' => 5,   
            'picture' =>'dummy-9.png',
            'description' => 'Bangkit Sentosa',
            'masterid' => 5,
            'masterid_key' => '',
            'remarks' => '',
            'published' => 1
        ]);
        
        
        DB::table('directory_product')->insert([
            'name' => 'Sepatu Boot Caterpillar ',
            'brand' => 'Caterppillar',
            'slug' => 'sepatu-boot-caterpillar',
            'directory_id' => 6,   
            'picture' =>'dummy-10.png',
            'description' => 'NES Shoes Shop',
            'masterid' => 6,
            'masterid_key' => '',
            'remarks' => '',
            'published' => 1
        ]);
        
        
        DB::table('directory_product')->insert([
            'name' => 'Atap Baja Ringan',
            'brand' => 'Baja',
            'slug' => 'atap-baja-ringan',
            'directory_id' => 6,   
            'picture' =>'dummy-11.png',
            'description' => 'Tatalogam Lestari',
            'masterid' => 6,
            'masterid_key' => '',
            'remarks' => '',
            'published' => 1
        ]);
        
        
        DB::table('directory_product')->insert([
            'name' => 'Roller Conveyor ',
            'brand' => 'Conveyor',
            'slug' => 'roller-conveyor',
            'directory_id' => 6,   
            'picture' =>'dummy-12.png',
            'description' => 'PT. Sarana Teknik Conveyor',
            'masterid' => 6,
            'masterid_key' => '',
            'remarks' => '',
            'published' => 1
        ]);
    }
}
