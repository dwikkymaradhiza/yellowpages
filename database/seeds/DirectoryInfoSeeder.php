<?php

use Illuminate\Database\Seeder;

class DirectoryInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directory_info')->insert([
            'directory_id' => '1',
            'message' => 'Diskon 10% Bebek Kaleyo untuk pemesanan',
            'user_id' => '1',
            'via' => 'web',
            'created_at' => date("Y-m-d"),
        ]);
        
        DB::table('directory_info')->insert([
            'directory_id' => '2',
            'message' => 'DP 0% Honda Tebet Hanya di Desember 2016',
            'user_id' => '1',
            'via' => 'web',
            'created_at' => date("Y-m-d"),
        ]);
        
        DB::table('directory_info')->insert([
            'directory_id' => '3',
            'message' => 'Pesta Akhir Tahun - Gratis servis di Piggio Teb',
            'user_id' => '1',
            'via' => 'web',
            'created_at' => date("Y-m-d"),
        ]);
        
        DB::table('directory_info')->insert([
            'directory_id' => '4',
            'message' => 'Ayam Bakar Mas Mono kini hadir di Jakarta',
            'user_id' => '1',
            'via' => 'web',
            'created_at' => date("Y-m-d"),
        ]);
        
        DB::table('directory_info')->insert([
            'directory_id' => '5',
            'message' => 'Selama libur Tahun Baru, Seafood Santa hanya',
            'user_id' => '1',
            'via' => 'web',
            'created_at' => date("Y-m-d"),
        ]);
    }
}
