<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDirectoryInquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('directory_inquiry', function (Blueprint $table) {
            $table->string('name', 200)->after('email');
            $table->string('phone', 30)->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $add_field = ['name', 'phone'];

        foreach($add_field as $field) {
            if(Schema::hasColumn('directory_inquiry', $field)) {
                Schema::table('directory_inquiry', function (Blueprint $table) use ($field) {
                    $table->dropColumn($field);
                });
            }
        }
    }
}
