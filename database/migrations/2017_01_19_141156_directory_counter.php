<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DirectoryCounter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directory_counter', function (Blueprint $table) {
            $table->increments('directory_counter_id');
            $table->integer('directory_id');
            $table->string('counter_date',6);
            $table->string('segment',50);
            $table->integer('counter');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directory_counter');
    }
}
