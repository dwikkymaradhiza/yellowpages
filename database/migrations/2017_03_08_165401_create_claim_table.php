<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directory_verification', function (Blueprint $table) {
            $table->increments('id');
            $table->string('verify_number', 30);
            $table->string('file_name_siup', 200);
            $table->string('file_name_ktp', 200);
            $table->integer('user_id');
            $table->integer('directory_id');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directory_verification');
    }
}
