<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('msisdn');
            $table->string('trx_id')->unique();
            $table->integer('directory_id');
            $table->integer('user_id');
            $table->string('msg', 300);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_updates');
    }
}
