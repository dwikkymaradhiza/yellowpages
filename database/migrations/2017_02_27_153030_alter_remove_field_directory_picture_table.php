<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRemoveFieldDirectoryPictureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('directory_picture', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('title_len');
            $table->dropColumn('title_lid');
            $table->dropColumn('description_len');
            $table->dropColumn('description_lid');
            $table->dropColumn('published');
            $table->dropColumn('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('directory_picture', function (Blueprint $table) {
            $table->integer('user_id');
            $table->string('title_len', 255);
            $table->string('title_lid', 255);
            $table->text('description_len');
            $table->text('description_lid');
            $table->tinyInteger('published');
            $table->string('slug', 255);
        });
    }
}
