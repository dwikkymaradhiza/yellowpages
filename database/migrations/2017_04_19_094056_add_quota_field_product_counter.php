<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuotaFieldProductCounter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_counter', function (Blueprint $table) {
            $table->string('kuota')->default(0);
            $table->timestamps();
            $table->dropColumn(['counter_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_counter', function (Blueprint $table) {
            $table->dropColumn(['kuota']);
        });
    }
}
