<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MainBanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_banner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image','100');
            $table->string('main_text','100');
            $table->string('dynamic_text','100');
            $table->integer('status');
            $table->datetime('created_at');
            $table->datetime('updated_at');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_banner');
    }
}
