<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BtConfirmation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bt_confirmation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_order_id');
            $table->string('bank_sender',100);
            $table->string('bank_owner_name',100);
            $table->integer('bank_transfer_amount');
            $table->string('bank_recipient',100);
            $table->string('receipt_photo',100);
            $table->integer('approved_status');
            $table->integer('approved_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bt_confirmation');
    }
}
