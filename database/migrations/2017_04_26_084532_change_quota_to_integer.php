<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeQuotaToInteger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_counter', function (Blueprint $table) {
            $table->integer('kuota')->default(0)->change();
        });

        Schema::table('directory_counter', function (Blueprint $table) {
            $table->integer('kuota')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_counter', function (Blueprint $table) {
            $table->string('kuota')->default(0);
        });

        Schema::table('directory_counter', function (Blueprint $table) {
            $table->string('kuota')->default(0);
        });
    }
}
