<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldOrderPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_order', function (Blueprint $table) {
            $table->string('directories_id')->nullable();
            $table->string('products_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_order', function (Blueprint $table) {
            $table->dropColumn(['directories_id']);
            $table->dropColumn(['products_id']);
        });
    }
}
