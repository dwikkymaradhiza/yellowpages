<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DirectoryProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('directory_product', function (Blueprint $table) {
            $table->string('slug', 200);
            $table->tinyInteger('published');
            $table->integer('directory_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $add_field = ['slug', 'published', 'directory_id'];

        foreach($add_field as $field) {
            if(Schema::hasColumn('directory_product', $field)) {
                Schema::table('directory_product', function (Blueprint $table) use ($field) {
                    $table->dropColumn($field);
                });
            }
        }
    }
}
