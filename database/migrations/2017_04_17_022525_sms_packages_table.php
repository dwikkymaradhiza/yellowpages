<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class SmsPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('content')->default('');
            $table->integer('quantity')->default(0);
            $table->date('blast_start')->default(Carbon::now());
            $table->date('blast_end')->default(Carbon::now());
            $table->enum('type', ['BLAST', 'TARGETED', 'CALL_HISTORY'])->default('BLAST');
            $table->enum('status', ['DRAFT', 'PAID', 'SEND', 'FINISH', 'FAIL'])->default('DRAFT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_packages');
    }
}
