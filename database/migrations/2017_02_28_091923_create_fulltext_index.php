<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFulltextIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE FULLTEXT INDEX directory_search ON directory (`name`,`description_lid`,`address_1`)');
        DB::statement('CREATE FULLTEXT INDEX product_search ON directory_product (`name`,`description`)');
        DB::statement('CREATE FULLTEXT INDEX city_search ON city (`name`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE city DROP INDEX city_search');
        DB::statement('ALTER TABLE directory DROP INDEX directory_search');
        DB::statement('ALTER TABLE product DROP INDEX product_search');
    }
}
