<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AffiliateArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_article', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100);
            $table->string('headline',150);
            $table->text('desc');
            $table->string('image_url',100);
            $table->string('tag',100);
            $table->string('full_url',100);
            $table->datetime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliate_article');
    }
}
