<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProductRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_rating', function (Blueprint $table) {
            $table->string('name', 200)->after('comment')->nullable();
            $table->string('email', 200)->after('comment')->nullable();
            $table->string('phone', 30)->after('comment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $add_field = ['name', 'email', 'phone'];

        foreach($add_field as $field) {
            if(Schema::hasColumn('product_rating', $field)) {
                Schema::table('product_rating', function (Blueprint $table) use ($field) {
                    $table->dropColumn($field);
                });
            }
        }
    }
}
