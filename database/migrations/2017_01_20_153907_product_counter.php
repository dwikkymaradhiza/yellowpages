<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductCounter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_counter', function (Blueprint $table) {
            $table->increments('product_counter_id');
            $table->integer('product_id');
            $table->string('counter_date',6);
            $table->string('segment',50);
            $table->integer('counter');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_counter');
    }
}
