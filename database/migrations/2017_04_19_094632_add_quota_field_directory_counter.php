<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuotaFieldDirectoryCounter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('directory_counter', function (Blueprint $table) {
            $table->string('kuota')->default(0);
            $table->timestamps();
            $table->dropColumn(['counter_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('directory_counter', function (Blueprint $table) {
            $table->dropColumn(['kuota']);
        });
    }
}
