-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2017 at 09:00 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yellowpages`
--

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_article`
--

CREATE TABLE `affiliate_article` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `headline` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `desc` text COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `full_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `affiliate_article`
--

INSERT INTO `affiliate_article` (`id`, `title`, `headline`, `desc`, `image_url`, `tag`, `full_url`, `created_at`) VALUES
(1, 'Kuliner Nusantara di Pasar Modern Bintaro', 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah', 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah', 'http://ytimg.googleusercontent.com/vi/j6fjizJef38/0.jpg', 'Restaurants', 'http://videomusiclink.com/', '2017-01-23 00:00:00'),
(2, 'Kuliner Nusantara di Pasar Modern Bintaro', 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah', 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah', 'http://ytimg.googleusercontent.com/vi/j6fjizJef38/0.jpg', 'Restaurants Nusantara', 'http://videomusiclink.com/', '2017-01-23 00:00:00'),
(3, 'Kuliner Nusantara di Pasar Modern Bintaro', 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah', 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah', 'http://ytimg.googleusercontent.com/vi/j6fjizJef38/0.jpg', 'Kuliner Nusantara', 'http://videomusiclink.com/view/j6fjizJef38', '2017-01-23 00:00:00'),
(4, 'Kuliner Nusantara di Pasar Modern Bintaro', 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah', 'Jika pasar tradisional yang ada di sebagian wilayah terkesan becek dan kumuh, tapi tidak dengan Pasar Modern Bintaro. Pandangan inilah', 'http://ytimg.googleusercontent.com/vi/j6fjizJef38/0.jpg', 'Kuliner Nusantara', 'http://videomusiclink.com/view/j6fjizJef38', '2017-01-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `body` text CHARACTER SET utf8,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `career_viewer`
--

CREATE TABLE `career_viewer` (
  `id` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `create_time` datetime NOT NULL,
  `career_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name_len` varchar(255) NOT NULL,
  `name_lid` varchar(255) NOT NULL,
  `description_len` text,
  `description_lid` text NOT NULL,
  `classification` varchar(45) DEFAULT NULL,
  `popular` tinyint(1) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name_len`, `name_lid`, `description_len`, `description_lid`, `classification`, `popular`, `slug`, `weight`, `category_id`) VALUES
(2, 'Agriculture, Forestry & Fishing', 'Pertanian, Kehutanan & Perikanan', 'Data bisnis yang ruang lingkup usahanya di bidang pertanian, perkebunan, kehutanan, perikanan dan peternakan baik manajemennya, pelayanannya, pasokan untuk menghasilkan bahan pangan, bahan baku industri, atau sumber energi', 'Data bisnis yang ruang lingkup usahanya di bidang pertanian, perkebunan, kehutanan, perikanan dan peternakan baik manajemennya, pelayanannya, pasokan untuk menghasilkan bahan pangan, bahan baku industri, atau sumber energi', NULL, 0, 'agriculture-forestry-fishing', 10, NULL),
(3, 'Arts & Gifts', 'Kesenian & Hadiah', 'Data bisnis yang ruang lingkup usahanya menghasilkan berbagai produk kesenian, kerajinan tangan, hadiah dan cinderamata', 'Data bisnis yang ruang lingkup usahanya menghasilkan berbagai produk kesenian, kerajinan tangan, hadiah dan cinderamata', NULL, 0, 'arts-gifts', 10, NULL),
(4, 'Automotive', 'Otomotif', 'Data bisnis yang ruang lingkup usahanya meliputi otomotif, baik produk kendaraan (mobil, motor, skuter, mobil balap, bis, kapal, truk, dll), sparepart (aksesoris, onderdil), pelayanan serta perawatan (bengkel, perbaikan, pencucian, derek, sewa, klub otomotif, dll ) dan produk yang dipadukan (sistem otomotif, yaitu : mesin (bahan bakar, AC, karburator, pelumas, dll), pemindah daya/ power train, sistem kemudi/ steering, sistem suspensi, sistem rem, body, sistem kelistrikan)', 'Data bisnis yang ruang lingkup usahanya meliputi otomotif, baik produk kendaraan (mobil, motor, skuter, mobil balap, bis, kapal, truk, dll), sparepart (aksesoris, onderdil), pelayanan serta perawatan (bengkel, perbaikan, pencucian, derek, sewa, klub otomotif, dll ) dan produk yang dipadukan (sistem otomotif, yaitu : mesin (bahan bakar, AC, karburator, pelumas, dll), pemindah daya/ power train, sistem kemudi/ steering, sistem suspensi, sistem rem, body, sistem kelistrikan)', NULL, 0, 'automotive', 10, NULL),
(5, 'Chemicals', 'Kimia', 'Data bisnis yang ruang lingkup usahanya menghasilkan, memproduksi, pelayanan, pasokan bahan kimia ataupun yang berbahan baku kimia', 'Data bisnis yang ruang lingkup usahanya menghasilkan, memproduksi, pelayanan, pasokan bahan kimia ataupun yang berbahan baku kimia', NULL, 0, 'chemicals', 10, NULL),
(6, 'Education', 'Pendidikan', 'Data bisnis yang ruang lingkup usahanya meliputi pendidikan, baik Sekolah (Pra Sekolah, TK s.d Universitas), tempat kursus, pelayanan pendidikan (pelatihan , consulting, dll), perlengkapan pendidikan (perlengkapan lab, papan, dll) serta buku (tokonya, agent, perpustakaan, dll)', 'Data bisnis yang ruang lingkup usahanya meliputi pendidikan, baik Sekolah (Pra Sekolah, TK s.d Universitas), tempat kursus, pelayanan pendidikan (pelatihan , consulting, dll), perlengkapan pendidikan (perlengkapan lab, papan, dll) serta buku (tokonya, agent, perpustakaan, dll)', NULL, 0, 'education', 10, NULL),
(7, 'Electric & Electronic', 'Kelistrikan & Elektronik', 'Data bisnis yang ruang lingkup usahanya meliputi pelayanan & perlengkapan listrik (kabel, sinar x, lampu, tenaga listrik, keamanan & proteksi) serta pelayanan & perlengkapan elektronik (AC, Antene, Audio Visual, dll)', 'Data bisnis yang ruang lingkup usahanya meliputi pelayanan & perlengkapan listrik (kabel, sinar x, lampu, tenaga listrik, keamanan & proteksi) serta pelayanan & perlengkapan elektronik (AC, Antene, Audio Visual, dll)', NULL, 0, 'electric-electronic', 10, NULL),
(8, 'Fashion ', 'Pakaian ', 'Data bisnis yang ruang lingkup usahanya meliputi pakaian, baik Pakaian dan Aksesorisnya, Designer, Binatu/ laundry, Barang Tekstil serta Pabrik Tekstil', 'Data bisnis yang ruang lingkup usahanya meliputi pakaian, baik Pakaian dan Aksesorisnya, Designer, Binatu/ laundry, Barang Tekstil serta Pabrik Tekstil', NULL, 0, 'fashion-', 10, NULL),
(9, 'Finance', 'Keuangan', 'Data bisnis yang ruang lingkup usahanya di bidang jasa & layanan keuangan', 'Data bisnis yang ruang lingkup usahanya di bidang jasa & layanan keuangan', NULL, 0, 'finance', 10, NULL),
(10, 'Food, Beverages, Cigar, Cigarette & Tobacco', 'Makanan, Minuman, Cerutu, Rokok & Tembakau', 'Data bisnis yang ruang lingkup usahanya di bidang Makanan (Bahan baku : bahan kue, telur, bumbu masak, beras, gula, dll; Hasil : makanan, mie, biskuit, roti, sosis, dll & Pelayanan : bar, cafe, catering,coffee shop, restoran, bar, jasa boga, dll), Minuman & Es, Cerutu, Rokok & Tembakau', 'Data bisnis yang ruang lingkup usahanya di bidang Makanan (Bahan baku : bahan kue, telur, bumbu masak, beras, gula, dll; Hasil : makanan, mie, biskuit, roti, sosis, dll & Pelayanan : bar, cafe, catering,coffee shop, restoran, bar, jasa boga, dll), Minuman & Es, Cerutu, Rokok & Tembakau', NULL, 0, 'food-beverages-cigar-cigarette-tobacco', 10, NULL),
(11, 'Government', 'Pemerintah', 'Data Pemerintahan (Badan, lembaga tertinggi negara - Departemen)', 'Data Pemerintahan (Badan, lembaga tertinggi negara - Departemen)', NULL, 0, 'government', 10, NULL),
(12, 'Health & Care', 'Kesehatan & Perawatan', 'Data bisnis yang ruang lingkup usahanya di bidang Kesehatan & Perawatan (Health Provider, Medical Support, Medical & Health Support, Beauty & Personal Care, Farmasi, Herbal & Health Food Product)', 'Data bisnis yang ruang lingkup usahanya di bidang Kesehatan & Perawatan (Health Provider, Medical Support, Medical & Health Support, Beauty & Personal Care, Farmasi, Herbal & Health Food Product)', NULL, 0, 'health-care', 10, NULL),
(13, 'Home & Office', 'Rumah & Kantor', 'Data bisnis yang ruang lingkup usahanya di bidang Perlengkapan Rumah (Jam, Kaca, rak, lentera, dll), Dapur (alat makan & minum, gelas, pisau, kitchen set, dll), Tempat Tidur (bed cover, sprei, kasur, tempat tidur, dll), Perabot & Interior (Karpet, Gorden, Tirai, Furnitur, dll), Pendekorasian (Taman, Air Mancur, dll), Pasokan Kantor (Stationery, Mesin Kantor, ID Card, Lemari Besi, dll) , Pelayanan Kebersihan (Sanitary Ware, House Cleaning, Cleaning Service & Maintenance, Tissue, dll)', 'Data bisnis yang ruang lingkup usahanya di bidang Perlengkapan Rumah (Jam, Kaca, rak, lentera, dll), Dapur (alat makan & minum, gelas, pisau, kitchen set, dll), Tempat Tidur (bed cover, sprei, kasur, tempat tidur, dll), Perabot & Interior (Karpet, Gorden, Tirai, Furnitur, dll), Pendekorasian (Taman, Air Mancur, dll), Pasokan Kantor (Stationery, Mesin Kantor, ID Card, Lemari Besi, dll) , Pelayanan Kebersihan (Sanitary Ware, House Cleaning, Cleaning Service & Maintenance, Tissue, dll)', NULL, 0, 'home-office', 10, NULL),
(14, 'Hotel & Motel', 'Hotel & Penginapan', 'Data bisnis yang ruang lingkup usahanya di bidang Hotel & Penginapan (Hotel, Motel, Hostel, Bungalow, Cottages, Villa, Resort, serta relatednya seperti : Hotel & Motel Equipment & Supplies dan Hotel & Motel Consultants, dll)', 'Data bisnis yang ruang lingkup usahanya di bidang Hotel & Penginapan (Hotel, Motel, Hostel, Bungalow, Cottages, Villa, Resort, serta relatednya seperti : Hotel & Motel Equipment & Supplies dan Hotel & Motel Consultants, dll)', NULL, 0, 'hotel-motel', 10, NULL),
(15, 'Industry', 'Industri', 'Data bisnis yang ruang lingkup usahanya di bidang Industri yaitu : Layanan Industri : Aluminium Services, Packaging, Metal Services, Welding (Pengelasan), Mouldings (mencetak), Pengecoran (Besi, Baja), Tanners (Penyamak), Pipe Services, Products Designers, Tank Cleaning, Engineers, Layanan Lainnya (pelapisan, Cutting service, labelling, dll); Hasil Pabrik (Logam Hasil Pabrik : Logam, Alumunium, Baja, Besi, Pegas, Pipa Tabung, Timbangan, dll; Karet & PLastik : Karet, Plastik, Pipa, Sikat, Tali, Plester, dll;  Kertas : Kertas Karton, Kertas Limbah, Kotak Kertas, dll; Batu, Tanah Liat, Gelas & Beton; Perlengkapan Militer :  Senjata Api, Amunisi, Alat penyidik, dll; Produk lainnya : Kotak Peti, Korek Api, Pita, Tenda, Terpal, dll ),  Alat Berat  (Alat Cor; Alat Keruk; Derek; Perlengkapan Hidrolik & Pengangkat; Macam-macam Alat Berat; Mesin Giling & Alat Getar; Pemecah Batu; Traktor); Permesinan untuk industri & perdagangan (Permesinan : Mesin Jahit, Mesin Pertanian, Mesin Sulam, Mesin Pemotong Rumput, dll; Industri permesinan : Mesin pengolah logam, Mesin pembuat sikat, Mesin pembuat botol, dll; Perlengkapan Industri : Peralatan pencampur aspal, Gas Compressor, Filtering material, dll; Kebersihan - Perlengkapan Untuk Industri : pengelolaan limbah, Tehnik pengolahan air, dll; Perlengkapan Pengamanan : Alat Pemadam Kebakaran, Bahan tahan api, Aksesoris pengamanan, dll; Alat kendali & Pengukuran : Alat pengontrol bakteri, Teropong, Kalibrasi, Dinamometer, dll)', 'Data bisnis yang ruang lingkup usahanya di bidang Industri yaitu : Layanan Industri : Aluminium Services, Packaging, Metal Services, Welding (Pengelasan), Mouldings (mencetak), Pengecoran (Besi, Baja), Tanners (Penyamak), Pipe Services, Products Designers, Tank Cleaning, Engineers, Layanan Lainnya (pelapisan, Cutting service, labelling, dll); Hasil Pabrik (Logam Hasil Pabrik : Logam, Alumunium, Baja, Besi, Pegas, Pipa Tabung, Timbangan, dll; Karet & PLastik : Karet, Plastik, Pipa, Sikat, Tali, Plester, dll;  Kertas : Kertas Karton, Kertas Limbah, Kotak Kertas, dll; Batu, Tanah Liat, Gelas & Beton; Perlengkapan Militer :  Senjata Api, Amunisi, Alat penyidik, dll; Produk lainnya : Kotak Peti, Korek Api, Pita, Tenda, Terpal, dll ),  Alat Berat  (Alat Cor; Alat Keruk; Derek; Perlengkapan Hidrolik & Pengangkat; Macam-macam Alat Berat; Mesin Giling & Alat Getar; Pemecah Batu; Traktor); Permesinan untuk industri & perdagangan (Permesinan : Mesin Jahit, Mesin Pertanian, Mesin Sulam, Mesin Pemotong Rumput, dll; Industri permesinan : Mesin pengolah logam, Mesin pembuat sikat, Mesin pembuat botol, dll; Perlengkapan Industri : Peralatan pencampur aspal, Gas Compressor, Filtering material, dll; Kebersihan - Perlengkapan Untuk Industri : pengelolaan limbah, Tehnik pengolahan air, dll; Perlengkapan Pengamanan : Alat Pemadam Kebakaran, Bahan tahan api, Aksesoris pengamanan, dll; Alat kendali & Pengukuran : Alat pengontrol bakteri, Teropong, Kalibrasi, Dinamometer, dll)', NULL, 0, 'industry', 10, NULL),
(16, 'Information Technology', 'Teknologi Informasi', 'Data bisnis yang ruang lingkup usahanya di bidang Teknologi Informasi, yaitu : Teknologi Informasi (Computer Networking, Computer Total Solution, Computer Site Preparation, dll); Komputer & Aplikasi (Computer Equipment, Computer Services, Computer Software), Internet (Internet Portal, Internet Services, Internet Game Online, dll), Data Services (Content Provider, Data Processing, Data Communication, Storage Document & Record)', 'Data bisnis yang ruang lingkup usahanya di bidang Teknologi Informasi, yaitu : Teknologi Informasi (Computer Networking, Computer Total Solution, Computer Site Preparation, dll); Komputer & Aplikasi (Computer Equipment, Computer Services, Computer Software), Internet (Internet Portal, Internet Services, Internet Game Online, dll), Data Services (Content Provider, Data Processing, Data Communication, Storage Document & Record)', NULL, 0, 'information-technology', 10, NULL),
(17, 'Mining', 'Pertambangan', 'Data bisnis yang ruang lingkup usahanya di bidang Pertambangan, yaitu : Pertambangan & Penggalian (baik Perusahaan Pertambangan, Layanan Pertambangan, Hasil dan Pasokan Pertambangan & Penggalian, Pengeboran, Penggalian, Pengerukan) serta Oil & Gas (baik Produknya, Produksi, Peralatan, Layanan dan Ladang Minyak)', 'Data bisnis yang ruang lingkup usahanya di bidang Pertambangan, yaitu : Pertambangan & Penggalian (baik Perusahaan Pertambangan, Layanan Pertambangan, Hasil dan Pasokan Pertambangan & Penggalian, Pengeboran, Penggalian, Pengerukan) serta Oil & Gas (baik Produknya, Produksi, Peralatan, Layanan dan Ladang Minyak)', NULL, 0, 'mining', 10, NULL),
(18, 'Property & Building Materials', 'Properti & Bahan Bangunan', 'Data bisnis yang ruang lingkup usahanya di bidang Properti & Bahan Bangunan, meliputi : Properti ( Real Estate, Property Management, Warehouses, Property Professional Services); Bahan Bangunan (Batu Bata, Semen, Pintu & Jendela, Kaca, Kunci, Gipsum, Cat, dll); Konstruksi (Konstruksi - Jasa Profesional, Konstruksi Lainnya seperti : Geotekstil, Kontraktor peledakan, Arsitek & Pasokan, Pasokan Konstruksi & Kontraktor, Building Construction, General Contractors & Operative Builder, Pembangunan Jalan serta Pipeline/ Saluran Pipa) ; Pelayanan Bangunan (Airport Maintenance, Swimming Pool Services, Layanan Parkir, Pemeliharaan Gedung & Pembersihan, Pelayanan Pemindahan & Penyimpanan, Home Services & Repairs,Woodworkers) ', 'Data bisnis yang ruang lingkup usahanya di bidang Properti & Bahan Bangunan, meliputi : Properti ( Real Estate, Property Management, Warehouses, Property Professional Services); Bahan Bangunan (Batu Bata, Semen, Pintu & Jendela, Kaca, Kunci, Gipsum, Cat, dll); Konstruksi (Konstruksi - Jasa Profesional, Konstruksi Lainnya seperti : Geotekstil, Kontraktor peledakan, Arsitek & Pasokan, Pasokan Konstruksi & Kontraktor, Building Construction, General Contractors & Operative Builder, Pembangunan Jalan serta Pipeline/ Saluran Pipa) ; Pelayanan Bangunan (Airport Maintenance, Swimming Pool Services, Layanan Parkir, Pemeliharaan Gedung & Pembersihan, Pelayanan Pemindahan & Penyimpanan, Home Services & Repairs,Woodworkers) ', NULL, 0, 'property-building-materials', 10, NULL),
(19, 'Services', 'Pelayanan', 'Data bisnis yang ruang lingkup usahanya di bidang Pelayanan, baik Bisnis Service (Konsultan, Event Organizer, Money Changers, Survey & Riset, Marketing & Trading, dll); Profesional Service (Advocates, Baby Sitter, Engineers, Ahli Forensik, Ahli Geofisika, Penerjemah & Juru Bahasa, dll); Organisasi, Keagamaan & Layanan Sosial (Asosiasi & Perkumpulan, Panti Asuhan, Layanan Sosial, dll); Pelayanan Sanitasi & Lingkungan (Sanitation Service, Environment Product, Plastic - Recycling, dll) serta layanan lainnya (Embassies, Consulates & Legation, dll) ', 'Data bisnis yang ruang lingkup usahanya di bidang Pelayanan, baik Bisnis Service (Konsultan, Event Organizer, Money Changers, Survey & Riset, Marketing & Trading, dll); Profesional Service (Advocates, Baby Sitter, Engineers, Ahli Forensik, Ahli Geofisika, Penerjemah & Juru Bahasa, dll); Organisasi, Keagamaan & Layanan Sosial (Asosiasi & Perkumpulan, Panti Asuhan, Layanan Sosial, dll); Pelayanan Sanitasi & Lingkungan (Sanitation Service, Environment Product, Plastic - Recycling, dll) serta layanan lainnya (Embassies, Consulates & Legation, dll) ', NULL, 0, 'services', 10, NULL),
(20, 'Shopping', 'Belanja', 'Data bisnis yang ruang lingkup usahanya meliputi Tempat Perbelanjaan dan relatednya (Supplier, Perlengkapan Pasar, pemasok, dll), yaitu : Shopping Centre & Supermarket (Department Stores, Supermarket, Traditional Market, General Merchandise - Wholesale, dll) serta Mini Market dan toko lainnya (Factory Outlet, Mini Market, Duty Free Stores, dll)', 'Data bisnis yang ruang lingkup usahanya meliputi Tempat Perbelanjaan dan relatednya (Supplier, Perlengkapan Pasar, pemasok, dll), yaitu : Shopping Centre & Supermarket (Department Stores, Supermarket, Traditional Market, General Merchandise - Wholesale, dll) serta Mini Market dan toko lainnya (Factory Outlet, Mini Market, Duty Free Stores, dll)', NULL, 0, 'shopping', 10, NULL),
(21, 'Sports, Recreations & Amusement', 'Olahraga, Rekreasi & Hiburan', 'Data bisnis yang ruang lingkup usahanya meliputi Tempat Olahraga, Rekreasi, Hiburan dan relatednya, yaitu : Olahraga (Olahraga, Pusat Olahraga, Klub Olahraga & Perlengkapan Olahraga); Rekreasi (Pusat Rekreasi, Kebun Binatang, Gelanggang Samudera, Monumen & Museum, Memancing, Pariwisata & Pesiar); Hiburan (Tempat Hiburan, Pelayanan Hiburan, Alat Hiburan, Tari/Berdansa, Musik, Karaoke, Disko); Hiburan Anak (Taman Bermain, Hiburan anak-anak, Tukang Sulap, Badut, dll) serta Mainan & Hobi (Mianan, Pelayanan Maianan, Bola & Balon, Hobi & Model Konstruksi, dll)', 'Data bisnis yang ruang lingkup usahanya meliputi Tempat Olahraga, Rekreasi, Hiburan dan relatednya, yaitu : Olahraga (Olahraga, Pusat Olahraga, Klub Olahraga & Perlengkapan Olahraga); Rekreasi (Pusat Rekreasi, Kebun Binatang, Gelanggang Samudera, Monumen & Museum, Memancing, Pariwisata & Pesiar); Hiburan (Tempat Hiburan, Pelayanan Hiburan, Alat Hiburan, Tari/Berdansa, Musik, Karaoke, Disko); Hiburan Anak (Taman Bermain, Hiburan anak-anak, Tukang Sulap, Badut, dll) serta Mainan & Hobi (Mianan, Pelayanan Maianan, Bola & Balon, Hobi & Model Konstruksi, dll)', NULL, 0, 'sports-recreations-amusement', 10, NULL),
(22, 'Transportation & Communications', 'Transportasi & Komunikasi', 'Data bisnis yang ruang lingkup usahanya di bidang Transportasi dan Komunikasi, meliputi Pelayanan Transportasi (Bus & Taksi, Kereta Api, Transport Cargo, Agen & Biro Perjalanan, Delivery Services (Freight Forwarding, dll)), Transportasi Udara (Perusahaan, Pelayanan, Konsultan, Maintenance Pesawat Terbang; Perusahaan Pelayanan, Pelayanan, Konsultan Penerbangan; Penyewaan Pesawat Terbang & Helikopter, Transport Cargo Udara, dll); Transportasi Air (Penyewaan Kapal, Pelayanan Kapal, Jasa Pelabuhan, Shipping, dll); Perlengkapan Transportasi (Perlengkapan : Pesawat Terbang, Penerbangan, Bandar Udara, kereta Api, Lalu lintas, dll); Pelayanan Pos (Delivery Services : Jasa Kurir, Jasa Pengiriman Uang, dll; Benda Pos : Perangko, Kartu, dll) , Pelayanan Komunikasi (Layanan Telekomunikasi, Penyedia Jasa Telekomunikasi, Wartel, Transmisi, dll) serta Perlengkapan Komunikasi (Radio Telekomunikasi, Kartu Telepon, Handphone - Retail, Peralatan Telepon, dll)', 'Data bisnis yang ruang lingkup usahanya di bidang Transportasi dan Komunikasi, meliputi Pelayanan Transportasi (Bus & Taksi, Kereta Api, Transport Cargo, Agen & Biro Perjalanan, Delivery Services (Freight Forwarding, dll)), Transportasi Udara (Perusahaan, Pelayanan, Konsultan, Maintenance Pesawat Terbang; Perusahaan Pelayanan, Pelayanan, Konsultan Penerbangan; Penyewaan Pesawat Terbang & Helikopter, Transport Cargo Udara, dll); Transportasi Air (Penyewaan Kapal, Pelayanan Kapal, Jasa Pelabuhan, Shipping, dll); Perlengkapan Transportasi (Perlengkapan : Pesawat Terbang, Penerbangan, Bandar Udara, kereta Api, Lalu lintas, dll); Pelayanan Pos (Delivery Services : Jasa Kurir, Jasa Pengiriman Uang, dll; Benda Pos : Perangko, Kartu, dll) , Pelayanan Komunikasi (Layanan Telekomunikasi, Penyedia Jasa Telekomunikasi, Wartel, Transmisi, dll) serta Perlengkapan Komunikasi (Radio Telekomunikasi, Kartu Telepon, Handphone - Retail, Peralatan Telepon, dll)', NULL, 0, 'transportation-communications', 10, NULL),
(23, 'Abattoir', 'Rumah Pemotongan Hewan', NULL, '', NULL, 0, 'abattoir', 10, 2),
(24, 'Academic, Colleges, Universities', 'Akademi, Sekolah Tinggi, Universitas', NULL, '', NULL, 0, 'academic-colleges-universities', 10, 6),
(25, 'Accessories - Outomotive', 'Aksesori Otomotif', NULL, '', NULL, 0, 'accessories-outomotive', 10, 4),
(26, 'Accessories, Jewellery & Watches', 'Aksesori, Perhiasan & Jam Tangan', NULL, '', NULL, 0, 'accessories-jewellery-watches', 10, 8),
(27, 'Accountant', 'Akuntan', NULL, '', NULL, 0, 'accountant', 10, 9),
(28, 'Acetylene', 'Asetilene', NULL, '', NULL, 0, 'acetylene', 10, 5),
(29, 'Actuaries', 'Aktuaria', NULL, '', NULL, 0, 'actuaries', 10, 9),
(30, 'Additives', 'Aditif', NULL, '', NULL, 0, 'additives', 10, 5),
(31, 'Adhesives & Glues', 'Perekat & Lem', NULL, '', NULL, 0, 'adhesives-glues', 10, 5),
(32, 'Administration', 'Administrasi', NULL, '', NULL, 0, 'administration', 10, 19),
(33, 'Advertising Media', 'Periklanan - Cetak', NULL, '', NULL, 0, 'advertising-media', 10, 1),
(34, 'Advertising Services', 'Periklanan - Pelayanan', NULL, '', NULL, 0, 'advertising-services', 10, 1),
(35, 'Advocates', 'Pengacara', NULL, '', NULL, 0, 'advocates', 10, 19),
(36, 'Agriculture', 'Pertanian', NULL, '', NULL, 0, 'agriculture', 10, 2),
(37, 'Air Compressors', 'Angin, Kompresor', NULL, '', NULL, 0, 'air-compressors', 10, 4),
(38, 'Air Conditioning', 'AC', NULL, '', NULL, 0, 'air-conditioning', 10, 4),
(39, 'Air Freshener', 'Penyejuk Udara', NULL, '', NULL, 0, 'air-freshener', 10, 5),
(40, 'Air Processing Equipment', 'Pembersih Udara, Perlengkapan', NULL, '', NULL, 0, 'air-processing-equipment', 10, 7),
(41, 'Aircraft', 'Pesawat Terbang', NULL, '', NULL, 0, 'aircraft', 10, 22),
(42, 'Aircraft Equipment', 'Pesawat Terbang, Perlengkapan', NULL, '', NULL, 0, 'aircraft-equipment', 10, 22),
(43, 'Airlines Equipment', 'Pesawat Terbang, Perlengkapan', NULL, '', NULL, 0, 'airlines-equipment', 10, 22),
(44, 'Airport Equipment', 'Bandar Udara, Perlengkapan', NULL, '', NULL, 0, 'airport-equipment', 10, 22),
(45, 'Airport Maintenance', 'Bandar Udara, Pemeliharaan', NULL, '', NULL, 0, 'airport-maintenance', 10, 18),
(46, 'Alternators', 'Dinamo', NULL, '', NULL, 0, 'alternators', 10, 4),
(47, 'Aluminium Services', 'Aluminium - Layanan', NULL, '', NULL, 0, 'aluminium-services', 10, 15),
(48, 'Amusement Devices', 'Hiburan, Alat-Alat', NULL, '', NULL, 0, 'amusement-devices', 10, 21),
(49, 'Amusement Places', 'Hiburan, Tempat', NULL, '', NULL, 0, 'amusement-places', 10, 21),
(50, 'Amusement Services', 'Hiburan, Pelayanan', NULL, '', NULL, 0, 'amusement-services', 10, 21),
(51, 'Animation Services', 'Animasi, Pelayanan', NULL, '', NULL, 0, 'animation-services', 10, 1),
(52, 'Antene', 'Antena', NULL, '', NULL, 0, 'antene', 10, 7),
(53, 'Apartments', 'Apartemen', NULL, '', NULL, 0, 'apartments', 10, 18),
(54, 'Architects & Architect Supplies', 'Arsitek & Pasokan', NULL, '', NULL, 0, 'architects-architect-supplies', 10, 18),
(55, 'Art Glass', 'Kaca Seni', NULL, '', NULL, 0, 'art-glass', 10, 3),
(56, 'Art Goods', 'Barang Seni', NULL, '', NULL, 0, 'art-goods', 10, 3),
(57, 'Artist & Artist Supplies', 'Seniman & Pasokan', NULL, '', NULL, 0, 'artist-artist-supplies', 10, 3),
(58, 'Association & Club', 'Asosiasi & Perkumpulan', NULL, '', NULL, 0, 'association-club', 10, 19),
(59, 'Assurance Business Advisory Services', 'Laporan Bisnis Jaminan, Pelayanan', NULL, '', NULL, 0, 'assurance-business-advisory-services', 10, 9),
(60, 'Auctioneers', 'Juru Lelang', NULL, '', NULL, 0, 'auctioneers', 10, 9),
(61, 'Audio - Visual', 'Audio - Visual', NULL, '', NULL, 0, 'audio-visual', 10, 7),
(62, 'Automation Equipment', 'Otomatis, Perlengkapan', NULL, '', NULL, 0, 'automation-equipment', 10, 7),
(63, 'Automations', 'Otomatisasi', NULL, '', NULL, 0, 'automations', 10, 19),
(64, 'Aviation', 'Penerbangan', NULL, '', NULL, 0, 'aviation', 10, 22),
(65, 'Aviation Equipment', 'Penerbangan, Perlengkapan', NULL, '', NULL, 0, 'aviation-equipment', 10, 22),
(66, 'Baby & Children Equipment', 'Bayi & Anak, Perlengkapan', NULL, '', NULL, 0, 'baby-children-equipment', 10, 8),
(67, 'Baby Sitter', 'Pramusiwi', NULL, '', NULL, 0, 'baby-sitter', 10, 19),
(68, 'Bags, Shoes & Sandals', 'Tas, Sepatu & Sendal', NULL, '', NULL, 0, 'bags-shoes-sandals', 10, 8),
(69, 'Bakery', 'Roti, Perusahaan', NULL, '', NULL, 0, 'bakery', 10, 10),
(70, 'Ball & Balloon', 'Bola & Balon', NULL, '', NULL, 0, 'ball-balloon', 10, 21),
(71, 'Bank Representatives', 'Bank, Perwakilan', NULL, '', NULL, 0, 'bank-representatives', 10, 9),
(72, 'Bank Supplies', 'Bank, Pasokan', NULL, '', NULL, 0, 'bank-supplies', 10, 9),
(73, 'Banks', 'Bank', NULL, '', NULL, 0, 'banks', 10, 9),
(74, 'Bar', 'Bar', NULL, '', NULL, 0, 'bar', 10, 10),
(75, 'Barber & Barber Supplies', 'Pemangkas Rambut & Pasokan', NULL, '', NULL, 0, 'barber-barber-supplies', 10, 12),
(76, 'Beauty Salon & Supplies', 'Salon Kecantikan & Pasokan', NULL, '', NULL, 0, 'beauty-salon-supplies', 10, 12),
(77, 'Bed Cover', 'Seprai', NULL, '', NULL, 0, 'bed-cover', 10, 13),
(78, 'Bed Springs', 'Kasur Pegas', NULL, '', NULL, 0, 'bed-springs', 10, 13),
(79, 'Bee', 'Lebah', NULL, '', NULL, 0, 'bee', 10, 2),
(80, 'Beer & Liquor', 'Bir & Minuman Keras', NULL, '', NULL, 0, 'beer-liquor', 10, 10),
(81, 'Bicycles', 'Sepeda', NULL, '', NULL, 0, 'bicycles', 10, 22),
(82, 'Bird', 'Burung', NULL, '', NULL, 0, 'bird', 10, 2),
(83, 'Biscuits', 'Biskuit', NULL, '', NULL, 0, 'biscuits', 10, 10),
(84, 'Boarding House', 'Asrama, Rumah Kos', NULL, '', NULL, 0, 'boarding-house', 10, 18),
(85, 'Boat', 'Kapal', NULL, '', NULL, 0, 'boat', 10, 4),
(86, 'Boat & Ship Charter', 'Kapal, Penyewaan', NULL, '', NULL, 0, 'boat-ship-charter', 10, 22),
(87, 'Books', 'Buku', NULL, '', NULL, 0, 'books', 10, 6),
(88, 'Boutique', 'Butik', NULL, '', NULL, 0, 'boutique', 10, 8),
(89, 'Brass Products', 'Kuningan', NULL, '', NULL, 0, 'brass-products', 10, 3),
(90, 'Breeder', 'Pembibitan', NULL, '', NULL, 0, 'breeder', 10, 2),
(91, 'Bricks', 'Batu Bata', NULL, '', NULL, 0, 'bricks', 10, 18),
(92, 'Broadcasting', 'Penyiaran', NULL, '', NULL, 0, 'broadcasting', 10, 1),
(93, 'Broadcasting Equipment', 'Pemancar Siaran, Perlengkapan', NULL, '', NULL, 0, 'broadcasting-equipment', 10, 22),
(94, 'Building - Portable', 'Bangunan - Terpindahkan', NULL, '', NULL, 0, 'building-portable', 10, 18),
(95, 'Building Construction, General Contractors & Operative Builder', 'Konstruksi, Kontraktor Umum & Operasi Gedung', NULL, '', NULL, 0, 'building-construction-general-contractors-operative-builder', 10, 18),
(96, 'Building Maintenance & Cleaning', 'Pemeliharaan Gedung & Pembersihan', NULL, '', NULL, 0, 'building-maintenance-cleaning', 10, 18),
(97, 'Bungalow', 'Bungalo', NULL, '', NULL, 0, 'bungalow', 10, 14),
(98, 'Bus', 'Bis', NULL, '', NULL, 0, 'bus', 10, 4),
(99, 'Bus & Taxicab', 'Bus & Taksi', NULL, '', NULL, 0, 'bus-taxicab', 10, 22),
(100, 'Cafe', 'Kafe', NULL, '', NULL, 0, 'cafe', 10, 10),
(101, 'Cake Ingredients', 'Kue Bahan-Bahan', NULL, '', NULL, 0, 'cake-ingredients', 10, 10),
(102, 'Calculating & Adding Machine', 'Mesin Kalkulasi & Penambahan', NULL, '', NULL, 0, 'calculating-adding-machine', 10, 7),
(103, 'Camphor & Napthalene', 'Kamper & Naptalena', NULL, '', NULL, 0, 'camphor-napthalene', 10, 5),
(104, 'Candles', 'Lilin', NULL, '', NULL, 0, 'candles', 10, 5),
(105, 'Carbon', 'Karbon', NULL, '', NULL, 0, 'carbon', 10, 5),
(106, 'Carbon Products', 'Karbon - Barang-barang', NULL, '', NULL, 0, 'carbon-products', 10, 5),
(107, 'Cards', 'Kartu', NULL, '', NULL, 0, 'cards', 10, 13),
(108, 'Carpets & Rugs', 'Karpet & Karpet Tebal', NULL, '', NULL, 0, 'carpets-rugs', 10, 13),
(109, 'Catering', 'Pelayanan Pangan', NULL, '', NULL, 0, 'catering', 10, 10),
(110, 'Ceiling', 'Langit-langit', NULL, '', NULL, 0, 'ceiling', 10, 18),
(111, 'Cements', 'Semen', NULL, '', NULL, 0, 'cements', 10, 18),
(112, 'Charter & Rental - Aircraft & Helicopter', 'Pesawat Terbang & Helikopter, Penyewaan', NULL, '', NULL, 0, 'charter-rental-aircraft-helicopter', 10, 22),
(113, 'Chemical - Analytical', 'Bahan Kimia - Analitik', NULL, '', NULL, 0, 'chemical-analytical', 10, 5),
(114, 'Chemical Cleaning', 'Kimia, Pembersihan', NULL, '', NULL, 0, 'chemical-cleaning', 10, 5),
(115, 'Chemical Industrial', 'Industri Kimia', NULL, '', NULL, 0, 'chemical-industrial', 10, 5),
(116, 'Chemical Supplies', 'Kimia, Pasokan', NULL, '', NULL, 0, 'chemical-supplies', 10, 5),
(117, 'Chemicals', 'Bahan Kimia', NULL, '', NULL, 0, 'chemicals-1', 10, 5),
(118, 'Children Entertainment', 'Anak-anak, Hiburan', NULL, '', NULL, 0, 'children-entertainment', 10, 21),
(119, 'Chrome', 'Krom', NULL, '', NULL, 0, 'chrome', 10, 5),
(120, 'Cigar, Cigarette & Tobacco', 'Cerutu, Rokok & Tembakau', NULL, '', NULL, 0, 'cigar-cigarette-tobacco', 10, 10),
(121, 'Cigar, Cigarette & Tobacco Equipment & Supplies', 'Cerutu, Rokok & Tembakau - Perlengkapan & Pasokan', NULL, '', NULL, 0, 'cigar-cigarette-tobacco-equipment-supplies', 10, 10),
(122, 'Clinic', 'Klinik', NULL, '', NULL, 0, 'clinic', 10, 12),
(123, 'Clipping Bureaus', 'Biro Kliping', NULL, '', NULL, 0, 'clipping-bureaus', 10, 19),
(124, 'Clocks', 'Jam', NULL, '', NULL, 0, 'clocks', 10, 13),
(125, 'Clothings', 'Pakaian', NULL, '', NULL, 0, 'clothings', 10, 8),
(126, 'Clown', 'Badut', NULL, '', NULL, 0, 'clown', 10, 21),
(127, 'Cocoa', 'Coklat', NULL, '', NULL, 0, 'cocoa', 10, 10),
(128, 'Coffee', 'Kopi', NULL, '', NULL, 0, 'coffee', 10, 10),
(129, 'Coffee Shops', 'Kedai Kopi', NULL, '', NULL, 0, 'coffee-shops', 10, 10),
(130, 'Collection', 'Koleksi, Biro', NULL, '', NULL, 0, 'collection', 10, 19),
(131, 'Computer Consultants', 'Komputer, Konsultan', NULL, '', NULL, 0, 'computer-consultants', 10, 16),
(132, 'Computer Equipment', 'Komputer, Perlengkapan', NULL, '', NULL, 0, 'computer-equipment', 10, 16),
(133, 'Computer Internet', 'Internet Komputer', NULL, '', NULL, 0, 'computer-internet', 10, 16),
(134, 'Computer Services', 'Komputer, Pelayanan', NULL, '', NULL, 0, 'computer-services', 10, 16),
(135, 'Computer Site Preparation', 'Komputer, Perencana Penempatan', NULL, '', NULL, 0, 'computer-site-preparation', 10, 16),
(136, 'Computer Software', 'Komputer, Piranti Lunak', NULL, '', NULL, 0, 'computer-software', 10, 16),
(137, 'Computer Total Solution', 'Komputer - Solusi Menyeluruh', NULL, '', NULL, 0, 'computer-total-solution', 10, 16),
(138, 'Confectionery & Candy', 'Kue-Kue & Kembang Gula', NULL, '', NULL, 0, 'confectionery-candy', 10, 10),
(139, 'Construction - Professional Services', 'Konstruksi - Jasa Profesional', NULL, '', NULL, 0, 'construction-professional-services', 10, 18),
(140, 'Construction & Contractor Supplies', 'Konstruksi & Kontraktor, Pasokan', NULL, '', NULL, 0, 'construction-contractor-supplies', 10, 18),
(141, 'Consultants', 'Konsultan', NULL, '', NULL, 0, 'consultants', 10, 19),
(142, 'Containers', 'Peti Kemas', NULL, '', NULL, 0, 'containers', 10, 22),
(143, 'Content Provider', 'Penyedia Isi', NULL, '', NULL, 0, 'content-provider', 10, 16),
(144, 'Contraceptive Equipment', 'Perlengkapan Kontrasepsi', NULL, '', NULL, 0, 'contraceptive-equipment', 10, 12),
(145, 'Control  & Measuring Devices', 'Kendali & Pengukuran - Alat', NULL, '', NULL, 0, 'control-measuring-devices', 10, 15),
(146, 'Convention Services & Facilities', 'Pertemuan, Pelayanan & Fasilitas/Kemudahan', NULL, '', NULL, 0, 'convention-services-facilities', 10, 18),
(147, 'Cooking Utensils', 'Perkakas Dapur', NULL, '', NULL, 0, 'cooking-utensils', 10, 13),
(148, 'Cooks', 'Juru Masak', NULL, '', NULL, 0, 'cooks', 10, 10),
(149, 'Cooperative Buying Service', 'Koperasi, Pelayanan Pembelian Secara', NULL, '', NULL, 0, 'cooperative-buying-service', 10, 19),
(150, 'Copying Machine', 'Mesin Fotokopi', NULL, '', NULL, 0, 'copying-machine', 10, 7),
(151, 'Copying, Duplicating', 'Fotokopi & Duplikat', NULL, '', NULL, 0, 'copying-duplicating-1', 10, 19),
(152, 'Corn Products', 'Makanan Hasil Jagung', NULL, '', NULL, 0, 'corn-products', 10, 10),
(153, 'Cosmetic', 'Kosmetik', NULL, '', NULL, 0, 'cosmetic', 10, 12),
(154, 'Cottages', 'Pondok Peristirahatan', NULL, '', NULL, 0, 'cottages', 10, 14),
(155, 'Cotton Goods', 'Bahan Katun', NULL, '', NULL, 0, 'cotton-goods', 10, 8),
(156, 'Courses', 'Kursus', NULL, '', NULL, 0, 'courses', 10, 6),
(157, 'Cow', 'Sapi', NULL, '', NULL, 0, 'cow', 10, 2),
(158, 'Cranes', 'Derek', NULL, '', NULL, 0, 'cranes', 10, 15),
(159, 'Cricket', 'Jangkrik', NULL, '', NULL, 0, 'cricket', 10, 2),
(160, 'Crocodile', 'Buaya', NULL, '', NULL, 0, 'crocodile', 10, 2),
(161, 'Crystal, Porcelain & Ceramic products', 'Kristal, Porselain & Keramik', NULL, '', NULL, 0, 'crystal-porcelain-ceramic-products', 10, 3),
(162, 'Curtains & Draperies', 'Gorden & Tirai', NULL, '', NULL, 0, 'curtains-draperies', 10, 13),
(163, 'Dancing', 'Tari/Berdansa', NULL, '', NULL, 0, 'dancing', 10, 21),
(164, 'Data Communication', 'Komunikasi Data', NULL, '', NULL, 0, 'data-communication', 10, 16),
(165, 'Data Processing', 'Pengolahan Data', NULL, '', NULL, 0, 'data-processing', 10, 16),
(166, 'Delivery Services', 'Pengantaran - Pelayanan', NULL, '', NULL, 0, 'delivery-services', 10, 22),
(167, 'Delivery, Courier & Distribution', 'Penyalur', NULL, '', NULL, 0, 'delivery-courier-distribution', 10, 19),
(168, 'Department Stores', 'Toko Serba Ada', NULL, '', NULL, 0, 'department-stores', 10, 20),
(169, 'Designer', 'Perancang', NULL, '', NULL, 0, 'designer', 10, 8),
(170, 'Disco', 'Disko', NULL, '', NULL, 0, 'disco', 10, 21),
(171, 'Doors & Windows ', 'Pintu & Jendela', NULL, '', NULL, 0, 'doors-windows-', 10, 18),
(172, 'Dredges', 'Alat Keruk', NULL, '', NULL, 0, 'dredges', 10, 15),
(173, 'Dredging', 'Pengerukan', NULL, '', NULL, 0, 'dredging', 10, 17),
(174, 'Drilling', 'Pengeboran', NULL, '', NULL, 0, 'drilling', 10, 17),
(175, 'Dry Ice', 'Es Kering', NULL, '', NULL, 0, 'dry-ice', 10, 10),
(176, 'Drying Equipment', 'Pengering, Perlengkapan', NULL, '', NULL, 0, 'drying-equipment', 10, 7),
(177, 'Duty Free Stores', 'Toko Bebas Bea', NULL, '', NULL, 0, 'duty-free-stores', 10, 20),
(178, 'Educational Consultants', 'Pendidikan, Konsultan ', NULL, '', NULL, 0, 'educational-consultants', 10, 6),
(179, 'Educational Organization', 'Pendidikan, Organisasi', NULL, '', NULL, 0, 'educational-organization', 10, 6),
(180, 'Educational Research', 'Pendidikan, Penelitian', NULL, '', NULL, 0, 'educational-research', 10, 6),
(181, 'Eggs', 'Telur', NULL, '', NULL, 0, 'eggs', 10, 10),
(182, 'Electric Motor', 'Listrik, Motor', NULL, '', NULL, 0, 'electric-motor', 10, 7),
(183, 'Electrical Components', 'Listrik, Komponen', NULL, '', NULL, 0, 'electrical-components', 10, 7),
(184, 'Electrical Contractor', 'Listrik Kontraktor', NULL, '', NULL, 0, 'electrical-contractor', 10, 7),
(185, 'Electrical Operating Devices', 'Listrik, Alat Penggerak', NULL, '', NULL, 0, 'electrical-operating-devices', 10, 7),
(186, 'Electrical Services', 'Listrik, Pelayanan', NULL, '', NULL, 0, 'electrical-services', 10, 7),
(187, 'Electronic Instrument', 'Electronik - Alat-alat', NULL, '', NULL, 0, 'electronic-instrument', 10, 7),
(188, 'Electronic Power Supplies', 'Elektronik, Pembangkit Tenaga', NULL, '', NULL, 0, 'electronic-power-supplies', 10, 7),
(189, 'Electronic Services & Repairing', 'Elektronik, Pelayanan & Perbaikan', NULL, '', NULL, 0, 'electronic-services-repairing', 10, 7),
(190, 'Embassies, Consulates & Legation', 'Kedutaan Besar, Konsulat & Kedutaan', NULL, '', NULL, 0, 'embassies-consulates-legation', 10, 19),
(191, 'Employment Agencies', 'Agen Tenaga Kerja', NULL, '', NULL, 0, 'employment-agencies', 10, 19),
(192, 'Engineers', 'Tehnik - Ahli', NULL, '', NULL, 0, 'engineers', 10, 15),
(193, 'Environmental Services', 'Lingkungan - Layanan', NULL, '', NULL, 0, 'environmental-services', 10, 19),
(194, 'Estate', 'Perkebunan', NULL, '', NULL, 0, 'estate', 10, 2),
(195, 'Estate Products', 'Perkebunan, Hasil', NULL, '', NULL, 0, 'estate-products', 10, 2),
(196, 'Event Organizer', 'Penyelenggara Acara', NULL, '', NULL, 0, 'event-organizer', 10, 19),
(197, 'Explosives', 'Bahan Peledak', NULL, '', NULL, 0, 'explosives', 10, 5),
(198, 'Fabricated Metal Products', 'Logam - Hasil Pabrik', NULL, '', NULL, 0, 'fabricated-metal-products', 10, 15),
(199, 'Factory Outlet', 'Toko Hasil Pabrik', NULL, '', NULL, 0, 'factory-outlet', 10, 20),
(200, 'Fans', 'Kipas Angin', NULL, '', NULL, 0, 'fans', 10, 7),
(201, 'Feeds', 'Makanan Ternak', NULL, '', NULL, 0, 'feeds', 10, 2),
(202, 'Fence Material', 'Pagar - Bahan', NULL, '', NULL, 0, 'fence-material', 10, 18),
(203, 'Fertilizer', 'Pupuk', NULL, '', NULL, 0, 'fertilizer', 10, 2),
(204, 'Financial Counsultant', 'Keuangan - Konsultan', NULL, '', NULL, 0, 'financial-counsultant', 10, 9),
(205, 'Fish & Seafood', 'Ikan & Hidangan Laut', NULL, '', NULL, 0, 'fish-seafood', 10, 10),
(206, 'Fish Farms', 'Ikan, Tambak', NULL, '', NULL, 0, 'fish-farms', 10, 2),
(207, 'Fish Products', 'Ikan, Hasil', NULL, '', NULL, 0, 'fish-products', 10, 2),
(208, 'Fishing', 'Memancing', NULL, '', NULL, 0, 'fishing', 10, 21),
(209, 'Fishing Services', 'Ikan, Pelayanan', NULL, '', NULL, 0, 'fishing-services', 10, 2),
(210, 'Fishing Supplies', 'Ikan, Pasokan', NULL, '', NULL, 0, 'fishing-supplies', 10, 2),
(211, 'Flower', 'Bunga', NULL, '', NULL, 0, 'flower', 10, 3),
(212, 'Food Brokers', 'Makanan, Pialang', NULL, '', NULL, 0, 'food-brokers', 10, 10),
(213, 'Food Processing', 'Makanan, Pengolahan', NULL, '', NULL, 0, 'food-processing', 10, 10),
(214, 'Food Products - Dehydrated', 'Makanan Diawetkan', NULL, '', NULL, 0, 'food-products-dehydrated', 10, 10),
(215, 'Food Seasonings', 'Bumbu Masak', NULL, '', NULL, 0, 'food-seasonings', 10, 10),
(216, 'Food Service Management', 'Makanan, Pelayanan', NULL, '', NULL, 0, 'food-service-management', 10, 10),
(217, 'Forensic Pathologist', 'Ahli Forensik', NULL, '', NULL, 0, 'forensic-pathologist', 10, 19),
(218, 'Forest Products', 'Hutan, Hasil', NULL, '', NULL, 0, 'forest-products', 10, 2),
(219, 'Forestry', 'Kehutanan', NULL, '', NULL, 0, 'forestry', 10, 2),
(220, 'Formica', 'Formika', NULL, '', NULL, 0, 'formica', 10, 18),
(221, 'Foundries', 'Pengecoran', NULL, '', NULL, 0, 'foundries', 10, 15),
(222, 'Foundry Equipment', 'Alat Cor', NULL, '', NULL, 0, 'foundry-equipment-1', 10, 15),
(223, 'Frame & Picture', 'Pigura & Gambar', NULL, '', NULL, 0, 'frame-picture', 10, 3),
(224, 'Frozen Food', 'Makanan Beku', NULL, '', NULL, 0, 'frozen-food', 10, 10),
(225, 'Fruit Juices', 'Buah, Sari', NULL, '', NULL, 0, 'fruit-juices', 10, 10),
(226, 'Fruits & Vegetables', 'Buah & Sayur Mayur', NULL, '', NULL, 0, 'fruits-vegetables', 10, 2),
(227, 'Furniture Material', 'Perabot, Bahan', NULL, '', NULL, 0, 'furniture-material', 10, 13),
(228, 'Furniture Product', 'Perabot, Barang-barang', NULL, '', NULL, 0, 'furniture-product', 10, 13),
(229, 'Furniture Services', 'Perabot, Pelayanan', NULL, '', NULL, 0, 'furniture-services', 10, 13),
(230, 'Gasket', 'Penyekat', NULL, '', NULL, 0, 'gasket', 10, 4),
(231, 'Gelatine', 'Agar-Agar', NULL, '', NULL, 0, 'gelatine', 10, 10),
(232, 'Geophysicists', 'Ahli Geofisika', NULL, '', NULL, 0, 'geophysicists', 10, 19),
(233, 'Gifts', 'Hadiah', NULL, '', NULL, 0, 'gifts', 10, 3),
(234, 'Glass', 'Kaca', NULL, '', NULL, 0, 'glass', 10, 18),
(235, 'Glucose', 'Glukosa', NULL, '', NULL, 0, 'glucose', 10, 5),
(236, 'Government', 'Pemerintah', NULL, '', NULL, 0, 'government-1', 10, 11),
(237, 'Graphic Designers', 'Perancang Grafis', NULL, '', NULL, 0, 'graphic-designers', 10, 1),
(238, 'Gravier', 'Ukiran', NULL, '', NULL, 0, 'gravier', 10, 3),
(239, 'Grinding Machines & Vibrators', 'Mesin Giling & Alat Getar', NULL, '', NULL, 0, 'grinding-machines-vibrators', 10, 15),
(240, 'Grouting Compounds', 'Plester/Lantai,Bahan', NULL, '', NULL, 0, 'grouting-compounds', 10, 18),
(241, 'Gypsum', 'Gipsum', NULL, '', NULL, 0, 'gypsum', 10, 18),
(242, 'Haircare', 'Perawatan Rambut', NULL, '', NULL, 0, 'haircare', 10, 12),
(243, 'Hand Dryer', 'Pengering Tangan', NULL, '', NULL, 0, 'hand-dryer', 10, 7),
(244, 'Handicraft', 'Kerajinan Tangan', NULL, '', NULL, 0, 'handicraft', 10, 3),
(245, 'Hatchery', 'Penetasan', NULL, '', NULL, 0, 'hatchery', 10, 2),
(246, 'Health Center', 'Pusat Kesehatan', NULL, '', NULL, 0, 'health-center', 10, 12),
(247, 'Health Food', 'Makanan Kesehatan', NULL, '', NULL, 0, 'health-food', 10, 12),
(248, 'Heater', 'Pemanas', NULL, '', NULL, 0, 'heater', 10, 7),
(249, 'Helium', 'Helium', NULL, '', NULL, 0, 'helium', 10, 5),
(250, 'Herbal Products', 'Jamu', NULL, '', NULL, 0, 'herbal-products', 10, 12),
(251, 'Hobby & Model Construction', 'Hobi & Model, Konstruksi', NULL, '', NULL, 0, 'hobby-model-construction', 10, 21),
(252, 'Holding Companies', 'Pemegang Saham, Perusahaan', NULL, '', NULL, 0, 'holding-companies', 10, 9),
(253, 'Home Appliances', 'Peralatan Rumah', NULL, '', NULL, 0, 'home-appliances', 10, 7),
(254, 'Home Health Care', 'Peralatan Kesehatan Dirumah', NULL, '', NULL, 0, 'home-health-care', 10, 12),
(255, 'Home Services & Repairs', 'Rumah, Pelayanan & Perbaikan', NULL, '', NULL, 0, 'home-services-repairs', 10, 18),
(256, 'Hospital', 'Rumah Sakit', NULL, '', NULL, 0, 'hospital', 10, 12),
(257, 'Hostels', 'Penginapan', NULL, '', NULL, 0, 'hostels', 10, 14),
(258, 'Hotel & Motel', 'Hotel & Penginapan', NULL, '', NULL, 0, 'hotel-motel-1', 10, 14),
(259, 'Hotel & Motel Counsultant', 'Hotel & Penginapan, Konsultan', NULL, '', NULL, 0, 'hotel-motel-counsultant', 10, 14),
(260, 'Hotel & Motel Equipment', 'Hotel & Penginapan, Perlengkapan', NULL, '', NULL, 0, 'hotel-motel-equipment', 10, 14),
(261, 'House Maid Agency', 'Penyalur Pembantu', NULL, '', NULL, 0, 'house-maid-agency', 10, 19),
(262, 'Housewares', 'Rumah, Perlengkapan', NULL, '', NULL, 0, 'housewares', 10, 13),
(263, 'Housing', 'Perumahan', NULL, '', NULL, 0, 'housing', 10, 18),
(264, 'Hydraulic & Lifting Equipment', 'Hidrolik & Pengangkat - Perlengkapan', NULL, '', NULL, 0, 'hydraulic-lifting-equipment', 10, 15),
(265, 'Ice', 'Es ', NULL, '', NULL, 0, 'ice', 10, 10),
(266, 'Ice Cream', 'Es Krim', NULL, '', NULL, 0, 'ice-cream', 10, 10),
(267, 'Importer & Exporters', 'Impor & Ekspor', NULL, '', NULL, 0, 'importer-exporters', 10, 19),
(268, 'Industrial Equipment', 'Industri, Perlengkapan', NULL, '', NULL, 0, 'industrial-equipment', 10, 15),
(269, 'Industrial Estate', 'Kawasan Industri', NULL, '', NULL, 0, 'industrial-estate', 10, 18),
(270, 'Industrial Machinery', 'Industri - Permesinan', NULL, '', NULL, 0, 'industrial-machinery', 10, 15),
(271, 'Information Technology', 'Teknologi Informasi', NULL, '', NULL, 0, 'information-technology-1', 10, 16),
(272, 'Insecticides & Medicine Veterinary', 'Insektisida & Obat Hewan', NULL, '', NULL, 0, 'insecticides-medicine-veterinary', 10, 5),
(273, 'Inspection & Testing Services', 'Jasa Inspeksi & Pengujian', NULL, '', NULL, 0, 'inspection-testing-services', 10, 19),
(274, 'Insurance Brokers', 'Asuransi, Pialang', NULL, '', NULL, 0, 'insurance-brokers', 10, 9),
(275, 'Insurance Consultant', 'Asuransi, Konsultan', NULL, '', NULL, 0, 'insurance-consultant', 10, 9),
(276, 'Insurance Services', 'Asuransi, Pelayanan', NULL, '', NULL, 0, 'insurance-services', 10, 9),
(277, 'Intercommunications', 'Macam-macam Perlengkapan Komunikasi', NULL, '', NULL, 0, 'intercommunications', 10, 22),
(278, 'Interior Decorators & Designers', 'Dekorator & Perancang Bagian Dalam', NULL, '', NULL, 0, 'interior-decorators-designers', 10, 13),
(279, 'International School', 'Sekolah Internasional', NULL, '', NULL, 0, 'international-school', 10, 6),
(280, 'Internet Data', 'Data Internet', NULL, '', NULL, 0, 'internet-data', 10, 16),
(281, 'Internet Portal', 'Portal Internet', NULL, '', NULL, 0, 'internet-portal', 10, 16),
(282, 'Internet Provider', 'Internet, Penyedia Jasa', NULL, '', NULL, 0, 'internet-provider', 10, 16),
(283, 'Internet Services', 'Internet, Pelayanan', NULL, '', NULL, 0, 'internet-services-1', 10, 16),
(284, 'Investment Services', 'Penanaman Modal, Pelayanan', NULL, '', NULL, 0, 'investment-services', 10, 9),
(285, 'Irrigation', 'Perairan', NULL, '', NULL, 0, 'irrigation', 10, 2),
(286, 'Janitor Services', 'Kuras WC', NULL, '', NULL, 0, 'janitor-services', 10, 19),
(287, 'Karaoke', 'Karaoke', NULL, '', NULL, 0, 'karaoke', 10, 21),
(288, 'Karts Motorized', 'Mobil Balap', NULL, '', NULL, 0, 'karts-motorized', 10, 4),
(289, 'Kindergarten & Pre-School', 'Taman Kanak-kanak & Pra Sekolah', NULL, '', NULL, 0, 'kindergarten-pre-school', 10, 6),
(290, 'Kindergarten Supplies', 'Taman Kanak-kanak, Pasokan', NULL, '', NULL, 0, 'kindergarten-supplies', 10, 6),
(291, 'Kitchen & Kitchen Products', 'Dapur & Perlengkapan Dapur', NULL, '', NULL, 0, 'kitchen-kitchen-products', 10, 13),
(292, 'Kitchen Equipment', 'Dapur, Peralatan', NULL, '', NULL, 0, 'kitchen-equipment', 10, 7),
(293, 'Knives', 'Pisau', NULL, '', NULL, 0, 'knives', 10, 13),
(294, 'Laboratories', 'Laboratorium', NULL, '', NULL, 0, 'laboratories', 10, 12),
(295, 'Landscape', 'Pertamanan', NULL, '', NULL, 0, 'landscape', 10, 13),
(296, 'Language School Supplies', 'Sekolah Bahasa, Pasokan', NULL, '', NULL, 0, 'language-school-supplies', 10, 6),
(297, 'Lanterns', 'Lentera', NULL, '', NULL, 0, 'lanterns', 10, 13),
(298, 'Laser & X-Ray', 'Laser & Sinar X', NULL, '', NULL, 0, 'laser-x-ray', 10, 7),
(299, 'Laundries', 'Binatu', NULL, '', NULL, 0, 'laundries', 10, 8),
(300, 'Laundry Equipment', 'Binatu, Perlengkapan', NULL, '', NULL, 0, 'laundry-equipment', 10, 8),
(301, 'Leasing Services', 'Kontrak Sewa Beli, Layanan', NULL, '', NULL, 0, 'leasing-services', 10, 9),
(302, 'Legal Services', 'Legal, Pelayanan', NULL, '', NULL, 0, 'legal-services', 10, 19),
(303, 'Libraries', 'Perpustakaan', NULL, '', NULL, 0, 'libraries', 10, 6),
(304, 'Lighting Equipment', 'Penerang/Lampu, Perlengkapan', NULL, '', NULL, 0, 'lighting-equipment', 10, 7),
(305, 'Lithographers', 'Pembuat Litograf', NULL, '', NULL, 0, 'lithographers', 10, 19),
(306, 'Livestock Products', 'Peternakan, Hasil', NULL, '', NULL, 0, 'livestock-products', 10, 2),
(307, 'Livestock Supplies', 'Peternakan, Pasokan', NULL, '', NULL, 0, 'livestock-supplies', 10, 2),
(308, 'Locks', 'Kunci', NULL, '', NULL, 0, 'locks', 10, 18),
(309, 'Lounge', 'Kamar/Ruang Tunggu', NULL, '', NULL, 0, 'lounge', 10, 18),
(310, 'Machinery', 'Permesinan', NULL, '', NULL, 0, 'machinery', 10, 15),
(311, 'Magician', 'Tukang Sulap', NULL, '', NULL, 0, 'magician', 10, 21),
(312, 'Mail Order', 'Pesanan Lewat Pos', NULL, '', NULL, 0, 'mail-order', 10, 22),
(313, 'Mailing Lists', 'Daftar Kiriman', NULL, '', NULL, 0, 'mailing-lists', 10, 22),
(314, 'Mannequin', 'Boneka Peraga', NULL, '', NULL, 0, 'mannequin', 10, 8),
(315, 'Manual Books', 'Buku, Manual', NULL, '', NULL, 0, 'manual-books', 10, 6),
(316, 'Marine Service', 'Kapal, Pelayanan', NULL, '', NULL, 0, 'marine-service', 10, 22),
(317, 'Marine Services', 'Kapal, Pelayanan', NULL, '', NULL, 0, 'marine-services', 10, 22),
(318, 'Marketing & Trading', 'Pemasaran & Perdagangan', NULL, '', NULL, 0, 'marketing-trading', 10, 19),
(319, 'Marriage Bureau', 'Biro Jodoh', NULL, '', NULL, 0, 'marriage-bureau', 10, 19),
(320, 'Mattresses', 'Kasur ', NULL, '', NULL, 0, 'mattresses', 10, 13),
(321, 'Meat', 'Daging', NULL, '', NULL, 0, 'meat', 10, 10),
(322, 'Medical Equipment', 'Medis, Perlengkapan', NULL, '', NULL, 0, 'medical-equipment', 10, 12),
(323, 'Medical Services', 'Medis, Layanan', NULL, '', NULL, 0, 'medical-services', 10, 12),
(324, 'Medical Traditional & Alternative', 'Pengobatan Tradisional & Alternatif', NULL, '', NULL, 0, 'medical-traditional-alternative', 10, 12),
(325, 'Medicine Veterinary', 'Obat Hewan', NULL, '', NULL, 0, 'medicine-veterinary', 10, 2),
(326, 'Metal Services', 'Logam - Layanan', NULL, '', NULL, 0, 'metal-services', 10, 15),
(327, 'Military Equipment', 'Militer - Perlengkapan', NULL, '', NULL, 0, 'military-equipment', 10, 15),
(328, 'Milk & Milk Products', 'Susu & Hasil Susu', NULL, '', NULL, 0, 'milk-milk-products', 10, 10),
(329, 'Mini Market', 'Pasar Swalayan Mini', NULL, '', NULL, 0, 'mini-market', 10, 20),
(330, 'Mining & Quarrying Products', 'Pertambangan & Penggalian, Hasil', NULL, '', NULL, 0, 'mining-quarrying-products', 10, 17),
(331, 'Mining & Quarrying Supplies', 'Pertambangan & Penggalian, Pasokan', NULL, '', NULL, 0, 'mining-quarrying-supplies', 10, 17),
(332, 'Mining Companies', 'Pertambangan - Perusahaan', NULL, '', NULL, 0, 'mining-companies', 10, 17),
(333, 'Mining Services', 'Pertambangan - Layanan', NULL, '', NULL, 0, 'mining-services', 10, 17),
(334, 'Mirrors', 'Cermin', NULL, '', NULL, 0, 'mirrors', 10, 13),
(335, 'Miscellanaeous Beverages Products', 'Macam-macam Minuman', NULL, '', NULL, 0, 'miscellanaeous-beverages-products', 10, 10),
(336, 'Miscellaneous Beds & Beddings', 'Macam-macam Tempat Tidur & Perlengkapan Tempat Tidur+F92', NULL, '', NULL, 0, 'miscellaneous-beds-beddings', 10, 13),
(337, 'Miscellaneous Building Materials', 'Macam-macam Bahan Bangunan', NULL, '', NULL, 0, 'miscellaneous-building-materials', 10, 18),
(338, 'Miscellaneous Communication Equipment', 'Macam-macam Perlengkapan Komunikasi', NULL, '', NULL, 0, 'miscellaneous-communication-equipment', 10, 22),
(339, 'Miscellaneous Communication Services', 'Macam-macam Pelayanan Komunikasi', NULL, '', NULL, 0, 'miscellaneous-communication-services', 10, 22),
(340, 'Miscellaneous Electronic Product & Services', 'Macam-macam Produk Elektronik & Pelayanan', NULL, '', NULL, 0, 'miscellaneous-electronic-product-services', 10, 7),
(341, 'Miscellaneous Food Products', 'Macam-macam Makanan', NULL, '', NULL, 0, 'miscellaneous-food-products', 10, 10),
(342, 'Miscellaneous Heavy Equipment', 'Macam-macam Alat Berat', NULL, '', NULL, 0, 'miscellaneous-heavy-equipment', 10, 15),
(343, 'Model', 'Model', NULL, '', NULL, 0, 'model', 10, 1),
(344, 'Money Changers', 'Pedagang Valuta Asing', NULL, '', NULL, 0, 'money-changers', 10, 19),
(345, 'Monuments & Museums', 'Monumen & Museum', NULL, '', NULL, 0, 'monuments-museums', 10, 21),
(346, 'Motion Picture', 'Film/Gambar Hidup', NULL, '', NULL, 0, 'motion-picture', 10, 1),
(347, 'Motorcar', 'Mobil', NULL, '', NULL, 0, 'motorcar', 10, 4),
(348, 'Motorcar Body', 'Mobil, Badan', NULL, '', NULL, 0, 'motorcar-body', 10, 4),
(349, 'Motorcar Rental & Leasing', 'Mobil - Sewa & Kontrak Sewa', NULL, '', NULL, 0, 'motorcar-rental-leasing', 10, 4),
(350, 'Motorcar Service Station', 'Mobil - Bengkel', NULL, '', NULL, 0, 'motorcar-service-station', 10, 4),
(351, 'Motorcycle Rental & Leasing', 'Sepeda Motor - Sewa & Kontrak Sewa', NULL, '', NULL, 0, 'motorcycle-rental-leasing', 10, 4),
(352, 'Motorcycles & Motorcycle Service', 'Sepeda Motor, Pelayanan', NULL, '', NULL, 0, 'motorcycles-motorcycle-service', 10, 4),
(353, 'Motorcycles & Motorscooters', 'Sepeda Motor & Skuter', NULL, '', NULL, 0, 'motorcycles-motorscooters', 10, 4),
(354, 'Mouldings', 'Mencetak', NULL, '', NULL, 0, 'mouldings', 10, 15),
(355, 'Moving & Storage Services', 'Pemindahan & Penyimpanan, Pelayanan', NULL, '', NULL, 0, 'moving-storage-services', 10, 18),
(356, 'Multimedia', 'Multimedia', NULL, '', NULL, 0, 'multimedia', 10, 1),
(357, 'Music', 'Musik', NULL, '', NULL, 0, 'music', 10, 21),
(358, 'Musical School Supplies', 'Sekolah Musik, Pasokan', NULL, '', NULL, 0, 'musical-school-supplies', 10, 6),
(359, 'Nails & Tacks', 'Paku & Paku Jamur', NULL, '', NULL, 0, 'nails-tacks', 10, 18),
(360, 'Noodles', 'Mi', NULL, '', NULL, 0, 'noodles', 10, 10),
(361, 'Nursery', 'Tempat Pembibitan', NULL, '', NULL, 0, 'nursery', 10, 2),
(362, 'Oceanariums', 'Gelanggang Samudera', NULL, '', NULL, 0, 'oceanariums', 10, 21),
(363, 'Office', 'Kantor', NULL, '', NULL, 0, 'office', 10, 18),
(364, 'Office Equipment & Supplies', 'Kantor, Perlengkapan & Pasokan', NULL, '', NULL, 0, 'office-equipment-supplies', 10, 13),
(365, 'Office Furniture', 'Perabot Kantor', NULL, '', NULL, 0, 'office-furniture', 10, 13),
(366, 'Office Machine', 'Kantor - Mesin', NULL, '', NULL, 0, 'office-machine', 10, 13),
(367, 'Oil & Gas Appliances', 'Minyak & Gas, Peralatan', NULL, '', NULL, 0, 'oil-gas-appliances', 10, 17),
(368, 'Oil & Gas Production', 'Minyak & Gas - Produksi', NULL, '', NULL, 0, 'oil-gas-production', 10, 17);
INSERT INTO `category` (`id`, `name_len`, `name_lid`, `description_len`, `description_lid`, `classification`, `popular`, `slug`, `weight`, `category_id`) VALUES
(369, 'Oil & Gas Products', 'Minyak & Gas, Hasil', NULL, '', NULL, 0, 'oil-gas-products', 10, 17),
(370, 'Oil & Gas Services', 'Minyak & Gas - Layanan', NULL, '', NULL, 0, 'oil-gas-services', 10, 17),
(371, 'Oil Field', 'Ladang Minyak', NULL, '', NULL, 0, 'oil-field', 10, 17),
(372, 'Oil For Food', 'Minyak Makanan', NULL, '', NULL, 0, 'oil-for-food', 10, 10),
(373, 'Optical Goods', 'Optik, Barang', NULL, '', NULL, 0, 'optical-goods', 10, 12),
(374, 'Other Accommodation', 'Akomodasi Lain', NULL, '', NULL, 0, 'other-accommodation', 10, 14),
(375, 'Other Agricultural Supplies', 'Pertanian, Pasokan Lainnya', NULL, '', NULL, 0, 'other-agricultural-supplies', 10, 2),
(376, 'Other Business Services', 'Bisnis, Pelayanan Lain-lain', NULL, '', NULL, 0, 'other-business-services', 10, 19),
(377, 'Other Construction Services', 'Kontstruksi - Jasa Lainnya', NULL, '', NULL, 0, 'other-construction-services', 10, 18),
(378, 'Other Financial Services', 'Keuangan - Jasa Lainnya', NULL, '', NULL, 0, 'other-financial-services', 10, 9),
(379, 'Other Industrial Product', 'Industri - Produk Lainnya', NULL, '', NULL, 0, 'other-industrial-product', 10, 15),
(380, 'Other Industrial Services', 'Industri - Layanan Lainnya', NULL, '', NULL, 0, 'other-industrial-services', 10, 15),
(381, 'Other Raw Materials', 'Bahan Baku Makanan Lainnya', NULL, '', NULL, 0, 'other-raw-materials', 10, 10),
(382, 'Outboard Motors', 'Motor Tempel', NULL, '', NULL, 0, 'outboard-motors', 10, 4),
(383, 'Oxygen', 'Zat Asam/Oksigen', NULL, '', NULL, 0, 'oxygen', 10, 5),
(384, 'Packaging', 'Kemasan', NULL, '', NULL, 0, 'packaging', 10, 15),
(385, 'Paint & Painter Equipment', 'Cat & Perlengkapan Pengecatan', NULL, '', NULL, 0, 'paint-painter-equipment', 10, 18),
(386, 'Paper', 'Kertas', NULL, '', NULL, 0, 'paper', 10, 15),
(387, 'Parking Service', 'Parkir - Layanan', NULL, '', NULL, 0, 'parking-service', 10, 18),
(388, 'Partitions', 'Sekat', NULL, '', NULL, 0, 'partitions', 10, 18),
(389, 'Peanut Products', 'Makanan Dari Kacang Tanah', NULL, '', NULL, 0, 'peanut-products', 10, 10),
(390, 'Pension Fund', 'Dana Pensiun', NULL, '', NULL, 0, 'pension-fund', 10, 9),
(391, 'Perfumery', 'Senyawa Wewangian', NULL, '', NULL, 0, 'perfumery', 10, 5),
(392, 'Perfumes', 'Parfum', NULL, '', NULL, 0, 'perfumes', 10, 5),
(393, 'Personal Supplies', 'Perawatan Pribadi, Pasokan', NULL, '', NULL, 0, 'personal-supplies', 10, 12),
(394, 'Pest Control', 'Hama, Pengendalian', NULL, '', NULL, 0, 'pest-control', 10, 2),
(395, 'Pet Specialities', 'Hewan Peliharaan, Kekhususan', NULL, '', NULL, 0, 'pet-specialities', 10, 2),
(396, 'Petrol & Oil', 'Bahan Bakar & Minyak', NULL, '', NULL, 0, 'petrol-oil', 10, 4),
(397, 'Pharmaceutical Products', 'Farmasi, Barang-barang', NULL, '', NULL, 0, 'pharmaceutical-products', 10, 12),
(398, 'Photograpic', 'Fotografi', NULL, '', NULL, 0, 'photograpic', 10, 3),
(399, 'Physicians', 'Para Normal', NULL, '', NULL, 0, 'physicians', 10, 19),
(400, 'Piling', 'Tiang Pancang', NULL, '', NULL, 0, 'piling', 10, 18),
(401, 'Pipe Services', 'Pipa - Layanan', NULL, '', NULL, 0, 'pipe-services', 10, 15),
(402, 'Pipeline', 'Pipa Saluran', NULL, '', NULL, 0, 'pipeline', 10, 18),
(403, 'Plants', 'Tanaman', NULL, '', NULL, 0, 'plants', 10, 2),
(404, 'Playground', 'Taman Bermain', NULL, '', NULL, 0, 'playground', 10, 21),
(405, 'Polishes', 'Pengkilap', NULL, '', NULL, 0, 'polishes', 10, 5),
(406, 'Polyethylene & Polyurethane', 'Polietilena & Poliuretan', NULL, '', NULL, 0, 'polyethylene-polyurethane', 10, 5),
(407, 'Port Services', 'Pelabuhan, Jasa', NULL, '', NULL, 0, 'port-services', 10, 22),
(408, 'Postal Products', 'Pos, Barang', NULL, '', NULL, 0, 'postal-products', 10, 22),
(409, 'Poultry', 'Ayam', NULL, '', NULL, 0, 'poultry', 10, 2),
(410, 'Power Supply Equipment & Services', 'Tenaga Listrik, Perlengkapan & Pelayanan', NULL, '', NULL, 0, 'power-supply-equipment-services', 10, 7),
(411, 'Printers', 'Pencetak', NULL, '', NULL, 0, 'printers', 10, 7),
(412, 'Printing & Publishing', 'Pencetakan & Penerbitan', NULL, '', NULL, 0, 'printing-publishing', 10, 1),
(413, 'Production House', 'Rumah Produksi', NULL, '', NULL, 0, 'production-house', 10, 1),
(414, 'Products Designers', 'Produk - Perancang', NULL, '', NULL, 0, 'products-designers', 10, 15),
(415, 'Profesional Health Service', 'Pelayanan Kesehatan Profesional', NULL, '', NULL, 0, 'profesional-health-service', 10, 12),
(416, 'Promotion Services', 'Promosi, Pelayanan', NULL, '', NULL, 0, 'promotion-services', 10, 1),
(417, 'Property Management', 'Manajemen Kepemilikan', NULL, '', NULL, 0, 'property-management', 10, 18),
(418, 'Property Professional Services', 'Properti - Layanan Profesional', NULL, '', NULL, 0, 'property-professional-services', 10, 18),
(419, 'Quarrying', 'Penggalian', NULL, '', NULL, 0, 'quarrying', 10, 17),
(420, 'Racks', 'Rak', NULL, '', NULL, 0, 'racks', 10, 13),
(421, 'Radar & Navigation Equipment', 'Navigasi, Perlengkapan', NULL, '', NULL, 0, 'radar-navigation-equipment', 10, 22),
(422, 'Radiator', 'Radiator', NULL, '', NULL, 0, 'radiator', 10, 4),
(423, 'Radio Communication', 'Radio - Komunikasi', NULL, '', NULL, 0, 'radio-communication', 10, 22),
(424, 'Radio Paging', 'Radio Panggil', NULL, '', NULL, 0, 'radio-paging', 10, 22),
(425, 'Railroad Equipment', 'Kereta Api, Perlengkapan', NULL, '', NULL, 0, 'railroad-equipment', 10, 22),
(426, 'Railroad Transportation', 'Kereta Api', NULL, '', NULL, 0, 'railroad-transportation', 10, 22),
(427, 'Real Estate', 'Real Estate/ Tanah & Bangunan', NULL, '', NULL, 0, 'real-estate', 10, 18),
(428, 'Recreation Centres', 'Pusat Rekreasi', NULL, '', NULL, 0, 'recreation-centres', 10, 21),
(429, 'Refrigerators & Freezers', 'Lemari Es & Lemari Pembeku', NULL, '', NULL, 0, 'refrigerators-freezers', 10, 7),
(430, 'Religious', 'Keagamaan', NULL, '', NULL, 0, 'religious', 10, 19),
(431, 'Resorts', 'Peristirahatan Tepi Pantai', NULL, '', NULL, 0, 'resorts', 10, 14),
(432, 'Restaurants', 'Rumah Makan', NULL, '', NULL, 0, 'restaurants', 10, 10),
(433, 'Restaurants & Bars Supplies', 'Rumah Makan & Bar, Pasokan', NULL, '', NULL, 0, 'restaurants-bars-supplies', 10, 10),
(434, 'Rice', 'Beras', NULL, '', NULL, 0, 'rice', 10, 10),
(435, 'Rice Products', 'Makanan Dari Beras', NULL, '', NULL, 0, 'rice-products', 10, 10),
(436, 'Road Building', 'Pembangunan Jalan', NULL, '', NULL, 0, 'road-building', 10, 18),
(437, 'Rollers', 'Kerai Gulung', NULL, '', NULL, 0, 'rollers', 10, 18),
(438, 'Roofing Materials', 'Atap, Bahan', NULL, '', NULL, 0, 'roofing-materials', 10, 18),
(439, 'Rubber & Plastic Products', 'Karet & Plastik, Hasil', NULL, '', NULL, 0, 'rubber-plastic-products', 10, 15),
(440, 'Rubber Estate Equipment', 'Kebun Karet - Perlengkapan', NULL, '', NULL, 0, 'rubber-estate-equipment', 10, 2),
(441, 'Rust Preventive', 'Anti Karat', NULL, '', NULL, 0, 'rust-preventive', 10, 4),
(442, 'Safes & Vaults', 'Lemari Besi & Ruangan Besi', NULL, '', NULL, 0, 'safes-vaults', 10, 13),
(443, 'Safety Equipment', 'Pengaman, Perlengkapan', NULL, '', NULL, 0, 'safety-equipment', 10, 15),
(444, 'Sandpaper', 'Amplas', NULL, '', NULL, 0, 'sandpaper', 10, 18),
(445, 'Sanitary Equipment For Industrial', 'Kebersihan - Perlengkapan Untuk Industri', NULL, '', NULL, 0, 'sanitary-equipment-for-industrial', 10, 15),
(446, 'Sanitary Services', 'Kebersihan, Pelayanan', NULL, '', NULL, 0, 'sanitary-services', 10, 13),
(447, 'Sanitary Ware', 'Kebersihan, Perlengkapan', NULL, '', NULL, 0, 'sanitary-ware', 10, 13),
(448, 'Sanitation Services', 'Sanitasi, Pelayanan', NULL, '', NULL, 0, 'sanitation-services', 10, 19),
(449, 'Sausage', 'Sosis', NULL, '', NULL, 0, 'sausage', 10, 10),
(450, 'Scalp Treatments', 'Perawatan Kulit Kepala', NULL, '', NULL, 0, 'scalp-treatments', 10, 12),
(451, 'School - Elementary', 'Sekolah Dasar', NULL, '', NULL, 0, 'school-elementary', 10, 6),
(452, 'School - General', 'Sekolah Umum', NULL, '', NULL, 0, 'school-general', 10, 6),
(453, 'School - Junior High', 'Sekolah Menengah Pertama', NULL, '', NULL, 0, 'school-junior-high', 10, 6),
(454, 'School - Senior High', 'Sekolah Menengah Atas', NULL, '', NULL, 0, 'school-senior-high', 10, 6),
(455, 'School - Special Purpose', 'Sekolah Khusus', NULL, '', NULL, 0, 'school-special-purpose', 10, 6),
(456, 'School Information & Placement Bureaus', 'Sekolah, Informasi & Penempatan Belajar', NULL, '', NULL, 0, 'school-information-placement-bureaus', 10, 6),
(457, 'School Supplies - General', 'Sekolah, Pasokan - Umum', NULL, '', NULL, 0, 'school-supplies-general', 10, 6),
(458, 'Scientific Apparatus', 'Ilmiah, Perlengkapan', NULL, '', NULL, 0, 'scientific-apparatus', 10, 6),
(459, 'Security & Protection Products', 'Keamanan & Proteksi, Perlengkapan', NULL, '', NULL, 0, 'security-protection-products', 10, 7),
(460, 'Security & Safety', 'Keamanan & Keselamatan', NULL, '', NULL, 0, 'security-safety', 10, 19),
(461, 'Seeds & Bulbs', 'Bibit & Umbi', NULL, '', NULL, 0, 'seeds-bulbs', 10, 2),
(462, 'Automotive - Other Services', 'Otomotif - Layanan Lain', NULL, '', NULL, 0, 'automotive-other-services', 10, 4),
(463, 'Ship & Shipping Equipment', 'Kapal & Perkapalan, Perlengkapan', NULL, '', NULL, 0, 'ship-shipping-equipment', 10, 22),
(464, 'Shipping', 'Perkapalan', NULL, '', NULL, 0, 'shipping', 10, 22),
(465, 'Shopping Centres', 'Pusat Perbelanjaan', NULL, '', NULL, 0, 'shopping-centres', 10, 20),
(466, 'Silicones', 'Silikon', NULL, '', NULL, 0, 'silicones', 10, 5),
(467, 'Skin Treatments', 'Perawatan Kulit', NULL, '', NULL, 0, 'skin-treatments', 10, 12),
(468, 'Slipper Reflexion', 'Sandal Refleksi', NULL, '', NULL, 0, 'slipper-reflexion', 10, 12),
(469, 'Soap & Detergents', 'Sabun & Deterjen', NULL, '', NULL, 0, 'soap-detergents', 10, 5),
(470, 'Social Services', 'Panti Asuhan', NULL, '', NULL, 0, 'social-services', 10, 19),
(471, 'Soft Drinks', 'Minuman Ringan', NULL, '', NULL, 0, 'soft-drinks', 10, 10),
(472, 'Souvenirs', 'Cinderamata', NULL, '', NULL, 0, 'souvenirs', 10, 3),
(473, 'Spa & Sauna', 'Spa & Mandi Uap', NULL, '', NULL, 0, 'spa-sauna', 10, 12),
(474, 'Space Research & Development', 'Angkasa, Ruang - Penelitian & Pengembangan', NULL, '', NULL, 0, 'space-research-development', 10, 19),
(475, 'Spare Parts', 'Onderdil', NULL, '', NULL, 0, 'spare-parts', 10, 4),
(476, 'Sport Center', 'Pusat Olahraga', NULL, '', NULL, 0, 'sport-center', 10, 21),
(477, 'Sporting Goods', 'Olahraga, Perlengkapan', NULL, '', NULL, 0, 'sporting-goods', 10, 21),
(478, 'Sports & Sport Club', 'Olahraga & Klub Olahraga', NULL, '', NULL, 0, 'sports-sport-club', 10, 21),
(479, 'Stairs', 'Tangga', NULL, '', NULL, 0, 'stairs', 10, 18),
(480, 'Stationery', 'Alat Tulis', NULL, '', NULL, 0, 'stationery', 10, 13),
(481, 'Stock Exchanges', 'Bursa Efek', NULL, '', NULL, 0, 'stock-exchanges', 10, 9),
(482, 'Stone Clay Glass & Concrete Products', 'Batu, Tanah Liat, Gelas & Beton - Hasil', NULL, '', NULL, 0, 'stone-clay-glass-concrete-products', 10, 15),
(483, 'Stone Crusher', 'Pemecah Batu', NULL, '', NULL, 0, 'stone-crusher', 10, 15),
(484, 'Storage Document & Record', 'Dokumen & Arsip - Penyimpanan ', NULL, '', NULL, 0, 'storage-document-record', 10, 16),
(485, 'Sugar', 'Gula', NULL, '', NULL, 0, 'sugar', 10, 10),
(486, 'Sundries Stores', 'Toko Kelontong', NULL, '', NULL, 0, 'sundries-stores', 10, 20),
(487, 'Supermarket', 'Pasar Swalayan', NULL, '', NULL, 0, 'supermarket', 10, 20),
(488, 'Survey & Research', 'Survei & Penelitian', NULL, '', NULL, 0, 'survey-research', 10, 19),
(489, 'Swallow Bird', 'Burung Walet', NULL, '', NULL, 0, 'swallow-bird', 10, 2),
(490, 'Swimming Pool Equipment', 'Kolam Renang, Perlengkapan', NULL, '', NULL, 0, 'swimming-pool-equipment', 10, 18),
(491, 'Swimming Pool Services', 'Kolam Renang, Pelayanan', NULL, '', NULL, 0, 'swimming-pool-services', 10, 18),
(492, 'Table Clothes & Covers', 'Taplak & Kain Penutup Meja', NULL, '', NULL, 0, 'table-clothes-covers', 10, 13),
(493, 'Tableware', 'Makan & Minum, Peralatan', NULL, '', NULL, 0, 'tableware', 10, 13),
(494, 'Tank Cleaning', 'Tangki, Pembersihan', NULL, '', NULL, 0, 'tank-cleaning', 10, 15),
(495, 'Tank Lining & Coating', 'Tangki, Penyaluran & Pelapisan', NULL, '', NULL, 0, 'tank-lining-coating', 10, 15),
(496, 'Tanners', 'Penyamak', NULL, '', NULL, 0, 'tanners', 10, 15),
(497, 'Tax Services', 'Pajak, Pelayanan', NULL, '', NULL, 0, 'tax-services', 10, 9),
(498, 'Tekstil, Pabrik - Pasokan', 'Textile Mill Supplies', NULL, '', NULL, 0, 'tekstil-pabrik-pasokan', 10, 8),
(499, 'Telecommunication Equipment', 'Telekomunikasi, Perlengkapan', NULL, '', NULL, 0, 'telecommunication-equipment', 10, 22),
(500, 'Telecommunication Services', 'Telekomunikasi, Pelayanan', NULL, '', NULL, 0, 'telecommunication-services', 10, 22),
(501, 'Textile Products', 'Barang Tekstil', NULL, '', NULL, 0, 'textile-products', 10, 8),
(502, 'Tiles, Floors & Bathroom Equipment', 'Ubin, Lantai & Perlengkapan Kamar Mandi', NULL, '', NULL, 0, 'tiles-floors-bathroom-equipment', 10, 18),
(503, 'Timber', 'Kayu', NULL, '', NULL, 0, 'timber', 10, 18),
(504, 'Toiletries', 'Toilet - Perlengkapan', NULL, '', NULL, 0, 'toiletries', 10, 13),
(505, 'Tours & Cruises', 'Pariwisata & Pesiar', NULL, '', NULL, 0, 'tours-cruises', 10, 21),
(506, 'Toys', 'Mainan', NULL, '', NULL, 0, 'toys', 10, 21),
(507, 'Toys Service', 'Mainan, Pelayanan', NULL, '', NULL, 0, 'toys-service', 10, 21),
(508, 'Tractor', 'Traktor', NULL, '', NULL, 0, 'tractor', 10, 15),
(509, 'Traffic Equipment', 'Lalu Lintas - Perlengkapan', NULL, '', NULL, 0, 'traffic-equipment', 10, 22),
(510, 'Trailers', 'Gandengan', NULL, '', NULL, 0, 'trailers', 10, 4),
(511, 'Training Services', 'Pelatihan, Pelayanan', NULL, '', NULL, 0, 'training-services', 10, 6),
(512, 'Translators & Interpreters', 'Penerjemah & Juru Bahasa', NULL, '', NULL, 0, 'translators-interpreters', 10, 19),
(513, 'Transport Cargo', 'Pengangkutan Barang', NULL, '', NULL, 0, 'transport-cargo-1', 10, 22),
(514, 'Transport Services', 'Pengangkutan, Pelayanan', NULL, '', NULL, 0, 'transport-services', 10, 22),
(515, 'Travel Agencies & Bureaus', 'Perjalanan, Agen & Biro', NULL, '', NULL, 0, 'travel-agencies-bureaus', 10, 22),
(516, 'Trophy & Medals', 'Piala & Medali', NULL, '', NULL, 0, 'trophy-medals', 10, 3),
(517, 'Truck', 'Truk', NULL, '', NULL, 0, 'truck', 10, 4),
(518, 'Turbochargers', 'Mesin Turbo', NULL, '', NULL, 0, 'turbochargers', 10, 4),
(519, 'Underwear', 'Pakaian Dalam', NULL, '', NULL, 0, 'underwear', 10, 8),
(520, 'Ventilating Equipment', 'Ventilasi, Perlengkapan', NULL, '', NULL, 0, 'ventilating-equipment-1', 10, 18),
(521, 'Venture Capital', 'Modal Bersama', NULL, '', NULL, 0, 'venture-capital', 10, 9),
(522, 'Video', 'Video', NULL, '', NULL, 0, 'video', 10, 1),
(523, 'Wallcovers', 'Dinding, Pelapis', NULL, '', NULL, 0, 'wallcovers', 10, 18),
(524, 'Warehouses', 'Gudang', NULL, '', NULL, 0, 'warehouses', 10, 18),
(525, 'Washing Machines', 'Mesin Cuci', NULL, '', NULL, 0, 'washing-machines', 10, 7),
(526, 'Water - Mineral', 'Air Mineral', NULL, '', NULL, 0, 'water-mineral', 10, 10),
(527, 'Water Processing Equipment', 'Air, Pengolah', NULL, '', NULL, 0, 'water-processing-equipment', 10, 7),
(528, 'Web Design', 'Web, Merancang', NULL, '', NULL, 0, 'web-design', 10, 1),
(529, 'Wedding Supplies', 'Perkawinan, Pasokan', NULL, '', NULL, 0, 'wedding-supplies', 10, 8),
(530, 'Welding', 'Las/Pengelasan', NULL, '', NULL, 0, 'welding', 10, 15),
(531, 'Woodworkers', 'Tukang Kayu', NULL, '', NULL, 0, 'woodworkers', 10, 18),
(532, 'Worm', 'Cacing', NULL, '', NULL, 0, 'worm', 10, 2),
(533, 'Yeast', 'Ragi', NULL, '', NULL, 0, 'yeast', 10, 5),
(534, 'Zinc', 'Seng', NULL, '', NULL, 0, 'zinc', 10, 18),
(535, 'Zoological Gardens', 'Kebun Binatang', NULL, '', NULL, 0, 'zoological-gardens', 10, 21),
(536, '24 Hours Centre', 'Pusat (Layanan) 24 Jam', NULL, '', '26606T', 0, '24-hours-centre', 10, 376),
(537, 'Abattoir', 'Rumah Pemotongan Hewan', NULL, '', 'A3275M', 0, 'abattoir-1', 10, 23),
(538, 'Abrasives', 'Gerinda,batu', NULL, '', 'A1002A', 0, 'abrasives', 10, 482),
(539, 'Accesories Manufacturer', 'Aksesori - Produsen', NULL, '', 'A4130F', 0, 'accesories-manufacturer', 10, 26),
(540, 'Access Control System', 'Pengendali Akses, Sistem', NULL, '', 'A3573N', 0, 'access-control-system', 10, 459),
(541, 'Accessories Retail', 'Aksesori - Eceran', NULL, '', 'A4129D', 0, 'accessories-retail', 10, 26),
(542, 'Accessories Supplies', 'Aksesori - Pasokan', NULL, '', 'A4131H', 0, 'accessories-supplies', 10, 26),
(543, 'Accommodation', 'Akomodasi', NULL, '', 'A3699C', 0, 'accommodation', 10, 374),
(544, 'Accountants - Public', 'Akuntan Umum', NULL, '', 'A4858K', 0, 'accountants-public', 10, 27),
(545, 'Accountants Services', 'Akuntan Pelayanan', NULL, '', 'A4132K', 0, 'accountants-services', 10, 27),
(546, 'Accounting & Bookkeeping, Machines', 'Akunting & Pembukuan, Mesin', NULL, '', 'A4133M', 0, 'accounting-bookkeeping-machines', 10, 366),
(547, 'Accounting Supplies', 'Akunting Pasokan', NULL, '', 'A4134N', 0, 'accounting-supplies', 10, 366),
(548, 'Acetylene Manufacture', 'Asetilene/Gas Karnit-Produsen', NULL, '', 'A4136T', 0, 'acetylene-manufacture', 10, 28),
(549, 'Acetylene Materials', 'Asetilene/Gas Karbit-Bahan Material', NULL, '', 'A4135R', 0, 'acetylene-materials', 10, 28),
(550, 'Acetylene Supplies', 'Asetilene/Gas Karbit-Pasokan', NULL, '', 'A4137A', 0, 'acetylene-supplies', 10, 28),
(551, 'Acoustic Consultant', 'Konsultan Akustik', NULL, '', 'A4119F', 0, 'acoustic-consultant', 10, 141),
(552, 'Acoustical Materials', 'Akustik - Bahan/Material', NULL, '', 'A1006F', 0, 'acoustical-materials', 10, 198),
(553, 'Acrylic - Sheet', 'Akrilik - Lembar', NULL, '', 'A4227C', 0, 'acrylic-sheet', 10, 482),
(554, 'Acrylic Product', 'Akrilik, Produk', NULL, '', 'A7217Z', 0, 'acrylic-product', 10, 482),
(555, 'Actuaries', 'Aktuaris - Biro', NULL, '', 'A1007H', 0, 'actuaries-1', 10, 29),
(556, 'Acupuncture', 'Akupunktur', NULL, '', 'A3948R', 0, 'acupuncture', 10, 415),
(557, 'Addressing Machines & Supplies', 'Alamat, Mesin & Pasokan', NULL, '', 'A1009M', 0, 'addressing-machines-supplies', 10, 366),
(558, 'Adhesives & Glues - For Consumer Use', 'Perekat & Lem - Untuk Keperluan Rumah Tangga', NULL, '', 'A3436D', 0, 'adhesives-glues-for-consumer-use', 10, 31),
(559, 'Adhesives & Glues - For Industrial Use', 'Perekat & Lem - Untuk Keperluan Industri', NULL, '', 'A1010N', 0, 'adhesives-glues-for-industrial-use', 10, 31),
(560, 'Adhesives & Gluing - Equipment', 'Perekat & Lem - Perlengkapan', NULL, '', 'A3513D', 0, 'adhesives-gluing-equipment', 10, 31),
(561, 'Adjusters', 'Pengatur, Alat', NULL, '', 'A1011R', 0, 'adjusters', 10, 145),
(562, 'Administration Bureaus', 'Administrasi, Biro', NULL, '', 'A1012T', 0, 'administration-bureaus', 10, 32),
(563, 'Administration, Archives', 'Administrasi Kearsipan', NULL, '', 'A3649M', 0, 'administration-archives', 10, 32),
(564, 'Administrative Consultants', 'Administrasi, Konsultan', NULL, '', 'A1013A', 0, 'administrative-consultants', 10, 32),
(565, 'Advertising - Agencies', 'Periklanan, Agen Biro', NULL, '', 'A4139C', 0, 'advertising-agencies', 10, 34),
(566, 'Advertising - Cinema', 'Iklan, Bioskop', NULL, '', 'A2924N', 0, 'advertising-cinema', 10, 33),
(567, 'Advertising - Counsellors', 'Periklanan, Penasehat', NULL, '', 'A4138B', 0, 'advertising-counsellors', 10, 34),
(568, 'Advertising - Directory & Guide', 'Periklanan - Buku Petunjuk & Pedoman', NULL, '', 'A1016D', 0, 'advertising-directory-guide', 10, 33),
(569, 'Advertising - Indoor', 'Periklanan - Dalam Ruang', NULL, '', 'A1018H', 0, 'advertising-indoor', 10, 33),
(570, 'Advertising - Internet', 'Periklanan - Internet', NULL, '', 'A3688C', 0, 'advertising-internet', 10, 33),
(571, 'Advertising - Media Representative', 'Periklanan - Wakil Media Asing', NULL, '', 'A4232M', 0, 'advertising-media-representative', 10, 34),
(572, 'Advertising - Motion Picture', 'Periklanan - Film', NULL, '', 'A1019K', 0, 'advertising-motion-picture', 10, 33),
(573, 'Advertising - Newspaper', 'Periklanan - Surat Kabar', NULL, '', 'A1020M', 0, 'advertising-newspaper', 10, 33),
(574, 'Advertising - Outdoor', 'Periklanan - Luar Ruang', NULL, '', 'A1021N', 0, 'advertising-outdoor', 10, 33),
(575, 'Advertising - Personnel Recruitment', 'Periklanan - Pengerahan Tenaga Kerja', NULL, '', 'A1022R', 0, 'advertising-personnel-recruitment', 10, 34),
(576, 'Advertising - Printing', 'Periklanan - Cetak', NULL, '', 'A3955H', 0, 'advertising-printing', 10, 33),
(577, 'Advertising - Radio', 'Periklanan - Radio', NULL, '', 'A1014B', 0, 'advertising-radio', 10, 33),
(578, 'Advertising Equipment', 'Periklanan, Perlengkapan', NULL, '', 'A7284B', 0, 'advertising-equipment', 10, 33),
(579, 'Advertising Specialties', 'Periklanan - Khusus', NULL, '', 'A1017F', 0, 'advertising-specialties', 10, 34),
(580, 'Advertising Supplier', 'Periklanan, Penyalur', NULL, '', 'A4256R', 0, 'advertising-supplier', 10, 34),
(581, 'Advocates - Lawyers', 'Pengacara - Hukum', NULL, '', 'A3766D', 0, 'advocates-lawyers', 10, 35),
(582, 'Aerial Ropeways', 'Tali Gantung Udara', NULL, '', 'A1024A', 0, 'aerial-ropeways', 10, 379),
(583, 'Aerial Surveying & Mapping', 'Udara - Survei & Pemetaan', NULL, '', 'A1025B', 0, 'aerial-surveying-mapping', 10, 488),
(584, 'Aerosols Product - Services', 'Obat Penyemprot - Product - Pelayanan', NULL, '', 'A4140D', 0, 'aerosols-product-services', 10, 272),
(585, 'Agricultural Chemicals', 'Pertanian, Bahan Kimia', NULL, '', 'A1027D', 0, 'agricultural-chemicals', 10, 36),
(586, 'Agricultural Consultants', 'Pertanian - Konsultan', NULL, '', 'A1028F', 0, 'agricultural-consultants', 10, 36),
(587, 'Agricultural Equipment & Supplies', 'Pertanian - Perlengkapan & Pasokan', NULL, '', 'A1029H', 0, 'agricultural-equipment-supplies', 10, 375),
(588, 'Agricultural Marketing', 'Pertanian, Pemasaran', NULL, '', 'A1030K', 0, 'agricultural-marketing', 10, 36),
(589, 'Agriculture', 'Pertanian', NULL, '', 'A3044M', 0, 'agriculture-1', 10, 36),
(590, 'Agriculture Machinery', 'Pertanian-Mesin', NULL, '', 'A7150R', 0, 'agriculture-machinery', 10, 310),
(591, 'Air Cargo Service', 'Udara, Pelayanan Angkutan Barang', NULL, '', 'A1031M', 0, 'air-cargo-service', 10, 513),
(592, 'Air Cleaner Manufacturer', 'Pembersih Udara-Produsen', NULL, '', 'A4143K', 0, 'air-cleaner-manufacturer', 10, 40),
(593, 'Air Cleaner Retail', 'Pembersih Udara-Eceran', NULL, '', 'A4141F', 0, 'air-cleaner-retail', 10, 40),
(594, 'Air Cleaner Wholesale', 'Pembersih Udara-Grosir', NULL, '', 'A4142H', 0, 'air-cleaner-wholesale', 10, 40),
(595, 'Air Cleaning Equipment', 'Udara Perlengkapan Pembersih', NULL, '', 'A4768F', 0, 'air-cleaning-equipment', 10, 40),
(596, 'Air Compressors Distributors', 'Angin, Kompresor - Penyalur', NULL, '', 'A4851T', 0, 'air-compressors-distributors', 10, 37),
(597, 'Air Compressors Manufacturer', 'Angin Kompresor-Produsen', NULL, '', 'A4146R', 0, 'air-compressors-manufacturer', 10, 37),
(598, 'Air Compressors Rental', 'Angin Kompresor - Sewa', NULL, '', 'A4848M', 0, 'air-compressors-rental', 10, 37),
(599, 'Air Compressors Repairing', 'Angin Kompresor - Perbaikan', NULL, '', 'A4849N', 0, 'air-compressors-repairing', 10, 462),
(600, 'Air Compressors Retail', 'Angin Kompresor-Eceran', NULL, '', 'A4147T', 0, 'air-compressors-retail', 10, 37),
(601, 'Air Compressors Spare Parts', 'Angin Kompresor - Suku Cadang', NULL, '', 'A4864B', 0, 'air-compressors-spare-parts', 10, 37),
(602, 'Air Compressors Supplies', 'Angin Kompresor-Pasokan', NULL, '', 'A4148A', 0, 'air-compressors-supplies', 10, 37),
(603, 'Air Conditioning - Distributors', 'AC - Penyalur', NULL, '', 'A4808A', 0, 'air-conditioning-distributors', 10, 38),
(604, 'Air Conditioning - Part - Retail', 'AC, Suku Cadang - Eceran', NULL, '', 'A4825K', 0, 'air-conditioning-part-retail', 10, 38),
(605, 'Air Conditioning - Rental', 'AC - Sewa', NULL, '', 'A4118D', 0, 'air-conditioning-rental', 10, 38),
(606, 'Air Conditioning Brazing Material Supplies', 'Ac, Bahan Solder Suasa - Pasokan', NULL, '', 'A4144M', 0, 'air-conditioning-brazing-material-supplies', 10, 38),
(607, 'Air Conditioning Contractors', 'AC, Kontraktor', NULL, '', 'A1036B', 0, 'air-conditioning-contractors', 10, 189),
(608, 'Air Conditioning Equipment', 'AC - Perlengkapan', NULL, '', 'A4159A', 0, 'air-conditioning-equipment', 10, 38),
(609, 'Air Conditioning Equipment   &   Supplies', 'AC, Perlengkapan   &   Pasokan', NULL, '', 'A2986F', 0, 'air-conditioning-equipment-supplies', 10, 38),
(610, 'Air Conditioning For Commercial   &   Industrial', 'AC untuk Komersial   &   Industrial', NULL, '', 'A3707T', 0, 'air-conditioning-for-commercial-industrial', 10, 38),
(611, 'Air Conditioning Manufacturer', 'AC - Produsen', NULL, '', 'A4149B', 0, 'air-conditioning-manufacturer', 10, 38),
(612, 'Air Conditioning Refrigerant', 'AC, Pendingin', NULL, '', 'A7239M', 0, 'air-conditioning-refrigerant', 10, 38),
(613, 'Air Conditioning Repairing', 'AC - Perbaikan', NULL, '', 'A4160B', 0, 'air-conditioning-repairing', 10, 189),
(614, 'Air Conditioning Retail', 'AC - Eceran', NULL, '', 'A4155M', 0, 'air-conditioning-retail', 10, 38),
(615, 'Air Conditioning Supplies', 'AC - Pasokan', NULL, '', 'A4156N', 0, 'air-conditioning-supplies', 10, 38),
(616, 'Air Conditioning Systems', 'AC - Sistem', NULL, '', 'A4857H', 0, 'air-conditioning-systems', 10, 38),
(617, 'Air Conditioning Wholesale', 'AC - Grosir', NULL, '', 'A4162D', 0, 'air-conditioning-wholesale', 10, 38),
(618, 'Air Curtains', 'Tirai Udara', NULL, '', 'A1044R', 0, 'air-curtains', 10, 162),
(619, 'Air Freshener Manufacturer', 'Penyejuk Udara-Produsen', NULL, '', 'A4164H', 0, 'air-freshener-manufacturer', 10, 39),
(620, 'Air Freshener Retail', 'Penyejuk Udara-Eceran', NULL, '', 'A4163F', 0, 'air-freshener-retail', 10, 39),
(621, 'Air Freshener Wholesale', 'Penyejuk Udara - Grosir', NULL, '', 'A4769H', 0, 'air-freshener-wholesale', 10, 39),
(622, 'Air Motors', 'Angin, Motor', NULL, '', 'A1047B', 0, 'air-motors', 10, 268),
(623, 'Air Pollution Control', 'Pengendalian Polusi/Pengotoran Udara', NULL, '', 'A1048C', 0, 'air-pollution-control', 10, 193),
(624, 'Air Purifying Equipment', 'Udara Perlengkapan Penjernih', NULL, '', 'A4145N', 0, 'air-purifying-equipment', 10, 40),
(625, 'Aircraft - Model', 'Pesawat Terbang - Model', NULL, '', 'A4856F', 0, 'aircraft-model', 10, 41),
(626, 'Aircraft Charter Service', 'Pesawat Terbang, Layanan Carter', NULL, '', 'A4166M', 0, 'aircraft-charter-service', 10, 112),
(627, 'Aircraft Companies', 'Perusahaan Pesawat Terbang', NULL, '', 'A1051H', 0, 'aircraft-companies', 10, 41),
(628, 'Aircraft Consultants', 'Pesawat Terbang, Konsultan', NULL, '', 'A4812F', 0, 'aircraft-consultants', 10, 41),
(629, 'Aircraft Dealers - Civil', 'Pesawat Terbang, Agen Penjualan-Sipil', NULL, '', 'A4168R', 0, 'aircraft-dealers-civil', 10, 41),
(630, 'Aircraft Dealers - Military', 'Pesawat Terbang, Agen Penjualan Militer', NULL, '', 'A4169T', 0, 'aircraft-dealers-military', 10, 41),
(631, 'Aircraft Distributors - Civil', 'Pesawat Terbang, Penyalur-Sipil', NULL, '', 'A4170A', 0, 'aircraft-distributors-civil', 10, 41),
(632, 'Aircraft Distributors - Military', 'Pesawat Terbang, Penyalur-Militer', NULL, '', 'A4171B', 0, 'aircraft-distributors-military', 10, 41),
(633, 'Aircraft Engines - Servicing & Maintenance', 'Pesawat Terbang, Mesin - Pelayanan & Perawatan', NULL, '', 'A1056T', 0, 'aircraft-engines-servicing-maintenance', 10, 42),
(634, 'Aircraft Equipment', 'Pesawat Terbang, Peralatan', NULL, '', 'A4770K', 0, 'aircraft-equipment-1', 10, 42),
(635, 'Aircraft Ground Service Equipment', 'Pesawat Terbang, Peralatan Pelayanan di darat', NULL, '', 'A4176K', 0, 'aircraft-ground-service-equipment', 10, 41),
(636, 'Aircraft Ground Support Equipment', 'Pesawat Terbang, Peralatan Bantu di darat', NULL, '', 'A4175H', 0, 'aircraft-ground-support-equipment', 10, 41),
(637, 'Aircraft Instrument', 'Pesawat Terbang, Alat-Alat', NULL, '', 'A1059C', 0, 'aircraft-instrument', 10, 42),
(638, 'Aircraft Maintenance', 'Pesawat Terbang - Pemeliharaan', NULL, '', 'A4855D', 0, 'aircraft-maintenance', 10, 41),
(639, 'Aircraft Parts', 'Pesawat Terbang, Suku Cadang', NULL, '', 'A4174F', 0, 'aircraft-parts', 10, 42),
(640, 'Aircraft School', 'Penerbangan, Sekolah', NULL, '', 'A3510A', 0, 'aircraft-school', 10, 455),
(641, 'Aircraft Service', 'Pesawat Terbang - Pelayanan', NULL, '', 'A4854C', 0, 'aircraft-service', 10, 41),
(642, 'Aircraft Supplies', 'Pesawat Terbang, Pasokan', NULL, '', 'A4173D', 0, 'aircraft-supplies', 10, 42),
(643, 'Airline Training Schools', 'Penerbangan, Sekolah Pelatihan Dinas', NULL, '', 'A1046A', 0, 'airline-training-schools', 10, 455),
(644, 'Airlines Consultants', 'Penerbangan Perusahaan, Konsultan', NULL, '', 'A4862T', 0, 'airlines-consultants', 10, 64),
(645, 'Airlines Equipment', 'Penerbangan Perusahaan-Perlengkapan', NULL, '', 'A4177M', 0, 'airlines-equipment-1', 10, 43),
(646, 'Airlines Service', 'Penerbangan Perusahaan-Pelayanan', NULL, '', 'A4178N', 0, 'airlines-service', 10, 64),
(647, 'Airport Equipment & Supplies', 'Bandar Udara, Peralatan & Pasokan', NULL, '', 'A1062H', 0, 'airport-equipment-supplies', 10, 44),
(648, 'Airport Maintenance', 'Bandar Udara, Pemeliharaan', NULL, '', 'A3427H', 0, 'airport-maintenance-1', 10, 45),
(649, 'Albums', 'Album Foto', NULL, '', 'A1064M', 0, 'albums', 10, 398),
(650, 'Alcohol', 'Alkohol', NULL, '', 'A1065N', 0, 'alcohol', 10, 30),
(651, 'Alloys', 'Logam Campuran', NULL, '', 'A1066R', 0, 'alloys', 10, 198),
(652, 'Alternative Medical Treatment', 'Pengobatan Alternatif', NULL, '', 'A4075F', 0, 'alternative-medical-treatment', 10, 324),
(653, 'Alternators Equipment', 'Dinamo - Niaga - Perlengkapan', NULL, '', 'A4182B', 0, 'alternators-equipment', 10, 46),
(654, 'Alternators Manufacturer', 'Dinamo - Niaga - Produsen', NULL, '', 'A4183C', 0, 'alternators-manufacturer', 10, 46),
(655, 'Alternators Repairing', 'Dinamo - Perbaikan', NULL, '', 'A4793M', 0, 'alternators-repairing', 10, 380),
(656, 'Alternators Retail', 'Dinamo - Niaga - Eceran', NULL, '', 'A4181A', 0, 'alternators-retail', 10, 46),
(657, 'Aluminium Anodizing', 'Aluminium, Penganodisasian', NULL, '', 'A1069B', 0, 'aluminium-anodizing', 10, 47),
(658, 'Aluminium Die Casting Machine Equipment', 'Aluminium, Peralatan Mesin Pengecoran Cetak', NULL, '', 'A3589C', 0, 'aluminium-die-casting-machine-equipment', 10, 270),
(659, 'Aluminium Extruders', 'Aluminium, Ekstrusi', NULL, '', 'A1070C', 0, 'aluminium-extruders', 10, 47),
(660, 'Aluminium Fabricators', 'Aluminium - Produsen', NULL, '', 'A1071D', 0, 'aluminium-fabricators', 10, 198),
(661, 'Aluminium Foil', 'Aluminium, Kertas Lembaran', NULL, '', 'A2996D', 0, 'aluminium-foil', 10, 198),
(662, 'Aluminium Manufacturer', 'Aluminium-Produsen', NULL, '', 'A4184D', 0, 'aluminium-manufacturer', 10, 198),
(663, 'Aluminium Ornamental Work', 'Aluminium, Barang Hiasan', NULL, '', 'A1072F', 0, 'aluminium-ornamental-work', 10, 198),
(664, 'Aluminium Products', 'Aluminium, Barang', NULL, '', 'A1073H', 0, 'aluminium-products', 10, 198),
(665, 'Ambulance Service', 'Ambulan, Pelayanan', NULL, '', 'A1074K', 0, 'ambulance-service', 10, 323),
(666, 'Ammunition - Wholesale & Manufacturers', 'Amunisi - Grosir & Produsen', NULL, '', 'A1075M', 0, 'ammunition-wholesale-manufacturers', 10, 327),
(667, 'Amusement Devices', 'Hiburan, Alat-Alat', NULL, '', 'A1077R', 0, 'amusement-devices-1', 10, 48),
(668, 'Amusement Devices - Home', 'Hiburan, Alat-Alat (di) Rumah', NULL, '', 'A1079A', 0, 'amusement-devices-home', 10, 48),
(669, 'Amusement Devices Commercial', 'Hiburan, Alat - Komersial', NULL, '', 'A1078T', 0, 'amusement-devices-commercial', 10, 48),
(670, 'Amusement Places', 'Hiburan, Tempat', NULL, '', 'A1080B', 0, 'amusement-places-1', 10, 49),
(671, 'Amusement Services', 'Hiburan-Pelayanan', NULL, '', 'A4185F', 0, 'amusement-services-1', 10, 50),
(672, 'Animal Brokers & Dealers - Zoo, Circus, Etc', 'Hewan, Pialang & Agen Penjualan - Kebun Binatang, Sirkus, Dll', NULL, '', 'A1081C', 0, 'animal-brokers-dealers-zoo-circus-etc', 10, 395),
(673, 'Animal Circus Etc', 'Hewan Sirkus', NULL, '', 'A4188M', 0, 'animal-circus-etc', 10, 395),
(674, 'Animal Feed Manufacturer', 'Makanan Ternak, Produsen', NULL, '', 'A3417K', 0, 'animal-feed-manufacturer', 10, 201),
(675, 'Animal Feed Retail', 'Makanan Ternak, Eceran', NULL, '', 'A4187K', 0, 'animal-feed-retail', 10, 201),
(676, 'Animal Feed Supplies', 'Makanan Ternak, Pasokan', NULL, '', 'A4186H', 0, 'animal-feed-supplies', 10, 201),
(677, 'Animal Health Products', 'Hewan, Produk Kesehatan', NULL, '', 'A3217F', 0, 'animal-health-products', 10, 395),
(678, 'Animal Nutritionist - Consultants', 'Hewan, Ahli Gizi - Konsultan', NULL, '', 'A1082D', 0, 'animal-nutritionist-consultants', 10, 415),
(679, 'Animation Poster - Retail', 'Poster Animasi - Eceran', NULL, '', 'A4870M', 0, 'animation-poster-retail', 10, 412),
(680, 'Animation Services', 'Animasi-Pelayanan', NULL, '', 'A4189N', 0, 'animation-services-1', 10, 51),
(681, 'Anodizing', 'Penganodisasian', NULL, '', 'A1083F', 0, 'anodizing', 10, 47),
(682, 'Antennas - Repairing', 'Antene - Perbaikan', NULL, '', 'A4836K', 0, 'antennas-repairing', 10, 189),
(683, 'Antennas Manufacturer', 'Antene-Produsen', NULL, '', 'A4190R', 0, 'antennas-manufacturer', 10, 52),
(684, 'Antennas Retail', 'Antene-Eceran', NULL, '', 'A4192A', 0, 'antennas-retail', 10, 52),
(685, 'Antennas Wholesale', 'Antene-Grosir', NULL, '', 'A4191T', 0, 'antennas-wholesale', 10, 52),
(686, 'Antiques Dealers', 'Barang Antik - Agen Penjualan', NULL, '', 'A3381D', 0, 'antiques-dealers', 10, 56),
(687, 'Antiques Product', 'Antik, Barang', NULL, '', 'A4193B', 0, 'antiques-product', 10, 56),
(688, 'Apartments', 'Rumah Pangsa/Flat', NULL, '', 'A2982A', 0, 'apartments-1', 10, 53),
(689, 'Appliances, Household - Refinishing', 'Perabot, Rumah Tangga - Pembaruan', NULL, '', 'A1087N', 0, 'appliances-household-refinishing', 10, 229),
(690, 'Appraisers', 'Ahli Penilai/Penaksir', NULL, '', 'A1088R', 0, 'appraisers', 10, 418),
(691, 'Aqiqah', 'Aqiqah', NULL, '', 'A7310B', 0, 'aqiqah', 10, 109),
(692, 'Aquaculture Products', 'Air, Hasil Budidaya', NULL, '', 'A3031H', 0, 'aquaculture-products', 10, 193),
(693, 'Aquarium Fish', 'Ikan Hias', NULL, '', 'A4019D', 0, 'aquarium-fish', 10, 207),
(694, 'Aquariums', 'Akuarium', NULL, '', 'A3049B', 0, 'aquariums', 10, 395),
(695, 'Aquariums Manufacturers', 'Akuarium Produsen', NULL, '', 'A4196F', 0, 'aquariums-manufacturers', 10, 395),
(696, 'Aquariums Supplies', 'Akuarium Pasokan', NULL, '', 'A4194C', 0, 'aquariums-supplies', 10, 395),
(697, 'Aquariums Wholesale', 'Akuarium-Grosir', NULL, '', 'A4195D', 0, 'aquariums-wholesale', 10, 395),
(698, 'Architects & Consulting Engineers', 'Arsitek & Konsultan Ahli Teknik', NULL, '', 'A1093D', 0, 'architects-consulting-engineers', 10, 54),
(699, 'Architects'' Supplies', 'Arsitek, Pasokan', NULL, '', 'A1091B', 0, 'architects-supplies', 10, 54),
(700, 'Architectural Interior Illustrator', 'Arsitektural, Ilustrator Tata Ruang', NULL, '', 'A3691H', 0, 'architectural-interior-illustrator', 10, 54),
(701, 'Architectural Model', 'Arsitektur, Model', NULL, '', 'A3584N', 0, 'architectural-model', 10, 54),
(702, 'Art Dealers', 'Barang Seni - Agen Penjualan', NULL, '', 'A4198K', 0, 'art-dealers', 10, 56),
(703, 'Art Galleries', 'Galeri Seni', NULL, '', 'A3050C', 0, 'art-galleries', 10, 56),
(704, 'Art Glass - Manufacturers', 'Kaca Seni - Produsen', NULL, '', 'A4885A', 0, 'art-glass-manufacturers', 10, 55),
(705, 'Art Goods - Retail', 'Seni, Barang-Barang - Eceran', NULL, '', 'A1097M', 0, 'art-goods-retail', 10, 56),
(706, 'Art Goods Manufacturers', 'Seni, Barang-Barang-Produsen', NULL, '', 'A4201R', 0, 'art-goods-manufacturers', 10, 56),
(707, 'Art Goods Wholesale', 'Seni, Barang-Barang-Grosir', NULL, '', 'A4200N', 0, 'art-goods-wholesale', 10, 56),
(708, 'Art Retail', 'Barang Seni - Eceran', NULL, '', 'A4199M', 0, 'art-retail', 10, 56),
(709, 'Art School', 'Sekolah Seni', NULL, '', 'A1100T', 0, 'art-school', 10, 455),
(710, 'Art Shop', 'Barang Seni, Toko', NULL, '', 'A1101A', 0, 'art-shop', 10, 56),
(711, 'Art Wholesale', 'Barang Seni - Grosir', NULL, '', 'A4197H', 0, 'art-wholesale', 10, 56),
(712, 'Artificial Flowers & Plants', 'Bunga & Tanaman Tiruan', NULL, '', 'A1102B', 0, 'artificial-flowers-plants', 10, 211),
(713, 'Artificial Flowers Manufacture', 'Bunga Tiruan-Produsen', NULL, '', 'A4203A', 0, 'artificial-flowers-manufacture', 10, 211),
(714, 'Artificial Flowers Retail', 'Bunga Tiruan-Eceran', NULL, '', 'A4202T', 0, 'artificial-flowers-retail', 10, 211),
(715, 'Artificial Flowers Supplies', 'Bunga Tiruan-Pasokan', NULL, '', 'A4204B', 0, 'artificial-flowers-supplies', 10, 211),
(716, 'Artist Kids', 'Seniman Anak', NULL, '', 'A5605H', 0, 'artist-kids', 10, 57),
(717, 'Artists'' Agents', 'Seniman, Biro/Perwakilan', NULL, '', 'A1106H', 0, 'artists-agents', 10, 57),
(718, 'Artists Materials & Supplies', 'Seniman, Material/Bahan - Pasokan', NULL, '', 'A1105F', 0, 'artists-materials-supplies', 10, 57),
(719, 'Artists Materials Manufacturers', 'Seniman, Bahan/Material - Produsen', NULL, '', 'A4206D', 0, 'artists-materials-manufacturers', 10, 57),
(720, 'Artists Materials Retail', 'Seniman, Bahan/Material-Eceran', NULL, '', 'A4207F', 0, 'artists-materials-retail', 10, 57),
(721, 'Artists Materials Wholesale', 'Seniman, Bahan/Material - Grosir', NULL, '', 'A4205C', 0, 'artists-materials-wholesale', 10, 57),
(722, 'Asbestos & Asbestos Products', 'Asbes & Produk Asbes', NULL, '', 'A1107K', 0, 'asbestos-asbestos-products', 10, 438),
(723, 'Asphalt Mixing Equipment', 'Aspal, Peralatan Pencampuran', NULL, '', 'A1110R', 0, 'asphalt-mixing-equipment', 10, 268),
(724, 'Asphalt Product', 'Aspal Produk', NULL, '', 'A4209K', 0, 'asphalt-product', 10, 330),
(725, 'Assembly & Fabricating Contract Service', 'Perakitan & Kontrak Pemabrikan/Pembuatan - Pelayanan', NULL, '', 'A1112A', 0, 'assembly-fabricating-contract-service', 10, 380),
(726, 'Association', 'Asosiasi/Persatuan', NULL, '', 'A1113B', 0, 'association', 10, 58),
(727, 'Assurance Business Advisory Services', 'Layanan laporan bisnis jaminan', NULL, '', 'A4052D', 0, 'assurance-business-advisory-services-1', 10, 59),
(728, 'Auction Company', 'Lelang Perusahaan', NULL, '', 'A4165K', 0, 'auction-company', 10, 60),
(729, 'Auctioneers', 'Juru Lelang', NULL, '', 'A1114C', 0, 'auctioneers-1', 10, 60),
(730, 'Audio - Visual Equipment & Supplies', 'Audio - Visual, Peralatan & Pasokan', NULL, '', 'A1115D', 0, 'audio-visual-equipment-supplies', 10, 61),
(731, 'Audio - Visual Manufacturer', 'Audio - Visual, Produsen', NULL, '', 'A4213T', 0, 'audio-visual-manufacturer', 10, 61),
(732, 'Audio - Visual Production - Services', 'Audio - Visual, Produksi - Pelayanan', NULL, '', 'A1117H', 0, 'audio-visual-production-services', 10, 61),
(733, 'Audio - Visual Rental', 'Audio - Visual, Sewa', NULL, '', 'A4806R', 0, 'audio-visual-rental', 10, 61),
(734, 'Audio - Visual Retail', 'Audio - Visual, Eceran', NULL, '', 'A4212R', 0, 'audio-visual-retail', 10, 61),
(735, 'Audio - Visual Services', 'Audio - Visual, Pelayanan', NULL, '', 'A4211N', 0, 'audio-visual-services', 10, 61),
(736, 'Automation Consultants', 'Automatisasi, Konsultan', NULL, '', 'A3052F', 0, 'automation-consultants', 10, 63),
(737, 'Automation Services', 'Otomatisasi - Pelayanan', NULL, '', 'A4216C', 0, 'automation-services', 10, 63),
(738, 'Automation Systems & Equipment', 'Automatisasi, Sistem & Peralatan', NULL, '', 'A1119M', 0, 'automation-systems-equipment', 10, 62),
(739, 'Automobile Parts & Supplies', 'Mobil, Suku Cadang & Pasokan', NULL, '', 'A4217D', 0, 'automobile-parts-supplies', 10, 475),
(740, 'Automobile Parts & Supplies-Manufacturers', 'Mobil, Suku Cadang & Pasokan - Produsen', NULL, '', 'A4219H', 0, 'automobile-parts-supplies-manufacturers', 10, 475),
(741, 'Automobile Parts & Supplies-Wholesale', 'Mobil, Suku Cadang & Pasokan - Grosir', NULL, '', 'A4218F', 0, 'automobile-parts-supplies-wholesale', 10, 475),
(742, 'Automotive Bodies Assembly', 'Mobil, Badan - Pabrik & Perakitan Komersial', NULL, '', 'A4221M', 0, 'automotive-bodies-assembly', 10, 350),
(743, 'Automotive Bodies Manufacturing', 'Mobil, Badan - Pabrik & Perakitan Komersial', NULL, '', 'A4220K', 0, 'automotive-bodies-manufacturing', 10, 348),
(744, 'Automotive Club', 'Klub Otomotif', NULL, '', 'A4997C', 0, 'automotive-club', 10, 462),
(745, 'Automotive Importers', 'Mobil - Pengimpor', NULL, '', 'A4801F', 0, 'automotive-importers', 10, 347),
(746, 'Automotive Spare Parts Manufacturer', 'Mobil, Suku Cadang Produsen', NULL, '', 'A4224T', 0, 'automotive-spare-parts-manufacturer', 10, 475),
(747, 'Automotive Spare Parts Retail', 'Mobil, Suku Cadang Pengecer', NULL, '', 'A4223R', 0, 'automotive-spare-parts-retail', 10, 475),
(748, 'Automotive Spare Parts Supplies', 'Mobil, Suku Cadang Pasokan', NULL, '', 'A4225A', 0, 'automotive-spare-parts-supplies', 10, 475),
(749, 'Automotive Spare Parts Wholesale', 'Mobil, Suku Cadang Grosir', NULL, '', 'A4222N', 0, 'automotive-spare-parts-wholesale', 10, 475),
(750, 'Aviation Consultants', 'Penerbangan Konsultan', NULL, '', 'A1125C', 0, 'aviation-consultants', 10, 64),
(751, 'Aviation Equipment', 'Penerbangan, Perlengkapan', NULL, '', 'A4771M', 0, 'aviation-equipment-1', 10, 65),
(752, 'Aviation Services', 'Penerbangan, Pelayanan', NULL, '', 'A4226B', 0, 'aviation-services', 10, 64),
(753, 'Awnings', 'Atap Oning / Tenda Rumah', NULL, '', 'A1127F', 0, 'awnings', 10, 438),
(754, 'Baby Accessories - Manufacturers', 'Bayi, Perlengkapan - Produsen', NULL, '', 'B4228D', 0, 'baby-accessories-manufacturers', 10, 66),
(755, 'Baby Accessories - Rental', 'Bayi, Perlengkapan - Sewa', NULL, '', 'B7214N', 0, 'baby-accessories-rental', 10, 66),
(756, 'Baby Accessories - Retail', 'Bayi, Perlengkapan - Eceran', NULL, '', 'B1128H', 0, 'baby-accessories-retail', 10, 66),
(757, 'Baby Accessories - Supplies', 'Bayi, Perlengkapan - Pasokan', NULL, '', 'B4229F', 0, 'baby-accessories-supplies', 10, 66),
(758, 'Baby Shops', 'Bayi, Toko Perlengkapan', NULL, '', 'B1130M', 0, 'baby-shops', 10, 66),
(759, 'Baby Sitter', 'Pramusiwi', NULL, '', 'B3937R', 0, 'baby-sitter-1', 10, 67),
(760, 'Bacteria Control Equipment', 'Bakteri, Alat Pengontrol', NULL, '', 'B1131N', 0, 'bacteria-control-equipment', 10, 145),
(761, 'Badges Product', 'Lencana Produk', NULL, '', 'B4230H', 0, 'badges-product', 10, 26),
(762, 'Bags - Burlap & Cotton', 'Tas/Kantong - Goni & Katun', NULL, '', 'B1133T', 0, 'bags-burlap-cotton', 10, 68),
(763, 'Bags - Paper', 'Tas/Kantong - Kertas', NULL, '', 'B1134A', 0, 'bags-paper', 10, 68),
(764, 'Bags - Plastic', 'Tas/Kantong - Plastik', NULL, '', 'B1135B', 0, 'bags-plastic', 10, 68),
(765, 'Bags - Specialty', 'Tas/Kantong - Khusus', NULL, '', 'B1136C', 0, 'bags-specialty', 10, 68),
(766, 'Bags Distributor', 'Tas/Kantong - Penyalur', NULL, '', 'B4815M', 0, 'bags-distributor', 10, 68),
(767, 'Bags Manufacturer', 'Tas/Kantong - Produsen', NULL, '', 'B4231K', 0, 'bags-manufacturer', 10, 68),
(768, 'Bags Repairing', 'Tas/Kantong - Perbaikan', NULL, '', 'B6815C', 0, 'bags-repairing', 10, 68),
(769, 'Bags Retail', 'Tas/Kantong-Eceran', NULL, '', 'B4232M', 0, 'bags-retail', 10, 68),
(770, 'Bags School - Manufacturer', 'Tas/Kantong Sekolah - Produsen', NULL, '', 'B5606K', 0, 'bags-school-manufacturer', 10, 457),
(771, 'Bags Wholesale', 'Tas/Kantong - Grosir', NULL, '', 'B4845F', 0, 'bags-wholesale', 10, 68),
(772, 'Bags, Gunny & Plastic', 'Tas, Goni & Plastik', NULL, '', 'B7376C', 0, 'bags-gunny-plastic', 10, 68),
(773, 'Bakers', 'Perusahaan Roti', NULL, '', 'B3055M', 0, 'bakers', 10, 69),
(774, 'Bakers - Distributor', 'Roti, Perusahaan - Penyalur', NULL, '', 'B4902K', 0, 'bakers-distributor', 10, 69),
(775, 'Bakers - Equipment', 'Roti, Perusahaan - Peralatan', NULL, '', 'B1139H', 0, 'bakers-equipment', 10, 69),
(776, 'Bakers - Retail', 'Roti, Perusahaan - Eceran', NULL, '', 'B1140K', 0, 'bakers-retail', 10, 69),
(777, 'Bakers - Supplies', 'Roti, Perusahaan - Pasokan', NULL, '', 'B1137D', 0, 'bakers-supplies', 10, 69),
(778, 'Bakers - Wholesale', 'Roti, Perusahaan - Grosir', NULL, '', 'B1141M', 0, 'bakers-wholesale', 10, 69),
(779, 'Bakers Manufacturers', 'Roti, Perusahaan - Produsen', NULL, '', 'B4772N', 0, 'bakers-manufacturers', 10, 69),
(780, 'Baking Powder', 'Ragi Roti', NULL, '', 'B1142N', 0, 'baking-powder', 10, 101),
(781, 'Balancing Equipment', 'Pengimbang, Peralatan', NULL, '', 'B3193C', 0, 'balancing-equipment', 10, 268),
(782, 'Ball Soccer', 'Bola Sepak', NULL, '', 'B3982T', 0, 'ball-soccer', 10, 477),
(783, 'Ballon Decoration Products', 'Balon Dekorasi - Produk', NULL, '', 'B7053F', 0, 'ballon-decoration-products', 10, 70),
(784, 'Balloons Product', 'Balon-Balon, Produk', NULL, '', 'B4234R', 0, 'balloons-product', 10, 70),
(785, 'Balls Plastics', 'Bola-Bola - Plastik', NULL, '', 'B3188N', 0, 'balls-plastics', 10, 70),
(786, 'Bamboo Products', 'Bambu, Hasil', NULL, '', 'B6715A', 0, 'bamboo-products', 10, 228),
(787, 'Bank Equipment & Supplies', 'Bank, Peralatan & Pasokan', NULL, '', 'B1143R', 0, 'bank-equipment-supplies', 10, 72),
(788, 'Bank Representatives', 'Bank, Perwakilan', NULL, '', 'B1144T', 0, 'bank-representatives-1', 10, 71),
(789, 'Banks', 'Bank', NULL, '', 'B1145A', 0, 'banks-1', 10, 73),
(790, 'Banks - Merchant', 'Bank - Pedagang', NULL, '', 'B1146B', 0, 'banks-merchant', 10, 72),
(791, 'Banks Islamic', 'Bank Syariah', NULL, '', 'B7071N', 0, 'banks-islamic', 10, 73),
(792, 'Banquet Rooms', 'Ruang Perjamuan', NULL, '', 'B1147C', 0, 'banquet-rooms', 10, 146),
(793, 'Bar Coding', 'Kode Batang', NULL, '', 'B4253K', 0, 'bar-coding', 10, 379),
(794, 'Bar Coding - Equipment & Systems', 'Kode Batang - Perlengkapan & Sistem', NULL, '', 'B3470F', 0, 'bar-coding-equipment-systems', 10, 268),
(795, 'Barber Shops', 'Pemangkas Rambut', NULL, '', 'B1151K', 0, 'barber-shops', 10, 75),
(796, 'Barbers Equipment & Supplies', 'Pemangkas Rambut - Perlengkapan & Pasokan', NULL, '', 'B1150H', 0, 'barbers-equipment-supplies', 10, 75),
(797, 'Barges', 'Tongkang', NULL, '', 'B1152M', 0, 'barges', 10, 464),
(798, 'Bars', 'Bar', NULL, '', 'B1154R', 0, 'bars', 10, 74),
(799, 'Bars Equipment', 'Bar, Perlengkapan', NULL, '', 'B4236A', 0, 'bars-equipment', 10, 433),
(800, 'Bars Supplies', 'Bar, Pasokan', NULL, '', 'B4237B', 0, 'bars-supplies', 10, 433),
(801, 'Batching Equipment', 'Semen - Pasir, Pengaduk - Peralatan', NULL, '', 'B3278T', 0, 'batching-equipment', 10, 222),
(802, 'Bathroom Accessories', 'Kamar Mandi, Asesoris', NULL, '', 'B4239D', 0, 'bathroom-accessories', 10, 502),
(803, 'Bathroom Equipment', 'Kamar Mandi, Perlengkapan', NULL, '', 'B4238C', 0, 'bathroom-equipment', 10, 502),
(804, 'Batik - Retail', 'Batik - Eceran', NULL, '', 'B1156A', 0, 'batik-retail', 10, 125),
(805, 'Batik Manufacturers', 'Batik - Produsen', NULL, '', 'B4241H', 0, 'batik-manufacturers', 10, 125),
(806, 'Batik Wholesale', 'Batik - Grosir', NULL, '', 'B4240F', 0, 'batik-wholesale', 10, 125),
(807, 'Batik, Equipment & Supplies', 'Batik, Peralatan & Pasokan', NULL, '', 'B4102R', 0, 'batik-equipment-supplies', 10, 125),
(808, 'Batteries - Dry Cell Manufacturers', 'Aki/Batu Baterai - Elemen Kering - Produsen', NULL, '', 'B4244N', 0, 'batteries-dry-cell-manufacturers', 10, 410),
(809, 'Batteries - Dry Cell Wholesale', 'Aki/Batu Baterai - Elemen Kering - Grosir', NULL, '', 'B4245R', 0, 'batteries-dry-cell-wholesale', 10, 410),
(810, 'Batteries - Nickel Cadmium', 'Baterai Nikel Kadmium', NULL, '', 'B2912M', 0, 'batteries-nickel-cadmium', 10, 410),
(811, 'Batteries - Storage - Retail', 'Aki - Penyimpanan - Eceran', NULL, '', 'B1159D', 0, 'batteries-storage-retail', 10, 410),
(812, 'Batteries - Storage Manufacturers', 'Aki - Penyimpanan, Produsen', NULL, '', 'B4246T', 0, 'batteries-storage-manufacturers', 10, 410),
(813, 'Batteries - Storage Wholesale', 'Aki - Penyimpanan, Grosir', NULL, '', 'B4247A', 0, 'batteries-storage-wholesale', 10, 410),
(814, 'Battery Charging Equipment', 'Aki, Perlengkapan Pengisi', NULL, '', 'B1161H', 0, 'battery-charging-equipment', 10, 410),
(815, 'Battery Charging Manufacturer', 'Aki, Pengisi - Produsen', NULL, '', 'B5543R', 0, 'battery-charging-manufacturer', 10, 410),
(816, 'Battery Component Manufacturers', 'Aki - Produsen  Komponen', NULL, '', 'B4871N', 0, 'battery-component-manufacturers', 10, 410),
(817, 'Battery Manufacturer', 'Aki - Produsen', NULL, '', 'B4248B', 0, 'battery-manufacturer', 10, 410),
(818, 'Battery Repairing & Rebuilding', 'Aki, Perbaikan & Pembaruan', NULL, '', 'B1162K', 0, 'battery-repairing-rebuilding', 10, 410),
(819, 'Battery Retail', 'Aki - Eceran', NULL, '', 'B4249C', 0, 'battery-retail', 10, 410),
(820, 'Battery service', 'Aki - layanan', NULL, '', 'B4055K', 0, 'battery-service', 10, 410),
(821, 'Battery Supplies', 'Aki, Pasokan', NULL, '', 'B1163M', 0, 'battery-supplies', 10, 410),
(822, 'Beach Cleaning', 'Pantai, Pembersihan', NULL, '', 'B3262H', 0, 'beach-cleaning', 10, 448),
(823, 'Bearings', 'Poros, Bantalan', NULL, '', 'B1164N', 0, 'bearings', 10, 198),
(824, 'Beauty Culture Schools', 'Kecantikan, Kursus', NULL, '', 'B1165R', 0, 'beauty-culture-schools', 10, 156),
(825, 'Beauty Salons', 'Kecantikan, Salon', NULL, '', 'B1166T', 0, 'beauty-salons', 10, 76),
(826, 'Beauty Salons - Equipment & Supplies', 'Kecantikan, Salon - Perlengkapan & Pasokan', NULL, '', 'B1167A', 0, 'beauty-salons-equipment-supplies', 10, 76),
(827, 'Beauty Salons Repairing', 'Kecantikan, Salon - Perbaikan', NULL, '', 'B4250D', 0, 'beauty-salons-repairing', 10, 76),
(828, 'Beauty, Aroma Therapy', 'Kecantikan, Aromaterapi', NULL, '', 'B4170A', 0, 'beauty-aroma-therapy', 10, 76),
(829, 'Bed Clothes', 'Baju Tidur', NULL, '', 'B7255T', 0, 'bed-clothes', 10, 125),
(830, 'Bed Cover Manufacturer', 'Seprai, Produsen', NULL, '', 'B4252H', 0, 'bed-cover-manufacturer', 10, 77),
(831, 'Bed Cover Retail', 'Seprai, Eceran', NULL, '', 'B4251F', 0, 'bed-cover-retail', 10, 77),
(832, 'Bed Cover Wholesale', 'Seprai, Grosir', NULL, '', 'B4773R', 0, 'bed-cover-wholesale', 10, 77),
(833, 'Bed Springs - Material', 'Kasur Pegas - Bahan Baku', NULL, '', 'B4907A', 0, 'bed-springs-material', 10, 78),
(834, 'Bed Springs Manufacturers', 'Kasur, Pegas - Produsen', NULL, '', 'B4255N', 0, 'bed-springs-manufacturers', 10, 78),
(835, 'Bed Springs Wholesale', 'Kasur, Pegas - Grosir', NULL, '', 'B4254M', 0, 'bed-springs-wholesale', 10, 78),
(836, 'Beds & Bedding', 'Tempat Tidur & Perlengkapan Tempat Tidur / Sprei, Selimut', NULL, '', 'B3058T', 0, 'beds-bedding', 10, 336),
(837, 'Beds & Bedding Equipment', 'Tempat Tidur & Perlengkapan Tempat Tidur / Sprei, Selimut-Perlengkapan', NULL, '', 'B4257T', 0, 'beds-bedding-equipment', 10, 336),
(838, 'Beds & Bedding Manufacturer', 'Tempat Tidur & Perlengkapan Tempat Tidur / Sprei, Selimut - Produsen', NULL, '', 'B4259B', 0, 'beds-bedding-manufacturer', 10, 336);
INSERT INTO `category` (`id`, `name_len`, `name_lid`, `description_len`, `description_lid`, `classification`, `popular`, `slug`, `weight`, `category_id`) VALUES
(839, 'Beds & Bedding Retail', 'Tempat Tidur & Perlengkapan Tempat Tidur / Sprei, Selimut - Eceran', NULL, '', 'B4256R', 0, 'beds-bedding-retail', 10, 336),
(840, 'Beds & Bedding Wholesale', 'Tempat Tidur & Perlengkapan Tempat Tidur / Sprei, Selimut - Grosir', NULL, '', 'B4258A', 0, 'beds-bedding-wholesale', 10, 336),
(841, 'Beer Distributor', 'Bir, Penyalur', NULL, '', 'B4260C', 0, 'beer-distributor', 10, 80),
(842, 'Beer Wholesale', 'Bir, Grosir', NULL, '', 'B4261D', 0, 'beer-wholesale', 10, 80),
(843, 'Belting - Mechanical', 'Pemasangan Sabuk Mekanis', NULL, '', 'B1172H', 0, 'belting-mechanical', 10, 380),
(844, 'Belts - Manufactures', 'Ikat Pinggang - Produsen', NULL, '', 'B1173K', 0, 'belts-manufactures', 10, 26),
(845, 'Belts - Men''s Manufacturers', 'Ikat Pinggang Pria - Produsen', NULL, '', 'B1174M', 0, 'belts-mens-manufacturers', 10, 26),
(846, 'Belts - Retail', 'Ikat Pinggang - Eceran', NULL, '', 'B1175N', 0, 'belts-retail', 10, 26),
(847, 'Belts - Women''s Manufacturers', 'Ikat Pinggang Wanita - Produsen', NULL, '', 'B1176R', 0, 'belts-womens-manufacturers', 10, 26),
(848, 'Beverage Cooling Equipment', 'Minuman, Penyejuk - Perlengkapan', NULL, '', 'B1177T', 0, 'beverage-cooling-equipment', 10, 527),
(849, 'Beverages - Dealers', 'Minuman - Agen Penjualan', NULL, '', 'B4865C', 0, 'beverages-dealers', 10, 335),
(850, 'Beverages - Distributors', 'Minuman - Penyalur', NULL, '', 'B4263H', 0, 'beverages-distributors', 10, 335),
(851, 'Beverages - Manufacturers', 'Minuman - Produsen', NULL, '', 'B4262F', 0, 'beverages-manufacturers', 10, 335),
(852, 'Beverages - Retail', 'Minuman - Eceran', NULL, '', 'B4264K', 0, 'beverages-retail', 10, 335),
(853, 'Beverages Filling & Processing Machines', 'Minuman, Mesin Pemroses Dan Pengisian', NULL, '', 'B3448F', 0, 'beverages-filling-processing-machines', 10, 527),
(854, 'Beverages, Soft Drinks', 'Minuman Tanpa Alkohol', NULL, '', 'B4265M', 0, 'beverages-soft-drinks', 10, 471),
(855, 'Bicycles - Dealers', 'Sepeda - Agen Penjualan', NULL, '', 'B4791H', 0, 'bicycles-dealers', 10, 81),
(856, 'Bicycles - Repairing', 'Sepeda - Perbaikan', NULL, '', 'B4792K', 0, 'bicycles-repairing', 10, 81),
(857, 'Bicycles Parts & Manufacturer', 'Sepeda - Suku Cadang & Produsen', NULL, '', 'B4267R', 0, 'bicycles-parts-manufacturer', 10, 81),
(858, 'Bicycles Parts & Retail', 'Sepeda - Suku Cadang & Eceran', NULL, '', 'B4269A', 0, 'bicycles-parts-retail', 10, 81),
(859, 'Bicycles Parts & Supplies', 'Sepeda - Suku Cadang & Pasokan', NULL, '', 'B4266N', 0, 'bicycles-parts-supplies', 10, 81),
(860, 'Bicycles Parts & Wholesale', 'Sepeda - Suku Cadang & Grosir', NULL, '', 'B4268T', 0, 'bicycles-parts-wholesale', 10, 81),
(861, 'Billiard Equipment & Supplies', 'Biliar, Perlengkapan & Pasokan', NULL, '', 'B1185M', 0, 'billiard-equipment-supplies', 10, 477),
(862, 'Billiards', 'Biliar', NULL, '', 'B1187R', 0, 'billiards', 10, 478),
(863, 'Billing Service', 'Penagihan Jasa', NULL, '', 'B3965F', 0, 'billing-service', 10, 130),
(864, 'Binatu, Bahan Kimia', 'Laundry Chemical', NULL, '', 'L7265M', 0, 'binatu-bahan-kimia', 10, 299),
(865, 'Binoculars', 'Teropong', NULL, '', 'B1189A', 0, 'binoculars', 10, 145),
(866, 'Bird Cage-Services', 'Sangkar Burung - Pelayanan', NULL, '', 'B6953M', 0, 'bird-cage-services', 10, 395),
(867, 'Birds', 'Burung', NULL, '', 'B1191C', 0, 'birds', 10, 82),
(868, 'Biscuits - Distributors', 'Biskuit - Penyalur', NULL, '', 'B4790F', 0, 'biscuits-distributors', 10, 83),
(869, 'Biscuits - Manufacturers', 'Biskuit - Produsen', NULL, '', 'B4271C', 0, 'biscuits-manufacturers', 10, 83),
(870, 'Biscuits - Retail', 'Biskuit - Eceran', NULL, '', 'B1180C', 0, 'biscuits-retail', 10, 83),
(871, 'Biscuits - Wholesale', 'Biskuit - Grosir', NULL, '', 'B4270B', 0, 'biscuits-wholesale', 10, 83),
(872, 'Black Boards', 'Papan Tulis', NULL, '', 'B1193F', 0, 'black-boards', 10, 457),
(873, 'Blacksmith''s Equipment & Supplies', 'Pandai Besi, Perlengkapan & Pasokan', NULL, '', 'B1194H', 0, 'blacksmiths-equipment-supplies', 10, 268),
(874, 'Blankets', 'Selimut', NULL, '', 'B1195K', 0, 'blankets', 10, 501),
(875, 'Blasting Contractors', 'Peledakan, Kontraktor', NULL, '', 'B4273F', 0, 'blasting-contractors', 10, 377),
(876, 'Blasting Contractors - Abrasives Grit, Shot, Etc', 'Peledakan, Kontraktor - Pasir Penggosok, Tembakan, Dll', NULL, '', 'B1196M', 0, 'blasting-contractors-abrasives-grit-shot-etc', 10, 377),
(877, 'Blasting Equipment & Supplies', 'Peledakan, Peralatan & Pasokan', NULL, '', 'B4274H', 0, 'blasting-equipment-supplies', 10, 268),
(878, 'Blind - Mosquito Insect', 'Serangga - Penghadang', NULL, '', 'B4221M', 0, 'blind-mosquito-insect', 10, 379),
(879, 'Blowers & Blower Systems', 'Peniup, Alat & Sistem Peniup', NULL, '', 'B1199T', 0, 'blowers-blower-systems', 10, 268),
(880, 'Blowtorches', 'Obor Las', NULL, '', 'B1198R', 0, 'blowtorches', 10, 198),
(881, 'Blue Printing Equipment & Supplies', 'Cetak Biru - Peralatan & Pasokan', NULL, '', 'B1201B', 0, 'blue-printing-equipment-supplies', 10, 140),
(882, 'Boarding House', 'Asrama ,Rumah Kos', NULL, '', 'B1202C', 0, 'boarding-house-1', 10, 84),
(883, 'Boat - Builders & Repairers', 'Kapal, Pembuat & Tukang Memperbaiki', NULL, '', 'B1218M', 0, 'boat-builders-repairers', 10, 85),
(884, 'Boat - Dealers', 'Kapal, Agen Penjualan', NULL, '', 'B1203D', 0, 'boat-dealers', 10, 316),
(885, 'Boat - Equipment & Supplies', 'Kapal, Peralatan & Pasokan', NULL, '', 'B1204F', 0, 'boat-equipment-supplies', 10, 463),
(886, 'Boat Charter & Renting,Leasing', 'Kapal,Carter & Sewa,Kontrak', NULL, '', 'B3816R', 0, 'boat-charter-rentingleasing', 10, 86),
(887, 'Boat Cruises', 'Kapal Pesiar', NULL, '', 'B3597T', 0, 'boat-cruises', 10, 505),
(888, 'Boating Manufacturers', 'Kapal - Produsen', NULL, '', 'B6529A', 0, 'boating-manufacturers', 10, 463),
(889, 'Boats Services', 'Kapal, Pelayanan', NULL, '', 'B4275K', 0, 'boats-services', 10, 316),
(890, 'Boiler Compounds', 'Ketel Uap, Campuran', NULL, '', 'B1207M', 0, 'boiler-compounds', 10, 198),
(891, 'Boiler Equipment & Supplies', 'Ketel Uap, Peralatan & Pasokan', NULL, '', 'B1208N', 0, 'boiler-equipment-supplies', 10, 268),
(892, 'Boiler Fuel Oil Catalyst', 'Ketel Uap, Katalis Minyak Bahan Bakar', NULL, '', 'B3604K', 0, 'boiler-fuel-oil-catalyst', 10, 198),
(893, 'Boiler Repairing & Cleaning', 'Ketel Uap, Perbaikan & Pembersihan', NULL, '', 'B1209R', 0, 'boiler-repairing-cleaning', 10, 380),
(894, 'Boiler Tubes', 'Ketel Uap, Pipa', NULL, '', 'B1210T', 0, 'boiler-tubes', 10, 198),
(895, 'Boilers Distributors', 'Ketel Uap, Penyalur', NULL, '', 'B4277N', 0, 'boilers-distributors', 10, 198),
(896, 'Boilers Equipment & Servicing', 'Ketel Uap, Pelayanan dan Peralatan', NULL, '', 'B5008C', 0, 'boilers-equipment-servicing', 10, 268),
(897, 'Boilers Manufacturers', 'Ketel Uap, Produsen', NULL, '', 'B4276M', 0, 'boilers-manufacturers', 10, 198),
(898, 'Bolts & Nuts', 'Baut & Mur', NULL, '', 'B1213C', 0, 'bolts-nuts', 10, 198),
(899, 'Book Binders', 'Buku, Penjilid', NULL, '', 'B1217K', 0, 'book-binders', 10, 87),
(900, 'Book Dealers', 'Buku, Agen Penjualan', NULL, '', 'B2942F', 0, 'book-dealers', 10, 87),
(901, 'Book Distributors', 'Buku, Penyalur', NULL, '', 'B4279T', 0, 'book-distributors', 10, 87),
(902, 'Book Manufacturers', 'Buku, Produsen', NULL, '', 'B4282C', 0, 'book-manufacturers', 10, 87),
(903, 'Book Retail', 'Buku, Eceran', NULL, '', 'B4280A', 0, 'book-retail', 10, 87),
(904, 'Book Spiritual - Sales', 'Buku Rohani - Penjualan', NULL, '', 'B4924K', 0, 'book-spiritual-sales', 10, 87),
(905, 'Book Store', 'Toko Buku', NULL, '', 'B7367H', 0, 'book-store', 10, 87),
(906, 'Book Wholesale', 'Buku, Grosir', NULL, '', 'B4281B', 0, 'book-wholesale', 10, 87),
(907, 'Bottle Caps & Seals', 'Botol, Tutup & Segel', NULL, '', 'B1219N', 0, 'bottle-caps-seals', 10, 198),
(908, 'Bottle Distributor', 'Botol Penyalur', NULL, '', 'B5607M', 0, 'bottle-distributor', 10, 268),
(909, 'Bottle Manufacturers', 'Botol, Produsen', NULL, '', 'B3320N', 0, 'bottle-manufacturers', 10, 482),
(910, 'Bottlers Equipment & Supplies', 'Pembuat Botol, Perlengkapan & Pasokan', NULL, '', 'B2993A', 0, 'bottlers-equipment-supplies', 10, 268),
(911, 'Bottling Machinery', 'Pembuat Botol, Mesin', NULL, '', 'B4283D', 0, 'bottling-machinery', 10, 270),
(912, 'Boutique', 'Butik', NULL, '', 'B1222A', 0, 'boutique-1', 10, 88),
(913, 'Boutique Moslem', 'Butik Muslim', NULL, '', 'B3631A', 0, 'boutique-moslem', 10, 88),
(914, 'Bowling', 'Boling', NULL, '', 'B1223B', 0, 'bowling', 10, 478),
(915, 'Bowling Equipment & Supplies', 'Boling - Perlengkapan & Pasokan', NULL, '', 'B4285H', 0, 'bowling-equipment-supplies', 10, 477),
(916, 'Boxes - Corrugated & Fibre', 'Kotak - Bergelombang & Fiber', NULL, '', 'B1226F', 0, 'boxes-corrugated-fibre', 10, 379),
(917, 'Boxes - Corrugated Carton - Manufacturers', 'Kotak - Kartun Bergelombang - Produsen', NULL, '', 'B4875B', 0, 'boxes-corrugated-carton-manufacturers', 10, 386),
(918, 'Boxes - Industries Machinery', 'Kotak - Industri Mesin', NULL, '', 'B5107C', 0, 'boxes-industries-machinery', 10, 310),
(919, 'Boxes - Paper', 'Kotak - Kertas', NULL, '', 'B1227H', 0, 'boxes-paper', 10, 386),
(920, 'Boxes - Paper, Retail', 'Kotak - Kertas, Eceran', NULL, '', 'B6739C', 0, 'boxes-paper-retail', 10, 386),
(921, 'Boxes - Wooden', 'Kotak/Peti - Kayu', NULL, '', 'B1228K', 0, 'boxes-wooden', 10, 379),
(922, 'Boxes Compact Disc Dealers', 'Kotak - Disket Kompak - Agen Penjualan', NULL, '', 'B4811D', 0, 'boxes-compact-disc-dealers', 10, 480),
(923, 'Boy''s Clothing - Distributor', 'Baju Anak-Anak (Laki-Laki) - Penyalur', NULL, '', 'B4288N', 0, 'boys-clothing-distributor', 10, 66),
(924, 'Boy''s Clothing - Manufacturers', 'Baju Anak-Anak (Laki-Laki) - Produsen', NULL, '', 'B4287M', 0, 'boys-clothing-manufacturers', 10, 66),
(925, 'Boy''s Clothing - Wholesale', 'Baju Anak-Anak (Laki-Laki) - Grosir', NULL, '', 'B4286K', 0, 'boys-clothing-wholesale', 10, 66),
(926, 'Brakes Lining Distributors', 'Rem, Penyalur', NULL, '', 'B4290T', 0, 'brakes-lining-distributors', 10, 475),
(927, 'Brakes Lining Manufacturers', 'Rem, Produsen', NULL, '', 'B4289R', 0, 'brakes-lining-manufacturers', 10, 475),
(928, 'Brakes Lining Retail', 'Rem, Penyalur', NULL, '', 'B4291A', 0, 'brakes-lining-retail', 10, 475),
(929, 'Brass Products', 'Barang-Barang Kuningan', NULL, '', 'B1231R', 0, 'brass-products-1', 10, 89),
(930, 'Brassiere Materials', 'Bahan Beha', NULL, '', 'B1232T', 0, 'brassiere-materials', 10, 519),
(931, 'Brassieres - Manufacturers', 'Beha - Produsen', NULL, '', 'B1233A', 0, 'brassieres-manufacturers', 10, 519),
(932, 'Brewers', 'Bir - Produsen', NULL, '', 'B1234B', 0, 'brewers', 10, 80),
(933, 'Bricks - Building', 'Batu Bata, Bangunan', NULL, '', 'B1235C', 0, 'bricks-building', 10, 91),
(934, 'Bricks - Fire', 'Batu Bata Api', NULL, '', 'B1236D', 0, 'bricks-fire', 10, 91),
(935, 'Bricks - Industries', 'Batu Bata - Industri', NULL, '', 'B7147K', 0, 'bricks-industries', 10, 91),
(936, 'Bridge Clubs', 'Bridge, Perkumpulan', NULL, '', 'B1237F', 0, 'bridge-clubs', 10, 478),
(937, 'Broadcasting Equipment & Supplies', 'Pemancar Siaran Perlengkapan dan Pasokan', NULL, '', 'B4233N', 0, 'broadcasting-equipment-supplies', 10, 93),
(938, 'Brokers - Interbank', 'Pialang Antar Bank', NULL, '', 'B6536M', 0, 'brokers-interbank', 10, 318),
(939, 'Brokers Representatives - Paper', 'Pialang, Perwakilan - Kertas', NULL, '', 'B4292B', 0, 'brokers-representatives-paper', 10, 386),
(940, 'Bronze', 'Perunggu', NULL, '', 'B1240M', 0, 'bronze', 10, 198),
(941, 'Brush - Making Machines', 'Sikat, Mesin Pembuat', NULL, '', 'B3605M', 0, 'brush-making-machines', 10, 270),
(942, 'Brushes Equipment & Supplies', 'Sikat, Peralatan & Pasokan', NULL, '', 'B4295F', 0, 'brushes-equipment-supplies', 10, 268),
(943, 'Brushes Manufacturers', 'Sikat, Produsen', NULL, '', 'B4293C', 0, 'brushes-manufacturers', 10, 439),
(944, 'Brushes Wholesale', 'Sikat, Grosir', NULL, '', 'B4294D', 0, 'brushes-wholesale', 10, 439),
(945, 'Buckets - Manufacturers', 'Keruk, Alat - Produsen', NULL, '', 'B3196H', 0, 'buckets-manufacturers', 10, 172),
(946, 'Buckles - Wholesale', 'Gesper - Grosir', NULL, '', 'B4800D', 0, 'buckles-wholesale', 10, 26),
(947, 'Building - Portable', 'Bangunan - Terpindahkan', NULL, '', 'B1249H', 0, 'building-portable-1', 10, 94),
(948, 'Building - Pre-Cut & Prefabricated', 'Bangunan, Pra & Pracetak', NULL, '', 'B1250K', 0, 'building-pre-cut-prefabricated', 10, 95),
(949, 'Building Automation Systems', 'Bangunan, Sistem Otomasi', NULL, '', 'B3366T', 0, 'building-automation-systems', 10, 95),
(950, 'Building Cleaning', 'Bangunan, Pembersihan', NULL, '', 'B1245B', 0, 'building-cleaning', 10, 96),
(951, 'Building Construction Consultants', 'Gedung, Pembangunan - Konsultan', NULL, '', 'B1246C', 0, 'building-construction-consultants', 10, 95),
(952, 'Building Draughtsman', 'Bangunan, Perancang', NULL, '', 'B1247D', 0, 'building-draughtsman', 10, 139),
(953, 'Building Maintenance', 'Pemeliharaan Gedung', NULL, '', 'B3437F', 0, 'building-maintenance', 10, 96),
(954, 'Building Material - Retail', 'Bahan Bangunan - Eceran', NULL, '', 'B7044M', 0, 'building-material-retail', 10, 337),
(955, 'Building Materials', 'Bahan Bangunan', NULL, '', 'B1248F', 0, 'building-materials', 10, 337),
(956, 'Building Materials - Manufacturers', 'Bahan Bangunan - Produsen', NULL, '', 'B3354R', 0, 'building-materials-manufacturers', 10, 337),
(957, 'Bungalow', 'Bungalo', NULL, '', 'B1251M', 0, 'bungalow-1', 10, 97),
(958, 'Buoys - Marine', 'Pelampung Pelayaran', NULL, '', 'B1252N', 0, 'buoys-marine', 10, 464),
(959, 'Burglar Alarm Systems', 'Pencuri, Tanda Bahaya/Alarm - Sistem', NULL, '', 'B1244A', 0, 'burglar-alarm-systems', 10, 459),
(960, 'Burners - Oil/Gas', 'Tungku - Minyak/Gas', NULL, '', 'B3281C', 0, 'burners-oil-gas', 10, 198),
(961, 'Bus Lines', 'Bus, Jalur/Trayek', NULL, '', 'B1253R', 0, 'bus-lines', 10, 99),
(962, 'Buses - Bodies', 'Bus, Badan', NULL, '', 'B1255A', 0, 'buses-bodies', 10, 475),
(963, 'Buses - Charter & Rental', 'Bus - Carter & Sewa', NULL, '', 'B1254T', 0, 'buses-charter-rental', 10, 99),
(964, 'Buses - Distributors & Manufacturers', 'Bus - Penyalur & Produsen', NULL, '', 'B1256B', 0, 'buses-distributors-manufacturers', 10, 98),
(965, 'Buses - Parts & Supplies', 'Bus - Suku Cadang & Pasokan', NULL, '', 'B1257C', 0, 'buses-parts-supplies', 10, 475),
(966, 'Business & Trade Organizations', 'Organisasi Perusahaan & Niaga', NULL, '', 'B1258D', 0, 'business-trade-organizations', 10, 58),
(967, 'Business Brokers', 'Usaha/Perusahaan, Pialang', NULL, '', 'B1259F', 0, 'business-brokers', 10, 318),
(968, 'Business Cards', 'Kartu Niaga', NULL, '', 'B1260H', 0, 'business-cards', 10, 107),
(969, 'Business Centres', 'Pusat Pelayanan Bisnis', NULL, '', 'B3407M', 0, 'business-centres', 10, 376),
(970, 'Business Consultants', 'Perusahaan, Konsultan', NULL, '', 'B1262M', 0, 'business-consultants', 10, 141),
(971, 'Business Management', 'Manajemen Perusahaan', NULL, '', 'B1264R', 0, 'business-management', 10, 141),
(972, 'Button Covering Machines', 'Kancing, Pembungkus - Mesin', NULL, '', 'B1265T', 0, 'button-covering-machines', 10, 26),
(973, 'Buttons', 'Kancing', NULL, '', 'B1266A', 0, 'buttons', 10, 26),
(974, 'Bycycles, Electric', 'Sepeda, Elektrik', NULL, '', 'B7142C', 0, 'bycycles-electric', 10, 81),
(975, 'Cable Flexible', 'Cabel Lunak', NULL, '', 'C4242K', 0, 'cable-flexible', 10, 183),
(976, 'Cable Reels', 'Kabel Gulung', NULL, '', 'C3581H', 0, 'cable-reels', 10, 183),
(977, 'Cable Splicing', 'Kabel, Penyambungan', NULL, '', 'C1267B', 0, 'cable-splicing', 10, 183),
(978, 'Cable Support System', 'Kabel, Sistem - Bantuan', NULL, '', 'C6870D', 0, 'cable-support-system', 10, 186),
(979, 'Cables - Manufacturers', 'Kabel, Produsen', NULL, '', 'C1268C', 0, 'cables-manufacturers', 10, 183),
(980, 'Cables - Wire', 'Kabel - Kawat', NULL, '', 'C3368B', 0, 'cables-wire', 10, 183),
(981, 'Cafe', 'Kafe', NULL, '', 'C3997D', 0, 'cafe-1', 10, 100),
(982, 'Cafeteria', 'Kafetaria', NULL, '', 'C1269D', 0, 'cafeteria', 10, 100),
(983, 'Cake Ingredients', 'Kue Bahan-Bahan', NULL, '', 'C3984B', 0, 'cake-ingredients-1', 10, 101),
(984, 'Calculating & Adding Machines - Manufacturers', 'Mesin Hitung & Mesin Penjumlahan - Produsen', NULL, '', 'C4298M', 0, 'calculating-adding-machines-manufacturers', 10, 102),
(985, 'Calculating & Adding Machines - Wholesale', 'Mesin Hitung & Mesin Penjumlahan - Grosir', NULL, '', 'C4297K', 0, 'calculating-adding-machines-wholesale', 10, 102),
(986, 'Calculating & Adding Machines Supplies', 'Mesin Hitung & Mesin Penjumlah - Pasokan', NULL, '', 'C1270F', 0, 'calculating-adding-machines-supplies', 10, 102),
(987, 'Calculator', 'Kalkulator', NULL, '', 'C4169T', 0, 'calculator', 10, 102),
(988, 'Calendars', 'Kalender', NULL, '', 'C1272K', 0, 'calendars', 10, 412),
(989, 'Calibration', 'Kalibrasi', NULL, '', 'C3980N', 0, 'calibration', 10, 145),
(990, 'Call Center', 'Panggilan, Pusat', NULL, '', 'C4250D', 0, 'call-center', 10, 376),
(991, 'Camphor', 'Kamper', NULL, '', 'C3216D', 0, 'camphor', 10, 103),
(992, 'Camping Equipment', 'Perkemahan, Peralatan', NULL, '', 'C3400A', 0, 'camping-equipment', 10, 477),
(993, 'Candles', 'Lilin', NULL, '', 'C1274N', 0, 'candles-1', 10, 104),
(994, 'Canned Goods', 'Makanan Kalengan', NULL, '', 'C1276T', 0, 'canned-goods', 10, 214),
(995, 'Canners', 'Kaleng - Pembuat', NULL, '', 'C1277A', 0, 'canners', 10, 326),
(996, 'Canning Boxes - Metal', 'Kotak Kaleng Logam', NULL, '', 'C1278B', 0, 'canning-boxes-metal', 10, 198),
(997, 'Cans', 'Kaleng', NULL, '', 'C3223T', 0, 'cans', 10, 198),
(998, 'Canvas Products', 'Barang-Barang Kain Terpal', NULL, '', 'C1280D', 0, 'canvas-products', 10, 501),
(999, 'Canvas-Manufacturers', 'Terpal - Produsen', NULL, '', 'C4299N', 0, 'canvas-manufacturers', 10, 379),
(1000, 'Canvas-Wholesale', 'Terpal - Grosir', NULL, '', 'C4300R', 0, 'canvas-wholesale', 10, 379),
(1001, 'Capacitors', 'Kapasitor', NULL, '', 'C1282H', 0, 'capacitors', 10, 183);

-- --------------------------------------------------------

--
-- Table structure for table `category_old`
--

CREATE TABLE `category_old` (
  `id` int(11) NOT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `dty` varchar(45) DEFAULT NULL,
  `area_code` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` text,
  `slug` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `province_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `dty`, `area_code`, `name`, `description`, `slug`, `weight`, `province_id`) VALUES
(2, 'C401', '0283', 'Adiwerna', '', 'adiwerna', 10, 10),
(3, 'O301', '0624', 'Aekanopan', '', 'aekanopan', 10, 33),
(4, 'O312', '0624', 'Aekgoti', '', 'aekgoti', 10, 33),
(5, 'O311', '0624', 'Aekkotabatu', '', 'aekkotabatu', 10, 33),
(6, 'O302', '0624', 'Aeknabara', '', 'aeknabara', 10, 33),
(7, '3503', '0902', 'Agats', '', 'agats', 10, 25),
(8, 'R101', '0431', 'Airmadidi', '', 'airmadidi', 10, 30),
(9, 'W201', '0769', 'Airmolek', '', 'airmolek', 10, 26),
(10, 'C308', '0281', 'Ajibarang', '', 'ajibarang', 10, 10),
(11, 'Y301', '0755', 'Alahanpanjang', '', 'alahanpanjang', 10, 31),
(12, 'J509', '0372', 'Alas', '', 'alas', 10, 21),
(13, 'H101', '0298', 'Ambarawa', '', 'ambarawa', 10, 10),
(14, '4100', '0911', 'Ambon', '', 'ambon', 10, 18),
(15, 'E301', '0336', 'Ambulu', '', 'ambulu', 10, 11),
(16, '7213', '0328', 'Ambunten', '', 'ambunten', 10, 11),
(17, 'S101', '0363', 'Amlapura', '', 'amlapura', 10, 1),
(18, 'L416', '0522', 'Ampah', '', 'ampah', 10, 13),
(19, 'U501', '0464', 'Ampana', '', 'ampana', 10, 28),
(20, 'T217', '0341', 'Ampelgading', '', 'ampelgading', 10, 11),
(21, 'L401', '0527', 'Amuntai', '', 'amuntai', 10, 13),
(22, 'R102', '0430', 'Amurang', '', 'amurang', 10, 30),
(23, 'I201', '0737', 'Argamakmur', '', 'argamakmur', 10, 4),
(24, 'E311', '0331', 'Arjasa', '', 'arjasa', 10, 11),
(25, '7210', '0327', 'Arjasakangean', '', 'arjasakangean', 10, 11),
(26, 'F101', '0231', 'Arjawinangun', '', 'arjawinangun', 10, 9),
(27, 'P336', '0357', 'Arjosari', '', 'arjosari', 10, 11),
(28, '7209', '031', 'Arosbaya', '', 'arosbaya', 10, 11),
(29, 'E601', '0338', 'Asembagus', '', 'asembagus', 10, 11),
(30, 'J401', '0389', 'Atambua', '', 'atambua', 10, 22),
(31, 'J402', '0380', 'Baa', '', 'baa', 10, 22),
(32, 'D107', '0322', 'Babad', '', 'babad', 10, 11),
(33, 'P101', '0322', 'Babad', '', 'babad-1', 10, 11),
(34, '3504', '0974', 'Bade', '', 'bade', 10, 25),
(35, 'W105', '0765', 'Bagan Batu', '', 'bagan-batu', 10, 26),
(36, 'W103', '0767', 'Bagansiapi-Api', '', 'bagansiapi-api', 10, 26),
(37, 'J412', '0384', 'Bajawa', '', 'bajawa', 10, 22),
(38, 'M112', '0657', 'Bakongan', '', 'bakongan', 10, 20),
(39, 'L317', '0564', 'Balaikarangan', '', 'balaikarangan', 10, 12),
(40, 'Y101', '0757', 'Balaiselasa', '', 'balaiselasa', 10, 31),
(41, 'C402', '0283', 'Balapulang', '', 'balapulang', 10, 10),
(42, 'O401', '0632', 'Balige', '', 'balige', 10, 33),
(43, 'L100', '0542', 'Balikpapan', '', 'balikpapan', 20, 15),
(44, 'F118', '0234', 'Balongan', '', 'balongan', 10, 9),
(45, 'E302', '0336', 'Balung', '', 'balung', 10, 11),
(46, 'D106', '031', 'Balung Panggang', '', 'balung-panggang', 10, 11),
(47, 'P110', '0356', 'Bancar', '', 'bancar', 10, 11),
(48, 'M100', '0651', 'Banda Aceh', '', 'banda-aceh', 10, 20),
(49, '4103', '0910', 'Bandaneira', '', 'bandaneira', 10, 18),
(50, 'P331', '0357', 'Bandar', '', 'bandar', 10, 11),
(51, 'O109', '0628', 'Bandarbaru', '', 'bandarbaru', 10, 33),
(52, 'O310', '0624', 'Bandardurian', '', 'bandardurian', 10, 33),
(53, 'V209', '0725', 'Bandarjaya', '', 'bandarjaya', 10, 17),
(54, 'V200', '0721', 'Bandarlampung', '', 'bandarlampung', 10, 17),
(55, 'C201', '0285', 'Bandarsedayu', '', 'bandarsedayu', 10, 10),
(56, 'B100', '022', 'Bandung', '', 'bandung', 20, 9),
(57, 'U201', '0462', 'Banggai', '', 'banggai', 10, 28),
(58, 'T301', '0343', 'Bangil', '', 'bangil', 10, 11),
(59, '7201', '031', 'Bangkalan', '', 'bangkalan', 10, 11),
(60, 'W202', '0762', 'Bangkinang', '', 'bangkinang', 10, 26),
(61, 'I301', '0746', 'Bangko', '', 'bangko', 10, 8),
(62, 'Z600', 'B''kok', 'Bangkok', '', 'bangkok', 10, 34),
(63, 'S102', '0366', 'Bangli', '', 'bangli', 10, 1),
(64, 'C601', '0291', 'Bangsri', '', 'bangsri', 10, 10),
(65, 'K301', '0265', 'Banjar', '', 'banjar', 10, 9),
(66, 'B101', '022', 'Banjaran', '', 'banjaran', 10, 9),
(67, 'L402', '0511', 'Banjarbaru', '', 'banjarbaru', 10, 13),
(68, 'L400', '0511', 'Banjarmasin', '', 'banjarmasin', 10, 13),
(69, 'C301', '0286', 'Banjarnegara', '', 'banjarnegara', 10, 10),
(70, 'K302', '0265', 'Banjarsari', '', 'banjarsari', 10, 9),
(71, 'N101', '0413', 'Bantaeng', '', 'bantaeng', 10, 27),
(72, 'G104', '0274', 'Bantul', '', 'bantul', 10, 5),
(73, 'T218', '0341', 'Bantur', '', 'bantur', 10, 11),
(74, 'C302', '0281', 'Banyumas', '', 'banyumas', 10, 10),
(75, 'E100', '0333', 'Banyuwangi', '', 'banyuwangi', 10, 11),
(76, 'L403', '0517', 'Barabai', '', 'barabai', 10, 13),
(77, 'N201', '0427', 'Barru', '', 'barru', 10, 27),
(78, 'W300', '0778', 'Batam', '', 'batam', 10, 16),
(79, 'C202', '0285', 'Batang', '', 'batang', 10, 10),
(80, '7215', '0328', 'Batang-Batang', '', 'batang-batang', 10, 11),
(81, 'O402', '0634', 'Batangtoru', '', 'batangtoru', 10, 33),
(82, 'T201', '0341', 'Batu', '', 'batu', 10, 11),
(83, 'B120', '022', 'Batujajar', '', 'batujajar', 10, 9),
(84, 'I100', '0735', 'Baturaja', '', 'baturaja', 10, 32),
(85, 'G201', '0273', 'Baturetno', '', 'baturetno', 10, 10),
(86, 'S103', '0368', 'Baturiti', '', 'baturiti', 10, 1),
(87, 'Y201', '0752', 'Batusangkar', '', 'batusangkar', 10, 31),
(88, 'U101', '0402', 'Bau-Bau', '', 'bau-bau', 10, 29),
(89, 'D103', '0325', 'Bawean', '', 'bawean', 10, 11),
(90, 'K707', '0252', 'Bayah', '', 'bayah', 10, 9),
(91, 'T302', '0343', 'Beji', '', 'beji', 10, 11),
(92, 'A101', '021', 'Bekasi', '', 'bekasi', 40, 9),
(93, 'X101', '061', 'Belawan', '', 'belawan', 10, 33),
(94, 'I507', '0715', 'Belinyu', '', 'belinyu', 10, 32),
(95, 'I101', '0735', 'Belitang', '', 'belitang', 10, 32),
(96, 'E101', '0333', 'Benculuk', '', 'benculuk', 10, 11),
(97, 'W101', '0766', 'Bengkalis', '', 'bengkalis', 10, 26),
(98, 'L301', '0562', 'Bengkayang', '', 'bengkayang', 10, 12),
(99, 'I200', '0736', 'Bengkulu', '', 'bengkulu', 10, 4),
(100, 'R109', '0432', 'Beo', '', 'beo', 10, 30),
(101, 'P324', '0351', 'Beringin', '', 'beringin', 10, 11),
(102, 'E602', '0338', 'Besuki', '', 'besuki', 10, 11),
(103, 'V105', '0711', 'Betung', '', 'betung', 10, 32),
(104, 'M101', '0653', 'Beureuneun', '', 'beureuneun', 10, 20),
(105, '3100', '0981', 'Biak', '', 'biak', 10, 24),
(106, 'J504', '0374', 'Bima', '', 'bima', 10, 21),
(107, 'T106', '0342', 'Binangun', '', 'binangun', 10, 11),
(108, 'X102', '061', 'Binjai', '', 'binjai', 10, 33),
(109, 'W301', '0765', 'Bintan', '', 'bintan', 10, 26),
(110, 'I202', '0739', 'Bintuhan', '', 'bintuhan', 10, 4),
(111, '3401', '0955', 'Bintuni', '', 'bintuni', 10, 23),
(112, 'M301', '0644', 'Bireuen', '', 'bireuen', 10, 20),
(113, 'R103', '0438', 'Bitung', '', 'bitung', 10, 30),
(114, 'V213', '0723', 'Blambangan Umpu', '', 'blambangan-umpu', 10, 17),
(115, 'O101', '0642', 'Blangkejeren', '', 'blangkejeren', 10, 20),
(116, 'M113', '0659', 'Blangpidie', '', 'blangpidie', 10, 20),
(117, '7207', '031', 'Blega', '', 'blega', 10, 11),
(118, 'T100', '0342', 'Blitar', '', 'blitar', 10, 11),
(119, 'C801', '0296', 'Blora', '', 'blora', 10, 10),
(120, 'C305', '0281', 'Bobotsari', '', 'bobotsari', 10, 10),
(121, 'Q100', '0251', 'Bogor', '', 'bogor', 30, 9),
(122, 'H102', '0294', 'Boja', '', 'boja', 10, 10),
(123, 'P100', '0353', 'Bojonegoro', '', 'bojonegoro', 10, 11),
(124, 'K201', '0266', 'Bojonglopang', '', 'bojonglopang', 10, 9),
(125, 'E200', '0332', 'Bondowoso', '', 'bondowoso', 10, 11),
(126, 'L202', '0548', 'Bontang', '', 'bontang', 10, 15),
(127, 'G202', '0276', 'Boyolali', '', 'boyolali', 10, 10),
(128, 'O102', '0628', 'Brastagi', '', 'brastagi', 10, 33),
(129, 'C403', '0283', 'Brebes', '', 'brebes', 10, 10),
(130, 'D108', '0322', 'Brondong', '', 'brondong', 10, 11),
(131, 'P102', '0322', 'Brondong', '', 'brondong-1', 10, 11),
(132, 'Z300', 'Brunei', 'Brunei', '', 'brunei', 10, 34),
(133, 'P112', '0353', 'Bubulan', '', 'bubulan', 10, 11),
(134, 'V212', '0724', 'Bukit Kemuning', '', 'bukit-kemuning', 10, 17),
(135, 'Y200', '0752', 'Bukittinggi', '', 'bukittinggi', 10, 31),
(136, '4107', '0915', 'Bula', '', 'bula', 10, 18),
(137, 'N102', '0413', 'Bulukumba', '', 'bulukumba', 10, 27),
(138, 'T202', '0341', 'Bululawang', '', 'bululawang', 10, 11),
(139, 'C404', '0289', 'Bumiayu', '', 'bumiayu', 10, 10),
(140, 'U202', '0463', 'Bunta', '', 'bunta', 10, 28),
(141, 'L404', '0525', 'Buntok', '', 'buntok', 10, 14),
(142, 'U307', '0445', 'Buol', '', 'buol', 10, 28),
(143, 'T214', '0341', 'Buring', '', 'buring', 10, 11),
(144, 'M114', '0654', 'Calang', '', 'calang', 10, 20),
(145, 'J403', '0380', 'Camplong', '', 'camplong', 10, 22),
(146, 'P401', '0355', 'Campurdarat', '', 'campurdarat', 10, 11),
(147, 'C712', '0293', 'Candimulyo', '', 'candimulyo', 10, 10),
(148, 'Y209', '0752', 'Candung', '', 'candung', 10, 31),
(149, 'N217', '0484', 'Cangadi', '', 'cangadi', 10, 27),
(150, 'P301', '0351', 'Caruban', '', 'caruban', 10, 11),
(151, 'C800', '0296', 'Cepu', '', 'cepu', 10, 10),
(152, 'D101', '031', 'Cerme', '', 'cerme', 10, 11),
(153, 'K303', '0265', 'Ciamis', '', 'ciamis', 10, 9),
(154, 'K600', '0263', 'Cianjur', '', 'cianjur', 10, 9),
(155, 'K802', '0260', 'Ciasem', '', 'ciasem', 10, 9),
(156, 'K308', '0265', 'Ciawi', '', 'ciawi', 10, 9),
(157, 'K202', '0266', 'Cibadak', '', 'cibadak', 10, 9),
(158, 'K304', '0265', 'Cibalong', '', 'cibalong', 10, 9),
(159, 'K501', '0262', 'Cibatu', '', 'cibatu', 10, 9),
(160, 'K601', '0263', 'Cibeber', '', 'cibeber', 10, 9),
(161, 'A102', '021', 'Cibinong', '', 'cibinong', 10, 9),
(162, 'B102', '022', 'Cicalengka', '', 'cicalengka', 10, 9),
(163, 'K203', '0266', 'Cicurug', '', 'cicurug', 10, 9),
(164, 'K502', '0262', 'Cikajang', '', 'cikajang', 10, 9),
(165, 'K602', '0263', 'Cikalongkulon', '', 'cikalongkulon', 10, 9),
(166, 'B103', '022', 'Cikalongwetan', '', 'cikalongwetan', 10, 9),
(167, 'K204', '0266', 'Cikembang', '', 'cikembang', 10, 9),
(168, 'F116', '0233', 'Cikijing', '', 'cikijing', 10, 9),
(169, 'C500', '0282', 'Cilacap', '', 'cilacap', 10, 10),
(170, 'K101', '0254', 'Cilegon', '', 'cilegon', 10, 3),
(171, 'C307', '0281', 'Cilongok', '', 'cilongok', 10, 10),
(172, 'B104', '022', 'Cimahi', '', 'cimahi', 10, 9),
(173, 'K209', '0266', 'Cimangkok', '', 'cimangkok', 10, 9),
(174, 'B119', '022', 'Cipatat', '', 'cipatat', 10, 9),
(175, 'K603', '0263', 'Ciranjang', '', 'ciranjang', 10, 9),
(176, 'F100', '0231', 'Cirebon', '', 'cirebon', 10, 9),
(177, 'K503', '0262', 'Cisompet', '', 'cisompet', 10, 9),
(178, 'B105', '022', 'Ciwidey', '', 'ciwidey', 10, 9),
(179, 'C203', '0285', 'Comal', '', 'comal', 10, 10),
(180, 'I203', '0732', 'Curup', '', 'curup', 10, 4),
(181, 'W401', '0776', 'Dabosingkep', '', 'dabosingkep', 10, 16),
(182, 'T203', '0341', 'Dampit', '', 'dampit', 10, 11),
(183, 'Z400', '8', 'Darwin', '', 'darwin', 10, 34),
(184, 'G203', '0272', 'Delangu', '', 'delangu', 10, 10),
(185, 'C602', '0291', 'Demak', '', 'demak', 10, 10),
(186, 'S100', '0361', 'Denpasar', '', 'denpasar', 20, 1),
(187, 'A103', '021', 'Depok', '', 'depok', 25, 9),
(188, '7108', '0321', 'Dlanggu', '', 'dlanggu', 10, 11),
(189, '4301', '0917', 'Dobo', '', 'dobo', 10, 18),
(190, 'O403', '0633', 'Dolok Sanggul', '', 'dolok-sanggul', 10, 33),
(191, 'J505', '0373', 'Dompu', '', 'dompu', 10, 21),
(192, 'U301', '0457', 'Donggala', '', 'donggala', 10, 28),
(193, 'T219', '0341', 'Donomulyo', '', 'donomulyo', 10, 11),
(194, 'D105', '031', 'Duduk Sampeyan', '', 'duduk-sampeyan', 10, 11),
(195, 'W100', '0765', 'Dumai', '', 'dumai', 10, 26),
(196, 'P402', '0355', 'Durenan', '', 'durenan', 10, 11),
(197, 'W102', '0765', 'Duri', '', 'duri', 10, 26),
(198, '4304', '0916', 'Elat', '', 'elat', 10, 18),
(199, 'J411', '0381', 'Ende', '', 'ende', 10, 22),
(200, 'N203', '0420', 'Enrekang', '', 'enrekang', 10, 27),
(201, '3200', '0956', 'Fakfak', '', 'fakfak', 10, 23),
(202, 'X106', '061', 'Galang', '', 'galang', 10, 33),
(203, '4208', '0924', 'Galela', '', 'galela', 10, 19),
(204, 'K500', '0262', 'Garut', '', 'garut', 10, 9),
(205, '7305', '031', 'Gedangan', '', 'gedangan', 10, 11),
(206, 'V201', '0721', 'Gedongtataan', '', 'gedongtataan', 10, 17),
(207, 'P319', '0351', 'Gemarang', '', 'gemarang', 10, 11),
(208, 'G211', '0271', 'Gemolong', '', 'gemolong', 10, 10),
(209, 'T303', '0343', 'Gempol', '', 'gempol', 10, 11),
(210, 'E504', '0335', 'Gending', '', 'gending', 10, 11),
(211, 'E102', '0333', 'Genteng', '', 'genteng', 10, 11),
(212, 'J501', '0370', 'Gerung', '', 'gerung', 10, 21),
(213, 'S104', '0361', 'Gianyar', '', 'gianyar', 10, 1),
(214, 'E510', '0335', 'Gili', '', 'gili', 10, 11),
(215, 'E103', '0333', 'Glenmore', '', 'glenmore', 10, 11),
(216, 'G101', '0274', 'Godean', '', 'godean', 10, 5),
(217, 'C603', '0292', 'Godong', '', 'godong', 10, 10),
(218, 'C701', '0287', 'Gombong', '', 'gombong', 10, 10),
(219, 'P212', '0358', 'Gondang', '', 'gondang', 10, 11),
(220, 'T309', '0343', 'Gondang Wetan', '', 'gondang-wetan', 10, 11),
(221, 'T204', '0341', 'Gondanglegi', '', 'gondanglegi', 10, 11),
(222, 'P302', '0351', 'Gorang Gareng', '', 'gorang-gareng', 10, 11),
(223, 'R200', '0435', 'Gorontalo', '', 'gorontalo', 10, 7),
(224, 'C714', '0293', 'Grabag', '', 'grabag', 10, 10),
(225, 'T304', '0343', 'Grati', '', 'grati', 10, 11),
(226, 'D102', '031', 'Gresik', '', 'gresik', 10, 11),
(227, 'C604', '0292', 'Gubug', '', 'gubug', 10, 10),
(228, 'B106', '022', 'Gununghalu', '', 'gununghalu', 10, 9),
(229, 'T215', '0341', 'Gunungkawi', '', 'gunungkawi', 10, 11),
(230, 'O404', '0639', 'Gunungsitoli', '', 'gunungsitoli', 10, 33),
(231, 'P209', '0354', 'Gurah', '', 'gurah', 10, 11),
(232, 'Y210', '0752', 'Harau', '', 'harau', 10, 31),
(233, 'F117', '0234', 'Haur Geulis', '', 'haur-geulis', 10, 9),
(234, 'Z500', 'Hk', 'Hongkong', '', 'hongkong', 10, 34),
(235, 'R110', '0432', 'Hulusiau', '', 'hulusiau', 10, 30),
(236, 'M201', '0646', 'Idi', '', 'idi', 10, 20),
(237, 'Z900', 'India', 'India', '', 'india', 10, 34),
(238, 'F102', '0234', 'Indramayu', '', 'indramayu', 10, 9),
(239, 'I208', '0737', 'Ipuh', '', 'ipuh', 10, 4),
(240, 'R204', '0435', 'Isimu', '', 'isimu', 10, 7),
(241, '4205', '0922', 'Jailolo', '', 'jailolo', 10, 19),
(242, 'A100', '021', 'Jakarta', '', 'jakarta', 50, 6),
(243, 'K806', '0260', 'Jalan Cagak', '', 'jalan-cagak', 10, 9),
(244, 'I300', '0741', 'Jambi', '', 'jambi', 10, 8),
(245, 'F103', '0231', 'Jamblang', '', 'jamblang', 10, 9),
(246, 'K205', '0266', 'Jampang Kulon', '', 'jampang-kulon', 10, 9),
(247, 'M102', '0651', 'Jantho', '', 'jantho', 10, 20),
(248, 'Z800', 'Japan', 'Japan', '', 'japan', 10, 34),
(249, 'F104', '0234', 'Jatibarang', '', 'jatibarang', 10, 9),
(250, 'P213', '0358', 'Jatikalen', '', 'jatikalen', 10, 11),
(251, 'P103', '0356', 'Jatirogo', '', 'jatirogo', 10, 11),
(252, 'E401', '0334', 'Jatiroto', '', 'jatiroto', 10, 11),
(253, 'F105', '0233', 'Jatiwangi', '', 'jatiwangi', 10, 9),
(254, '3300', '0967', 'Jayapura', '', 'jayapura', 10, 25),
(255, 'E300', '0331', 'Jember', '', 'jember', 10, 11),
(256, 'P318', '0352', 'Jenangan', '', 'jenangan', 10, 11),
(257, 'N103', '0419', 'Jeneponto', '', 'jeneponto', 10, 27),
(258, 'E303', '0331', 'Jenggawah', '', 'jenggawah', 10, 11),
(259, 'C605', '0291', 'Jepara', '', 'jepara', 10, 10),
(260, 'P330', '0357', 'Jeruk', '', 'jeruk', 10, 11),
(261, 'M115', '0655', 'Jeuram', '', 'jeuram', 10, 20),
(262, 'P317', '0351', 'Jogorogo', '', 'jogorogo', 10, 11),
(263, '7101', '0321', 'Jombang', '', 'jombang', 10, 11),
(264, 'C606', '0295', 'Juwana', '', 'juwana', 10, 10),
(265, 'O100', '0628', 'Kabanjahe', '', 'kabanjahe', 10, 33),
(266, 'F106', '0233', 'Kadipaten', '', 'kadipaten', 10, 9),
(267, 'K504', '0262', 'Kadungora', '', 'kadungora', 10, 9),
(268, '3201', '0957', 'Kaimana', '', 'kaimana', 10, 23),
(269, 'C208', '0285', 'Kajen', '', 'kajen', 10, 10),
(270, 'J404', '0386', 'Kalabahi', '', 'kalabahi', 10, 22),
(271, 'K206', '0266', 'Kalapanunggal', '', 'kalapanunggal', 10, 9),
(272, 'G102', '0274', 'Kalasan', '', 'kalasan', 10, 5),
(273, 'V202', '0727', 'Kalianda', '', 'kalianda', 10, 17),
(274, 'E104', '0333', 'Kalibaru', '', 'kalibaru', 10, 11),
(275, 'C209', '0285', 'Kalibening', '', 'kalibening', 10, 10),
(276, 'P406', '0355', 'Kalidawir', '', 'kalidawir', 10, 11),
(277, 'K805', '0260', 'Kalijati', '', 'kalijati', 10, 9),
(278, 'E304', '0331', 'Kalisat', '', 'kalisat', 10, 11),
(279, 'P111', '0353', 'Kalitidu', '', 'kalitidu', 10, 11),
(280, '7202', '031', 'Kamal', '', 'kamal', 10, 11),
(281, 'F121', '0231', 'Kanci', '', 'kanci', 10, 9),
(282, 'L405', '0517', 'Kandangan', '', 'kandangan', 10, 13),
(283, 'P201', '0354', 'Kandangan Pare', '', 'kandangan-pare', 10, 11),
(284, 'F107', '0234', 'Karangampel', '', 'karangampel', 10, 9),
(285, 'C702', '0287', 'Karanganyar', '', 'karanganyar', 10, 10),
(286, 'G204', '0271', 'Karanganyarsurakarta', '', 'karanganyarsurakarta', 10, 10),
(287, 'P325', '0351', 'Karangjati', '', 'karangjati', 10, 11),
(288, 'K311', '0265', 'Karangnunggal', '', 'karangnunggal', 10, 9),
(289, '7220', '0323', 'Karangpenang', '', 'karangpenang', 10, 11),
(290, 'T213', '0341', 'Karangploso', '', 'karangploso', 10, 11),
(291, 'K402', '0267', 'Karawang', '', 'karawang', 10, 9),
(292, 'P320', '0351', 'Kare', '', 'kare', 10, 11),
(293, 'C611', '0297', 'Karimunjawa', '', 'karimunjawa', 10, 10),
(294, 'P113', '0353', 'Kasiman', '', 'kasiman', 10, 11),
(295, 'L501', '0536', 'Kasongan', '', 'kasongan', 10, 14),
(296, 'K310', '0265', 'Kawali', '', 'kawali', 10, 9),
(297, 'Y108', '0751', 'Kayu Tanam', '', 'kayu-tanam', 10, 31),
(298, 'V101', '0712', 'Kayuagung', '', 'kayuagung', 10, 32),
(299, 'C703', '0287', 'Kebumen', '', 'kebumen', 10, 10),
(300, 'P200', '0354', 'Kediri', '', 'kediri', 10, 11),
(301, 'P114', '0353', 'Kedungadem', '', 'kedungadem', 10, 11),
(302, 'C204', '0285', 'Kedungwuni', '', 'kedungwuni', 10, 10),
(303, 'J405', '0388', 'Kefamenanu', '', 'kefamenanu', 10, 22),
(304, 'C607', '0291', 'Keling', '', 'keling', 10, 10),
(305, 'E305', '0336', 'Kencong', '', 'kencong', 10, 11),
(306, 'H103', '0294', 'Kendal', '', 'kendal', 10, 10),
(307, 'U100', '0401', 'Kendari', '', 'kendari', 10, 28),
(308, 'L302', '0534', 'Kendawangan', '', 'kendawangan', 10, 12),
(309, 'I204', '0732', 'Kepahiang', '', 'kepahiang', 10, 4),
(310, 'T205', '0341', 'Kepanjen', '', 'kepanjen', 10, 11),
(311, 'P115', '0353', 'Kepuhbaru', '', 'kepuhbaru', 10, 11),
(312, 'P109', '0356', 'Kerek', '', 'kerek', 10, 11),
(313, 'P202', '0358', 'Kertosono', '', 'kertosono', 10, 11),
(314, 'T101', '0342', 'Kesamben', '', 'kesamben', 10, 11),
(315, 'C405', '0283', 'Ketanggungan Timur', '', 'ketanggungan-timur', 10, 10),
(316, 'L303', '0534', 'Ketapang', '', 'ketapang', 10, 12),
(317, '7203', '0323', 'Ketapang', '', 'ketapang-1', 10, 11),
(318, 'E111', '0333', 'Ketapang', '', 'ketapang-2', 10, 11),
(319, 'W505', '0765', 'Khairah Mandah', '', 'khairah-mandah', 10, 26),
(320, 'O202', '0623', 'Kisaran', '', 'kisaran', 10, 33),
(321, 'E402', '0334', 'Klakah', '', 'klakah', 10, 11),
(322, 'C306', '0286', 'Klampok', '', 'klampok', 10, 10),
(323, 'G205', '0272', 'Klaten', '', 'klaten', 10, 10),
(324, 'I503', '0718', 'Koba Palembang', '', 'koba-palembang', 10, 32),
(325, 'U102', '0405', 'Kolaka', '', 'kolaka', 10, 29),
(326, 'U503', '0465', 'Kolonedale', '', 'kolonedale', 10, 28),
(327, 'V203', '0722', 'Kotaagung', '', 'kotaagung', 10, 17),
(328, 'W508', '0765', 'Kotabaru Siberida', '', 'kotabaru-siberida', 10, 26),
(329, 'L406', '0518', 'Kotabarupulaulaut', '', 'kotabarupulaulaut', 10, 13),
(330, 'V204', '0724', 'Kotabumi', '', 'kotabumi', 10, 17),
(331, 'R105', '0434', 'Kotamobagu', '', 'kotamobagu', 10, 30),
(332, 'O405', '0636', 'Kotanopan', '', 'kotanopan', 10, 33),
(333, 'O303', '0624', 'Kotapinang', '', 'kotapinang', 10, 33),
(334, 'E501', '0335', 'Kraksaan', '', 'kraksaan', 10, 11),
(335, '7301', '031', 'Krian', '', 'krian', 10, 11),
(336, 'C501', '0282', 'Kroya', '', 'kroya', 10, 10),
(337, 'E508', '0335', 'Krucil', '', 'krucil', 10, 11),
(338, 'P508', '0335', 'Krucil', '', 'krucil-1', 10, 11),
(339, 'V205', '0728', 'Krue', '', 'krue', 10, 17),
(340, 'X103', '061', 'Kuala', '', 'kuala', 10, 33),
(341, 'M202', '0641', 'Kuala Simpang', '', 'kuala-simpang', 10, 20),
(342, 'I302', '0742', 'Kuala Tungkal', '', 'kuala-tungkal', 10, 8),
(343, 'L407', '0513', 'Kualakapuas', '', 'kualakapuas', 10, 14),
(344, '3310', '0901', 'Kualakencana', '', 'kualakencana', 10, 25),
(345, 'L502', '0539', 'Kualakuayan', '', 'kualakuayan', 10, 14),
(346, 'L503', '0537', 'Kualakurun', '', 'kualakurun', 10, 14),
(347, 'L504', '0538', 'Kualapembuang', '', 'kualapembuang', 10, 14),
(348, 'C600', '0291', 'Kudus', '', 'kudus', 10, 10),
(349, 'L509', '0532', 'Kumai', '', 'kumai', 10, 14),
(350, 'F108', '0232', 'Kuningan', '', 'kuningan', 10, 9),
(351, 'J400', '0380', 'Kupang', '', 'kupang', 10, 22),
(352, 'O103', '0629', 'Kutacane', '', 'kutacane', 10, 20),
(353, 'C704', '0275', 'Kutoarjo', '', 'kutoarjo', 10, 10),
(354, 'C705', '0287', 'Kutowinangun', '', 'kutowinangun', 10, 10),
(355, 'R203', '0442', 'Kwandang', '', 'kwandang', 10, 7),
(356, 'P321', '0351', 'Kwarakan', '', 'kwarakan', 10, 11),
(357, 'K701', '0253', 'Labuan', '', 'labuan', 10, 3),
(358, '4201', '0927', 'Labuha', '', 'labuha', 10, 19),
(359, 'J418', '0385', 'Labuhan Bajo', '', 'labuhan-bajo', 10, 22),
(360, 'O304', '0624', 'Labuhanbilik', '', 'labuhanbilik', 10, 33),
(361, 'O203', '0623', 'Labuhanruku', '', 'labuhanruku', 10, 33),
(362, 'I106', '0731', 'Lahat', '', 'lahat', 10, 32),
(363, 'M103', '0651', 'Lam No', '', 'lam-no', 10, 20),
(364, 'D109', '0322', 'Lamongan', '', 'lamongan', 10, 11),
(365, 'P104', '0322', 'Lamongan', '', 'lamongan-1', 10, 11),
(366, 'O305', '0624', 'Langgapayung', '', 'langgapayung', 10, 33),
(367, 'R104', '0431', 'Langowan', '', 'langowan', 10, 30),
(368, 'M200', '0641', 'Langsa', '', 'langsa', 10, 20),
(369, 'J413', '0383', 'Larantuka', '', 'larantuka', 10, 22),
(370, '4303', '0918', 'Larat', '', 'larat', 10, 18),
(371, 'C802', '0295', 'Lasem', '', 'lasem', 10, 10),
(372, 'T206', '0341', 'Lawang', '', 'lawang', 10, 11),
(373, 'E503', '0335', 'Leces', '', 'leces', 10, 11),
(374, 'B107', '022', 'Lembang', '', 'lembang', 10, 9),
(375, 'K702', '0252', 'Leuwidamar', '', 'leuwidamar', 10, 3),
(376, 'J419', '0383', 'Lewoleba', '', 'lewoleba', 10, 22),
(377, 'M300', '0645', 'Lhokseumawe', '', 'lhokseumawe', 10, 20),
(378, 'M302', '0645', 'Lhoksukon', '', 'lhoksukon', 10, 20),
(379, 'E105', '0333', 'Licin', '', 'licin', 10, 11),
(380, 'K507', '0262', 'Limbangan', '', 'limbangan', 10, 9),
(381, 'R201', '0435', 'Limboto', '', 'limboto', 10, 7),
(382, 'F109', '0232', 'Linggarjati', '', 'linggarjati', 10, 9),
(383, 'V210', '0728', 'Liwa', '', 'liwa', 10, 17),
(384, 'T102', '0342', 'Lodoyo', '', 'lodoyo', 10, 11),
(385, 'Z700', 'London', 'London', '', 'london', 10, 34),
(386, 'P303', '0357', 'Lorog', '', 'lorog', 10, 11),
(387, 'F110', '0234', 'Losarang', '', 'losarang', 10, 9),
(388, 'F111', '0231', 'Losari', '', 'losari', 10, 9),
(389, 'Y105', '0751', 'Lubuk Alung', '', 'lubuk-alung', 10, 31),
(390, 'Y202', '0752', 'Lubukbasung', '', 'lubukbasung', 10, 31),
(391, 'I107', '0733', 'Lubuklinggau', '', 'lubuklinggau', 10, 32),
(392, 'X107', '061', 'Lubukpakam', '', 'lubukpakam', 10, 33),
(393, 'Y203', '0753', 'Lubuksikaping', '', 'lubuksikaping', 10, 31),
(394, 'E400', '0334', 'Lumajang', '', 'lumajang', 10, 11),
(395, 'U200', '0461', 'Luwuk', '', 'luwuk', 10, 28),
(396, 'P300', '0351', 'Madiun', '', 'madiun', 10, 11),
(397, 'C700', '0293', 'Magelang', '', 'magelang', 10, 10),
(398, 'P304', '0351', 'Magetan', '', 'magetan', 10, 11),
(399, 'B108', '022', 'Majalaya', '', 'majalaya', 10, 9),
(400, 'F112', '0233', 'Majalengka', '', 'majalengka', 10, 9),
(401, 'C502', '0280', 'Majenang', '', 'majenang', 10, 10),
(402, 'N204', '0422', 'Majene', '', 'majene', 10, 27),
(403, 'N205', '0423', 'Makale', '', 'makale', 10, 27),
(404, 'N100', '0411', 'Makassar', '', 'makassar', 20, 27),
(405, 'T200', '0341', 'Malang', '', 'malang', 10, 11),
(406, 'K508', '0262', 'Malangbong', '', 'malangbong', 10, 9),
(407, 'Z200', 'M''sia', 'Malaysia', '', 'malaysia', 10, 34),
(408, 'N220', '0474', 'Malili', '', 'malili', 10, 27),
(409, 'L804', '0553', 'Malinau', '', 'malinau', 10, 15),
(410, 'K703', '0252', 'Malingping', '', 'malingping', 10, 3),
(411, 'N104', '0417', 'Malino', '', 'malino', 10, 27),
(412, 'J514', '0372', 'Maluk', '', 'maluk', 10, 21),
(413, 'N206', '0426', 'Mamuju', '', 'mamuju', 10, 27),
(414, 'R100', '0431', 'Manado', '', 'manado', 10, 30),
(415, '7218', '0323', 'Mandangin', '', 'mandangin', 10, 11),
(416, 'I506', '0719', 'Manggar', '', 'manggar', 10, 32),
(417, '4206', '0929', 'Mangole', '', 'mangole', 10, 19),
(418, 'P337', '0357', 'Mangunharjo', '', 'mangunharjo', 10, 11),
(419, 'Y204', '0752', 'Maninjau', '', 'maninjau', 10, 31),
(420, 'I205', '0739', 'Manna', '', 'manna', 10, 4),
(421, '3400', '0986', 'Manokwari', '', 'manokwari', 10, 23),
(422, 'K312', '0265', 'Manonjaya', '', 'manonjaya', 10, 9),
(423, 'C503', '0282', 'Maos', '', 'maos', 10, 10),
(424, 'P305', '0351', 'Maospati', '', 'maospati', 10, 11),
(425, 'L408', '0511', 'Marabahan', '', 'marabahan', 10, 13),
(426, 'R202', '0443', 'Marisa', '', 'marisa', 10, 7),
(427, 'N106', '0411', 'Maros', '', 'maros', 10, 27),
(428, 'I102', '0735', 'Martapura', '', 'martapura', 10, 32),
(429, 'L409', '0511', 'Martapura', '', 'martapura-1', 10, 13),
(430, '7214', '0327', 'Masalembu', '', 'masalembu', 10, 11),
(431, 'N215', '0473', 'Masamba', '', 'masamba', 10, 27),
(432, 'J515', '0376', 'Masbagik', '', 'masbagik', 10, 21),
(433, '4101', '0914', 'Masohi', '', 'masohi', 10, 18),
(434, 'M303', '0644', 'Matang Glumpang Dua', '', 'matang-glumpang-dua', 10, 20),
(435, 'M307', '0645', 'Matangkuli', '', 'matangkuli', 10, 20),
(436, 'J500', '0370', 'Mataram', '', 'mataram', 10, 21),
(437, 'J414', '0382', 'Maumere', '', 'maumere', 10, 22),
(438, 'X100', '061', 'Medan', '', 'medan', 20, 33),
(439, 'L203', '0545', 'Melak', '', 'melak', 10, 15),
(440, 'L304', '0561', 'Mempawah', '', 'mempawah', 10, 12),
(441, 'K704', '0253', 'Menes', '', 'menes', 10, 3),
(442, 'V211', '0726', 'Menggala', '', 'menggala', 10, 17),
(443, 'P107', '0356', 'Merakurak', '', 'merakurak', 10, 11),
(444, '3500', '0971', 'Merauke', '', 'merauke', 10, 25),
(445, 'O306', '0624', 'Merbau', '', 'merbau', 10, 33),
(446, 'C713', '0293', 'Mertoyudan', '', 'mertoyudan', 10, 10),
(447, 'V208', '0725', 'Metro', '', 'metro', 10, 17),
(448, 'M111', '0655', 'Meulaboh', '', 'meulaboh', 10, 20),
(449, 'M104', '0653', 'Meureudu', '', 'meureudu', 10, 20),
(450, 'E603', '0338', 'Mlandingan', '', 'mlandingan', 10, 11),
(451, '7109', '0321', 'Mlirip', '', 'mlirip', 10, 11),
(452, 'C210', '0284', 'Moga', '', 'moga', 10, 10),
(453, '7102', '0321', 'Mojoagung', '', 'mojoagung', 10, 11),
(454, '7100', '0321', 'Mojokerto', '', 'mojokerto', 10, 11),
(455, '7103', '0321', 'Mojosari', '', 'mojosari', 10, 11),
(456, '4207', '0923', 'Morotai', '', 'morotai', 10, 19),
(457, 'U305', '0455', 'Moutong', '', 'moutong', 10, 28),
(458, 'I206', '0738', 'Muara Aman', '', 'muara-aman', 10, 4),
(459, 'I304', '0743', 'Muara Bulian', '', 'muara-bulian', 10, 8),
(460, 'I303', '0747', 'Muara Bungo', '', 'muara-bungo', 10, 8),
(461, 'I108', '0734', 'Muara Enim', '', 'muara-enim', 10, 32),
(462, 'L102', '0541', 'Muara Jawa', '', 'muara-jawa', 10, 15),
(463, 'I109', '0733', 'Muara Rupit', '', 'muara-rupit', 10, 32),
(464, 'I307', '0744', 'Muara Tebo', '', 'muara-tebo', 10, 8),
(465, 'I103', '0735', 'Muaradua', '', 'muaradua', 10, 32),
(466, 'Y302', '0755', 'Muaralabuh', '', 'muaralabuh', 10, 31),
(467, 'Y102', '0759', 'Muarasiberut', '', 'muarasiberut', 10, 31),
(468, 'L505', '0519', 'Muarateweh', '', 'muarateweh', 10, 14),
(469, 'I207', '0737', 'Muko Muko', '', 'muko-muko', 10, 4),
(470, 'E106', '0333', 'Muncar', '', 'muncar', 10, 11),
(471, 'C711', '0293', 'Mungkid', '', 'mungkid', 10, 10),
(472, 'C706', '0293', 'Muntilan', '', 'muntilan', 10, 10),
(473, 'I504', '0716', 'Muntok', '', 'muntok', 10, 32),
(474, '3101', '0984', 'Nabire', '', 'nabire', 10, 24),
(475, '4102', '0913', 'Namlea', '', 'namlea', 10, 18),
(476, 'L305', '0568', 'Nangapinoh', '', 'nangapinoh', 10, 12),
(477, 'B118', '022', 'Nanjung', '', 'nanjung', 10, 9),
(478, 'P327', '0357', 'Nawangan', '', 'nawangan', 10, 11),
(479, 'S105', '0365', 'Negara', '', 'negara', 10, 1),
(480, 'L410', '0517', 'Negara Kalsel', '', 'negara-kalsel', 10, 13),
(481, 'O307', '0624', 'Negeribaru', '', 'negeribaru', 10, 33),
(482, 'L306', '0563', 'Ngabang', '', 'ngabang', 10, 12),
(483, 'P203', '0354', 'Ngadiluwih', '', 'ngadiluwih', 10, 11),
(484, 'E507', '0335', 'Ngadisari', '', 'ngadisari', 10, 11),
(485, 'P507', '0335', 'Ngadisari', '', 'ngadisari-1', 10, 11),
(486, 'P116', '0353', 'Ngambon', '', 'ngambon', 10, 11),
(487, 'P204', '0358', 'Nganjuk', '', 'nganjuk', 10, 11),
(488, 'T220', '0341', 'Ngantang', '', 'ngantang', 10, 11),
(489, 'P117', '0353', 'Ngasem', '', 'ngasem', 10, 11),
(490, 'C803', '0296', 'Ngawen', '', 'ngawen', 10, 10),
(491, 'P306', '0351', 'Ngawi', '', 'ngawi', 10, 11),
(492, 'P328', '0352', 'Ngebel', '', 'ngebel', 10, 11),
(493, '7105', '0321', 'Ngoro Industri', '', 'ngoro-industri', 10, 11),
(494, '7107', '0321', 'Ngoro Jombang', '', 'ngoro-jombang', 10, 11),
(495, 'P118', '0353', 'Ngraho', '', 'ngraho', 10, 11),
(496, 'P334', '0352', 'Ngrayun', '', 'ngrayun', 10, 11),
(497, 'P403', '0355', 'Ngunut', '', 'ngunut', 10, 11),
(498, 'J406', '0388', 'Nikiniki', '', 'nikiniki', 10, 22),
(499, 'T305', '0343', 'Nongkojajar', '', 'nongkojajar', 10, 11),
(500, 'L801', '0556', 'Nunukan', '', 'nunukan', 10, 15),
(501, 'K207', '0266', 'Nyalindung', '', 'nyalindung', 10, 9),
(502, '7212', '0323', 'Omben', '', 'omben', 10, 11),
(503, 'F113', '0231', 'Pabuaran', '', 'pabuaran', 10, 9),
(504, 'K803', '0260', 'Pabuaran', '', 'pabuaran-1', 10, 9),
(505, '7106', '0321', 'Pacet', '', 'pacet', 10, 11),
(506, 'P307', '0357', 'Pacitan', '', 'pacitan', 10, 11),
(507, 'B109', '022', 'Padalarang', '', 'padalarang', 10, 9),
(508, 'Y100', '0751', 'Padang', '', 'padang', 10, 31),
(509, 'Y211', '0752', 'Padang Japang', '', 'padang-japang', 10, 31),
(510, 'P123', '0353', 'Padangan', '', 'padangan', 10, 11),
(511, 'L307', 'Pka', 'Padangkarimata', '', 'padangkarimata', 10, 12),
(512, 'Y205', '0752', 'Padangpanjang', '', 'padangpanjang', 10, 31),
(513, 'O406', '0634', 'Padangsidempuan', '', 'padangsidempuan', 10, 33),
(514, 'K804', '0260', 'Pagaden', '', 'pagaden', 10, 9),
(515, 'T221', '0341', 'Pagak', '', 'pagak', 10, 11),
(516, 'I110', '0730', 'Pagar Alam', '', 'pagar-alam', 10, 32),
(517, 'L415', '0518', 'Pagatan', '', 'pagatan', 10, 13),
(518, 'U203', '0461', 'Pagimana', '', 'pagimana', 10, 28),
(519, 'Y103', '0756', 'Painan', '', 'painan', 10, 31),
(520, 'E502', '0335', 'Paiton', '', 'paiton', 10, 11),
(521, 'G103', '0274', 'Pakem', '', 'pakem', 10, 5),
(522, 'T216', '0341', 'Pakis', '', 'pakis', 10, 11),
(523, '7219', '0324', 'Pakong', '', 'pakong', 10, 11),
(524, 'L500', '0536', 'Palangkaraya', '', 'palangkaraya', 10, 14),
(525, 'V100', '0711', 'Palembang', '', 'palembang', 10, 32),
(526, 'N207', '0471', 'Palopo', '', 'palopo', 10, 27),
(527, 'U300', '0451', 'Palu', '', 'palu', 10, 28),
(528, 'K801', '0260', 'Pamanukan', '', 'pamanukan', 10, 9),
(529, '7200', '0324', 'Pamekasan', '', 'pamekasan', 10, 11),
(530, 'K505', '0262', 'Pameungpeuk', '', 'pameungpeuk', 10, 9),
(531, 'T103', '0342', 'Panataran', '', 'panataran', 10, 11),
(532, 'T306', '0343', 'Pandaan', '', 'pandaan', 10, 11),
(533, 'K705', '0253', 'Pandeglang', '', 'pandeglang', 10, 3),
(534, 'B110', '022', 'Pangalengan', '', 'pangalengan', 10, 9),
(535, 'K306', '0265', 'Pangandaran', '', 'pangandaran', 10, 9),
(536, 'N208', '0421', 'Pangkajene Sidrap', '', 'pangkajene-sidrap', 10, 27),
(537, 'Y212', '0752', 'Pangkalan', '', 'pangkalan', 10, 31),
(538, 'M203', '0620', 'Pangkalan Brandan', '', 'pangkalan-brandan', 10, 20),
(539, 'W210', '0761', 'Pangkalan Kerinci', '', 'pangkalan-kerinci', 10, 26),
(540, 'M204', '0620', 'Pangkalan Susu', '', 'pangkalan-susu', 10, 20),
(541, 'V106', '0711', 'Pangkalanbalai', '', 'pangkalanbalai', 10, 32),
(542, 'L506', '0532', 'Pangkalanbun', '', 'pangkalanbun', 10, 14),
(543, 'I500', '0717', 'Pangkalpinang', '', 'pangkalpinang', 10, 2),
(544, 'N107', '0410', 'Pangkep', '', 'pangkep', 10, 27),
(545, 'P323', '0351', 'Pangkur', '', 'pangkur', 10, 11),
(546, 'O104', '0626', 'Pangururan', '', 'pangururan', 10, 33),
(547, 'M304', '0645', 'Panton Labu', '', 'panton-labu', 10, 20),
(548, 'P205', '0354', 'Papar', '', 'papar', 10, 11),
(549, 'C707', '0293', 'Parakan', '', 'parakan', 10, 10),
(550, 'P326', '0351', 'Parangmadiun', '', 'parangmadiun', 10, 11),
(551, 'O205', '0625', 'Parapat', '', 'parapat', 10, 33),
(552, 'P206', '0354', 'Pare', '', 'pare', 10, 11),
(553, 'P125', '0356', 'Parengan', '', 'parengan', 10, 11),
(554, 'N200', '0421', 'Pare-Pare', '', 'pare-pare', 10, 27),
(555, 'Y104', '0751', 'Pariaman', '', 'pariaman', 10, 31),
(556, 'U302', '0450', 'Parigi', '', 'parigi', 10, 28),
(557, 'E107', '0333', 'Pasanggaran/Pesanggaran', '', 'pasanggaran/pesanggaran', 10, 11),
(558, 'W209', '0762', 'Pasir Pangaraian', '', 'pasir-pangaraian', 10, 26),
(559, 'E403', '0334', 'Pasirian', '', 'pasirian', 10, 11),
(560, '4104', '0911', 'Passo', '', 'passo', 10, 18),
(561, 'T300', '0343', 'Pasuruan', '', 'pasuruan', 10, 11),
(562, 'C608', '0295', 'Pati', '', 'pati', 10, 10),
(563, 'F119', '0234', 'Patrol', '', 'patrol', 10, 9),
(564, 'Y206', '0752', 'Payakumbuh', '', 'payakumbuh', 10, 31),
(565, 'C609', '0291', 'Pecangaan', '', 'pecangaan', 10, 10),
(566, 'G206', '0272', 'Pedan', '', 'pedan', 10, 10),
(567, 'C200', '0285', 'Pekalongan', '', 'pekalongan', 10, 10),
(568, 'W200', '0761', 'Pekanbaru', '', 'pekanbaru', 10, 26),
(569, 'K208', '0266', 'Pelabuhan Ratu', '', 'pelabuhan-ratu', 10, 9),
(570, 'C205', '0284', 'Pemalang', '', 'pemalang', 10, 10),
(571, 'L308', '0562', 'Pemangkat', '', 'pemangkat', 10, 12),
(572, 'O200', '0622', 'Pematang Siantar', '', 'pematang-siantar', 10, 33),
(573, 'I111', '0731', 'Pendopo', '', 'pendopo', 10, 32),
(574, 'I105', '0713', 'Pendopo Talang Ubi', '', 'pendopo-talang-ubi', 10, 32),
(575, 'O407', '0636', 'Penyabungan', '', 'penyabungan', 10, 33),
(576, 'W207', '0761', 'Perawang', '', 'perawang', 10, 26),
(577, 'X108', '061', 'Perbaungan', '', 'perbaungan', 10, 33),
(578, 'O207', '0622', 'Perdagangan', '', 'perdagangan', 10, 33),
(579, 'M205', '0646', 'Peureulak', '', 'peureulak', 10, 20),
(580, 'N209', '0421', 'Pinrang', '', 'pinrang', 10, 27),
(581, 'L411', '0512', 'Pleihari', '', 'pleihari', 10, 13),
(582, 'F114', '0231', 'Pleredcirebon', '', 'pleredcirebon', 10, 9),
(583, '7104', '0321', 'Ploso', '', 'ploso', 10, 11),
(584, '4105', '0911', 'Poka', '', 'poka', 10, 18),
(585, 'N210', '0428', 'Polewali', '', 'polewali', 10, 27),
(586, 'P308', '0352', 'Ponorogo', '', 'ponorogo', 10, 11),
(587, 'L300', '0561', 'Pontianak', '', 'pontianak', 10, 12),
(588, 'O408', '0632', 'Porsea', '', 'porsea', 10, 33),
(589, 'U500', '0452', 'Poso', '', 'poso', 10, 28),
(590, 'I104', '0713', 'Prabumulih', '', 'prabumulih', 10, 32),
(591, '3403', '0986', 'Prafi', '', 'prafi', 10, 23),
(592, '7204', '0328', 'Pragaan', '', 'pragaan', 10, 11),
(593, 'E201', '0332', 'Prajekan', '', 'prajekan', 10, 11),
(594, 'P211', '0358', 'Prambon', '', 'prambon', 10, 11),
(595, 'J502', '0370', 'Praya', '', 'praya', 10, 21),
(596, 'T307', '0343', 'Prigen', '', 'prigen', 10, 11),
(597, 'P405', '0355', 'Prigi', '', 'prigi', 10, 11),
(598, 'V206', '0729', 'Pringsewu', '', 'pringsewu', 10, 17),
(599, 'E500', '0335', 'Probolinggo', '', 'probolinggo', 10, 11),
(600, 'E407', '0334', 'Pronojiwo', '', 'pronojiwo', 10, 11),
(601, 'E306', '0336', 'Puger', '', 'puger', 10, 11),
(602, 'T207', '0341', 'Pujon', '', 'pujon', 10, 11),
(603, 'L418', '0513', 'Pulangpisau', '', 'pulangpisau', 10, 13),
(604, 'W502', '0765', 'Pulau Burung', '', 'pulau-burung', 10, 26),
(605, 'W504', '0765', 'Pulau Kijang', '', 'pulau-kijang', 10, 26),
(606, 'W509', '0765', 'Pulau Sambu', '', 'pulau-sambu', 10, 26),
(607, 'W104', 'Phg', 'Pulauhalang', '', 'pulauhalang', 10, 26),
(608, 'O208', '0623', 'Pulaurakyat', '', 'pulaurakyat', 10, 33),
(609, 'P316', '0352', 'Pulung', '', 'pulung', 10, 11),
(610, 'P315', '0357', 'Punung', '', 'punung', 10, 11),
(611, 'C303', '0281', 'Purbalingga', '', 'purbalingga', 10, 10),
(612, 'L507', '0528', 'Purukcahu', '', 'purukcahu', 10, 14),
(613, 'K400', '0264', 'Purwakarta', '', 'purwakarta', 10, 9),
(614, 'C804', '0292', 'Purwodadigrobogan', '', 'purwodadigrobogan', 10, 10),
(615, 'C300', '0281', 'Purwokerto', '', 'purwokerto', 10, 10),
(616, 'C708', '0275', 'Purworejo', '', 'purworejo', 10, 10),
(617, 'T308', '0343', 'Purwosari', '', 'purwosari', 10, 11),
(618, 'L309', '0567', 'Putussibau', '', 'putussibau', 10, 12),
(619, 'U103', '0403', 'Raha', '', 'raha', 10, 29),
(620, 'K309', '0265', 'Raja Polah', '', 'raja-polah', 10, 9),
(621, 'F120', '0233', 'Rajagaluh', '', 'rajagaluh', 10, 9),
(622, 'E307', '0331', 'Rambipuji', '', 'rambipuji', 10, 11),
(623, 'W402', '0773', 'Ranai', '', 'ranai', 10, 26),
(624, 'B112', '022', 'Rancaekek', '', 'rancaekek', 10, 9),
(625, 'C805', '0296', 'Randublatung', '', 'randublatung', 10, 10),
(626, 'C206', '0284', 'Randudongkal', '', 'randudongkal', 10, 10),
(627, 'K700', '0252', 'Rangkasbitung', '', 'rangkasbitung', 10, 3),
(628, '3402', '0980', 'Ransiki', '', 'ransiki', 10, 23),
(629, 'L412', '0517', 'Rantau', '', 'rantau', 10, 13),
(630, 'O300', '0624', 'Rantauprapat', '', 'rantauprapat', 10, 33),
(631, 'N211', '0423', 'Rantepao', '', 'rantepao', 10, 27),
(632, 'N212', '0421', 'Rapang/Rappang', '', 'rapang/rappang', 10, 27),
(633, 'C806', '0295', 'Rembang', '', 'rembang', 10, 10),
(634, 'W203', '0769', 'Rengat', '', 'rengat', 10, 26),
(635, 'P108', '0356', 'Rengel', '', 'rengel', 10, 11),
(636, 'J415', '0385', 'Reo', '', 'reo', 10, 22),
(637, 'I308', '0747', 'Rimbo Bujang', '', 'rimbo-bujang', 10, 8),
(638, 'E108', '0333', 'Rogojampi', '', 'rogojampi', 10, 11),
(639, 'J416', '0385', 'Ruteng', '', 'ruteng', 10, 22),
(640, 'M105', '0652', 'Sabang', '', 'sabang', 10, 20),
(641, 'K210', '0266', 'Sagaranten', '', 'sagaranten', 10, 9),
(642, 'K706', '0253', 'Saketi', '', 'saketi', 10, 9),
(643, 'U204', '0462', 'Salakan', '', 'salakan', 10, 28),
(644, 'H104', '0298', 'Salatiga', '', 'salatiga', 10, 10),
(645, 'M305', '0644', 'Samalanga', '', 'samalanga', 10, 20),
(646, 'L200', '0541', 'Samarinda', '', 'samarinda', 10, 15),
(647, 'L310', '0562', 'Sambas', '', 'sambas', 10, 12),
(648, 'P210', '0354', 'Sambi', '', 'sambi', 10, 11),
(649, 'P312', '0352', 'Sambit', '', 'sambit', 10, 11),
(650, '7205', '0323', 'Sampang', '', 'sampang', 10, 11),
(651, 'L508', '0531', 'Sampit', '', 'sampit', 10, 14),
(652, 'P329', '0352', 'Sampung', '', 'sampung', 10, 11),
(653, '4202', '0929', 'Sanana', '', 'sanana', 10, 19),
(654, 'L204', '0549', 'Sanggatta', '', 'sanggatta', 10, 15),
(655, 'L311', '0564', 'Sanggau', '', 'sanggau', 10, 12),
(656, 'B113', 'Sto', 'Santosa', '', 'santosa', 10, 9),
(657, '4106', '0931', 'Saparua', '', 'saparua', 10, 18),
(658, 'W507', '0765', 'Sapat', '', 'sapat', 10, 26),
(659, 'J511', '0374', 'Sape', '', 'sape', 10, 21),
(660, '7216', '0327', 'Sapeken', '', 'sapeken', 10, 11),
(661, '7211', '0327', 'Sapudi', '', 'sapudi', 10, 11),
(662, 'P309', '0351', 'Sarangan', '', 'sarangan', 10, 11),
(663, '3305', '0966', 'Sarmi', '', 'sarmi', 10, 25),
(664, 'I305', '0745', 'Sarolangun', '', 'sarolangun', 10, 8),
(665, 'L417', '0512', 'Satui', '', 'satui', 10, 13),
(666, '4302', '0918', 'Saumlaki', '', 'saumlaki', 10, 18),
(667, 'P322', '0351', 'Sawahan', '', 'sawahan', 10, 11),
(668, 'Y303', '0754', 'Sawahlunto', '', 'sawahlunto', 10, 31),
(669, 'J420', '0380', 'Seba', '', 'seba', 10, 22),
(670, 'D104', '031', 'Sedayu', '', 'sedayu', 10, 11),
(671, 'O308', '0624', 'Seiberombang', '', 'seiberombang', 10, 33),
(672, 'L312', '0564', 'Sekadau', '', 'sekadau', 10, 12),
(673, 'V102', '0714', 'Sekayu', '', 'sekayu', 10, 32),
(674, 'W204', '0763', 'Selatpanjang', '', 'selatpanjang', 10, 26),
(675, 'N108', '0414', 'Selayar', '', 'selayar', 10, 27),
(676, 'J503', '0376', 'Selong', '', 'selong', 10, 21),
(677, 'H100', '024', 'Semarang', '', 'semarang', 20, 10),
(678, 'S106', '0366', 'Semarapura', '', 'semarapura', 10, 1),
(679, 'E308', '0331', 'Sempolan', '', 'sempolan', 10, 11),
(680, 'E405', '0334', 'Senduro', '', 'senduro', 10, 11),
(681, 'J512', '0370', 'Senggigi', '', 'senggigi', 10, 21),
(682, 'N213', '0485', 'Sengkang', '', 'sengkang', 10, 27),
(683, 'P126', '0356', 'Senori', '', 'senori', 10, 11),
(684, '3306', '0967', 'Sentani', '', 'sentani', 10, 25),
(685, '7302', '031', 'Sepanjang', '', 'sepanjang', 10, 11),
(686, 'K100', '0254', 'Serang', '', 'serang', 10, 3),
(687, 'O210', '0622', 'Serbelawan', '', 'serbelawan', 10, 33),
(688, '3102', '0983', 'Serui', '', 'serui', 10, 24),
(689, 'M106', '0651', 'Seulimeum', '', 'seulimeum', 10, 20),
(690, 'W206', '0764', 'Siak', '', 'siak', 10, 26),
(691, 'O400', '0631', 'Sibolga', '', 'sibolga', 10, 33),
(692, 'O409', '0633', 'Siborongborong', '', 'siborongborong', 10, 33),
(693, 'O413', '0636', 'Sibuhuan', '', 'sibuhuan', 10, 33),
(694, 'Y106', '0751', 'Sicincin', '', 'sicincin', 10, 31),
(695, 'C504', '0280', 'Sidareja', '', 'sidareja', 10, 10),
(696, 'O105', '0627', 'Sidikalang', '', 'sidikalang', 10, 33),
(697, '7300', '031', 'Sidoarjo', '', 'sidoarjo', 10, 11),
(698, 'M107', '0653', 'Sigli', '', 'sigli', 10, 20),
(699, 'Y304', '0754', 'Sijunjung', '', 'sijunjung', 10, 31),
(700, 'J506', '0374', 'Sila', '', 'sila', 10, 21),
(701, 'Y305', '0754', 'Silungkang', '', 'silungkang', 10, 31),
(702, 'Y208', '0753', 'Simpang Empat', '', 'simpang-empat', 10, 31),
(703, 'M116', '0650', 'Sinabang', '', 'sinabang', 10, 20),
(704, 'F115', '0231', 'Sindanglaut', '', 'sindanglaut', 10, 9),
(705, 'K604', '0263', 'Sindanglaya', '', 'sindanglaya', 10, 9),
(706, 'K307', '0265', 'Singaparna', '', 'singaparna', 10, 9),
(707, 'Z100', 'S''pore', 'Singapore', '', 'singapore', 10, 34),
(708, 'S107', '0362', 'Singaraja', '', 'singaraja', 10, 1),
(709, 'P124', '0356', 'Singgahan', '', 'singgahan', 10, 11),
(710, 'L313', '0562', 'Singkawang', '', 'singkawang', 10, 12),
(711, 'M109', '0658', 'Singkil', '', 'singkil', 10, 20),
(712, 'T208', '0341', 'Singosari', '', 'singosari', 10, 11),
(713, 'N109', '0482', 'Sinjai', '', 'sinjai', 10, 27),
(714, 'L314', '0565', 'Sintang', '', 'sintang', 10, 12),
(715, 'O411', '0634', 'Sipirok', '', 'sipirok', 10, 33),
(716, 'Y307', '0754', 'Sitiung', '', 'sitiung', 10, 31),
(717, 'E600', '0338', 'Situbondo', '', 'situbondo', 10, 11),
(718, 'N219', '0472', 'Siwa', '', 'siwa', 10, 27),
(719, 'P313', '0352', 'Slahung', '', 'slahung', 10, 11),
(720, 'C406', '0283', 'Slawi', '', 'slawi', 10, 10),
(721, 'G105', '0274', 'Sleman', '', 'sleman', 10, 5),
(722, '4203', '0921', 'Soa Siu', '', 'soa-siu', 10, 19),
(723, 'J408', '0388', 'Soe', '', 'soe', 10, 22),
(724, 'G200', '0271', 'Solo', '', 'solo', 10, 10),
(725, 'Y300', '0755', 'Solok', '', 'solok', 10, 31),
(726, 'P314', '0352', 'Somoroto', '', 'somoroto', 10, 11),
(727, 'B117', '022', 'Soreang', '', 'soreang', 10, 9),
(728, '3600', '0951', 'Sorong', '', 'sorong', 10, 23),
(729, 'N221', '0475', 'Sorowako', '', 'sorowako', 10, 27),
(730, 'G207', '0271', 'Sragen', '', 'sragen', 10, 10),
(731, 'G108', '0274', 'Srandakan', '', 'srandakan', 10, 5),
(732, 'T104', '0342', 'Srengat', '', 'srengat', 10, 11),
(733, 'X105', '061', 'Stabat', '', 'stabat', 10, 33),
(734, 'C207', '0285', 'Subah', '', 'subah', 10, 10),
(735, 'K800', '0260', 'Subang', '', 'subang', 10, 9),
(736, 'M110', '0627', 'Subulussalam', '', 'subulussalam', 10, 20),
(737, 'P335', '0357', 'Sudimoro', '', 'sudimoro', 10, 11),
(738, 'P119', '0353', 'Sugihwaras', '', 'sugihwaras', 10, 11),
(739, 'K200', '0266', 'Sukabumi', '', 'sukabumi', 10, 9),
(740, 'L315', 'San', 'Sukadanakalimantan', '', 'sukadanakalimantan', 10, 12),
(741, 'K605', '0263', 'Sukanegara', '', 'sukanegara', 10, 9),
(742, 'E505', '0335', 'Sukapura', '', 'sukapura', 10, 11),
(743, 'C304', '0281', 'Sukaraja', '', 'sukaraja', 10, 10),
(744, 'D110', '0322', 'Sukodadi', '', 'sukodadi', 10, 11),
(745, '7304', '031', 'Sukodono', '', 'sukodono', 10, 11),
(746, 'G208', '0271', 'Sukoharjo', '', 'sukoharjo', 10, 10),
(747, 'H105', '0294', 'Sukorejokendal', '', 'sukorejokendal', 10, 10),
(748, 'E202', '0332', 'Sukosari', '', 'sukosari', 10, 11),
(749, 'E309', '0331', 'Sukowono', '', 'sukowono', 10, 11),
(750, 'J508', '0371', 'Sumbawa', '', 'sumbawa', 10, 21),
(751, 'E511', '0335', 'Sumber', '', 'sumber', 10, 11),
(752, 'P105', '0353', 'Sumberejo', '', 'sumberejo', 10, 11),
(753, 'T209', '0341', 'Sumbermanjing', '', 'sumbermanjing', 10, 11),
(754, 'T210', '0341', 'Sumberpucung', '', 'sumberpucung', 10, 11),
(755, 'O108', '0627', 'Sumbul', '', 'sumbul', 10, 33),
(756, 'B114', '0261', 'Sumedang', '', 'sumedang', 10, 9),
(757, '7206', '0328', 'Sumenep', '', 'sumenep', 10, 11),
(758, 'W106', '0766', 'Sungai Apit', '', 'sungai-apit', 10, 26),
(759, 'W503', '0765', 'Sungai Guntung', '', 'sungai-guntung', 10, 26),
(760, 'Y107', '0751', 'Sungai Limau', '', 'sungai-limau', 10, 31),
(761, 'W107', '0766', 'Sungai Pakning', '', 'sungai-pakning', 10, 26),
(762, 'I306', '0748', 'Sungai Penuh', '', 'sungai-penuh', 10, 8),
(763, 'Y306', '0754', 'Sungaidareh', '', 'sungaidareh', 10, 31),
(764, 'L319', '0562', 'Sungaiduri', '', 'sungaiduri', 10, 12),
(765, 'I501', '0717', 'Sungailiat', '', 'sungailiat', 10, 2),
(766, 'L316', '0561', 'Sungaipinyuh', '', 'sungaipinyuh', 10, 12),
(767, 'O209', '0621', 'Sungairampah', '', 'sungairampah', 10, 33),
(768, 'V107', '0711', 'Sungsang', '', 'sungsang', 10, 32),
(769, 'D100', '031', 'Surabaya', '', 'surabaya', 25, 11),
(770, 'J513', '0370', 'Sweta', '', 'sweta', 10, 21),
(771, 'S108', '0361', 'Tabanan', '', 'tabanan', 10, 1),
(772, 'R111', '0432', 'Tagulandang', '', 'tagulandang', 10, 30),
(773, 'R106', '0432', 'Tahuna', '', 'tahuna', 10, 30),
(774, 'I209', '0736', 'Tais', '', 'tais', 10, 4),
(775, 'N111', '0418', 'Takalar', '', 'takalar', 10, 27),
(776, 'M306', '0643', 'Takengon', '', 'takengon', 10, 20),
(777, 'V207', '0729', 'Talangpadang', '', 'talangpadang', 10, 17),
(778, 'J510', '0372', 'Taliwang', '', 'taliwang', 10, 21),
(779, 'Y207', '0753', 'Talu', '', 'talu', 10, 31),
(780, 'W205', '0760', 'Talukkuantan', '', 'talukkuantan', 10, 26),
(781, 'P120', '0353', 'Tambakrejo', '', 'tambakrejo', 10, 11),
(782, '7217', '0323', 'Tambelangan', '', 'tambelangan', 10, 11),
(783, 'L414', '0526', 'Tamianglayang', '', 'tamianglayang', 10, 14),
(784, 'L101', '0543', 'Tanah Grogot', '', 'tanah-grogot', 10, 15),
(785, '3502', '0975', 'Tanah Merah', '', 'tanah-merah', 10, 25),
(786, 'A104', '021', 'Tangerang', '', 'tangerang', 35, 3),
(787, 'K606', '0263', 'Tanggeung', '', 'tanggeung', 10, 9),
(788, 'E310', '0336', 'Tanggul', '', 'tanggul', 10, 11),
(789, 'M108', '0653', 'Tangse', '', 'tangse', 10, 20),
(790, 'P122', '0356', 'Tanjung Awar-Awar', '', 'tanjung-awar-awar', 10, 11),
(791, 'O212', '0622', 'Tanjung Gading', '', 'tanjung-gading', 10, 33),
(792, 'I502', '0719', 'Tanjung Pandan', '', 'tanjung-pandan', 10, 2),
(793, 'W405', '0771', 'Tanjung Uban', '', 'tanjung-uban', 10, 26),
(794, 'O211', '0623', 'Tanjungbalai', '', 'tanjungbalai', 10, 33),
(795, 'W403', '0777', 'Tanjungbalaikarimun', '', 'tanjungbalaikarimun', 10, 26),
(796, 'W404', '0779', 'Tanjungbatu', '', 'tanjungbatu', 10, 26),
(797, 'I112', '0734', 'Tanjungenim', '', 'tanjungenim', 10, 32),
(798, 'O309', '0623', 'Tanjungleidong', '', 'tanjungleidong', 10, 33),
(799, 'W400', '0771', 'Tanjungpinang', '', 'tanjungpinang', 10, 16),
(800, 'X104', '061', 'Tanjungpura', '', 'tanjungpura', 10, 33),
(801, 'V104', '0712', 'Tanjungraja', '', 'tanjungraja', 10, 32),
(802, 'L802', '0554', 'Tanjungredep', '', 'tanjungredep', 10, 15),
(803, 'B115', '022', 'Tanjungsari', '', 'tanjungsari', 10, 9),
(804, 'L803', '0552', 'Tanjungselor', '', 'tanjungselor', 10, 15),
(805, 'L413', '0526', 'Tanjungtabalong', '', 'tanjungtabalong', 10, 13),
(806, 'C407', '0283', 'Tanjungtegal', '', 'tanjungtegal', 10, 10),
(807, 'N218', '0421', 'Tanrutedong', '', 'tanrutedong', 10, 27),
(808, 'M117', '0656', 'Tapaktuan', '', 'tapaktuan', 10, 20),
(809, 'L800', '0551', 'Tarakan', '', 'tarakan', 10, 15),
(810, 'O410', '0633', 'Tarutung', '', 'tarutung', 10, 33),
(811, 'K300', '0265', 'Tasikmalaya', '', 'tasikmalaya', 10, 9),
(812, 'U303', '0451', 'Tawaeli', '', 'tawaeli', 10, 28),
(813, 'G209', '0271', 'Tawangmangu', '', 'tawangmangu', 10, 10),
(814, 'C610', '0295', 'Tayu', '', 'tayu', 10, 10),
(815, 'L318', '0562', 'Tebas', '', 'tebas', 10, 12),
(816, 'I113', '0702', 'Tebingtinggi', '', 'tebingtinggi', 10, 32),
(817, 'I309', '0742', 'Tebingtinggi', '', 'tebingtinggi-1', 10, 8),
(818, 'O213', '0621', 'Tebingtinggi Deli', '', 'tebingtinggi-deli', 10, 33),
(819, 'C400', '0283', 'Tegal', '', 'tegal', 10, 10),
(820, 'P332', '0357', 'Tegalombo', '', 'tegalombo', 10, 11),
(821, 'R205', '0435', 'Telaga', '', 'telaga', 10, 7),
(822, 'W506', '0765', 'Teluk Pinang', '', 'teluk-pinang', 10, 26),
(823, 'O412', '0630', 'Telukdalam', '', 'telukdalam', 10, 33),
(824, 'C709', '0293', 'Temanggung', '', 'temanggung', 10, 10),
(825, 'P121', '0353', 'Temayang', '', 'temayang', 10, 11),
(826, '3307', '0901', 'Tembagapura', '', 'tembagapura', 10, 25),
(827, 'W500', '0768', 'Tembilahan', '', 'tembilahan', 10, 26),
(828, '3601', '0952', 'Teminabuan', '', 'teminabuan', 10, 23),
(829, 'E406', '0334', 'Tempeh', '', 'tempeh', 10, 11),
(830, 'E408', '0334', 'Tempursari', '', 'tempursari', 10, 11),
(831, 'L201', '0541', 'Tenggarong', '', 'tenggarong', 10, 15),
(832, 'J507', '0374', 'Tente', '', 'tente', 10, 21),
(833, 'U502', '0458', 'Tentena', '', 'tentena', 10, 28),
(834, 'W406', '0772', 'Terempa', '', 'terempa', 10, 26),
(835, '4200', '0921', 'Ternate', '', 'ternate', 10, 19),
(836, 'O107', '0628', 'Tigabinanga', '', 'tigabinanga', 10, 33),
(837, '3308', '0901', 'Timika', '', 'timika', 10, 25),
(838, 'U306', '0454', 'Tinombo', '', 'tinombo', 10, 28),
(839, 'E509', '0335', 'Tiris', '', 'tiris', 10, 11),
(840, 'P509', '0335', 'Tiris', '', 'tiris-1', 10, 11),
(841, '4204', '0924', 'Tobela', '', 'tobela', 10, 19),
(842, 'I505', '0718', 'Toboali', '', 'toboali', 10, 2),
(843, 'U308', '0450', 'Tolay', '', 'tolay', 10, 28),
(844, 'U304', '0453', 'Toli-Toli', '', 'toli-toli', 10, 28),
(845, 'R107', '0431', 'Tomohon', '', 'tomohon', 10, 30),
(846, 'R108', '0431', 'Tondano', '', 'tondano', 10, 30),
(847, 'E506', '0335', 'Tongas', '', 'tongas', 10, 11),
(848, 'T310', '0343', 'Tosari', '', 'tosari', 10, 11),
(849, 'E109', '0333', 'Treblasala', '', 'treblasala', 10, 11),
(850, 'P404', '0355', 'Trenggalek', '', 'trenggalek', 10, 11),
(851, '4300', '0916', 'Tual', '', 'tual', 10, 18),
(852, 'P106', '0356', 'Tuban', '', 'tuban', 10, 11),
(853, 'I114', '0733', 'Tugumulyo', '', 'tugumulyo', 10, 32),
(854, '7303', '031', 'Tulangan', '', 'tulangan', 10, 11),
(855, 'P400', '0355', 'Tulungagung', '', 'tulungagung', 10, 11),
(856, 'T211', '0341', 'Tumpang', '', 'tumpang', 10, 11),
(857, 'T212', '0341', 'Turen', '', 'turen', 10, 11),
(858, 'W208', '0762', 'Ujungbatu', '', 'ujungbatu', 10, 26),
(859, 'B116', '022', 'Ujungberung', '', 'ujungberung', 10, 9),
(860, 'U105', '0408', 'Unaaha', '', 'unaaha', 10, 29),
(861, 'H106', '024', 'Ungaran', '', 'ungaran', 10, 10),
(862, 'P310', '0351', 'Uteran', '', 'uteran', 10, 11),
(863, '3311', '0967', 'Waena', '', 'waena', 10, 25),
(864, '4108', '0914', 'Wahai', '', 'wahai', 10, 18),
(865, 'J410', '0387', 'Waikabubak', '', 'waikabubak', 10, 22),
(866, 'J409', '0387', 'Waingapu', '', 'waingapu', 10, 22),
(867, 'P311', '0351', 'Walikukun', '', 'walikukun', 10, 11),
(868, '3309', '0969', 'Wamena', '', 'wamena', 10, 25),
(869, 'K506', '0262', 'Wanaraja', '', 'wanaraja', 10, 9),
(870, 'U106', '0404', 'Wanci', '', 'wanci', 10, 29),
(871, '7208', '0324', 'Waru', '', 'waru', 10, 11),
(872, 'P207', '0358', 'Warujayeng', '', 'warujayeng', 10, 11);
INSERT INTO `city` (`id`, `dty`, `area_code`, `name`, `description`, `slug`, `weight`, `province_id`) VALUES
(873, 'N112', '0481', 'Watampone', '', 'watampone', 10, 27),
(874, 'N214', '0484', 'Watansoppeng', '', 'watansoppeng', 10, 27),
(875, 'P208', '0354', 'Wates', '', 'wates', 10, 11),
(876, 'G106', '0274', 'Watesyogya', '', 'watesyogya', 10, 5),
(877, 'P333', '0357', 'Watugede', '', 'watugede', 10, 11),
(878, 'H107', '0294', 'Weleri', '', 'weleri', 10, 10),
(879, 'C807', '0292', 'Wirosari', '', 'wirosari', 10, 10),
(880, 'T105', '0342', 'Wlingi', '', 'wlingi', 10, 11),
(881, 'J417', '0381', 'Wolowaru', '', 'wolowaru', 10, 22),
(882, 'E110', '0333', 'Wongsorejo', '', 'wongsorejo', 10, 11),
(883, 'G210', '0273', 'Wonogiri', '', 'wonogiri', 10, 10),
(884, 'N216', '0428', 'Wonomulyo', '', 'wonomulyo', 10, 27),
(885, 'G107', '0274', 'Wonosari', '', 'wonosari', 10, 5),
(886, 'C710', '0286', 'Wonosobo', '', 'wonosobo', 10, 10),
(887, 'G100', '0274', 'Yogyakarta', '', 'yogyakarta', 20, 5),
(888, 'E404', '0334', 'Yosowilangun', '', 'yosowilangun', 10, 11);

-- --------------------------------------------------------

--
-- Table structure for table `city_old`
--

CREATE TABLE `city_old` (
  `id` int(11) NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `claim_status`
--

CREATE TABLE `claim_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `idnas` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description_len` text,
  `description_lid` text,
  `logo` varchar(45) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company_bak`
--

CREATE TABLE `company_bak` (
  `id` int(11) NOT NULL,
  `nam` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `region1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `region2` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `areacode` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dir`
--

CREATE TABLE `dir` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `popular_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `info` text CHARACTER SET utf8,
  `keywords` text CHARACTER SET utf8,
  `alcon` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid_key` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_1` text CHARACTER SET utf8,
  `address_2` text CHARACTER SET utf8,
  `address_3` text CHARACTER SET utf8,
  `kelurahan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kecamatan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kabupaten` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kawasan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `building` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `phone_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `webshot` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `payment` text CHARACTER SET utf8,
  `op_day_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_day_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `anniversary` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `facility` text CHARACTER SET utf8,
  `ecatalog` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pl` enum('P1','P2','P3') CHARACTER SET utf8 DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `dtm_index2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text CHARACTER SET utf8,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL,
  `noindex` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directory`
--

CREATE TABLE `directory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `popular_name` varchar(255) DEFAULT NULL,
  `description_len` text,
  `description_lid` text,
  `info` text,
  `keywords` text,
  `alcon` varchar(45) DEFAULT NULL,
  `masterid` varchar(45) DEFAULT NULL,
  `masterid_key` varchar(50) DEFAULT NULL,
  `barcode` varchar(45) DEFAULT NULL,
  `address_name` varchar(45) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `address_3` text,
  `kelurahan` varchar(45) DEFAULT NULL,
  `kecamatan` varchar(45) DEFAULT NULL,
  `kabupaten` varchar(45) DEFAULT NULL,
  `kawasan` varchar(45) DEFAULT NULL,
  `building` varchar(45) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) DEFAULT NULL,
  `phone_1` varchar(45) DEFAULT NULL,
  `phone_2` varchar(45) DEFAULT NULL,
  `phone_3` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `webshot` varchar(45) DEFAULT NULL,
  `payment` text,
  `op_day_1` varchar(45) DEFAULT NULL,
  `op_time_1` varchar(45) DEFAULT NULL,
  `op_day_2` varchar(45) DEFAULT NULL,
  `op_time_2` varchar(45) DEFAULT NULL,
  `anniversary` varchar(45) DEFAULT NULL,
  `facility` text,
  `ecatalog` varchar(255) NOT NULL,
  `pl` enum('P1','P2','P3') DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) DEFAULT NULL,
  `dtm_index2` varchar(100) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL,
  `hits` int(255) NOT NULL DEFAULT '0',
  `type` varchar(50) DEFAULT 'free',
  `service_code` varchar(3) DEFAULT '000',
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `directory`
--

INSERT INTO `directory` (`id`, `name`, `popular_name`, `description_len`, `description_lid`, `info`, `keywords`, `alcon`, `masterid`, `masterid_key`, `barcode`, `address_name`, `address_1`, `address_2`, `address_3`, `kelurahan`, `kecamatan`, `kabupaten`, `kawasan`, `building`, `zipcode`, `latitude`, `longitude`, `area_code`, `phone_1`, `phone_2`, `phone_3`, `fax`, `email`, `website`, `facebook`, `twitter`, `logo`, `webshot`, `payment`, `op_day_1`, `op_time_1`, `op_day_2`, `op_time_2`, `anniversary`, `facility`, `ecatalog`, `pl`, `is_pi`, `dtm_index`, `dtm_index2`, `slug`, `weight`, `published`, `create_time`, `update_time`, `revision_remark`, `company_id`, `city_id`, `updater_id`, `hits`, `type`, `service_code`, `user_id`) VALUES
(1, 'Ayam Bakar Mas Mono', 'Ayam Bakar Mas Mono', 'Ayam Bakar Mas Mono mantap', NULL, NULL, 'Ayam Bakar, Ayam Bakar Mas Mono', NULL, NULL, NULL, NULL, 'Bali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dummy-1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, 'ayam-bakar-mas-mono', 10, 1, NULL, NULL, NULL, NULL, 2, NULL, 0, 'free', '000', NULL),
(2, 'Auto 2000 Tebet Saharjo', 'Auto 2000 Tebet Saharjo', 'Auto 2000 Tebet Saharjo', NULL, NULL, 'auto 2000', NULL, NULL, NULL, NULL, 'Jakarta Selatan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dummy-2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, 'auto-2000-tebet-saharjo', 10, 1, NULL, NULL, NULL, NULL, 2, NULL, 0, 'free', '000', NULL),
(3, 'Ayam Bakar Mas Mono', 'Ayam Bakar Mas Mono', 'Ayam Bakar Mas Mono mantap', NULL, NULL, 'Ayam Bakar, Ayam Bakar Mas Mono', NULL, NULL, NULL, NULL, 'Jakarta Selatan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dummy-3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, 'ayam-bakar-mas-mono', 10, 1, NULL, NULL, NULL, NULL, 2, NULL, 0, 'free', '000', NULL),
(4, 'Seafood 68 Santa', 'Seafood 68 Santa', 'Seafood 68 Santa', NULL, NULL, 'Seafood 68 Santa', NULL, NULL, NULL, NULL, 'Jakarta Selatan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dummy-4.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, 'seafood-68-santa', 10, 1, NULL, NULL, NULL, NULL, 2, NULL, 0, 'free', '000', NULL),
(5, 'Apotek K2 Slipi', 'Apotek K2 Slipi', 'Apotek K2 Slipi', NULL, NULL, 'Apotek K2 Slipi', NULL, NULL, NULL, NULL, 'Jakarta Selatan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dummy-5.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, 'apotek-k2-slipi', 10, 1, NULL, NULL, NULL, NULL, 2, NULL, 0, 'free', '000', NULL),
(6, 'Lorem Ipsum', 'Lorem Ipsum', 'Lorem Ipsum', NULL, NULL, 'Lorem Ipsum', NULL, NULL, NULL, NULL, 'Jakarta Selatan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dummy-5.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, 'lorem-ipsum', 10, 1, NULL, NULL, NULL, NULL, 2, NULL, 0, 'free', '000', NULL),
(7, 'Lorem Ipsum ABC', 'Lorem Ipsum ABC', 'Lorem Ipsum ABC', NULL, NULL, 'Lorem Ipsum ABC', NULL, NULL, NULL, NULL, 'Jakarta Selatan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dummy-6.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, 'lorem-ipsum-abc', 10, 1, NULL, NULL, NULL, NULL, 2, NULL, 0, 'free', '000', NULL),
(8, 'Lorem Ipsum XXX', 'Lorem Ipsum XXX', 'Lorem Ipsum XXX', NULL, NULL, 'Lorem Ipsum XXX', NULL, NULL, NULL, NULL, 'Jakarta Selatan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dummy-6.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, 'lorem-ipsum-xxx', 10, 1, NULL, NULL, NULL, NULL, 2, NULL, 0, 'free', '000', NULL),
(9, 'Lorem Ipsum ZZZ', 'Lorem Ipsum ZZZ', 'Lorem Ipsum ZZZ', NULL, NULL, 'Lorem Ipsum ZZZ', NULL, NULL, NULL, NULL, 'Jakarta Selatan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dummy-6.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, NULL, NULL, 'lorem-ipsum-zzz', 10, 1, NULL, NULL, NULL, NULL, 2, NULL, 0, 'free', '000', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `directory_bak`
--

CREATE TABLE `directory_bak` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `popular_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `info` text CHARACTER SET utf8,
  `keywords` text CHARACTER SET utf8,
  `alcon` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid_key` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_1` text CHARACTER SET utf8,
  `address_2` text CHARACTER SET utf8,
  `address_3` text CHARACTER SET utf8,
  `kelurahan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kecamatan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kabupaten` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kawasan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `building` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `phone_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `webshot` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `payment` text CHARACTER SET utf8,
  `op_day_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_day_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `anniversary` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `facility` text CHARACTER SET utf8,
  `ecatalog` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pl` enum('P1','P2','P3') CHARACTER SET utf8 DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `dtm_index2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text CHARACTER SET utf8,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directory_category`
--

CREATE TABLE `directory_category` (
  `id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `directory_category`
--

INSERT INTO `directory_category` (`id`, `directory_id`, `category_id`) VALUES
(1, 1, 432),
(2, 3, 432),
(3, 4, 432),
(4, 2, 462),
(5, 5, 432),
(6, 6, 432),
(7, 7, 432),
(8, 8, 432),
(9, 9, 462);

-- --------------------------------------------------------

--
-- Table structure for table `directory_category_bak`
--

CREATE TABLE `directory_category_bak` (
  `id` int(11) NOT NULL DEFAULT '0',
  `directory_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directory_claim`
--

CREATE TABLE `directory_claim` (
  `id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `claim_status_id` int(11) NOT NULL,
  `claim_name` varchar(45) DEFAULT NULL,
  `claim_position` varchar(45) DEFAULT NULL,
  `claim_email` varchar(45) DEFAULT NULL,
  `claim_phone` varchar(45) DEFAULT NULL,
  `claim_remark` text,
  `claim_document` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `verified_by` varchar(55) NOT NULL,
  `verify_desc` text NOT NULL,
  `noindex` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `directory_claim_history`
--

CREATE TABLE `directory_claim_history` (
  `id` int(11) NOT NULL,
  `directory_claim_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `claim_status_id` int(11) NOT NULL,
  `remark` text CHARACTER SET utf8,
  `document` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directory_counter`
--

CREATE TABLE `directory_counter` (
  `directory_counter_id` int(10) UNSIGNED NOT NULL,
  `directory_id` int(11) NOT NULL,
  `counter_date` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `segment` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `directory_counter`
--

INSERT INTO `directory_counter` (`directory_counter_id`, `directory_id`, `counter_date`, `segment`, `counter`) VALUES
(1, 1, '201701', 'feature', 41),
(2, 2, '201701', 'feature', 41),
(3, 3, '201701', 'feature', 41),
(4, 4, '201701', 'feature', 41),
(5, 5, '201701', 'feature', 41),
(6, 6, '201701', 'feature', 41),
(7, 7, '201701', 'feature', 41),
(8, 8, '201701', 'feature', 41),
(9, 9, '201701', 'feature', 41),
(10, 1, '201701', 'sponsor', 37),
(11, 2, '201701', 'sponsor', 37),
(12, 3, '201701', 'sponsor', 37),
(13, 4, '201701', 'sponsor', 36),
(14, 5, '201701', 'sponsor', 36),
(15, 6, '201701', 'sponsor', 36),
(16, 7, '201701', 'sponsor', 36),
(17, 8, '201701', 'sponsor', 36),
(18, 9, '201701', 'sponsor', 36);

-- --------------------------------------------------------

--
-- Table structure for table `directory_extended`
--

CREATE TABLE `directory_extended` (
  `id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `masterid_key` varchar(50) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `ext_value` text NOT NULL,
  `sites` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `is_indexed` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `directory_info`
--

CREATE TABLE `directory_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `directory_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `via` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `directory_info`
--

INSERT INTO `directory_info` (`id`, `directory_id`, `message`, `user_id`, `via`, `created_at`) VALUES
(1, 1, 'Diskon 10% Bebek Kaleyo untuk pemesanan', 1, 'web', '2017-01-23 00:00:00'),
(2, 2, 'DP 0% Honda Tebet Hanya di Desember 2016', 1, 'web', '2017-01-23 00:00:00'),
(3, 3, 'Pesta Akhir Tahun - Gratis servis di Piggio Tebet', 1, 'web', '2017-01-23 00:00:00'),
(4, 4, 'Ayam Bakar Mas Mono kini hadir di Jakarta', 1, 'web', '2017-01-23 00:00:00'),
(5, 5, 'Selama libur Tahun Baru, Seafood Santa hanya 70000', 1, 'web', '2017-01-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `directory_inquiry`
--

CREATE TABLE `directory_inquiry` (
  `id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `inquiry` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `directory_picture`
--

CREATE TABLE `directory_picture` (
  `id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title_len` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_lid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `picture_file` varchar(255) CHARACTER SET utf8 NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directory_product`
--

CREATE TABLE `directory_product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `masterid` varchar(45) NOT NULL,
  `masterid_key` varchar(50) NOT NULL,
  `remarks` text,
  `published` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `directory_product`
--

INSERT INTO `directory_product` (`id`, `name`, `brand`, `slug`, `directory_id`, `picture`, `description`, `masterid`, `masterid_key`, `remarks`, `published`) VALUES
(1, 'Paket Bebek Peking', 'Bebek', 'paket-bebek-deking', 1, 'dummy-5.png', 'Bebek Kaleo Tebet', '1', '', '', 1),
(2, 'Shockbraker Honda', 'Otomotif', 'shockbraker-honda', 2, 'dummy-6.png', 'Ledeng Motor Bandung', '2', '', '', 1),
(3, 'Topeng Kuda Latex', 'topeng latex', 'topeng-kuda-latex', 3, 'dummy-7.png', 'Susan Shop', '3', '', '', 1),
(4, 'Buldozer Komatsu', 'Komatsu', 'buldozer-komatsu', 4, 'dummy-8.png', 'Jago Rental', '4', '', '', 1),
(5, 'Regulator LPG Luzzini ', 'LPG', 'regulator-lpg-luzzini ', 5, 'dummy-9.png', 'Bangkit Sentosa', '5', '', '', 1),
(6, 'Sepatu Boot Caterpillar ', 'Caterppillar', 'sepatu-boot-caterpillar', 8, 'dummy-10.png', 'NES Shoes Shop', '6', '', '', 1),
(7, 'Atap Baja Ringan', 'Baja', 'atap-baja-ringan', 7, 'dummy-11.png', 'Tatalogam Lestari', '6', '', '', 1),
(8, 'Roller Conveyor ', 'Conveyor', 'roller-conveyor', 9, 'dummy-12.png', 'PT. Sarana Teknik Conveyor', '6', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `directory_rating`
--

CREATE TABLE `directory_rating` (
  `id` int(11) NOT NULL,
  `rate` float NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `directory_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directory_submitted`
--

CREATE TABLE `directory_submitted` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `popular_name` varchar(255) DEFAULT NULL,
  `description_len` text,
  `description_lid` text,
  `info` text,
  `keywords` text,
  `alcon` varchar(45) DEFAULT NULL,
  `masterid` varchar(45) DEFAULT NULL,
  `masterid_key` varchar(50) DEFAULT NULL,
  `barcode` varchar(45) DEFAULT NULL,
  `address_name` varchar(45) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `address_3` text,
  `kelurahan` varchar(45) DEFAULT NULL,
  `kecamatan` varchar(45) DEFAULT NULL,
  `kabupaten` varchar(45) DEFAULT NULL,
  `kawasan` varchar(45) DEFAULT NULL,
  `building` varchar(45) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) DEFAULT NULL,
  `phone_1` varchar(45) DEFAULT NULL,
  `phone_2` varchar(45) DEFAULT NULL,
  `phone_3` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `webshot` varchar(45) DEFAULT NULL,
  `payment` text,
  `op_day_1` varchar(45) DEFAULT NULL,
  `op_time_1` varchar(45) DEFAULT NULL,
  `op_day_2` varchar(45) DEFAULT NULL,
  `op_time_2` varchar(45) DEFAULT NULL,
  `anniversary` varchar(45) DEFAULT NULL,
  `facility` text,
  `pl` enum('P1','P2','P3') DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL,
  `noindex` tinyint(1) DEFAULT '0',
  `type` varchar(50) DEFAULT 'free'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `directory_submitted_category`
--

CREATE TABLE `directory_submitted_category` (
  `id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `directory_tag`
--

CREATE TABLE `directory_tag` (
  `id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `directory_video`
--

CREATE TABLE `directory_video` (
  `id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title_len` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_lid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `video_file` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dir_bak`
--

CREATE TABLE `dir_bak` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `popular_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `info` text CHARACTER SET utf8,
  `keywords` text CHARACTER SET utf8,
  `alcon` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid_key` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_1` text CHARACTER SET utf8,
  `address_2` text CHARACTER SET utf8,
  `address_3` text CHARACTER SET utf8,
  `kelurahan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kecamatan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kabupaten` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kawasan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `building` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `phone_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `webshot` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `payment` text CHARACTER SET utf8,
  `op_day_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_day_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `anniversary` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `facility` text CHARACTER SET utf8,
  `ecatalog` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pl` enum('P1','P2','P3') CHARACTER SET utf8 DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `dtm_index2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text CHARACTER SET utf8,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dir_bakectory`
--

CREATE TABLE `dir_bakectory` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `popular_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_len` text CHARACTER SET utf8,
  `description_lid` text CHARACTER SET utf8,
  `info` text CHARACTER SET utf8,
  `keywords` text CHARACTER SET utf8,
  `alcon` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `masterid_key` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `address_1` text CHARACTER SET utf8,
  `address_2` text CHARACTER SET utf8,
  `address_3` text CHARACTER SET utf8,
  `kelurahan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kecamatan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kabupaten` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `kawasan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `building` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `zipcode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area_code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `phone_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `phone_3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `twitter` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `webshot` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `payment` text CHARACTER SET utf8,
  `op_day_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_1` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_day_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `op_time_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `anniversary` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `facility` text CHARACTER SET utf8,
  `ecatalog` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pl` enum('P1','P2','P3') CHARACTER SET utf8 DEFAULT NULL,
  `is_pi` tinyint(1) DEFAULT '0',
  `dtm_index` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `dtm_index2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '10',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revision_remark` text CHARACTER SET utf8,
  `company_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `updater_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dir_hits`
--

CREATE TABLE `dir_hits` (
  `id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL,
  `hits` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `display_ad`
--

CREATE TABLE `display_ad` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` text,
  `display_file` varchar(45) DEFAULT NULL,
  `display_ad_type_id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `display_ad_45716`
--

CREATE TABLE `display_ad_45716` (
  `id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(45) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  `display_file` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `display_ad_type_id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `display_ad_backup`
--

CREATE TABLE `display_ad_backup` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` text,
  `display_file` varchar(45) DEFAULT NULL,
  `display_ad_type_id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `display_ad_backup_2`
--

CREATE TABLE `display_ad_backup_2` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` text,
  `display_file` varchar(45) DEFAULT NULL,
  `display_ad_type_id` int(11) NOT NULL,
  `directory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `display_ad_type`
--

CREATE TABLE `display_ad_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `bpt` varchar(45) NOT NULL,
  `description` text,
  `weight` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `main_banner`
--

CREATE TABLE `main_banner` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `main_text` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dynamic_text` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `main_banner`
--

INSERT INTO `main_banner` (`id`, `image`, `main_text`, `dynamic_text`, `status`, `created_at`, `updated_at`) VALUES
(1, '["building.jpg","home-hero.png"]', '["Solusi","Untuk Berkembang Dan Menjangkau"]', '[[ "UMKM", "TEST", "DETES" ],[ "Pasar Lokal", "Pasar Global", "Apapun" ]]', 1, '2017-01-23 00:00:00', '2017-01-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `micrositetmp`
--

CREATE TABLE `micrositetmp` (
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subdomain` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ket` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `microsite_tmp`
--

CREATE TABLE `microsite_tmp` (
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subdomain` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ket` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `microsite_tmp1`
--

CREATE TABLE `microsite_tmp1` (
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subdomain` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ket` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(19, '2014_10_12_000000_create_users_table', 1),
(20, '2014_10_12_100000_create_password_resets_table', 1),
(21, '2017_01_19_141156_directory_counter', 1),
(22, '2017_01_19_142145_main_banner', 1),
(23, '2017_01_20_153907_product_counter', 1),
(24, '2017_01_20_164125_directory_product', 1),
(25, '2017_01_23_140918_affiliate_article', 1),
(26, '2017_01_23_162001_directory_info', 2),
(27, '2017_01_24_110028_add_yp_user_fields', 3),
(28, '2017_01_24_164723_add_fields_to_directory_inquiry_table', 4),
(29, '2017_01_25_170335_add_user_id_to_directory', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_counter`
--

CREATE TABLE `product_counter` (
  `product_counter_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `counter_date` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `segment` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `counter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_counter`
--

INSERT INTO `product_counter` (`product_counter_id`, `product_id`, `counter_date`, `segment`, `counter`) VALUES
(1, 1, '201701', 'feature', 46),
(2, 2, '201701', 'feature', 46),
(3, 3, '201701', 'feature', 46),
(4, 4, '201701', 'feature', 46),
(5, 5, '201701', 'feature', 46),
(6, 6, '201701', 'feature', 46),
(7, 7, '201701', 'feature', 46),
(8, 8, '201701', 'feature', 46);

-- --------------------------------------------------------

--
-- Table structure for table `product_rating`
--

CREATE TABLE `product_rating` (
  `id` int(11) NOT NULL,
  `rate` float NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` text,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`id`, `name`, `description`, `slug`) VALUES
(2, 'Bangka Belitung', '', 'bangka-belitung'),
(3, 'Banten', '', 'banten'),
(4, 'Bengkulu', '', 'bengkulu'),
(5, 'DI Yogyakarta', '', 'di-yogyakarta'),
(6, 'DKI Jakarta', '', 'dki-jakarta'),
(7, 'Gorontalo', '', 'gorontalo'),
(8, 'Jambi', '', 'jambi'),
(9, 'Jawa Barat', '', 'jawa-barat'),
(10, 'Jawa Tengah', '', 'jawa-tengah'),
(11, 'Jawa Timur', '', 'jawa-timur'),
(12, 'Kalimantan Barat', '', 'kalimantan-barat'),
(13, 'Kalimantan Selatan', '', 'kalimantan-selatan'),
(14, 'Kalimantan Tengah', '', 'kalimantan-tengah'),
(15, 'Kalimantan Timur', '', 'kalimantan-timur'),
(16, 'Kepulauan Riau', '', 'kepulauan-riau'),
(17, 'Lampung', '', 'lampung'),
(18, 'Maluku', '', 'maluku'),
(19, 'Maluku Utara', '', 'maluku-utara'),
(20, 'Nanggroe Aceh Darussalam', '', 'nanggroe-aceh-darussalam'),
(21, 'Nusa Tenggara Barat', '', 'nusa-tenggara-barat'),
(22, 'Nusa Tenggara Timur', '', 'nusa-tenggara-timur'),
(23, 'Papua Barat', '', 'papua-barat'),
(24, 'Papua Tengah', '', 'papua-tengah'),
(25, 'Papua Timur', '', 'papua-timur'),
(26, 'Riau', '', 'riau'),
(27, 'Sulawesi Selatan', '', 'sulawesi-selatan'),
(28, 'Sulawesi Tengah', '', 'sulawesi-tengah'),
(29, 'Sulawesi Tenggara', '', 'sulawesi-tenggara'),
(30, 'Sulawesi Utara', '', 'sulawesi-utara'),
(31, 'Sumatera Barat', '', 'sumatera-barat'),
(32, 'Sumatera Selatan', '', 'sumatera-selatan'),
(33, 'Sumatera Utara', '', 'sumatera-utara'),
(34, 'International', '', 'international');

-- --------------------------------------------------------

--
-- Table structure for table `refine`
--

CREATE TABLE `refine` (
  `id` int(11) NOT NULL,
  `buswith` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nam` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `scategory`
--

CREATE TABLE `scategory` (
  `sid` int(11) NOT NULL,
  `sname` varchar(35) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE `search` (
  `id` int(11) NOT NULL,
  `sid` int(11) NOT NULL DEFAULT '1',
  `what` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `where` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `service_code`
--

CREATE TABLE `service_code` (
  `service_code` varchar(3) NOT NULL DEFAULT '000' COMMENT 'service code subscription (000 - 999)',
  `price` varchar(20) NOT NULL COMMENT 'service code price information',
  `description` text COMMENT 'service code information description',
  `timestamp` varchar(100) NOT NULL COMMENT 'service code create timestamp',
  `created_by` varchar(100) NOT NULL DEFAULT 'System' COMMENT 'service code created by',
  `updated_by` varchar(100) NOT NULL DEFAULT 'System' COMMENT 'service code update by'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table service code information detail';

-- --------------------------------------------------------

--
-- Table structure for table `service_subscription`
--

CREATE TABLE `service_subscription` (
  `id` int(11) NOT NULL,
  `alcon` varchar(45) DEFAULT NULL,
  `dtm_index` varchar(45) DEFAULT NULL,
  `service_code` varchar(3) NOT NULL DEFAULT '000',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `service_status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `expire` int(11) NOT NULL,
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subdomain`
--

CREATE TABLE `subdomain` (
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subdomain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ket` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `name_len` varchar(255) NOT NULL,
  `name_lid` varchar(255) NOT NULL,
  `description_len` text,
  `description_lid` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `slug_lid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_activation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `handphone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `webinstant`
--

CREATE TABLE `webinstant` (
  `id` int(11) NOT NULL,
  `firstname` varchar(35) NOT NULL,
  `lastname` varchar(35) NOT NULL,
  `companyname` varchar(150) NOT NULL,
  `address1` varchar(250) NOT NULL,
  `city` varchar(64) NOT NULL,
  `state` varchar(64) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country` varchar(35) NOT NULL,
  `phonenumber` varchar(35) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password2` varchar(100) NOT NULL,
  `currency` varchar(5) NOT NULL,
  `user_token` varchar(250) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `isactive` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `affiliate_article`
--
ALTER TABLE `affiliate_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `slug_2` (`slug`);

--
-- Indexes for table `career_viewer`
--
ALTER TABLE `career_viewer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `career_id` (`career_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `slug` (`slug`),
  ADD KEY `weight` (`weight`),
  ADD KEY `classification` (`classification`);

--
-- Indexes for table `category_old`
--
ALTER TABLE `category_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `fk_city_province` (`province_id`),
  ADD KEY `area_code` (`area_code`);

--
-- Indexes for table `city_old`
--
ALTER TABLE `city_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_status`
--
ALTER TABLE `claim_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug_UNIQUE` (`slug`);

--
-- Indexes for table `company_bak`
--
ALTER TABLE `company_bak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directory`
--
ALTER TABLE `directory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_directory_company1` (`company_id`),
  ADD KEY `fk_directory_city1` (`city_id`),
  ADD KEY `slug` (`slug`),
  ADD KEY `masterid_key` (`masterid_key`),
  ADD KEY `dtm_index` (`dtm_index`),
  ADD KEY `update_time` (`update_time`),
  ADD KEY `alcon` (`alcon`),
  ADD KEY `dtm_index2` (`dtm_index2`);

--
-- Indexes for table `directory_bak`
--
ALTER TABLE `directory_bak`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dtm_index` (`dtm_index`),
  ADD KEY `dtm_index2` (`dtm_index2`),
  ADD KEY `update_time` (`update_time`),
  ADD KEY `slug` (`slug`),
  ADD KEY `slug_2` (`slug`);

--
-- Indexes for table `directory_category`
--
ALTER TABLE `directory_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_directory_category_directory1` (`directory_id`),
  ADD KEY `fk_directory_category_category1` (`category_id`);

--
-- Indexes for table `directory_claim`
--
ALTER TABLE `directory_claim`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directory_id` (`directory_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `claim_status_id` (`claim_status_id`);

--
-- Indexes for table `directory_claim_history`
--
ALTER TABLE `directory_claim_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directory_claim_id` (`directory_claim_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `claim_status_id` (`claim_status_id`);

--
-- Indexes for table `directory_counter`
--
ALTER TABLE `directory_counter`
  ADD PRIMARY KEY (`directory_counter_id`);

--
-- Indexes for table `directory_extended`
--
ALTER TABLE `directory_extended`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directory_id` (`directory_id`),
  ADD KEY `name` (`name`),
  ADD KEY `sites` (`sites`),
  ADD KEY `slug` (`slug`),
  ADD KEY `masterid_key` (`masterid_key`),
  ADD KEY `is_indexed` (`is_indexed`);

--
-- Indexes for table `directory_info`
--
ALTER TABLE `directory_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directory_inquiry`
--
ALTER TABLE `directory_inquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directory_picture`
--
ALTER TABLE `directory_picture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directory_id` (`directory_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `directory_product`
--
ALTER TABLE `directory_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `masterid_key` (`masterid_key`);

--
-- Indexes for table `directory_rating`
--
ALTER TABLE `directory_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directory_id` (`directory_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `directory_submitted`
--
ALTER TABLE `directory_submitted`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_directory_company1` (`company_id`),
  ADD KEY `fk_directory_city1` (`city_id`),
  ADD KEY `slug` (`slug`),
  ADD KEY `masterid_key` (`masterid_key`),
  ADD KEY `dtm_index` (`dtm_index`);

--
-- Indexes for table `directory_submitted_category`
--
ALTER TABLE `directory_submitted_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_directory_category_directory1` (`directory_id`),
  ADD KEY `fk_directory_category_category1` (`category_id`);

--
-- Indexes for table `directory_tag`
--
ALTER TABLE `directory_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_directory_tag_directory1` (`directory_id`),
  ADD KEY `fk_directory_tag_tag1` (`tag_id`);

--
-- Indexes for table `directory_video`
--
ALTER TABLE `directory_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directory_id` (`directory_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `dir_hits`
--
ALTER TABLE `dir_hits`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `directory_id` (`directory_id`);

--
-- Indexes for table `display_ad`
--
ALTER TABLE `display_ad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_display_ad_display_ad_type` (`display_ad_type_id`),
  ADD KEY `fk_display_ad_directory1` (`directory_id`);

--
-- Indexes for table `display_ad_backup`
--
ALTER TABLE `display_ad_backup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_display_ad_display_ad_type` (`display_ad_type_id`),
  ADD KEY `fk_display_ad_directory1` (`directory_id`);

--
-- Indexes for table `display_ad_backup_2`
--
ALTER TABLE `display_ad_backup_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_display_ad_display_ad_type` (`display_ad_type_id`),
  ADD KEY `fk_display_ad_directory1` (`directory_id`);

--
-- Indexes for table `display_ad_type`
--
ALTER TABLE `display_ad_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_banner`
--
ALTER TABLE `main_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `product_counter`
--
ALTER TABLE `product_counter`
  ADD PRIMARY KEY (`product_counter_id`);

--
-- Indexes for table `product_rating`
--
ALTER TABLE `product_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directory_id` (`product_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug_UNIQUE` (`slug`);

--
-- Indexes for table `refine`
--
ALTER TABLE `refine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scategory`
--
ALTER TABLE `scategory`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `search`
--
ALTER TABLE `search`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_search` (`sid`);

--
-- Indexes for table `service_code`
--
ALTER TABLE `service_code`
  ADD PRIMARY KEY (`service_code`);

--
-- Indexes for table `service_subscription`
--
ALTER TABLE `service_subscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dtm_index` (`dtm_index`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subdomain`
--
ALTER TABLE `subdomain`
  ADD PRIMARY KEY (`slug`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD UNIQUE KEY `slug_lid` (`slug_lid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `webinstant`
--
ALTER TABLE `webinstant`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `affiliate_article`
--
ALTER TABLE `affiliate_article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `career_viewer`
--
ALTER TABLE `career_viewer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1002;
--
-- AUTO_INCREMENT for table `category_old`
--
ALTER TABLE `category_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=889;
--
-- AUTO_INCREMENT for table `city_old`
--
ALTER TABLE `city_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `claim_status`
--
ALTER TABLE `claim_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_bak`
--
ALTER TABLE `company_bak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory`
--
ALTER TABLE `directory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `directory_bak`
--
ALTER TABLE `directory_bak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=895670;
--
-- AUTO_INCREMENT for table `directory_category`
--
ALTER TABLE `directory_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `directory_claim`
--
ALTER TABLE `directory_claim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory_claim_history`
--
ALTER TABLE `directory_claim_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory_counter`
--
ALTER TABLE `directory_counter`
  MODIFY `directory_counter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `directory_extended`
--
ALTER TABLE `directory_extended`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory_info`
--
ALTER TABLE `directory_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `directory_inquiry`
--
ALTER TABLE `directory_inquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory_picture`
--
ALTER TABLE `directory_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory_product`
--
ALTER TABLE `directory_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `directory_rating`
--
ALTER TABLE `directory_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory_submitted`
--
ALTER TABLE `directory_submitted`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory_submitted_category`
--
ALTER TABLE `directory_submitted_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory_tag`
--
ALTER TABLE `directory_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `directory_video`
--
ALTER TABLE `directory_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dir_hits`
--
ALTER TABLE `dir_hits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `display_ad`
--
ALTER TABLE `display_ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `display_ad_backup`
--
ALTER TABLE `display_ad_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `display_ad_backup_2`
--
ALTER TABLE `display_ad_backup_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `display_ad_type`
--
ALTER TABLE `display_ad_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `main_banner`
--
ALTER TABLE `main_banner`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `product_counter`
--
ALTER TABLE `product_counter`
  MODIFY `product_counter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `product_rating`
--
ALTER TABLE `product_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `refine`
--
ALTER TABLE `refine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scategory`
--
ALTER TABLE `scategory`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `search`
--
ALTER TABLE `search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service_subscription`
--
ALTER TABLE `service_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `webinstant`
--
ALTER TABLE `webinstant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
