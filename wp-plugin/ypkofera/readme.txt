=== YPKofera ===
Contributors: dwikkymaradhiza
Tags: business,ypkofera,yellow pages
Requires at least: 1.0
Tested up to: 1.0
Stable tag: 1.0
License: MIT

A plugin to get directory data from ypkofera.

== Description ==
A plugin to get directory data from ypkofera.

== Installation ==
1. Upload the plugin files to the `/wp-content/plugins/ypkofera` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the \'Plugins\' screen in WordPress
3. Use the Settings->YPKofera screen to configure the plugin