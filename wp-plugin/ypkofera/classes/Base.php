<?php

namespace Ypkofera\classes;

/**
 * @description Base class
 * @author dwikky
 */
class Base {
    
    protected $routes = [
        'get_directory' => YP_API_GET_DIRECTORY
    ];

    public function routes() {
        $obj = new Base;
        foreach ($this->routes as $k => $v) {
            $obj->{$k} = $v;
        }
        
        return $obj;
    }

    public function trigger_api($uri) {
        try {
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $uri);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if (!$output) {
                return false;
            }

            if ($httpCode < 400) {
                return json_decode($output);
            } else {
                return false;
            }

            return false;
        } catch (Exception $ex) {
            
            return false;
        }
    }
}