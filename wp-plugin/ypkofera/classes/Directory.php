<?php

namespace Ypkofera\classes;

/**
 * @description Directory class
 * @author dwikky
 */
class Directory extends Base {
    
    protected $attributes = array(
        "tpl_css" => "<style>
                        .wp-yp-container {
                            padding: 10px 10px;
                            background: #EEE
                        }
                        .wp-yp-container ul {
                            width: 100%;
                            list-style: none;
                        }
                        .wp-yp-container .wp-yp-list {
                            width: 50%;
                            font-size: 12px;
                            font-weight: bold;
                            float:left;
                        }
                        .wp-yp-container .wp-yp-list img{
                            width: 40%;
                            float:left;
                            margin-right: 10px;
                            margin-left: 10px;
                        }
                        .wp-yp-container .wp-yp-list a{
                            font-weight: normal;
                            text-decoration: none;
                        }
                        .wp-yp-container .wp-yp-list a:hover{
                            font-weight: normal;
                        }
                        .wp-yp-container .wp-yp-list:first-child img{
                            margin-left: 0;
                        }
                        .wp-yp-container .clearboth {
                            clear: both;
                        }
                        .wp-yp-container a.wp-yp-label {
                            color: #FFBB10;
                            font-size: 12px;
                            text-decoration: none !important;
                            float:right;
                        }
                      </style>",
        "tpl_container_start" => "<div class='wp-yp-container'>",
        "tpl_heading_start" => "",
        "tpl_heading_end"   => "",
        "tpl_list_start"    => "<ul>",
        "tpl_list_end"      => "</ul>",
        "tpl_item_start"    => "<li class='wp-yp-list'>",
        "tpl_item_end"      => "</li>",
        "tpl_container_end" => "<div class='clearboth'></div></div>",
        "label"             => "Ads by yellow pages",
        "tags"              => "",
    );

    public static function directories_handler($incoming_attributes) {
        $directory = new self;
        
        //process incoming attributes assigning defaults if required
        $incoming_attributes = shortcode_atts($directory->attributes, $incoming_attributes);
        
        //if tags is not provided, get default tags post
        if(empty($incoming_attributes['tags'])) {
            $current_post_tag = wp_get_post_tags( get_the_ID(), ['fields' => 'names' ] );
            $tags = implode(",", $current_post_tag);
        }
        
        $incoming_attributes['data'] = $directory->trigger_api($directory->routes()->get_directory . '?tags=' . urlencode($tags));

        //run function that actually does the work of the plugin
        $directories_output = $directory->directories_function($incoming_attributes);
        
        //send back text to replace shortcode in post
        return $directories_output;
    }

    public function directories_function($incoming_attributes) {
        if(!$incoming_attributes['data']) {
            return false;
        }
        
        //count data
        $data_count = count($incoming_attributes["data"]);

        //add container start
        $directories_output = wp_specialchars_decode($incoming_attributes["tpl_css"]);
        
        //add container start
        $directories_output .= wp_specialchars_decode($incoming_attributes["tpl_container_start"]);

        //add heading start
        $directories_output .= wp_specialchars_decode($incoming_attributes["tpl_heading_start"]);
        
        //add list start
        $directories_output .= wp_specialchars_decode($incoming_attributes["tpl_list_start"]);

        if(is_array($incoming_attributes["data"])) {
            foreach($incoming_attributes["data"] as $key => $value) {
                $directories_output .= wp_specialchars_decode($incoming_attributes["tpl_item_start"]);
                $directories_output .= wp_specialchars_decode("<img src='{$value->logo}' />");
                $directories_output .= wp_specialchars($value->name);
                $directories_output .= "<br /><a href='" . $value->detail_uri . "'>Kunjungi</a>";
                $directories_output .= wp_specialchars_decode($incoming_attributes["tpl_item_end"]);
            }
        } else {
            $directories_output .= wp_specialchars($incoming_attributes["data"]);
        }

        //add list end
        $directories_output .= wp_specialchars_decode($incoming_attributes["tpl_list_end"]);
        
        //add heading end
        $directories_output .= wp_specialchars_decode($incoming_attributes["tpl_heading_end"]);
        
        //add label 
        $directories_output .= "<br /><a class='wp-yp-label' href='". YP_HOST ."'>". wp_specialchars_decode($incoming_attributes["label"]) ."</a>";
        
        //add container end
        $directories_output .= wp_specialchars_decode($incoming_attributes["tpl_container_end"]);
        
        //send back text to calling function
        return $directories_output;
    }
}