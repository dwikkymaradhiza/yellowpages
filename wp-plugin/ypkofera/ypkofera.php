<?php
defined('ABSPATH') or die('Direct access not allowed!');

/*
  Plugin Name: YP Kofera wordpress plugin
  Plugin URI: http://mysite.com
  Description: A plugin to get directory data from ypkofera.
  Version: 1.0
  Author: Dwikky Maradhiza
  Author URI: https://github.com/dwikkymaradhiza
  License: MIT or later
 */

require_once plugin_dir_path( __FILE__ ) . '/config.php';
require_once plugin_dir_path( __FILE__ ) . '/classes/Base.php';
require_once plugin_dir_path( __FILE__ ) . '/classes/Directory.php';

add_shortcode("yp-directory", array("Ypkofera\classes\Directory", "directories_handler"));

